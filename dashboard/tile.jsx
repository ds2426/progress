// libs
import React from 'react';
import PropTypes from 'prop-types';
import memoize from 'lodash/memoize';
import debounce from 'lodash/debounce';

// enhancers
import { tileEnhancer } from '@biw/dashboard';
//components
import ManagerOverviewTile from './tiles/manager/manager-overview';
import ManagerGoalSelectionTile from './tiles/manager/manager-goal-selection';
import ManagerProgressTile from './tiles/manager/manager-progress';
import ManagerNoProgressTile from './tiles/manager/manager-no-progress';
import ManagerFinalResultTile from './tiles/manager/manager-final-result';
import ProgramOverviewTile from './tiles/participant/program-overview';
import GoalSelectedTile from './tiles/participant/goal-selected';
import NoGoalSelectedTile from './tiles/participant/no-goal-selected';
import NoProgressTile from './tiles/participant/no-progress';
import ProgressTile from './tiles/participant/progress';
import WaitingForFinalResultTile from './tiles/participant/wating-for-final-result';
import FinalResultTile from './tiles/participant/final-result';
import LoadingTile from './tiles/loading';

import './tile.scss';

function getTileForType( tileType ) {
    switch ( tileType ) {
        case 'manager-overview':
            return ManagerOverviewTile;
        case 'manager-goal-selection':
            return ManagerGoalSelectionTile;
        case 'manager-no-progress':
            return ManagerNoProgressTile;
        case 'manager-progress':
            return ManagerProgressTile;
        case 'manager-final-result':
            return ManagerFinalResultTile;
        case 'program-overview':
            return ProgramOverviewTile;
        case 'goal-selected':
            return GoalSelectedTile;
        case 'no-goal-selected':
            return NoGoalSelectedTile;
        case 'no-progress':
            return NoProgressTile;
        case 'progress':
            return ProgressTile;
        case 'waiting-for-final-result':
            return WaitingForFinalResultTile;
        case 'final-result':
            return FinalResultTile;
        default:
            return LoadingTile;
    }
}

// only fetch tile progress data once every 30 seconds
const DEBOUNCE_TIME = 1000 * 30;
const DEBOUNCE_OPTS = { leading: true, trailing: false };
const getDebouncedFetcher = memoize( programViewId => debounce( fetch => fetch( programViewId ), DEBOUNCE_TIME, DEBOUNCE_OPTS ) );

export class Tile extends React.Component {
    static propTypes = {
        tileType: PropTypes.string,
        hasProgressData: PropTypes.bool,
        progressLoading: PropTypes.bool,
        fetchProgramViewProgress: PropTypes.func.isRequired,
        programViewId: PropTypes.string.isRequired
    }

    componentDidMount() {
        const { programViewId, hasProgressData, fetchProgramViewProgress } = this.props;
        if ( programViewId ) {
            const fetcher = getDebouncedFetcher( programViewId );
            if ( !hasProgressData ) { // this solves a weird edge case where the debounce persists across logins
                fetcher.cancel();
            }
            fetcher( fetchProgramViewProgress );
        }
    }

    shouldComponentUpdate( nextProps ) {
        if ( this.props.progressLoading && nextProps.progressLoading ) {
            return false;
        }
        return true;
    }

    render() {
        const { tileType, hasProgressData, progressLoading  } = this.props;
        const ContentTile = getTileForType( tileType );
        return (

            <div className="dashboard-list-item">
                { !hasProgressData && progressLoading ?
                    <LoadingTile { ...this.props } />
                :
                    <ContentTile { ...this.props } />
                }
            </div>
        );
    }
}

export default tileEnhancer( Tile );
