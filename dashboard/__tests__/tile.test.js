import React from 'react';
import { shallow } from 'enzyme';
import { Tile } from '../tile';
import FinalResultTile from '../tiles/participant/final-result';
import ProgressTile from '../tiles/participant/progress';
describe( 'Tile component:', () => {
    let props;
    beforeEach( () => {
        props = {
            tileType: '',
            programViewId: '',
            hasProgressData: true,
            replaceOptions: {},
            fetchProgramViewProgress: jest.fn(),
            program: {
                programId: 0
            }
        };
    } );

    it( 'renders without errors', () => {
        const wrapper = shallow(
            <Tile { ...props } />
        );
        expect( wrapper ).toMatchSnapshot();
    } );

    it( 'renders with a Final Result if they tile type is finalresult', () => {
        props.tileType = 'final-result';
        const wrapper = shallow( <Tile { ...props } /> );
        expect ( wrapper.find( FinalResultTile ).length ).toEqual( 1 );
    } );

    it( 'renders with a Progress if they tile type is progress', () => {
        props.tileType = 'progress';
        const wrapper = shallow( <Tile { ...props } /> );
        expect ( wrapper.find( ProgressTile ).length ).toEqual( 1 );
    } );
} );
