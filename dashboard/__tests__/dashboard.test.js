import React from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';
import { Dashboard } from '../dashboard';

describe( 'Dashboard component :', () => {
    let props;

    beforeEach( () => {
        props = {
            fetchProgramViewProgress: jest.fn(),
            programs: []
        };
    } );

    it( 'renders without errors', () => {
        const renderer = new ShallowRenderer();
        const wrapper = renderer.render(
            <Dashboard { ...props  } />
        );
        expect( wrapper ).toMatchSnapshot();
    } );
} );
