// libs
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
// enhancers
import { dashboardEnhancer } from '@biw/dashboard';
// components
import Tile from './tile';
import ProgramBranding from '../../program-branding';

// scss
import './dashboard.scss';

export class Dashboard extends React.Component {
    static propTypes = {
        fetchProgramViewProgress: PropTypes.func.isRequired,
        programs: PropTypes.array
    }

    static defaultProps = {
        programs: []
    }
    constructor( props ) {
        super( props );
        this.state = {
            viewportWidth: window.innerWidth,
        };
        window.addEventListener( 'resize', this.updateViewportWidth );
    }

    componentWillUnmount() {
        window.removeEventListener( 'resize', this.updateViewportWidth );
    }

    updateViewportWidth = event => {
        this.setState( { viewportWidth: window.innerWidth } );
    }
    render() {
        const { programs, fetchProgramViewProgress } = this.props;
        const singleTileWidth = this.state.viewportWidth <= 768 && this.state.viewportWidth >= 480;
        const isSingleTile = programs.length === 1 || singleTileWidth;
        return (
            <div className="dashboard-wrapper">
                <section className={`dashboard-list ${ isSingleTile ? 'single-tile' : '' }`}>
                    { programs.map( ( program, i ) =>
                        <article key={ program.programViewId } className={ classNames(
                            'dashboard-list-item-wrapper',
                            `branding-wrapper-${ program.programId }`,
                            {
                                'single': isSingleTile
                            }
                        )}>
                            <ProgramBranding
                                programViewId={program.programViewId}
                                scopingSelector={`.branding-wrapper-${ program.programId }`}
                                primaryColor={program.primaryColor}
                                secondaryColor={program.secondaryColor}
                            />
                            <Tile
                                program={ program }
                                fetchProgramViewProgress={ fetchProgramViewProgress }
                            />
                        </article>
                    ) }
                </section>
            </div>
        );
    }
}

export default dashboardEnhancer( Dashboard );
