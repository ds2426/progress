// libs
import React from 'react';
import PropTypes from 'prop-types';
//components
import Header from '../components/tile-header';
import Footer from '../components/tile-footer';
import DaysLeftCounter from '../components/days-left-counter';
import ProgramName from '../components/program-name';
import ProgramOverview from '../components/program-overview';

const ProgramOverviewTile = ( props ) => {
    const {
        programName,
        programViewId,
        programLogo,
        heroImageUrl,
        programOverviewCode,
        footerMessageCode,
        buttonTextCode,
        daysLeft,
        calendarLabel,
        lastDayLeftDate
    } = props;

    return (
        <div className="dashboard-tile-wrap dashboard-tile">
            <Header
                programLogo={programLogo}
                programName={ programName }
                heroImageUrl={heroImageUrl}
                isLastDay={daysLeft === 1}
            />
            <DaysLeftCounter
                daysLeft={ daysLeft }
                calendarLabel={ calendarLabel }
                lastDayLeftDate={ lastDayLeftDate }
            />
            <div className="dashboard-tile-content-wrap">
                <ProgramName programName={ programName } />
                <ProgramOverview programOverviewCode={ programOverviewCode } { ...props } />
            </div>
            <Footer
                programViewId={programViewId}
                buttonTextCode={buttonTextCode}
                footerMessageCode={footerMessageCode}
            />
        </div>
    );
};

const datePropTypes = PropTypes.oneOfType( [
    PropTypes.string,
    PropTypes.instanceOf( Date )
] );

ProgramOverviewTile.propTypes = {
    programName: PropTypes.string,
    programLogo: PropTypes.string,
    heroImageUrl: PropTypes.string,
    programViewId: PropTypes.string,
    programOverviewCode: PropTypes.string,
    footerMessageCode: PropTypes.string,
    buttonTextCode: PropTypes.string,
    daysLeft: PropTypes.number,
    calendarLabel: PropTypes.string,
    lastDayLeftDate: datePropTypes
};

export default ProgramOverviewTile;
