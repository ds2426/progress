// libs
import React from 'react';
import PropTypes from 'prop-types';
import { LocaleString, FormattedNumber } from '@biw/intl';
//components
import Header from '../components/tile-header';
import Footer from '../components/tile-footer';
import DaysLeftCounter from '../components/days-left-counter';
import ProgramName from '../components/program-name';
// shared
import { EarnedCheckMark, FailureIcon } from 'component-library';
import '../../tile.scss';

class FinalResultTile extends React.Component  {
    static propTypes = {
        programLogo: PropTypes.string,
        programName: PropTypes.string,
        programViewId: PropTypes.string.isRequired,
        heroImageUrl: PropTypes.string,
        resultMessageCode: PropTypes.shape( {
            cssClass: PropTypes.string,
            key: PropTypes.string
        } ),
        resultSubmessageCode: PropTypes.oneOfType( [
            PropTypes.string,
            PropTypes.number,
            PropTypes.shape( {
                cssClass: PropTypes.string,
                key: PropTypes.string
            } )
        ] ),
        footerMessageCode: PropTypes.string,
        pointsEarned: PropTypes.number,
        pointsMediaLabel: PropTypes.string,
        buttonTextCode: PropTypes.string,
        replaceOptions: PropTypes.shape( {
            mediaLabel: PropTypes.string
        } ).isRequired
    };

    getIcon = () => this.props.pointsEarned > 0 ?  EarnedCheckMark : FailureIcon;

    render() {
        const {
            programName,
            programViewId,
            resultMessageCode,
            resultSubmessageCode,
            footerMessageCode,
            pointsEarned,
            pointsMediaLabel,
            programLogo,
            heroImageUrl,
            buttonTextCode,
            replaceOptions,
        } = this.props;
        const Icon = this.getIcon();
        const messageClassName = resultMessageCode.cssClass;
        return (
            <div className="dashboard-tile-wrap dashboard-tile">
                <Header
                    programLogo={  programLogo }
                    programName={ programName }
                    heroImageUrl={ heroImageUrl }
                    programHasEnded
                />

                <DaysLeftCounter
                    isFinalResults
                    daysLeft={ 0 }
                    calendarLabel="dashboard.tile.common.finalResults"
                />

                <div className="dashboard-tile-content-wrap dashboard-final-results">
                    <ProgramName programName={programName} />
                        <div className="status-message-container">
                            <div className="status-icon"><Icon /></div>
                            <div className="status-messages">
                                <div className={ messageClassName }>
                                    <LocaleString code={ resultMessageCode.key } replaceOptions={ replaceOptions } />
                                </div>
                                { pointsEarned > 0 &&
                                    <div className="point-container">
                                        <FormattedNumber className="status-points" value={pointsEarned} />
                                        <span className="status-label">&nbsp;{ pointsMediaLabel }&nbsp;</span>
                                    </div>
                                }

                                { resultSubmessageCode.key !== '' &&
                                    <div className={ resultSubmessageCode.cssClass }>
                                        <LocaleString code={ resultSubmessageCode.key } replaceOptions={ replaceOptions } />
                                    </div>
                                }
                            </div>
                        </div>
                </div>
                <Footer
                    buttonTextCode={buttonTextCode}
                    footerMessageCode={footerMessageCode}
                    programViewId={programViewId}
                />
            </div>
        );
    }
}
export default FinalResultTile;
