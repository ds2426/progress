// libs
import React from 'react';
import PropTypes from 'prop-types';
//components
import Header from '../components/tile-header';
import Footer from '../components/tile-footer';
import { LocaleString } from '@biw/intl';


const NoGoalSelectedTile = ( {
    programName,
    programViewId,
    programLogo,
    heroImageUrl,
    footerMessageCode,
    buttonTextCode,
    daysLeft,
    calendarLabel,
    lastDayLeftDate
} ) => (
    <div className="dashboard-tile-wrap dashboard-tile">
        <Header
            programHasEnded
            programName={ programName }
            programLogo={programLogo}
            heroImageUrl={ heroImageUrl}
            isLastDay={daysLeft === 1}
        />
        <div className="dashboard-tile-content-wrap">
            <div className="no-goal-selected-message">
                <div className="program-name">{ programName }</div>
                <div className="goal-selection-message-sorry">
                    <h2><LocaleString code="dashboard.tile.common.sorry" /></h2>
                    <p><LocaleString code="dashboard.tile.common.nogoalselected" /></p>
                </div>
            </div>
        </div>
        <Footer
            programViewId={programViewId}
            buttonTextCode={buttonTextCode}
            footerMessageCode={footerMessageCode}
            alignButton
        />
    </div>
);

const datePropTypes = PropTypes.oneOfType( [
    PropTypes.string,
    PropTypes.instanceOf( Date )
] );

NoGoalSelectedTile.propTypes = {
    programName: PropTypes.string,
    programLogo: PropTypes.string,
    heroImageUrl: PropTypes.string,
    programViewId: PropTypes.string,
    buttonTextCode: PropTypes.string,
    footerMessageCode: PropTypes.string,

    daysLeft: PropTypes.number,
    calendarLabel: PropTypes.string,
    lastDayLeftDate: datePropTypes
};

export default NoGoalSelectedTile;
