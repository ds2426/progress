// libs
import React from 'react';
import PropTypes from 'prop-types';
//components
import Header from '../components/tile-header';
import Footer from '../components/tile-footer';
import DaysLeftCounter from '../components/days-left-counter';
import ProgressBar from '../components/progress-bar';
import ProgressMeter from '../components/progress-meter';
import ProgramName from '../components/program-name';
import DataThrough from '../components/data-through';


const ProgressTile = ( {
    programName,
    programViewId,
    programHasEnded,
    programLogo,
    heroImageUrl,
    statusTreatment,
    daysLeft,
    calendarLabel,
    lastDayLeftDate,
    footerMessageCode,
    buttonTextCode,
    progressPercent,
    progressLastUpdated,
    dateThroughPercentage,
    progressLastUpdatedCode,
    onTrackPct,
    programCalculationType,
    isReduction,
    isAverage,
    isStandard,
    currentGoalValue,
    goalAchieveAmount,
    goalLabel
} ) => (
    <div className="dashboard-tile-wrap dashboard-tile">
        <Header
            programLogo={programLogo}
            programName={ programName }
            heroImageUrl={heroImageUrl}
            programHasEnded={programHasEnded}
            isLastDay={ daysLeft === 1 }
            inProgress
        />
        <DaysLeftCounter
            programHasEnded={programHasEnded}
            daysLeft={daysLeft}
            calendarLabel={calendarLabel}
            lastDayLeftDate={lastDayLeftDate}
        />
        <div className="dashboard-tile-content-wrap progress-wrap">
            <ProgramName programName={programName} />
            { isStandard ?
                <ProgressBar
                    progressPercent={progressPercent}
                    statusTreatment={statusTreatment}
                    dateThroughPercentage={ dateThroughPercentage }
                />
            :
                <ProgressMeter
                    onTrackPct={onTrackPct}
                    goalLabel={goalLabel}
                    currentGoalValue={currentGoalValue}
                    goalAchieveAmount={goalAchieveAmount}
                    isInverted={isReduction}
                />
            }
            <DataThrough progressLastUpdated={progressLastUpdated} progressLastUpdatedCode={progressLastUpdatedCode} />
        </div>

        <Footer
            programViewId={programViewId}
            buttonTextCode={buttonTextCode}
            footerMessageCode={footerMessageCode}
        />
    </div>
);
const datePropTypes = PropTypes.oneOfType( [
    PropTypes.string,
    PropTypes.number
] );
ProgressTile.propTypes = {
    programViewId: PropTypes.string,
    programName: PropTypes.string,
    progressLastUpdatedCode: PropTypes.string,
    programLogo: PropTypes.string,
    heroImageUrl: PropTypes.string,
    programHasEnded: PropTypes.bool,
    progressPercent: PropTypes.number,
    buttonTextCode: PropTypes.string,
    footerMessageCode: PropTypes.string,
    progressLastUpdated: datePropTypes,
    statusTreatment: PropTypes.object,
    dateThroughPercentage: PropTypes.number,
    daysLeft: PropTypes.number,
    calendarLabel: PropTypes.string,
    lastDayLeftDate: PropTypes.string,
    currentGoalValue: PropTypes.number,
    goalAchieveAmount: PropTypes.number,
    onTrackPct: PropTypes.number,
    goalLabel: PropTypes.shape( {
        amount: PropTypes.number,
        labelType: PropTypes.string,
        labelText: PropTypes.string,
        labelPosition: PropTypes.string,
        currencyCode: PropTypes.string,
        currencySymbol: PropTypes.string,
        currencyPosition: PropTypes.string,
        precision: PropTypes.oneOfType( [
            PropTypes.string,
            PropTypes.number
         ] ),
        roundingMethod: PropTypes.string
    } ),
    programCalculationType: PropTypes.string,
    isReduction: PropTypes.bool,
    isAverage: PropTypes.bool,
    isStandard: PropTypes.bool
};

export default ProgressTile;
