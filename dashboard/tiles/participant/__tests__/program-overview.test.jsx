import React from 'react';
import renderer from 'react-test-renderer';
import ProgramOverviewTile from '../program-overview.jsx';
import addDays from 'date-fns/add_days';
import addHours from 'date-fns/add_hours';

function getNow() {
    return new Date( Date.now() );
}

function dateFromNow( days ) {
    return addHours( addDays( getNow(), days ), days > 0 ? 1 : -1 ).toISOString();
}
const defaultProps = {
    daysLeft: 3,
    endDate: dateFromNow( 10 ),
    startDate: dateFromNow( 3 )
};

describe( 'Overview component :', () => {
    it( 'renders without errors with the default props', () => {
        const props = {
            ...defaultProps
        };
        const component = renderer.create( <ProgramOverviewTile { ...props } /> );
        const tree = component.toJSON();
        expect( tree ).toMatchSnapshot();
    } );

} );