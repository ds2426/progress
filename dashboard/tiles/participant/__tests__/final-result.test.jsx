import React from 'react';
import renderer from 'react-test-renderer';
import FinalResultTile from '../final-result.jsx';

const defaultProps = {
    isGoalSelected: false,
    goalSelectionOver: false,
    programHasEnded: true
};

describe( 'final result component :', () => {
    it( 'renders without errors with the default props', () => {
        const props = {
            ...defaultProps,
            resultMessageCode: { key: '', cssClass: 'adjustment' },
            resultSubmessageCode: { key: '', cssClass: 'adjustment' },
            statusTreatmemt: {
                cssClass: 'test',
                label: 'foo'
            },
            programViewId: 'something',
            replaceOptions: {}
        };
        const component = renderer.create( <FinalResultTile { ...props } /> );
        const tree = component.toJSON();
        expect( tree ).toMatchSnapshot();
    } );
} );
