import React from 'react';
import renderer from 'react-test-renderer';
import ProgressTile from '../progress.jsx';

const defaultProps = {
    isGoalSelected: false,
    goalSelectionOver: false,
    statusTreatment: {
        cssClass: 'test',
        label: 'test'
    },
    goalLabel: {}
};

describe( 'Progress component :', () => {
    it( 'renders without errors with the default props', () => {
        const props = {
            ...defaultProps,
            statusTreatment: {
                cssClass: 'test',
                label: 'test'
            }
        };
        const component = renderer.create( <ProgressTile { ...props } /> );
        const tree = component.toJSON();
        expect( tree ).toMatchSnapshot();
    } );

} );
