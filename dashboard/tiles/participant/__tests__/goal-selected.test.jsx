import React from 'react';
import renderer from 'react-test-renderer';
import GoalSelectedTile from '../goal-selected.jsx';

const defaultProps = {
    isGoalSelected: false,
    goalSelectionOver: false
};

describe( 'Goal Selection component :', () => {
    it( 'renders without errors with the default props', () => {
        const props = {
            ...defaultProps,
            daysLeftTreatment: {
                calendarLabel: 'test',
                daysLeft: 2
            }
        };
        const component = renderer.create( <GoalSelectedTile { ...props } /> );
        const tree = component.toJSON();
        expect( tree ).toMatchSnapshot();
    } );

} );