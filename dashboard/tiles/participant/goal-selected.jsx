// libs
import React from 'react';
import PropTypes from 'prop-types';
//components
import Header from '../components/tile-header';
import Footer from '../components/tile-footer';
import DaysLeftCounter from '../components/days-left-counter';
import ProgramName from '../components/program-name';
import GoalOverview from '../components/goal-overview';


const GoalSelectedTile = ( {
    programName,
    programLogo,
    programViewId,
    heroImageUrl,
    programHasEnded,
    daysLeft,
    calendarLabel,
    lastDayLeftDate,
    footerMessageCode,
    buttonTextCode,
    isObjectives,
    goalLabel,
    levelName
} ) => (
    <div className="dashboard-tile-wrap dashboard-tile">
        <Header
            programLogo={programLogo}
            programName={ programName }
            heroImageUrl={ heroImageUrl}
            programHasEnded={programHasEnded}
            isLastDay={daysLeft === 1}
            inProgress
        />
        <DaysLeftCounter
            isObjectivesOverview
            daysLeft={ daysLeft }
            calendarLabel={ calendarLabel }
            lastDayLeftDate={ lastDayLeftDate }
        />
        <div className="dashboard-tile-content-wrap">
            <ProgramName programName={programName} />
            <GoalOverview
                goalLabel={goalLabel}
                levelName={levelName}
                isObjectives={isObjectives}
                describeLevel={isObjectives ? 'dashboard.tile.common.yourGoal' : 'dashboard.tile.common.youselect'}
                lastDayLeftDate={lastDayLeftDate}
                programViewId={programViewId}
            />
        </div>
        <Footer
            programViewId={programViewId}
            buttonTextCode={buttonTextCode}
            footerMessageCode={footerMessageCode}
        />
    </div>
);

const datePropTypes = PropTypes.oneOfType( [
    PropTypes.string,
    PropTypes.instanceOf( Date )
] );
GoalSelectedTile.propTypes = {
    programName: PropTypes.string,
    programViewId: PropTypes.string,
    programLogo: PropTypes.string,
    heroImageUrl: PropTypes.string,
    programHasEnded: PropTypes.bool,
    buttonTextCode: PropTypes.string,
    footerMessageCode: PropTypes.string,
    daysLeft: PropTypes.number,
    calendarLabel: PropTypes.string,
    lastDayLeftDate: datePropTypes,
    isObjectives: PropTypes.bool,
    levelName: PropTypes.string,
    goalLabel: PropTypes.shape( {
        amount: PropTypes.number,
        labelType: PropTypes.string,
        labelText: PropTypes.string,
        labelPosition: PropTypes.string,
        currencyCode: PropTypes.string,
        currencySymbol: PropTypes.string,
        currencyPosition: PropTypes.string,
        precision: PropTypes.oneOfType( [
            PropTypes.string,
            PropTypes.number
         ] ),
        roundingMethod: PropTypes.string
    } ),
};

export default GoalSelectedTile;
