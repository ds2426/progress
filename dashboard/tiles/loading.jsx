import React from 'react';

import '../tile.scss';

const LoadingTile = () => (
    <div className="dashboard-tile dashboard-tile-loading">
        <div className="box">
            <div className="shimmer">
                <div>
                    <div className="shimmer-mask mask-1" />
                    <div className="shimmer-mask mask-2" />
                </div>
                <div className="shimmer-mask shimmer-1" />
                <div className="shimmer-mask shimmer-2" />
                <div className="shimmer-mask shimmer-3" />
                <div className="shimmer-mask shimmer-4" />
            </div>
        </div>
    </div>
);

export default LoadingTile;
