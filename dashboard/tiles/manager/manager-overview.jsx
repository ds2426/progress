/* eslint-disable react/prop-types */
// libs
import React from 'react';
import PropTypes from 'prop-types';
//components
import Header from '../components/tile-header';
import Footer from '../components/tile-footer';
import DaysLeftCounter from '../components/days-left-counter';
import ManagerProgramOverview from '../components/manager-components/manager-program-overview';
import ProgramName from '../components/program-name';

import '../../tile.scss';

const ManagerOverviewTile = ( props ) => {
    const {
        programName,
        programViewId,
        programLogo,
        programHasEnded,
        heroImageUrl,
        programOverviewCode,
        daysLeft,
        calendarLabel,
        lastDayLeftDate,
        footerMessageCode,
        buttonTextCode,
    } = props;
    return (
        <div className=" dashboard-tile-wrap dashboard-tile">
            <Header
                programLogo={programLogo}
                programName={ programName }
                heroImageUrl={heroImageUrl}
                programHasEnded={programHasEnded}
                isLastDay={daysLeft === 1}
            />
            <DaysLeftCounter
                daysLeft={ daysLeft }
                calendarLabel={calendarLabel}
                lastDayLeftDate={lastDayLeftDate}
            />
            <div className="dashboard-tile-content-wrap">
                <ProgramName programName={programName} />
                <ManagerProgramOverview programOverviewCode={ programOverviewCode } { ...props } />
            </div>
            <Footer
                programViewId={programViewId}
                buttonTextCode={buttonTextCode}
                footerMessageCode={footerMessageCode}
            />
        </div>
    );
};

export default ManagerOverviewTile;

ManagerOverviewTile.propTypes = {
    programName: PropTypes.string,
    heroImageUrl: PropTypes.string,
    programLogo: PropTypes.string,
    programViewId: PropTypes.string,
    programOverviewCode: PropTypes.string,

    buttonTextCode: PropTypes.string,
    footerMessageCode: PropTypes.string,

    daysLeft: PropTypes.number,
    calendarLabel: PropTypes.string,
    lastDayLeftDate: PropTypes.string
};
