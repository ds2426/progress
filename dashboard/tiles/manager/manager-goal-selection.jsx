// libs
import React from 'react';
import PropTypes from 'prop-types';
//components
import Header from '../components/tile-header';
import Footer from '../components/tile-footer';
import DaysLeftCounter from '../components/days-left-counter';
import ManagerProgress from '../components/manager-components/manager-progress';
import ProgramName from '../components/program-name';

import '../../tile.scss';


const ManagerGoalSelectionTile = ( {
    programName,
    programViewId,
    count,
    total,
    percent,
    progressLastUpdatedCode,
    progressLastUpdated,
    statusCode,
    programLogo,
    heroImageUrl,
    daysLeft,
    calendarLabel,
    lastDayLeftDate,
    footerMessageCode,
    buttonTextCode
} ) => (
    <div className="dashboard-tile-manager-wrap dashboard-tile-wrap dashboard-tile">
        <Header
            programLogo={programLogo}
            programName={ programName }
            heroImageUrl={heroImageUrl}
            isLastDay={daysLeft === 1}
        />
        <DaysLeftCounter
            daysLeft={daysLeft}
            calendarLabel={calendarLabel}
            lastDayLeftDate={lastDayLeftDate}
        />
        <div className="dashboard-tile-content-wrap">
            <ProgramName programName={programName} />
            <ManagerProgress
                statusCode={statusCode}
                progressLastUpdated={progressLastUpdated}
                progressLastUpdatedCode={progressLastUpdatedCode}
                total={ total }
                count={ count }
                percent={ percent }
            />
        </div>
        <Footer
            programViewId={programViewId}
            buttonTextCode={buttonTextCode}
            footerMessageCode={footerMessageCode}
        />
    </div>
);

ManagerGoalSelectionTile.propTypes = {
    programName: PropTypes.string,
    programViewId: PropTypes.string,
    progressLastUpdated: PropTypes.number,
    progressLastUpdatedCode: PropTypes.string,
    count: PropTypes.number,
    total: PropTypes.number,
    percent: PropTypes.number,
    statusCode: PropTypes.string,
    programLogo: PropTypes.string,
    heroImageUrl: PropTypes.string,
    buttonTextCode: PropTypes.string,
    footerMessageCode: PropTypes.string,
    daysLeft: PropTypes.number,
    calendarLabel: PropTypes.string,
    lastDayLeftDate: PropTypes.string
};

export default ManagerGoalSelectionTile;
