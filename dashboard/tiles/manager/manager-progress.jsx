/* eslint-disable react/prop-types */
// libs
import React from 'react';
import PropTypes from 'prop-types';
//components
import Header from '../components/tile-header';
import Footer from '../components/tile-footer';
import DaysLeftCounter from '../components/days-left-counter';
import ManagerProgress from '../components/manager-components/manager-progress';
import ProgramName from '../components/program-name';
import DataThrough from '../components/data-through';

import '../../tile.scss';

const ManagerProgressTile = ( {
    programName,
    programViewId,
    count,
    total,
    percent,
    progressLastUpdatedCode,
    progressLastUpdated,
    statusCode,
    programHasEnded,
    programLogo,
    heroImageUrl,
    daysLeft,
    calendarLabel,
    lastDayLeftDate,
    checkingResults,
    footerMessageCode,
    buttonTextCode,
} ) => (
    <div className="dashboard-tile-manager-wrap dashboard-tile-wrap dashboard-tile">
        <Header
            programLogo={programLogo}
            programName={ programName }
            heroImageUrl={ heroImageUrl}
            programHasEnded={programHasEnded}
            isLastDay={daysLeft === 1}
            inProgress={ programHasEnded === false }
        />
        <DaysLeftCounter
            programHasEnded={programHasEnded}
            daysLeft={ daysLeft }
            calendarLabel={checkingResults ? 'dashboard.tile.common.waitingForFinal' : calendarLabel}
            lastDayLeftDate={lastDayLeftDate}
            checkingResults={checkingResults}
        />
        <div className="dashboard-tile-content-wrap">
            <ProgramName programName={programName} />
            <ManagerProgress
                statusCode={statusCode}
                progressLastUpdated={progressLastUpdated}
                progressLastUpdatedCode={progressLastUpdatedCode}
                total={ total }
                count={ count }
                percent={ percent }
            />
            <DataThrough progressLastUpdated={progressLastUpdated} progressLastUpdatedCode={progressLastUpdatedCode} />
        </div>
        <Footer
            programViewId={programViewId}
            buttonTextCode={buttonTextCode}
            footerMessageCode={footerMessageCode}
        />
    </div>
);

const datePropTypes = PropTypes.oneOfType( [
    PropTypes.string,
    PropTypes.number
] );
ManagerProgressTile.propTypes = {
    programName: PropTypes.string,
    progressLastUpdated: datePropTypes,
    progressLastUpdatedCode: PropTypes.string,
    programViewId: PropTypes.string.isRequired,
    count: PropTypes.number,
    total: PropTypes.number,
    percent: PropTypes.number,
    statusCode: PropTypes.string,
    programLogo: PropTypes.string,
    heroImageUrl: PropTypes.string,
    checkingResults: PropTypes.bool,
    buttonTextCode: PropTypes.string,
    footerMessageCode: PropTypes.string,
    daysLeft: PropTypes.number,
    calendarLabel: PropTypes.string,
    lastDayLeftDate: PropTypes.string
};
export default ManagerProgressTile;
