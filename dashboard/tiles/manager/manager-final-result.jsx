import React from 'react';
import PropTypes from 'prop-types';
//components
import Header from '../components/tile-header';
import Footer from '../components/tile-footer';
import DaysLeftCounter from '../components/days-left-counter';
import ManagerProgress from '../components/manager-components/manager-progress';
import ProgramName from '../components/program-name';
import ManagerEarnedResults from '../components/manager-components/manager-earned-results';

import '../../tile.scss';
const ManagerFinalResultTile = ( {
    programName,
    programViewId,
    count,
    total,
    percent,
    progressLastUpdatedCode,
    progressLastUpdated,
    programLogo,
    heroImageUrl,
    buttonTextCode,
    totalNumberofPaxWhoAchievedGoal,
    totalNumberOfPoints,
    mediaLabel,
    earnedOverride
} ) => (
    <div className="dasboard-tile-wrap dashboard-tile">
        <Header
            programHasEnded
            programName={ programName }
            programLogo={ programLogo }
            heroImageUrl={ heroImageUrl }
        />
        <DaysLeftCounter
            isFinalResults
            daysLeft={ 0 }
            calendarLabel={'dashboard.tile.common.finalResults'}
        />
        <div className="dashboard-tile-content-wrap dashboard-final-results">
            <ProgramName programName={programName} />
            { earnedOverride && totalNumberOfPoints ?
                <ManagerEarnedResults
                    totalNumberOfPoints={ totalNumberOfPoints }
                    mediaLabel={ mediaLabel }
                    totalNumberofPaxWhoAchievedGoal={ totalNumberofPaxWhoAchievedGoal }
                    progressLastUpdated={ progressLastUpdated }
                    progressLastUpdatedCode={ progressLastUpdatedCode }
                    total={ total }
                    count={ count }
                />
            :
                <ManagerProgress
                    statusCode="dashboard.tile.common.participantsAchieved"
                    totalNumberofPaxWhoAchievedGoal={ totalNumberofPaxWhoAchievedGoal }
                    progressLastUpdated={ progressLastUpdated }
                    progressLastUpdatedCode={ progressLastUpdatedCode }
                    total={ total }
                    count={ count }
                    percent={ percent }
                />
            }

        </div>
        <Footer
            programViewId={ programViewId }
            buttonTextCode={ buttonTextCode }
            footerMessageCode={ earnedOverride && totalNumberOfPoints ? 'dashboard.tile.common.youdidit' : 'dashboard.tile.manager.reviewResults' }
        />
    </div>
);

const datePropTypes = PropTypes.oneOfType( [
    PropTypes.string,
    PropTypes.number
] );
ManagerFinalResultTile.propTypes = {
    programName: PropTypes.string,
    programViewId: PropTypes.string,
    progressLastUpdated: datePropTypes,
    progressLastUpdatedCode: PropTypes.string,
    count: PropTypes.number,
    total: PropTypes.number,
    percent: PropTypes.number,
    programLogo: PropTypes.string,
    heroImageUrl: PropTypes.string,
    buttonTextCode: PropTypes.string,
    totalNumberOfPoints: PropTypes.number,
    totalNumberofPaxWhoAchievedGoal: PropTypes.number,
    mediaLabel: PropTypes.string,
    earnedOverride: PropTypes.bool
};

export default ManagerFinalResultTile;
