import React from 'react';
import PropTypes from 'prop-types';


const ProgramName = ( props, isSingle ) => {
        const  {
            programName,
        } = props;
        return (  <div className="program-name">{ programName }</div>  );
};

export default ProgramName;

ProgramName.propTypes = {
    programName: PropTypes.string
};
