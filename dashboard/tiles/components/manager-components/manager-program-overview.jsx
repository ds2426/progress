import React from 'react';
import PropTypes from 'prop-types';
import { LocaleString } from '@biw/intl';
import { WatchIcon, BullseyeIcon } from 'component-library';
import { constants } from 'contests-utils';
const { KEY_CODES } = constants;

const Icon = ( { programOverviewCode } ) => {
    return programOverviewCode === KEY_CODES.waitingForProgress ? <WatchIcon /> : <BullseyeIcon />;
};

Icon.propTypes = {
    programOverviewCode: PropTypes.string
};

const ManagerProgramOverview = ( { programOverviewCode } ) => {
        return (
            <div className="program-overview-wrap">
                <div className="program-overview-icon-container">
                    <div className="program-overview-icon objectives">
                        <Icon programOverviewCode={programOverviewCode} />
                    </div>
                </div>
                <div className="program-overview-copy">
                    <LocaleString code={programOverviewCode} />
                </div>
        </div>
    );
};

export default ManagerProgramOverview;

ManagerProgramOverview.propTypes = {
    programOverviewCode: PropTypes.string
};

