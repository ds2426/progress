import React from 'react';
import renderer from 'react-test-renderer';
import ManagerEarnedResults from '../manager-earned-results';
import { FormattedNumber } from '@biw/intl';
import { mount } from 'enzyme';

const props = {
    totalNumberOfPoints: 200
};
describe( 'Manager Earned Results component :', () => {
    it( 'renders without errors', () => {
        const wrapper = renderer.create(
            <ManagerEarnedResults { ...props } />
        );
        expect( wrapper ).toMatchSnapshot();
    } );
    it ( 'should render the total number of points', () => {
        const props = {
            totalNumberOfPoints: 200
        };
        const wrapper = mount( <ManagerEarnedResults { ...props } /> );
        expect( wrapper.find( FormattedNumber ).props().value ).toEqual( 200 );
    } );

} );
