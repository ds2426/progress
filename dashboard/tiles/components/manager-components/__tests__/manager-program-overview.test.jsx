import React from 'react';
import renderer from 'react-test-renderer';
import ManagerProgramOverview from '../manager-program-overview';
import { constants } from 'contests-utils';
import { WatchIcon, BullseyeIcon } from 'component-library';
import { mount } from 'enzyme';

const { KEY_CODES } = constants;
const props = {
    programOverviewCode: KEY_CODES.waitingForProgress
};

describe( 'Manager Program Overview component :', () => {
    it( 'renders without errors', () => {
        const wrapper = renderer.create(
            <ManagerProgramOverview { ...props } />
        );
        expect( wrapper ).toMatchSnapshot();
    } );
    it ( 'should render the watch Icon when program is in waiting for progress state', () => {
        const wrapper = mount( <ManagerProgramOverview { ...props } /> );
        expect( wrapper.find( WatchIcon ).exists() ).toEqual( true );
    } );

    it ( 'should render the Bullseye Icon when program has not started', () => {
        const props = {
            programOverviewCode: KEY_CODES.checkoutYourTeamsGoals
        };
        const wrapper = mount( <ManagerProgramOverview { ...props } /> );
        expect( wrapper.find( BullseyeIcon ).exists() ).toEqual( true );
    } );
} );
