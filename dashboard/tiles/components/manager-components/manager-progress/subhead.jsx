import React from 'react';
import PropTypes from 'prop-types';
import { LocaleString } from '@biw/intl';
import './manager-progress.scss';

const ManagerProgressSubhead = ( {
    progressLastUpdatedCode,
    progressLastUpdated,
    count,
    total,
    statusCode
} ) => (
    <div className='goal-count-container with-final-results'>
        <LocaleString
            className="goal-count"
            code='dashboard.tile.manager.participantsAchievedSubhead'
            replaceOptions={{ paxAchievedCount: total, paxCount: count }}
        />
    </div>
);

export default ManagerProgressSubhead;

ManagerProgressSubhead.propTypes = {
    progressLastUpdated: PropTypes.any,
    progressLastUpdatedCode: PropTypes.string,
    count: PropTypes.number,
    total: PropTypes.number,
    statusCode: PropTypes.string
};
