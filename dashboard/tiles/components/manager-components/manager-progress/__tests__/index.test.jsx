import React from 'react';
import renderer from 'react-test-renderer';
import ManagerProgress from '../index';

const props = {
    progressPercentage: 10
};

describe( 'Manager Progress component :', () => {
    it( 'renders without errors', () => {
        const wrapper = renderer.create(
            <ManagerProgress { ...props } />
        );
        expect( wrapper ).toMatchSnapshot();
    } );
} );
