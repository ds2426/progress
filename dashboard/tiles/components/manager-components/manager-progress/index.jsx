import React from 'react';
import PropTypes from 'prop-types';
import { LocaleString, FormattedNumber } from '@biw/intl';
import './manager-progress.scss';

const PERCENT = {
    style: 'percent'
};
const ManagerProgress = ( {
    progressLastUpdatedCode,
    progressLastUpdated,
    count,
    percent,
    total,
    statusCode
} ) => (
    <div className='goal-count-container'>
        <div className="goal-count">
            <span>{ count }&nbsp;/&nbsp;{ total }</span>
            <FormattedNumber className="goal-percentage" value={percent / 100} formatOptions={PERCENT}/>
        </div>
        <div className="goal-type"><LocaleString className="branding-primary-text" code={ statusCode } /></div>
    </div>
);

export default ManagerProgress;

ManagerProgress.propTypes = {
    progressLastUpdated: PropTypes.oneOfType( [ PropTypes.string, PropTypes.number ] ),
    progressLastUpdatedCode: PropTypes.string,
    count: PropTypes.number,
    percent: PropTypes.number,
    total: PropTypes.number,
    statusCode: PropTypes.string
};
