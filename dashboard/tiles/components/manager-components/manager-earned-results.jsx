import React from 'react';
import PropTypes from 'prop-types';
import memoize from 'lodash/memoize';
import { EarnedCheckMark } from 'component-library';
import { LocaleString, FormattedNumber } from '@biw/intl';
import ManagerProgressSubhead from './manager-progress/subhead';

import '../../../tile.scss';

const getMediaLabelObj = memoize( mediaLabel => ( { mediaLabel } ) );
const ManagerEarnedResults = ( {
    totalNumberOfPoints,
    totalNumberofPaxWhoAchievedGoal,
    progressLastUpdated,
    progressLastUpdatedCode,
    mediaLabel,
    total,
    count,
} ) => (
    <div className="status-message-container">
        <div className="status-icon"><EarnedCheckMark /></div>
        <div className="status-messages">
            <div className='you-earned'>
                <LocaleString
                    code='finalResults.selector.dashboard.message.earned'
                    replaceOptions={ getMediaLabelObj( mediaLabel ) }
                />
            </div>
            <div className="point-container">
                <FormattedNumber className="status-points" value={ totalNumberOfPoints } />
                <span className="status-label">&nbsp;{ mediaLabel }&nbsp;</span>
            </div>
            <ManagerProgressSubhead
                statusCode='dashboard.tile.manager.participantsAchievedSubhead'
                progressLastUpdated={progressLastUpdated}
                progressLastUpdatedCode={progressLastUpdatedCode}
                total={ total }
                count={ count }
                totalNumberofPaxWhoAchievedGoal={totalNumberofPaxWhoAchievedGoal}
            />
        </div>
    </div>
);

export default ManagerEarnedResults;

ManagerEarnedResults.propTypes = {
    totalNumberOfPoints: PropTypes.number,
    totalNumberofPaxWhoAchievedGoal: PropTypes.number,
    total: PropTypes.number,
    count: PropTypes.number,
    mediaLabel: PropTypes.string,
    progressLastUpdated: PropTypes.any,
    progressLastUpdatedCode: PropTypes.string,
};
