import React from 'react';
import PropTypes from 'prop-types';
//shared
import { LocaleString, FormattedDate } from '@biw/intl';

const DataThrough = ( { progressLastUpdatedCode, progressLastUpdated } ) => {
    return (
        <div className="updated">
            <LocaleString code={ progressLastUpdatedCode } />&nbsp;
            <FormattedDate value={ progressLastUpdated } />
        </div>
    );
};

export default DataThrough;

DataThrough.propTypes = {
    progressLastUpdatedCode: PropTypes.string,
    progressLastUpdated: PropTypes.oneOfType( [
        PropTypes.string,
        PropTypes.number
    ] )
};