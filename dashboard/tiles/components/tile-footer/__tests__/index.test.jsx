import React from 'react';
import renderer from 'react-test-renderer';
import TileFooter from '../index';

const defaultProps = {
   buttonTextCode: 'dashboard.tile.common.viewDetails',
   footerMessageCode: 'dashboard.tile.common.progress.getGoing'
};

describe( 'Tile Footer component :', () => {
    it( 'renders without errors', () => {
        const wrapper = renderer.create(
            <TileFooter { ...defaultProps } />
        );
        expect( wrapper ).toMatchSnapshot();
    } );
} );
