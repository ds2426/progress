import React from 'react';
import PropTypes from 'prop-types';
import Link from 'redux-first-router-link';
import memoize from 'lodash/memoize';
// shared
import { PROGRAM_VIEW } from 'app/router/types';
import { LocaleString } from '@biw/intl';

const programUrl = memoize( programViewId => ( { type: PROGRAM_VIEW, payload: { programViewId } } ) );

const Button = ( { programViewId, buttonTextCode } ) => (
    <div className="footer-item">
        <div className="button-container">
            <Link to={ programUrl( programViewId ) } className="action branding-primary-button">
                <LocaleString code={ buttonTextCode } />
            </Link>
        </div>
    </div>
);

export default Button;

Button.propTypes = {
    programViewId: PropTypes.string,
    buttonTextCode: PropTypes.string
};
