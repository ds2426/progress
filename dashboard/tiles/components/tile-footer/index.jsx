// libs
import React from 'react';
import PropTypes from 'prop-types';
import Button from './button';
//shared
import { LocaleString } from '@biw/intl';

import './tile-footer.scss';

const TileFooter = ( { footerMessageCode, programViewId, buttonTextCode } ) => (
    <div className="dashboard-tile-footer">
        <div className="footer-container">
            { footerMessageCode &&
                <div className="footer-item footer-message copy"><LocaleString code={ footerMessageCode } /></div>
            }
            <Button
                programViewId={programViewId}
                buttonTextCode={buttonTextCode}
            />
        </div>
    </div>
);

TileFooter.propTypes = {
    buttonTextCode: PropTypes.string,
    footerMessageCode: PropTypes.string,
    programViewId: PropTypes.string
};

TileFooter.defaultProps = {
    footerMessageCode: '',
};

export default TileFooter;
