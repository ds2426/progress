// libs
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
//components
import ImageSizer from 'components/image-sizer';
import { LocaleString } from '@biw/intl';
import { CalendarIcon, ExclamationIcon } from 'component-library';

import './tile-header.scss';
const getIcon = ( isLastDay ) => {
    return isLastDay ? ExclamationIcon : CalendarIcon;
};

const getCode = ( isLastDay ) => {
    return isLastDay ? 'dashboard.tile.common.mustselectgoal' : 'dashboard.tile.common.programHasEnded';
};

const TileHeader = ( {
    programLogo,
    programName,
    heroImageUrl,
    programHasEnded,
    isLastDay,
    inProgress
} ) => {
    const Icon = getIcon( isLastDay );
    const width = window.innerWidth > 890 || window.innerWidth > 440 && window.innerWidth <= 768 ? 768 : 440;
    const imageStyleBG = `url( ${ ImageSizer( heroImageUrl, width ) } )`;
    const heroImageStyle = {
        backgroundImage: imageStyleBG,
        backgroundSize: 'cover',
        width: '100%',
        backgroundPositionY: '50%'
    };
    return (
        <header className="dashboard-tile-header">
            { heroImageUrl ?
                <div className="dashboard-tile-header-image" style={ heroImageStyle }></div>
                :
                <div className="dashboard-tile-header-image"></div>
            }
            { programLogo &&
                <div className="header-logo"><img alt="Program Logo" src={ programLogo } /></div>
            }
            { ( programHasEnded || isLastDay && !inProgress ) &&
                <div className="dashboard-tile-header-overlay">
                    <div className={ classNames( { 'overlay': true, 'red': isLastDay } ) } />
                        <div className="overlay-container">
                            <div role="presentation" className={ classNames( { 'icon': true, 'red': isLastDay } ) }><Icon /></div>
                            <div className="overlay-copy"><LocaleString code={ getCode( isLastDay ) } />
                        </div>
                    </div>
                </div>
            }
        </header>
    );
};
export default TileHeader;

TileHeader.propTypes = {
    programLogo: PropTypes.string,
    programName: PropTypes.string,
    heroImageUrl: PropTypes.string,
    programHasEnded: PropTypes.bool,
    isLastDay: PropTypes.bool,
    inProgress: PropTypes.bool
};

TileHeader.defaultProps = {
    isLastDay: false,
    programHasEnded: false,
    daysLeft: 0,
    endDate: '',
    inProgress: false,
    heroImageUrl: null
};

