import React from 'react';
import { mount } from 'enzyme';
import renderer from 'react-test-renderer';
import TileHeader from '../index';

const defaultProps = {
   programLogo: 'logo',
   programName: 'name',
   heroImageUrl: 'image',
   programHasEnded: false,
   programHeaderOverlayCopy: 'copy',
   programHeaderOverlayIcon: 'icon',
   isLastDay: false,
   daysLeft: '10',
   calendarLabel: 'dashboard.tile.common.daylefttoselectgoal',
   endDate: '2017-03-08T23:59:59-0600'
};

describe( 'Tile Header component :', () => {
    it( 'renders without errors', () => {
        const wrapper = renderer.create(
            <TileHeader { ...defaultProps } />
        );

        expect( wrapper ).toMatchSnapshot();
    } );

    it( 'should render overlay when program has ended', () => {
        const props = {
            programHasEnded: true
        };
        const wrapper = mount( <TileHeader  { ...props }  /> );
        expect ( wrapper.find( '.overlay-copy' ).text() ).toEqual( 'dashboard.tile.common.programHasEnded' );
    } );

    it( 'should not render overlay when program is in progress', () => {
        const props = {
            inProgress: true
        };
        const wrapper = mount( <TileHeader  { ...props }  /> );
        expect ( wrapper.find( '.dashboard-tile-header-overlay' ).length ).toEqual( 0 );
    } );

    it( 'should render last day overlay on the last day', () => {
        const props = {
            ...defaultProps,
            programHasEnded: false,
            isLastDay: true
        };
        const wrapper = mount( <TileHeader { ...props } /> );
        expect ( wrapper.find( '.overlay-copy' ).text() ).toEqual( 'dashboard.tile.common.mustselectgoal' );
    } );
} );
