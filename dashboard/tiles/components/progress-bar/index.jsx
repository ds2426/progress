// libs
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
// shared
import { LocaleString, FormattedNumber } from '@biw/intl';
import { PinDropIcon } from 'component-library';
import { constants } from 'contests-utils';
const { KEY_CODES } = constants;
import './progress-bar.scss';

const PERCENT = { style: 'percent' };
const ProgressBar = ( { progressPercent, dateThroughPercentage, statusTreatment } ) => {
        const progressColor = classNames( statusTreatment.className, 'progress-fill' );
        const progress = progressPercent >= 100 ? 100 : progressPercent;
        const onTarget = dateThroughPercentage >= 100 ? 100 : dateThroughPercentage;
        return (
            <div className="dashboard-progress-bar">
                <FormattedNumber className="progress-number" value={progressPercent / 100} formatOptions={PERCENT}/>
                <LocaleString code={ KEY_CODES.togoal }/>
                <div className="progress-bar">
                    { progress > 0 &&
                        <div className="progress-marker" style={ { left: `${dateThroughPercentage}%`  } }>
                            <PinDropIcon />
                            { dateThroughPercentage < 100 && <div className="progress-line"></div> }
                            <FormattedNumber TextComponent='div' className="on-track-number" value={onTarget / 100} formatOptions={PERCENT}/>
                        </div>
                    }
                    <div className={ progressColor } style={ { width: `${ progress }%` } } />
                </div>

            </div>
        );
    };
export default ProgressBar;


ProgressBar.propTypes = {
    progressPercent: PropTypes.number,
    dateThroughPercentage: PropTypes.number,
    statusTreatment: PropTypes.object
};
