import React from 'react';
import { mount } from 'enzyme';
import renderer from 'react-test-renderer';
import ProgressBar from '../index';

const defaultProps = {
    progressPercent: 20,
    dateThroughPercentage: 50,
    statusTreatment: {
        className: 'get-going',
        label: 'progress.getGoing'
    }
};

describe( 'Dashboard Progress bar component :', () => {
    it( 'renders without errors with the default props', () => {
        const props = {
            ...defaultProps
        };
        const component = renderer.create( <ProgressBar { ...props } /> );
        const tree = component.toJSON();
        expect( tree ).toMatchSnapshot();
    } );

    it( 'renders progress percent and datethrough percentage in the dom as text', () => {
        const props = {
            ...defaultProps,
            progressPercent: 22,
            dateThroughPercentage: 50
        };
        const wrapper = mount( <ProgressBar { ...props } /> );
        expect ( wrapper.find( '.progress-number' ).text() ).toEqual( '0.22' );
    } );

    it( 'should be red if the percentage is 60 or below', () => {
        const props = {
            ...defaultProps,
            progressPercent: 44,
            dateThroughPercentage: 50,
            onTrackPercentage: 33
        };
        const wrapper = mount( <ProgressBar { ...props } /> );
        expect ( wrapper.find( '.get-going' ).exists() ).toEqual( true );
    } );

} );
