import React from 'react';
import PropTypes from 'prop-types';
import {
    Happy,
    Sad,
    UpArrow,
    DownArrow,
    Target,
} from 'component-library';

import './meter-items.scss';

const MeterItems = ( { isInverted, className } ) => isInverted ?
    <div className={ `meter-items inverted ${className}` }>
        <div className="happy"><Happy /></div>
        <div className="down-arrow"><DownArrow /></div>
        <div className="target"><Target /></div>
        <div className="up-arrow"><UpArrow /></div>
        <div className="sad"><Sad /></div>
    </div>
:
    <div className={ `meter-items ${className}` }>
        <div className="happy"><Happy /></div>
        <div className="up-arrow"><UpArrow /></div>
        <div className="target"><Target /></div>
        <div className="down-arrow"><DownArrow /></div>
        <div className="sad"><Sad /></div>
    </div>;

MeterItems.propTypes = {
    className: PropTypes.string,
    isInverted: PropTypes.bool
};

MeterItems.defaultProps = {
    className: '',
    isInverted: false
};

export default MeterItems;
