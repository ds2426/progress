import React from 'react';
// import { mount } from 'enzyme';
import renderer from 'react-test-renderer';
import ProgressMeterAverage from '../index';


describe( 'Dashboard Progress Meter component :', () => {
    let props;
    beforeEach( () => {
        props = {
            goalLabel: {
                currencyCode: 'currency.code',
                currencyPosition: 'after',
                currencySymbol: '$',
                labelPosition: 'before',
                labelText: 'label.text',
                labelType: 'none',
                precision: 2,
                roundingMethod: 'average',
            },
            onTrackPct: 50,
            currentGoalValue: 20,
            goalAchieveAmount: 70,
            isInverted: false
        };
    } );

    it( 'renders without errors with the default props', () => {
        const component = renderer.create( <ProgressMeterAverage { ...props } /> );
        const tree = component.toJSON();
        expect( tree ).toMatchSnapshot();
    } );

    it( 'renders inverted style without errors with the default props', () => {
        props.isInverted = true;
        const component = renderer.create( <ProgressMeterAverage { ...props } /> );
        const tree = component.toJSON();
        expect( tree ).toMatchSnapshot();
    } );
} );
