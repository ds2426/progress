// libs
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
// shared
import { GoalLabel } from 'shared';
import { LocaleString } from '@biw/intl';
import MeterItems from './meter-items';
import './progress-meter.scss';

const setClass = ( classLabel, onTrackPct ) => classNames( classLabel, {
    'on-track-goal': onTrackPct >= 100,
    'not-on-track': onTrackPct < 100
} );

const ProgressMeter = ( {
    onTrackPct,
    goalLabel,
    currentGoalValue,
    goalAchieveAmount,
    isInverted
} ) => (
    <div className={`dashboard-progress-meter${isInverted ? ' inverted' : ''}`}>
        <div className={ setClass( 'progress-meter-wrap', onTrackPct ) }>
            <div className={ setClass( 'number-wrap', onTrackPct ) }>
                <div className={ setClass( 'progress-number', onTrackPct ) }>
                    <span className={ setClass( 'number', onTrackPct ) }>
                        <span className="goal-label">
                            <GoalLabel
                                amount={currentGoalValue}
                                labelType={goalLabel.labelType}
                                labelText={goalLabel.labelText}
                                labelPosition={goalLabel.labelPosition}
                                currencyCode={goalLabel.currencyCode}
                                currencySymbol={goalLabel.currencySymbol}
                                precision={goalLabel.precision}
                                currencyPosition={goalLabel.currencyPosition}
                                roundingMethod={goalLabel.roundingMethod}
                            />
                        </span>
                    </span>
                </div>

                <div className={ setClass( 'target-container', onTrackPct ) }>
                <div className="target-line"></div>
                    <div className="goal-achieve-amount">
                        <span className="achieve-amount">
                            <GoalLabel
                                amount={goalAchieveAmount}
                                labelType={goalLabel.labelType}
                                labelText={''}
                                labelPosition={goalLabel.labelPosition}
                                currencyCode={goalLabel.currencyCode}
                                currencySymbol={goalLabel.currencySymbol}
                                precision={goalLabel.precision}
                                currencyPosition={goalLabel.currencyPosition}
                                roundingMethod={goalLabel.roundingMethod}
                            />&nbsp;<LocaleString className="goal-label" code="dashboard.tile.common.goal" />
                        </span>
                    </div>

                    <div className={ classNames(
                        'meter-items-wrap',
                        { 'inverted': isInverted,
                           'on-track-goal': onTrackPct >= 100,
                           'not-on-track': onTrackPct < 100
                        }
                    ) }>
                        <MeterItems
                            isInverted={isInverted}
                            className={classNames( {
                                'on-track-goal': onTrackPct >= 100,
                                'not-on-track': onTrackPct < 100
                            } )}
                        />
                    </div>
                </div>
            </div>

        </div>
    </div>
);

ProgressMeter.propTypes = {
    currentGoalValue: PropTypes.number,
    goalAchieveAmount: PropTypes.number,
    onTrackPct: PropTypes.number,
    goalLabel: PropTypes.shape( {
        amount: PropTypes.number,
        labelType: PropTypes.string,
        labelText: PropTypes.string,
        labelPosition: PropTypes.string,
        currencyCode: PropTypes.string,
        currencySymbol: PropTypes.string,
        currencyPosition: PropTypes.string,
        precision: PropTypes.oneOfType( [
            PropTypes.string,
            PropTypes.number
         ] ),
        roundingMethod: PropTypes.string
    } ),
    isInverted: PropTypes.bool
};

ProgressMeter.defaultProps = {
    isInverted: true
};
export default ProgressMeter;
