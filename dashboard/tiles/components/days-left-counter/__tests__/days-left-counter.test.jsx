import React from 'react';
import { mount } from 'enzyme';
import renderer from 'react-test-renderer';
import DaysLeftCounter from '../index.jsx';
import { constants } from 'contests-utils';
const { PVS__PROGRESS_LOADED_NO_ISSUANCE } = constants;

const defaultProps = {
    daysLeft: 10,
    description: 'Days left to earn',
    programViewStatus: PVS__PROGRESS_LOADED_NO_ISSUANCE
};

describe( 'Goal status component :', () => {
    it( 'renders without errors with the default props', () => {
        const props = {
            ...defaultProps
        };

        const component = renderer.create( <DaysLeftCounter { ...props } /> );
        const tree = component.toJSON();
        expect( tree ).toMatchSnapshot();
    } );

    it( 'renders last day text on the last day of goal selection', () => {
        const props = {
            daysLeft: 1
        };
        const wrapper = mount( <DaysLeftCounter { ...props } /> );
        expect ( wrapper.find( '.last-day' ).exists() ).toEqual( true );
    } );

    it( 'renders final results', () => {
        const props = {
            daysLeft: 0,
            isFinalResults: true
        };
        const wrapper = mount( <DaysLeftCounter { ...props } /> );
        expect ( wrapper.find( '.final' ).exists() ).toEqual( true );
    } );

    it( 'renders waiting for final results', () => {
        const props = {
            daysLeft: 0,
            checkingResults: true
        };
        const wrapper = mount( <DaysLeftCounter { ...props } /> );
        expect ( wrapper.find( '.waitingForFinal' ).exists() ).toEqual( true );
    } );
} );
