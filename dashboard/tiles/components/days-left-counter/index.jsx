import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
// shared
import { LocaleString, FormattedDate, FormattedTime } from '@biw/intl';
import {
    CalendarIcon,
    ExclamationIcon,
    FinalResultIcon
} from 'component-library';

import './days-left-counter.scss';
const getIcon = ( isLastDay, programHasEnded ) => {
    if ( isLastDay ) {
        return ExclamationIcon;
    } else if ( programHasEnded ) {
        return FinalResultIcon;
    } else {
        return CalendarIcon;
    }
};
const DaysLeftCounter = ( props ) => {
    const {
        daysLeft,
        calendarLabel,
        lastDayLeftDate,
        isObjectivesOverview,
        checkingResults,
        isFinalResults
    } = props;
    const Icon = getIcon( daysLeft === 1 && !checkingResults, ( isFinalResults || checkingResults ) );
    const daysLeftClasses = classNames( {
        'days': true,
        'number': true,
        [ 'days-left-' + daysLeft ]: true,
        'caution': daysLeft < 3,
        'warning': daysLeft < 2,
        'green': daysLeft >= 3 && !checkingResults,
        'last-day': daysLeft === 1
    } );

    const descriptionClasses = classNames( {
        'description': true,
        'caution': daysLeft < 3,
        'warning': daysLeft < 2,
        'green': daysLeft >= 3 && !checkingResults,
        'last-day': daysLeft === 1,
        'final': isFinalResults,
        'waitingForFinal': checkingResults
    } );

    const iconClasses = classNames( {
        'icon': true,
        'caution': daysLeft < 3 && isObjectivesOverview,
        'green': daysLeft >= 3 && !checkingResults,
        'final': isFinalResults || checkingResults,
        'last-day': daysLeft === 1
    } );
    return (
        <aside className="days-left-counter-container">
            <section className="days-left-counter">
                <div className={ iconClasses } >
                <Icon />
                </div>
                    { daysLeft > 1 && !checkingResults ?
                        <div className="description-container">
                            <div className={ daysLeftClasses }>{ daysLeft }</div>
                            <div className={ descriptionClasses }>
                                <span>
                                    <LocaleString code="goalSelection.headerDaysLeft" />&nbsp;
                                    <LocaleString data-sel="goal-selection-calendar-label" code={ calendarLabel } />
                                </span>
                            </div>
                        </div>
                    :
                        <div>
                            { ( isFinalResults || checkingResults ) && <p className={ descriptionClasses }><LocaleString data-sel="goal-selection-calendar-label" code={ calendarLabel } /></p> }
                            { daysLeft === 1 && !checkingResults && !isFinalResults &&
                                <div>
                                    <span className={ daysLeftClasses }>
                                        { isObjectivesOverview ?
                                            <LocaleString data-sel="goal-selection-last-day" code="programInfoHeader.objective.lastDay" />
                                        :
                                            <LocaleString data-sel="goal-selection-last-day" code="programInfoHeader.lastDay" />
                                        }
                                    </span>
                                    <span>
                                        <span className="description date">
                                            { isObjectivesOverview  ? // obj-temp
                                                <span><LocaleString code="programInfoHeader.objective.starts" /></span>
                                            :
                                                <span><LocaleString code="programInfoHeader.closes" /></span>
                                            }
                                            &nbsp;
                                            <span data-sel="goal-selection-closing-time" className="closes">
                                                <FormattedDate value={ lastDayLeftDate } />
                                                &nbsp;&#64;&nbsp;<FormattedTime value={ lastDayLeftDate } />
                                            </span>
                                        </span>
                                    </span>
                                </div>
                            }
                        </div>
                    }
            </section>
        </aside>
    );
};

const datePropTypes = PropTypes.oneOfType( [
    PropTypes.string,
    PropTypes.instanceOf( Date )
] );

DaysLeftCounter.propTypes = {
    daysLeft: PropTypes.number,
    calendarLabel: PropTypes.string,
    lastDayLeftDate: datePropTypes,
    isObjectivesOverview: PropTypes.bool,
    checkingResults: PropTypes.bool,
    isFinalResults: PropTypes.bool
};

DaysLeftCounter.defaultProps = {
    isObjectivesOverview: false,
    checkingResults: false,
    isFinalResults: false
};
export default DaysLeftCounter;
