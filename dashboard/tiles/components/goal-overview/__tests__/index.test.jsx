import React from 'react';
import renderer from 'react-test-renderer';
import GoalOverview from '../index';

const props = {
    buttonTextCode: '',
    footerMessageCode: '',
    programViewId: ''
};

describe( 'Goal Overview component :', () => {
    it( 'renders without errors', () => {
        const wrapper = renderer.create(
            <GoalOverview { ...props } />
        );
        expect( wrapper ).toMatchSnapshot();
    } );
} );
