import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
// shared
import { GoalLabel } from 'shared';
import { GoalLevelBarsIcon, WatchIcon } from 'component-library';
import { LocaleString, FormattedDate, FormattedTime } from '@biw/intl';
import './goal-overview.scss';

const GoalOverview = ( { goalLabel, levelName, describeLevel, isObjectives, lastDayLeftDate } ) => {
    return (
        <div className="goal-overview-wrap">
            <div className="goal-overview-icon-container">
                <div className={ classNames( 'goal-overview-icon icon', { 'objectives': isObjectives } ) }>
                    { isObjectives ? <WatchIcon /> : <GoalLevelBarsIcon />}
                </div>
            </div>
            <span className="goal-overview-copy">
                <LocaleString code={describeLevel} />
                { levelName && <h2 className="goal-overview-name branding-primary-text"> { levelName } </h2> }

                { goalLabel &&
                    <span className={ classNames( 'goal-overview-description', { 'is-objectives': isObjectives } ) }>
                        <GoalLabel
                            amount={goalLabel.amount}
                            labelType={goalLabel.labelType}
                            labelText={goalLabel.labelText}
                            labelPosition={goalLabel.labelPosition}
                            currencyCode={goalLabel.currencyCode}
                            currencySymbol={goalLabel.currencySymbol}
                            precision={Number( goalLabel.precision )}
                            currencyPosition={goalLabel.currencyPosition}
                            roundingMethod={goalLabel.roundingMethod}
                            className="branding-primary-text"
                        />
                        <span className="by-date"><LocaleString code="dashboard.tile.common.by" />&nbsp;<FormattedDate value={ lastDayLeftDate } />&nbsp;<FormattedTime value= { lastDayLeftDate } /></span>
                    </span>
                }
            </span>
        </div>
    );
};
export default GoalOverview;
GoalOverview.propTypes = {
    goalLabel: PropTypes.shape( {
        amount: PropTypes.number,
        labelType: PropTypes.string,
        labelText: PropTypes.string,
        labelPosition: PropTypes.string,
        currencyCode: PropTypes.string,
        currencySymbol: PropTypes.string,
        currencyPosition: PropTypes.string,
        precision: PropTypes.oneOfType( [
            PropTypes.string,
            PropTypes.number
         ] ),
        roundingMethod: PropTypes.string
    } ),
    levelName: PropTypes.string,
    describeLevel: PropTypes.string,
    isObjectives: PropTypes.bool,
    lastDayLeftDate: PropTypes.string
};
