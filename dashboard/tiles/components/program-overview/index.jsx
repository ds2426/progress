import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { LocaleString } from '@biw/intl';
import { CheckListIcon, BullseyeIcon } from 'component-library';

import './program-overview.scss';
const  Icon = ( { isObjectives } ) => isObjectives ? <BullseyeIcon /> : <CheckListIcon />;
Icon.propTypes = {
    isObjectives: PropTypes.bool
};

const ProgramOverview = ( { isObjectives, programOverviewCode } ) =>  {
    return (
        <div className="program-overview-wrap">
            <div className="program-overview-icon-container">
                <div className={ classNames( 'program-overview-icon', { 'objectives': isObjectives } ) }>
                    <Icon isObjectives={ isObjectives } />
                </div>
            </div>
            <div className="program-overview-copy">
                <LocaleString code={programOverviewCode} />
            </div>
        </div>
    );
};

export default ProgramOverview;

ProgramOverview.propTypes = {
    programOverviewCode: PropTypes.string,
    isObjectives: PropTypes.bool
};