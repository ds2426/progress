import React from 'react';
import renderer from 'react-test-renderer';
import DataThrough from '../data-through';

const props = {
    progressLastUpdated: 'today',
    progressLastUpdatedCode: 'dashboard.tile.common.dataThrough'
};


describe( 'Data Through component :', () => {
    it( 'renders without errors', () => {
        const wrapper = renderer.create(
            <DataThrough { ...props } />
        );
        expect( wrapper ).toMatchSnapshot();
    } );
} );
