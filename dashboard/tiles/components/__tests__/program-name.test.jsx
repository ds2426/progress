import React from 'react';
import renderer from 'react-test-renderer';
import ProgramName from '../program-name';

const props = {
    prograName: 'Program Name'
};

describe( 'Program Name component :', () => {
    it( 'renders without errors', () => {
        const wrapper = renderer.create(
            <ProgramName { ...props } />
        );
        expect( wrapper ).toMatchSnapshot();
    } );
} );
