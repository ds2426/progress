import React from 'react';
import renderer from 'react-test-renderer';
import ProgramOverview from '../program-overview';
import { constants } from 'contests-utils';
import { CheckListIcon, BullseyeIcon } from 'component-library';
import { mount } from 'enzyme';

const { KEY_CODES } = constants;
const props = {
    isObjectives: false
};

describe( 'Program Overview component :', () => {
    it( 'renders without errors', () => {
        const wrapper = renderer.create(
            <ProgramOverview { ...props } />
        );
        expect( wrapper ).toMatchSnapshot();
    } );
    it ( 'should render the checklist Icon when program is not objectives', () => {
        const wrapper = mount( <ProgramOverview { ...props } /> );
        expect( wrapper.find( CheckListIcon ).exists() ).toEqual( true );
    } );

    it ( 'should render the Bullseye Icon when program is objectives', () => {
        const props = {
            isObjectives: true,
            programOverviewCode: KEY_CODES.achieveyourGoalEarnRewards
        };
        const wrapper = mount( <ProgramOverview { ...props } /> );
        expect( wrapper.find( BullseyeIcon ).exists() ).toEqual( true );
    } );
} );
