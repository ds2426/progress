'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _tile = require('./tile');

Object.defineProperty(exports, 'tileEnhancer', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_tile).default;
  }
});

var _dashboard = require('./dashboard');

Object.defineProperty(exports, 'dashboardEnhancer', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_dashboard).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
//# sourceMappingURL=index.js.map
