'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _get2 = require('lodash/get');

var _get3 = _interopRequireDefault(_get2);

var _contestsUtils = require('contests-utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// shared


var KEY_CODES = _contestsUtils.constants.KEY_CODES;

var SHOULD_UPDATE_PROPS = ['program', 'goal', 'progressLoading'];

var GoalSelectioinEnhancer = function (_React$Component) {
    _inherits(GoalSelectioinEnhancer, _React$Component);

    function GoalSelectioinEnhancer() {
        _classCallCheck(this, GoalSelectioinEnhancer);

        return _possibleConstructorReturn(this, (GoalSelectioinEnhancer.__proto__ || Object.getPrototypeOf(GoalSelectioinEnhancer)).apply(this, arguments));
    }

    _createClass(GoalSelectioinEnhancer, [{
        key: 'shouldComponentUpdate',
        value: function shouldComponentUpdate(nextProps, nextState) {
            var _this2 = this;

            return SHOULD_UPDATE_PROPS.reduce(function (shouldUpdate, prop) {
                return shouldUpdate || nextProps[prop] !== _this2.props[prop];
            }, false);
        }
    }, {
        key: 'render',
        value: function render() {
            var _props = this.props,
                TileComponent = _props.TileComponent,
                program = _props.program;

            return _react2.default.createElement(TileComponent, {
                tileType: 'goal-selection',
                programType: program.programType,
                programName: program.programName,
                programLogo: program.programLogo,
                programViewId: program.programViewId,
                programHeaderImage: program.programHeaderImage,
                programEndDate: program.endDate

                /* footer*/
                , buttonTextCode: this.buttonTextCode,
                footerMessageCode: this.footerMessageCode

                /*countdown*/
                , daysLeft: this.getDaysLeft,
                calendarLabel: this.calendarLabel,
                lastDayLeftDate: program.startDate
            });
        }
    }, {
        key: 'buttonTextCode',
        get: function get() {
            return KEY_CODES.selectagoal;
        }
    }, {
        key: 'footerMessageCode',
        get: function get() {
            return KEY_CODES.upforchallenge;
        }
    }, {
        key: 'endDate',
        get: function get() {
            return (0, _get3.default)(this.props, 'program.endDate', Date.now());
        }
    }, {
        key: 'startDate',
        get: function get() {
            return (0, _get3.default)(this.props, 'program.startDate', Date.now());
        }
    }, {
        key: 'goalSelectionToDate',
        get: function get() {
            return (0, _get3.default)(this.props, 'program.goalSelectionToDate', Date.now());
        }
    }, {
        key: 'programType',
        get: function get() {
            return (0, _get3.default)(this.props, 'program.programType');
        }
    }, {
        key: 'goalLabel',
        get: function get() {
            return {
                amount: (0, _get3.default)(this.props, 'goal.paxGoalSelection.goalAchieveAmount', 0),
                labelType: (0, _get3.default)(this.props, 'goal.goalLabel.labelType', ''),
                labelText: (0, _get3.default)(this.props, 'goal.goalLabel.labelText', ''),
                lablPosition: (0, _get3.default)(this.props, 'goal.goalLabel.labelPosition', ''),
                curencyCode: (0, _get3.default)(this.props, 'goal.goalLabel.currencyCode', ''),
                currncySymbol: (0, _get3.default)(this.props, 'goal.goalLabel.currencySymbol', ''),
                currenyPosition: (0, _get3.default)(this.props, 'goal.goalLabel.currencyPosition', ''),
                precision: (0, _get3.default)(this.props, 'goal.goalLabel.precision', ''),
                rouningMethod: (0, _get3.default)(this.props, 'goal.goalLabel.roundingMethod', '')
            };
        }
    }, {
        key: 'getDaysLeft',
        get: function get() {
            return (0, _get3.default)(this.props, 'daysDifference.goalSelectionToDate', 0);
        }
    }, {
        key: 'calendarLabel',
        get: function get() {
            return 'goalSelection.toSelectGoal';
        }
    }]);

    return GoalSelectioinEnhancer;
}(_react2.default.Component);

GoalSelectioinEnhancer.propTypes = {
    program: _propTypes2.default.shape({
        programName: _propTypes2.default.string.isRequired,
        programLogo: _propTypes2.default.string,
        programType: _propTypes2.default.string,
        programHeaderImage: _propTypes2.default.string,
        programId: _propTypes2.default.number,
        programViewId: _propTypes2.default.string.isRequired,
        programViewStatus: _propTypes2.default.string,
        role: _propTypes2.default.string,
        endDate: _propTypes2.default.string,
        startDate: _propTypes2.default.string,
        goalSelectionToDate: _propTypes2.default.string,
        isOwnerEligibleForAward: _propTypes2.default.bool
    }).isRequired,
    TileComponent: _propTypes2.default.func.isRequired
};
GoalSelectioinEnhancer.defaultProps = {
    program: {}
};
exports.default = GoalSelectioinEnhancer;
module.exports = exports['default'];
//# sourceMappingURL=goal-overview-enhancer.js.map
