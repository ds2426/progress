'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _get2 = require('lodash/get');

var _get3 = _interopRequireDefault(_get2);

var _contestsUtils = require('contests-utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
// shared


var KEY_CODES = _contestsUtils.constants.KEY_CODES;

var SHOULD_UPDATE_PROPS = ['program', 'goal', 'progressLoading'];

var NoProgressEnhancer = function (_React$Component) {
    _inherits(NoProgressEnhancer, _React$Component);

    function NoProgressEnhancer() {
        _classCallCheck(this, NoProgressEnhancer);

        return _possibleConstructorReturn(this, (NoProgressEnhancer.__proto__ || Object.getPrototypeOf(NoProgressEnhancer)).apply(this, arguments));
    }

    _createClass(NoProgressEnhancer, [{
        key: 'shouldComponentUpdate',
        value: function shouldComponentUpdate(nextProps, nextState) {
            var _this2 = this;

            return SHOULD_UPDATE_PROPS.reduce(function (shouldUpdate, prop) {
                return shouldUpdate || nextProps[prop] !== _this2.props[prop];
            }, false);
        }
    }, {
        key: 'render',
        value: function render() {
            var _props = this.props,
                TileComponent = _props.TileComponent,
                program = _props.program,
                goal = _props.goal,
                programHasEnded = _props.programHasEnded,
                isObjectives = _props.isObjectives;

            return _react2.default.createElement(TileComponent, {
                tileType: 'goal-overview',
                goal: goal,
                programName: program.programName,
                programLogo: program.programLogo,
                programViewId: program.programViewId,
                programViewStatus: program.programViewStatus,
                programHasEnded: programHasEnded,
                programHeaderImage: program.programHeaderImage,
                endDate: program.endDate,
                startDate: program.startDate,
                isObjectives: isObjectives,
                isManager: this.props.isManager
                /* Goal Label */
                , levelName: (0, _get3.default)(goal, 'paxGoalSelection.name', ''),
                levelDescription: (0, _get3.default)(goal, 'paxGoalSelection.description', ''),
                goalLabel: this.goalLabel
                /* footer*/
                , buttonTextCode: this.buttonTextCode,
                footerMessageCode: this.footerMessageCode,

                daysLeft: this.getDaysLeft,
                calendarLabel: 'progress.daysToEarnLabel',
                lastDayLeftDate: program.endDate
            });
        }
    }, {
        key: 'goalLabel',
        get: function get() {
            return {
                amount: (0, _get3.default)(this.props, 'goal.paxGoalSelection.goalAchieveAmount', 0),
                labelType: (0, _get3.default)(this.props, 'goal.goalLabel.labelType', ''),
                labelText: (0, _get3.default)(this.props, 'goal.goalLabel.labelText', ''),
                lablPosition: (0, _get3.default)(this.props, 'goal.goalLabel.labelPosition', ''),
                curencyCode: (0, _get3.default)(this.props, 'goal.goalLabel.currencyCode', ''),
                currncySymbol: (0, _get3.default)(this.props, 'goal.goalLabel.currencySymbol', ''),
                currenyPosition: (0, _get3.default)(this.props, 'goal.goalLabel.currencyPosition', ''),
                precision: (0, _get3.default)(this.props, 'goal.goalLabel.precision', ''),
                rouningMethod: (0, _get3.default)(this.props, 'goal.goalLabel.roundingMethod', '')
            };
        }
    }, {
        key: 'buttonTextCode',
        get: function get() {
            return KEY_CODES.setyourplan;
        }
    }, {
        key: 'footerMessageCode',
        get: function get() {
            return KEY_CODES.onyourmark;
        }
    }, {
        key: 'getDaysLeft',
        get: function get() {
            return this.props.programNotStarted ? (0, _get3.default)(this.props, 'daysDifference.programStartDate', 0) : (0, _get3.default)(this.props, 'daysDifference.programEndDate', 0);
        }
    }]);

    return NoProgressEnhancer;
}(_react2.default.Component);

NoProgressEnhancer.propTypes = {
    program: _propTypes2.default.shape({
        programName: _propTypes2.default.string.isRequired,
        programLogo: _propTypes2.default.string,
        programType: _propTypes2.default.string,
        programHeaderImage: _propTypes2.default.string,
        programId: _propTypes2.default.number,
        programViewId: _propTypes2.default.string.isRequired,
        programViewStatus: _propTypes2.default.string,
        role: _propTypes2.default.string,
        endDate: _propTypes2.default.string,
        startDate: _propTypes2.default.string,
        goalSelectionToDate: _propTypes2.default.string,
        isOwnerEligibleForAward: _propTypes2.default.bool
    }).isRequired,
    goal: _propTypes2.default.shape({
        paxCount: _propTypes2.default.number,
        onTrackCount: _propTypes2.default.number,
        onTrackPct: _propTypes2.default.number,
        levelDescription: _propTypes2.default.string,
        levelName: _propTypes2.default.string,
        goalLabel: _propTypes2.default.shape({
            labelType: _propTypes2.default.string,
            labelText: _propTypes2.default.string,
            labelPosition: _propTypes2.default.string,
            currencyCode: _propTypes2.default.string,
            currencySymbol: _propTypes2.default.string,
            currencyPosition: _propTypes2.default.string,
            precision: _propTypes2.default.number,
            roundingMethod: _propTypes2.default.string
        })
    }),
    programHasEnded: _propTypes2.default.bool.isRequired,
    programNotStarted: _propTypes2.default.bool,
    isManager: _propTypes2.default.bool,
    isObjectives: _propTypes2.default.bool,
    TileComponent: _propTypes2.default.func.isRequired
};
NoProgressEnhancer.defaultProps = {
    goal: {},
    program: {}
};
exports.default = NoProgressEnhancer;
module.exports = exports['default'];
//# sourceMappingURL=no-progress-enhancer.js.map
