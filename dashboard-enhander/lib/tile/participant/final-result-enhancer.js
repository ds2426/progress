'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _get2 = require('lodash/get');

var _get3 = _interopRequireDefault(_get2);

var _contestsUtils = require('contests-utils');

var _messageCodes = require('../final-result/message-codes');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } // libs


var KEY_CODES = _contestsUtils.constants.KEY_CODES;

var SHOULD_UPDATE_PROPS = ['program', 'goal', 'finalResult', 'progressLoading'];

var FinalResultsEnhancer = function (_React$Component) {
    _inherits(FinalResultsEnhancer, _React$Component);

    function FinalResultsEnhancer() {
        _classCallCheck(this, FinalResultsEnhancer);

        return _possibleConstructorReturn(this, (FinalResultsEnhancer.__proto__ || Object.getPrototypeOf(FinalResultsEnhancer)).apply(this, arguments));
    }

    _createClass(FinalResultsEnhancer, [{
        key: 'shouldComponentUpdate',
        value: function shouldComponentUpdate(nextProps, nextState) {
            var _this2 = this;

            return SHOULD_UPDATE_PROPS.reduce(function (shouldUpdate, prop) {
                return shouldUpdate || nextProps[prop] !== _this2.props[prop];
            }, false);
        }
    }, {
        key: 'render',
        value: function render() {
            var _props = this.props,
                program = _props.program,
                finalResult = _props.finalResult,
                programHeaderImage = _props.programHeaderImage,
                progressLastUpdated = _props.progressLastUpdated,
                progressLastUpdatedCode = _props.progressLastUpdatedCode,
                TileComponent = _props.TileComponent;

            var points = (0, _get3.default)(finalResult, 'totalNumberOfPoints', 0);
            var achievedGoal = (0, _get3.default)(finalResult, 'isGoalAchieved', false);
            var achievedMQ = (0, _get3.default)(finalResult, 'isMQAchieved', null);
            var isMQEnabled = (0, _get3.default)(this.props, 'goal.isMinimumQualifierEnabled');
            var footerMessageCode = this.footerMessageCode;

            return _react2.default.createElement(TileComponent, {
                tileType: 'final-result',
                programName: program.programName,
                programViewId: program.programViewId,
                programLogo: program.programLogo,
                programHeaderImage: programHeaderImage,
                progressLastUpdated: progressLastUpdated,
                progressLastUpdatedCode: progressLastUpdatedCode,

                showSuccess: achievedGoal && isMQEnabled && achievedMQ || achievedGoal && !isMQEnabled,
                pointsEarned: points,
                pointsMediaLabel: this.mediaLabel,

                resultMessageCode: this.resultMessageCode,
                resultSubmessageCode: this.resultSubmessageCode,
                footerMessageCode: footerMessageCode.key ? footerMessageCode.key : footerMessageCode,
                buttonTextCode: KEY_CODES.seeResults,
                replaceOptions: { mediaLabel: this.mediaLabel }
            });
        }
    }, {
        key: 'mediaLabel',
        get: function get() {
            return (0, _get3.default)(this.props.goal, 'mediaLabel', 'Points');
        }
    }, {
        key: 'programType',
        get: function get() {
            return (0, _get3.default)(this.props, 'program.programType', '');
        }
    }, {
        key: 'resultMessageCode',
        get: function get() {
            return (0, _messageCodes.getResultMessageCode)(this.programType, this.props.finalResultCode);
        }
    }, {
        key: 'resultSubmessageCode',
        get: function get() {
            return (0, _messageCodes.getResultSubmessageCode)(this.programType, this.props.finalResultCode);
        }
    }, {
        key: 'footerMessageCode',
        get: function get() {
            return (0, _messageCodes.getFooterMessageCode)(this.programType, this.props.finalResultCode);
        }
    }]);

    return FinalResultsEnhancer;
}(_react2.default.Component);

FinalResultsEnhancer.propTypes = {
    program: _propTypes2.default.shape({
        programName: _propTypes2.default.string.isRequired,
        programType: _propTypes2.default.string.isRequired,
        role: _propTypes2.default.string,
        programViewStatus: _propTypes2.default.string,
        programViewId: _propTypes2.default.string.isRequired,
        goalSelectionToDate: _propTypes2.default.string,
        endDate: _propTypes2.default.string,
        isOwnerEligibleForAward: _propTypes2.default.bool
    }).isRequired,
    programHeaderImage: _propTypes2.default.string,
    progressLastUpdated: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]),
    progressLastUpdatedCode: _propTypes2.default.string,
    goal: _propTypes2.default.shape({
        mediaLabel: _propTypes2.default.string
    }),
    finalResult: _propTypes2.default.shape({
        totalPoints: _propTypes2.default.number,
        paxAchievedCount: _propTypes2.default.number
    }),
    finalResultCode: _propTypes2.default.string.isRequired,
    TileComponent: _propTypes2.default.func.isRequired
};
FinalResultsEnhancer.defaultProps = {};
exports.default = FinalResultsEnhancer;
module.exports = exports['default'];
//# sourceMappingURL=final-result-enhancer.js.map
