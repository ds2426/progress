'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = getFinalResultProps;

var _memoize = require('lodash/memoize');

var _memoize2 = _interopRequireDefault(_memoize);

var _contestsProgressDux = require('contests-progress-dux');

var _contestsUtils = require('contests-utils');

var _messageCodes = require('../final-result/message-codes');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var KEY_CODES = _contestsUtils.constants.KEY_CODES;

var getMediaLabelObj = (0, _memoize2.default)(function (mediaLabel) {
    return { mediaLabel: mediaLabel };
});

function getFinalResultProps(state, program) {
    var programViewId = program.programViewId,
        programType = program.programType;

    var finalResult = _contestsProgressDux.selectors.getProgramViewFinalResults(state, programViewId);
    var finalResultCode = _contestsProgressDux.selectors.getResultCode(state, programViewId);
    var goal = _contestsProgressDux.selectors.getProgramViewGoal(state, programViewId);

    var mediaLabelObj = getMediaLabelObj(goal.mediaLabel || 'Points');

    var achievedGoal = typeof finalResult.acheivedGoal !== 'undefined' ? finalResult.acheivedGoal : false;
    var achievedMQ = typeof finalResult.isMQAchieved !== 'undefined' ? finalResult.isMQAchieved : null;
    var isMQEnabled = goal.isMinimumQualifierEnabled;
    var footerMessageCode = (0, _messageCodes.getFooterMessageCode)(programType, finalResultCode);

    return {
        tileType: 'final-result',
        showSuccess: achievedGoal && isMQEnabled && achievedMQ || achievedGoal && !isMQEnabled,
        pointsEarned: finalResult.totalNumberOfPoints || 0,
        pointsMediaLabel: mediaLabelObj.mediaLabel,

        resultMessageCode: (0, _messageCodes.getResultMessageCode)(programType, finalResultCode),
        resultSubmessageCode: (0, _messageCodes.getResultSubmessageCode)(programType, finalResultCode),
        footerMessageCode: footerMessageCode.key || footerMessageCode,
        buttonTextCode: KEY_CODES.seeResults,
        replaceOptions: mediaLabelObj
    };
}
module.exports = exports['default'];
//# sourceMappingURL=final-result-props.js.map
