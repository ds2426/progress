'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _get2 = require('lodash/get');

var _get3 = _interopRequireDefault(_get2);

var _contestsUtils = require('contests-utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
// shared


var KEY_CODES = _contestsUtils.constants.KEY_CODES;

var SHOULD_UPDATE_PROPS = ['program', 'goal', 'progressLoading'];

var NoGoalSelectedEnhancer = function (_React$Component) {
    _inherits(NoGoalSelectedEnhancer, _React$Component);

    function NoGoalSelectedEnhancer() {
        _classCallCheck(this, NoGoalSelectedEnhancer);

        return _possibleConstructorReturn(this, (NoGoalSelectedEnhancer.__proto__ || Object.getPrototypeOf(NoGoalSelectedEnhancer)).apply(this, arguments));
    }

    _createClass(NoGoalSelectedEnhancer, [{
        key: 'shouldComponentUpdate',
        value: function shouldComponentUpdate(nextProps, nextState) {
            var _this2 = this;

            return SHOULD_UPDATE_PROPS.reduce(function (shouldUpdate, prop) {
                return shouldUpdate || nextProps[prop] !== _this2.props[prop];
            }, false);
        }
    }, {
        key: 'render',
        value: function render() {
            var _props = this.props,
                TileComponent = _props.TileComponent,
                program = _props.program;

            return _react2.default.createElement(TileComponent, {
                tileType: 'no-goal-selected',
                programViewId: program.programViewId,
                programName: program.programName,
                programLogo: program.programLogo,
                programHeaderImage: program.programHeaderImage,
                calendarLabel: '',
                lastDayLeftDate: program.goalSelectionToDate

                /* footer*/
                , buttonTextCode: this.buttonTextCode,
                footerMessageCode: this.footerMessageCode,

                daysLeft: 0
            });
        }
    }, {
        key: 'isGoalSelected',
        get: function get() {
            return (0, _get3.default)(this.props, 'program.isGoalSelected', false);
        }
    }, {
        key: 'buttonTextCode',
        get: function get() {
            return KEY_CODES.viewDetails;
        }
    }, {
        key: 'footerMessageCode',
        get: function get() {
            return '';
        }
    }]);

    return NoGoalSelectedEnhancer;
}(_react2.default.Component);

NoGoalSelectedEnhancer.propTypes = {
    program: _propTypes2.default.shape({
        programName: _propTypes2.default.string.isRequired,
        programViewId: _propTypes2.default.string.isRequired,
        programLogo: _propTypes2.default.string,
        programHeaderImage: _propTypes2.default.string,
        role: _propTypes2.default.string,
        programViewStatus: _propTypes2.default.string,
        goalSelectionToDate: _propTypes2.default.string,
        endDate: _propTypes2.default.string,
        isOwnerEligibleForAward: _propTypes2.default.bool
    }).isRequired,
    TileComponent: _propTypes2.default.func.isRequired
};
NoGoalSelectedEnhancer.defaultProps = {
    goal: {},
    program: {}
};
exports.default = NoGoalSelectedEnhancer;
module.exports = exports['default'];
//# sourceMappingURL=no-goal-selected-enhancer.js.map
