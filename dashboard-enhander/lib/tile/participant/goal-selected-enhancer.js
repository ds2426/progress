'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _get2 = require('lodash/get');

var _get3 = _interopRequireDefault(_get2);

var _is_before = require('date-fns/is_before');

var _is_before2 = _interopRequireDefault(_is_before);

var _parse = require('date-fns/parse');

var _parse2 = _interopRequireDefault(_parse);

var _contestsUtils = require('contests-utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
// shared


var KEY_CODES = _contestsUtils.constants.KEY_CODES;

var SHOULD_UPDATE_PROPS = ['program', 'goal', 'progressLoading'];
var goalPropType = _propTypes2.default.shape({
    payoutTierId: _propTypes2.default.number.isRequired
});

var GoalSelectedEnhancer = function (_React$Component) {
    _inherits(GoalSelectedEnhancer, _React$Component);

    function GoalSelectedEnhancer() {
        _classCallCheck(this, GoalSelectedEnhancer);

        return _possibleConstructorReturn(this, (GoalSelectedEnhancer.__proto__ || Object.getPrototypeOf(GoalSelectedEnhancer)).apply(this, arguments));
    }

    _createClass(GoalSelectedEnhancer, [{
        key: 'shouldComponentUpdate',
        value: function shouldComponentUpdate(nextProps, nextState) {
            var _this2 = this;

            return SHOULD_UPDATE_PROPS.reduce(function (shouldUpdate, prop) {
                return shouldUpdate || nextProps[prop] !== _this2.props[prop];
            }, false);
        }
    }, {
        key: 'render',
        value: function render() {
            var _props = this.props,
                TileComponent = _props.TileComponent,
                program = _props.program;

            return _react2.default.createElement(TileComponent, {
                tileType: 'goal-selected',
                programType: program.programType,
                programName: program.programName,
                programLogo: program.programLogo,
                programViewId: program.programViewId,
                programHeaderImage: program.programHeaderImage,
                programEndDate: program.endDate
                /* footer*/
                , buttonTextCode: this.buttonTextCode,
                footerMessageCode: this.footerMessageCode,
                isObjectives: this.props.isObjectives,
                levelName: this.levelName,
                goalLabel: this.goalLabel
                /*countdown*/
                , daysLeft: this.getDaysLeft,
                calendarLabel: this.calendarLabel,
                lastDayLeftDate: this.lastDayLeftDate
            });
        }
    }, {
        key: 'buttonTextCode',
        get: function get() {
            return this.props.isObjectives ? KEY_CODES.setyourplan : KEY_CODES.changegoal;
        }
    }, {
        key: 'footerMessageCode',
        get: function get() {
            if (this.props.isObjectives) {
                return KEY_CODES.onyourmark;
            }
            var selectedGoalId = (0, _get3.default)(this.props.goal, 'paxGoalSelection.payoutTierId', 0);
            var goalLevels = (0, _get3.default)(this.props.goal, 'goalLevels', []);

            var _goalLevels$filter = goalLevels.filter(function (g) {
                return g.payoutTierId === selectedGoalId;
            }),
                _goalLevels$filter2 = _slicedToArray(_goalLevels$filter, 1),
                selectedGoal = _goalLevels$filter2[0];

            var isHighestGoal = goalLevels.length === goalLevels.indexOf(selectedGoal) + 1;
            return isHighestGoal ? KEY_CODES.waytopush : KEY_CODES.pushyourself;
        }
    }, {
        key: 'endDate',
        get: function get() {
            return (0, _get3.default)(this.props, 'program.endDate', Date.now());
        }
    }, {
        key: 'startDate',
        get: function get() {
            return (0, _get3.default)(this.props, 'program.startDate', Date.now());
        }
    }, {
        key: 'goalSelectionToDate',
        get: function get() {
            return (0, _get3.default)(this.props, 'program.goalSelectionToDate', Date.now());
        }
    }, {
        key: 'programType',
        get: function get() {
            return (0, _get3.default)(this.props, 'program.programType');
        }
    }, {
        key: 'goalLabel',
        get: function get() {
            return {
                amount: (0, _get3.default)(this.props, 'goal.paxGoalSelection.goalAchieveAmount', 0),
                labelType: (0, _get3.default)(this.props, 'goal.goalLabel.labelType', ''),
                labelText: (0, _get3.default)(this.props, 'goal.goalLabel.labelText', ''),
                lablPosition: (0, _get3.default)(this.props, 'goal.goalLabel.labelPosition', ''),
                currencyCode: (0, _get3.default)(this.props, 'goal.goalLabel.currencyCode', ''),
                currencySymbol: (0, _get3.default)(this.props, 'goal.goalLabel.currencySymbol', ''),
                currencyPosition: (0, _get3.default)(this.props, 'goal.goalLabel.currencyPosition', ''),
                precision: (0, _get3.default)(this.props, 'goal.goalLabel.precision', 0),
                rouningMethod: (0, _get3.default)(this.props, 'goal.goalLabel.roundingMethod', '')
            };
        }
    }, {
        key: 'canSelectGoal',
        get: function get() {
            return (0, _is_before2.default)(new Date(), (0, _parse2.default)(this.goalSelectionToDate));
        }
    }, {
        key: 'calendarLabel',
        get: function get() {
            if (this.canSelectGoal) {
                return 'goalSelection.toChangeGoal';
            } else if (!this.props.programHasStarted) {
                return 'progress.daysToProgramStartLabel';
            }
            return 'progress.daysToEarnLabel';
        }
    }, {
        key: 'lastDayLeftDate',
        get: function get() {
            if (this.canSelectGoal) {
                return this.goalSelectionToDate;
            } else if (!this.props.programHasStarted) {
                return this.startDate;
            }
            return this.endDate;
        }
    }, {
        key: 'levelName',
        get: function get() {
            return (0, _get3.default)(this.props, 'goal.paxGoalSelection.name');
        }
    }, {
        key: 'getDaysLeft',
        get: function get() {
            if (this.canSelectGoal) {
                return (0, _get3.default)(this.props, 'daysDifference.goalSelectionToDate');
            } else if (!this.props.programHasStarted) {
                return (0, _get3.default)(this.props, 'daysDifference.programStartDate');
            }
            return (0, _get3.default)(this.props, 'daysDifference.programEndDate');
        }
    }]);

    return GoalSelectedEnhancer;
}(_react2.default.Component);

GoalSelectedEnhancer.propTypes = {
    program: _propTypes2.default.shape({
        programName: _propTypes2.default.string.isRequired,
        programLogo: _propTypes2.default.string,
        programType: _propTypes2.default.string,
        programHeaderImage: _propTypes2.default.string,
        programId: _propTypes2.default.number,
        programViewId: _propTypes2.default.string.isRequired,
        programViewStatus: _propTypes2.default.string,
        role: _propTypes2.default.string,
        endDate: _propTypes2.default.string,
        startDate: _propTypes2.default.string,
        goalSelectionToDate: _propTypes2.default.string,
        isOwnerEligibleForAward: _propTypes2.default.bool
    }).isRequired,
    goal: _propTypes2.default.shape({
        paxGoalSelection: goalPropType,
        goalLevels: _propTypes2.default.arrayOf(goalPropType)
    }),
    isObjectives: _propTypes2.default.bool,
    programHasStarted: _propTypes2.default.bool,
    TileComponent: _propTypes2.default.func.isRequired
};
GoalSelectedEnhancer.defaultProps = {
    program: {}
};
exports.default = GoalSelectedEnhancer;
module.exports = exports['default'];
//# sourceMappingURL=goal-selected-enhancer.js.map
