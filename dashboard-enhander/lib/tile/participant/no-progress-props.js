'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = getNoProgressProps;

var _get = require('lodash/get');

var _get2 = _interopRequireDefault(_get);

var _contestsProgressDux = require('contests-progress-dux');

var _contestsUtils = require('contests-utils');

var _selectors = require('../selectors');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var KEY_CODES = _contestsUtils.constants.KEY_CODES;
function getNoProgressProps(state, program) {
    var programViewId = program.programViewId;

    var goal = _contestsProgressDux.selectors.getProgramViewGoal(state, programViewId);

    var daysDifference = (0, _selectors.getDaysDifference)(program);

    return {
        tileType: 'no-progress',

        /* Goal Label */
        levelName: (0, _get2.default)(goal, 'paxGoalSelection.name', ''),
        levelDescription: (0, _get2.default)(goal, 'paxGoalSelection.description', ''),
        goalLabel: (0, _selectors.getGoalLabelSelector)(programViewId)(state),
        /* footer*/
        buttonTextCode: KEY_CODES.setyourplan,
        footerMessageCode: KEY_CODES.onyourmark,

        daysLeft: daysDifference.programEndDate,
        calendarLabel: 'progress.daysToEarnLabel'
    };
}
module.exports = exports['default'];
//# sourceMappingURL=no-progress-props.js.map
