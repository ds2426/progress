'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = getProgressProps;

var _contestsProgressDux = require('contests-progress-dux');

var _contestsUtils = require('contests-utils');

var _selectors = require('../selectors');

var KEY_CODES = _contestsUtils.constants.KEY_CODES;
function getProgressProps(state, program) {
    var programViewStatus = program.programViewStatus,
        programViewId = program.programViewId;

    var goal = _contestsProgressDux.selectors.getProgramViewGoal(state, programViewId);
    var progress = goal.paxProgressResponse || {};
    var goalSelection = goal.paxGoalSelection || {};

    var programHasEnded = (0, _selectors.getProgramHasEnded)(program);
    var daysDifference = (0, _selectors.getDaysDifference)(program);
    var programCalculationType = goal.programCalculationType || 'standard';
    var statusTreatment = (0, _contestsUtils.getProgressStatusTreatment)(progress.onTrackPercentage || 0, programViewStatus, progress.progressPercentage || 0, programCalculationType, progress.isMinQualifierAchieved);

    return {
        tileType: 'waiting-for-final-result',
        checkingResults: programHasEnded,
        goalLabel: (0, _selectors.getGoalLabelSelector)(programViewId)(state),
        programCalculationType: programCalculationType,

        isReduction: programCalculationType === 'reduction',
        isStandard: programCalculationType === 'standard',
        isAverage: programCalculationType === 'average',

        progressPercent: progress.progressPercentage || 0,
        onTrackPct: progress.onTrackPercentage || 0,
        currentGoalValue: progress.currentGoalValue,
        goalAchieveAmount: goalSelection.goalAchieveAmount,
        dateThroughPercentage: progress.dateThroughPercentage || 0,
        statusTreatment: statusTreatment,
        daysLeft: daysDifference.programEndDate,

        calendarLabel: programHasEnded ? 'dashboard.tile.common.waitingForFinal' : 'progress.daysToEarnLabel',
        footerMessageCode: programHasEnded ? KEY_CODES.checkingResults : statusTreatment.label,
        buttonTextCode: KEY_CODES.viewDetails
    };
}
module.exports = exports['default'];
//# sourceMappingURL=waiting-for-final-results-props.js.map
