'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

exports.default = getGoalSelectedProps;

var _get = require('lodash/get');

var _get2 = _interopRequireDefault(_get);

var _contestsProgressDux = require('contests-progress-dux');

var _contestsUtils = require('contests-utils');

var _is_before = require('date-fns/is_before');

var _is_before2 = _interopRequireDefault(_is_before);

var _parse = require('date-fns/parse');

var _parse2 = _interopRequireDefault(_parse);

var _selectors = require('../selectors');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var KEY_CODES = _contestsUtils.constants.KEY_CODES;

var EMPTY_ARRAY = [];

var canSelectGoal = function canSelectGoal(program) {
    return !(0, _selectors.isObjectives)(program) && (0, _is_before2.default)(new Date(), (0, _parse2.default)(program.goalSelectionToDate));
};
var getDaysLeft = function getDaysLeft(program) {
    var daysDifference = (0, _selectors.getDaysDifference)(program);
    if (canSelectGoal(program)) {
        return daysDifference.goalSelectionToDate;
    } else if (!(0, _selectors.getProgramHasStarted)(program)) {
        return daysDifference.programStartDate;
    }
    return daysDifference.programEndDate;
};
var getCalendarLabel = function getCalendarLabel(program) {
    if (canSelectGoal(program)) {
        return 'goalSelection.toChangeGoal';
    }
    return 'progress.daysToEarnLabel';
};
var getLastDayLeftDate = function getLastDayLeftDate(program) {
    if (canSelectGoal(program)) {
        return program.goalSelectionToDate || Date.now();
    } else if (!(0, _selectors.getProgramHasStarted)(program)) {
        return program.startDate || Date.now();
    }
    return program.endDate || Date.now();
};

function getGoalSelectedProps(state, program) {
    var programViewId = program.programViewId;

    var goal = _contestsProgressDux.selectors.getProgramViewGoal(state, programViewId);

    var selectedGoalId = (0, _get2.default)(goal, 'paxGoalSelection.payoutTierId', 0);
    var goalLevels = goal.goalLevels || EMPTY_ARRAY;

    var _goalLevels$filter = goalLevels.filter(function (g) {
        return g.payoutTierId === selectedGoalId;
    }),
        _goalLevels$filter2 = _slicedToArray(_goalLevels$filter, 1),
        selectedGoal = _goalLevels$filter2[0];

    var isHighestGoal = goalLevels.length === goalLevels.indexOf(selectedGoal) + 1;
    var footerMessageCode = isHighestGoal ? KEY_CODES.waytopush : KEY_CODES.pushyourself;

    return {
        tileType: 'goal-selected',
        /* footer*/
        buttonTextCode: (0, _selectors.isObjectives)(program) ? KEY_CODES.setyourplan : KEY_CODES.changegoal,
        footerMessageCode: footerMessageCode,
        levelName: (0, _get2.default)(goal, 'paxGoalSelection.name'),
        goalLabel: (0, _selectors.getGoalLabelSelector)(programViewId)(state),
        /*countdown*/
        daysLeft: getDaysLeft(program),
        calendarLabel: getCalendarLabel(program),
        lastDayLeftDate: getLastDayLeftDate(program)
    };
}
module.exports = exports['default'];
//# sourceMappingURL=goal-selected-props.js.map
