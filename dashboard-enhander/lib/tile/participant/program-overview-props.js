'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = getProgramOverviewProps;

var _contestsUtils = require('contests-utils');

var _selectors = require('../selectors');

var PVS__NO_PROGRESS = _contestsUtils.constants.PVS__NO_PROGRESS,
    KEY_CODES = _contestsUtils.constants.KEY_CODES;
function getProgramOverviewProps(state, program) {
    var programViewStatus = program.programViewStatus;

    var daysDifference = (0, _selectors.getDaysDifference)(program);
    var programIsObjectives = (0, _selectors.isObjectives)(program);

    return {
        tileType: 'program-overview',
        checkingResults: false,
        calendarLabel: programIsObjectives ? 'progress.daysToProgramStartLabel' : 'goalSelection.toSelectGoal',
        daysLeft: daysDifference[programIsObjectives ? 'programStartDate' : 'goalSelectionToDate'] || 0,
        lastDayLeftDate: (programIsObjectives ? program.startDate : program.goalSelectionToDate) || Date.now(),
        programOverviewCode: programIsObjectives ? KEY_CODES.achieveyourGoalEarnRewards : KEY_CODES.goalrightforyou,
        buttonTextCode: programIsObjectives && !(0, _selectors.getProgramHasStarted)(program) ? KEY_CODES.learnAboutProgram : KEY_CODES.selectagoal,
        footerMessageCode: programViewStatus === PVS__NO_PROGRESS && (0, _selectors.getProgramHasStarted)(program) ? KEY_CODES.onyourmark : KEY_CODES.upforchallenge
    };
}
module.exports = exports['default'];
//# sourceMappingURL=program-overview-props.js.map
