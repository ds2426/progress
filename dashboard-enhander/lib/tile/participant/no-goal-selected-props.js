'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = getNoGoalSelectedProps;

var _contestsUtils = require('contests-utils');

var KEY_CODES = _contestsUtils.constants.KEY_CODES;
function getNoGoalSelectedProps(state, program) {

    return {
        tileType: 'no-goal-selected',
        calendarLabel: '',
        lastDayLeftDate: program.goalSelectionToDate,

        /* footer*/
        buttonTextCode: KEY_CODES.viewDetails,
        footerMessageCode: '',

        daysLeft: 0
    };
}
module.exports = exports['default'];
//# sourceMappingURL=no-goal-selected-props.js.map
