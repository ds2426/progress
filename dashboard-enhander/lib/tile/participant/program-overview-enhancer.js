'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _get2 = require('lodash/get');

var _get3 = _interopRequireDefault(_get2);

var _contestsUtils = require('contests-utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// shared


var KEY_CODES = _contestsUtils.constants.KEY_CODES;


var SHOULD_UPDATE_PROPS = ['program', 'goal', 'progressLoading'];

var ProgramOverviewEnhancer = function (_React$Component) {
    _inherits(ProgramOverviewEnhancer, _React$Component);

    function ProgramOverviewEnhancer() {
        _classCallCheck(this, ProgramOverviewEnhancer);

        return _possibleConstructorReturn(this, (ProgramOverviewEnhancer.__proto__ || Object.getPrototypeOf(ProgramOverviewEnhancer)).apply(this, arguments));
    }

    _createClass(ProgramOverviewEnhancer, [{
        key: 'shouldComponentUpdate',
        value: function shouldComponentUpdate(nextProps, nextState) {
            var _this2 = this;

            return SHOULD_UPDATE_PROPS.reduce(function (shouldUpdate, prop) {
                return shouldUpdate || nextProps[prop] !== _this2.props[prop];
            }, false);
        }
    }, {
        key: 'render',
        value: function render() {
            var _props = this.props,
                TileComponent = _props.TileComponent,
                program = _props.program,
                programHasEnded = _props.programHasEnded,
                programHasStarted = _props.programHasStarted,
                isObjectives = _props.isObjectives;


            return _react2.default.createElement(TileComponent, {
                tileType: 'program-overview',
                programName: program.programName,
                programLogo: program.programLogo,
                programViewId: program.programViewId,
                programHeaderImage: program.programHeaderImage,
                programHasEnded: programHasEnded,
                goalSelectionToDate: program.goalSelectionToDate,
                endDate: program.endDate,
                startDate: program.startDate,
                isObjectives: isObjectives,
                programHasStarted: programHasStarted,
                checkingResults: false,
                daysLeft: this.getDaysLeft,
                calendarLabel: this.calendarLabel,
                lastDayLeftDate: this.getDaysLeftDate,
                programOverviewCode: this.programOverviewCode,
                buttonTextCode: this.buttonTextCode,
                footerMessageCode: this.footerMessageCode
            });
        }
    }, {
        key: 'buttonTextCode',
        get: function get() {
            return this.props.isObjectives ? KEY_CODES.learnAboutProgram : KEY_CODES.selectagoal;
        }
    }, {
        key: 'footerMessageCode',
        get: function get() {
            return this.programViewStatus === _contestsUtils.constants.PVS__NO_PROGRESS && this.propes.programNotStarted ? KEY_CODES.onyourmark : KEY_CODES.upforchallenge;
        }
    }, {
        key: 'getDaysLeft',
        get: function get() {
            var isObjectives = this.props.isObjectives;

            return (0, _get3.default)(this.props, 'daysDifference.' + (isObjectives ? 'programStartDate' : 'goalSelectionToDate'), 0);
        }
    }, {
        key: 'programOverviewCode',
        get: function get() {
            return this.props.isObjectives ? KEY_CODES.achieveyourGoalEarnRewards : KEY_CODES.goalrightforyou;
        }
    }, {
        key: 'calendarLabel',
        get: function get() {
            return this.props.isObjectives ? 'progress.daysToProgramStartLabel' : 'goalSelection.toSelectGoal';
        }
    }, {
        key: 'getDaysLeftDate',
        get: function get() {
            return this.props.isObjectives ? this.props.startDate : (0, _get3.default)(this.props, 'program.goalSelectionToDate', Date.now());
        }
    }]);

    return ProgramOverviewEnhancer;
}(_react2.default.Component);

ProgramOverviewEnhancer.propTypes = {
    program: _propTypes2.default.shape({
        programName: _propTypes2.default.string.isRequired,
        programLogo: _propTypes2.default.string,
        programType: _propTypes2.default.string,
        programHeaderImage: _propTypes2.default.string,
        programId: _propTypes2.default.number,
        programViewId: _propTypes2.default.string.isRequired,
        programViewStatus: _propTypes2.default.string,
        role: _propTypes2.default.string,
        endDate: _propTypes2.default.string,
        startDate: _propTypes2.default.string,
        goalSelectionToDate: _propTypes2.default.string,
        isOwnerEligibleForAward: _propTypes2.default.bool
    }).isRequired,
    isObjectives: _propTypes2.default.bool,
    programHasEnded: _propTypes2.default.bool,
    programHasStarted: _propTypes2.default.bool,
    startDate: _propTypes2.default.string,
    TileComponent: _propTypes2.default.func.isRequired
};
ProgramOverviewEnhancer.defaultProps = {
    goal: {},
    program: {}
};
exports.default = ProgramOverviewEnhancer;
module.exports = exports['default'];
//# sourceMappingURL=program-overview-enhancer.js.map
