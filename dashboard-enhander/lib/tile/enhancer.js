'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.mapStateToProps = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };
// actionCreators


var _reactRedux = require('react-redux');

var _is_before = require('date-fns/is_before');

var _is_before2 = _interopRequireDefault(_is_before);

var _parse = require('date-fns/parse');

var _parse2 = _interopRequireDefault(_parse);

var _contestsProgressDux = require('contests-progress-dux');

var _contestsUtils = require('contests-utils');

var _finalResultProps = require('./manager/final-result-props');

var _finalResultProps2 = _interopRequireDefault(_finalResultProps);

var _goalSelectionProps = require('./manager/goal-selection-props');

var _goalSelectionProps2 = _interopRequireDefault(_goalSelectionProps);

var _noProgressProps = require('./manager/no-progress-props');

var _noProgressProps2 = _interopRequireDefault(_noProgressProps);

var _overviewProps = require('./manager/overview-props');

var _overviewProps2 = _interopRequireDefault(_overviewProps);

var _progressProps = require('./manager/progress-props');

var _progressProps2 = _interopRequireDefault(_progressProps);

var _finalResultProps3 = require('./participant/final-result-props');

var _finalResultProps4 = _interopRequireDefault(_finalResultProps3);

var _programOverviewProps = require('./participant/program-overview-props');

var _programOverviewProps2 = _interopRequireDefault(_programOverviewProps);

var _goalSelectedProps = require('./participant/goal-selected-props');

var _goalSelectedProps2 = _interopRequireDefault(_goalSelectedProps);

var _noGoalSelectedProps = require('./participant/no-goal-selected-props');

var _noGoalSelectedProps2 = _interopRequireDefault(_noGoalSelectedProps);

var _noProgressProps3 = require('./participant/no-progress-props');

var _noProgressProps4 = _interopRequireDefault(_noProgressProps3);

var _progressProps3 = require('./participant/progress-props');

var _progressProps4 = _interopRequireDefault(_progressProps3);

var _waitingForFinalResultsProps = require('./participant/waiting-for-final-results-props');

var _waitingForFinalResultsProps2 = _interopRequireDefault(_waitingForFinalResultsProps);

var _selectors = require('./selectors');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PVS__GOAL_SELECTION = _contestsUtils.constants.PVS__GOAL_SELECTION,
    PVS__NO_PROGRESS = _contestsUtils.constants.PVS__NO_PROGRESS,
    PVS__NO_GOAL_SELECTED = _contestsUtils.constants.PVS__NO_GOAL_SELECTED,
    PVS__GOAL_SELECTED = _contestsUtils.constants.PVS__GOAL_SELECTED,
    PVS__PROGRESS_LOADED_NO_ISSUANCE = _contestsUtils.constants.PVS__PROGRESS_LOADED_NO_ISSUANCE,
    PVS__FINAL_ISSUANCE = _contestsUtils.constants.PVS__FINAL_ISSUANCE;


var getTileProps = function getTileProps(state, program) {
    var now = Date.now();
    var _program$programViewS = program.programViewStatus,
        programViewStatus = _program$programViewS === undefined ? '' : _program$programViewS,
        _program$startDate = program.startDate,
        startDate = _program$startDate === undefined ? now : _program$startDate;


    var managerGoalSelectionStates = [PVS__GOAL_SELECTION, PVS__GOAL_SELECTED];
    if ((0, _selectors.isManager)(program)) {
        if ((0, _selectors.isObjectives)(program) && (programViewStatus === PVS__NO_PROGRESS || (0, _is_before2.default)(now, (0, _parse2.default)(startDate)))) {
            return (0, _overviewProps2.default)(state, program);
        } else if (managerGoalSelectionStates.includes(programViewStatus)) {
            return (0, _goalSelectionProps2.default)(state, program);
        } else if (programViewStatus === PVS__NO_PROGRESS) {
            return (0, _noProgressProps2.default)(state, program);
        } else if (programViewStatus === PVS__FINAL_ISSUANCE) {
            return (0, _finalResultProps2.default)(state, program);
        } else {
            return (0, _progressProps2.default)(state, program);
        }
    }

    switch (programViewStatus) {
        case PVS__GOAL_SELECTION:
            return (0, _programOverviewProps2.default)(state, program);
        case PVS__NO_GOAL_SELECTED:
            return (0, _noGoalSelectedProps2.default)(state, program);
        case PVS__GOAL_SELECTED:
            return (0, _goalSelectedProps2.default)(state, program);
        case PVS__NO_PROGRESS:
            if ((0, _selectors.isObjectives)(program) && (0, _is_before2.default)(now, (0, _parse2.default)(startDate))) {
                return (0, _programOverviewProps2.default)(state, program);
            }
            return (0, _noProgressProps4.default)(state, program);
        case PVS__PROGRESS_LOADED_NO_ISSUANCE:
            return (0, _selectors.getIsCheckingResults)(program) ? (0, _waitingForFinalResultsProps2.default)(state, program) : (0, _progressProps4.default)(state, program);
        case PVS__FINAL_ISSUANCE:
            return (0, _finalResultProps4.default)(state, program);
    }

    // default if nothing is found so that we show *something*
    return (0, _programOverviewProps2.default)(state, program);
};

var mapStateToProps = exports.mapStateToProps = function mapStateToProps(state, _ref) {
    var _ref$program = _ref.program,
        program = _ref$program === undefined ? {} : _ref$program;
    var programViewId = program.programViewId;

    return _extends({
        program: program,
        programType: program.programType,
        programName: program.programName,
        programLogo: program.programLogo,
        programViewId: program.programViewId,
        programViewStatus: program.programViewStatus,
        heroImageUrl: program.heroImageUrl,
        endDate: program.endDate,
        startDate: program.startDate,
        goalSelectionToDate: program.goalSelectionToDate,
        lastDayLeftDate: program.endDate,
        isOwnerEligibleForAward: program.isOwnerEligibleForAward,
        isObjectives: (0, _selectors.isObjectives)(program),
        isManager: (0, _selectors.isManager)(program),

        programHasEnded: (0, _selectors.getProgramHasEnded)(program),
        programHasStarted: (0, _selectors.getProgramHasStarted)(program),
        checkingResults: (0, _selectors.getIsCheckingResults)(program),

        finalResult: _contestsProgressDux.selectors.getProgramViewFinalResults(state, programViewId),
        goal: _contestsProgressDux.selectors.getProgramViewGoal(state, programViewId),
        progressLastUpdated: _contestsProgressDux.selectors.getProgramViewProgressLastUpdated(state, programViewId),
        progressLoading: _contestsProgressDux.selectors.getProgressIsFetching(state, programViewId),
        hasProgressData: Object.keys(_contestsProgressDux.selectors.getProgramViewProgressData(state, programViewId)).length > 0,
        finalResultCode: _contestsProgressDux.selectors.getResultCode(state, programViewId),
        progressLastUpdatedCode: 'programInfoHeader.dataThrough'

    }, getTileProps(state, program));
};

exports.default = (0, _reactRedux.connect)(mapStateToProps);
//# sourceMappingURL=enhancer.js.map
