'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _get2 = require('lodash/get');

var _get3 = _interopRequireDefault(_get2);

var _is_after = require('date-fns/is_after');

var _is_after2 = _interopRequireDefault(_is_after);

var _difference_in_days = require('date-fns/difference_in_days');

var _difference_in_days2 = _interopRequireDefault(_difference_in_days);

var _parse = require('date-fns/parse');

var _parse2 = _interopRequireDefault(_parse);

var _contestsUtils = require('contests-utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } // libs


var KEY_CODES = _contestsUtils.constants.KEY_CODES;

var SHOULD_UPDATE_PROPS = ['program', 'goal', 'finalResults', 'progressLoading'];

var ProgressEnhancer = function (_React$Component) {
    _inherits(ProgressEnhancer, _React$Component);

    function ProgressEnhancer() {
        _classCallCheck(this, ProgressEnhancer);

        return _possibleConstructorReturn(this, (ProgressEnhancer.__proto__ || Object.getPrototypeOf(ProgressEnhancer)).apply(this, arguments));
    }

    _createClass(ProgressEnhancer, [{
        key: 'shouldComponentUpdate',
        value: function shouldComponentUpdate(nextProps, nextState) {
            var _this2 = this;

            return SHOULD_UPDATE_PROPS.reduce(function (shouldUpdate, prop) {
                return shouldUpdate || nextProps[prop] !== _this2.props[prop];
            }, false);
        }
    }, {
        key: 'render',
        value: function render() {
            var _props = this.props,
                program = _props.program,
                programHasEnded = _props.programHasEnded,
                TileComponent = _props.TileComponent,
                programHeaderImage = _props.programHeaderImage,
                progressLastUpdated = _props.progressLastUpdated;

            return _react2.default.createElement(TileComponent, {
                tileType: 'goal-progress',
                programName: program.programName,
                programViewId: program.programViewId,
                programLogo: program.programLogo,
                programHeaderImage: programHeaderImage,
                programHasEnded: programHasEnded,
                progressPercent: this.progressPercentage,
                onTrackPercentage: this.onTrackPercentage,
                dateThroughPercentage: this.dateThroughPercentage,
                progressLastUpdated: progressLastUpdated,
                progressLastUpdatedCode: 'programInfoHeader.dataThrough',
                dataThrough: this.dataThrough,
                statusTreatment: this.getProgressStatusTreatment,
                daysLeft: this.getDaysLeft,
                calendarLabel: this.calendarLabel,
                lastDayLeftDate: this.endDate,
                footerMessageCode: this.footerMessageCode,
                buttonTextCode: KEY_CODES.viewDetails,

                checkingResults: this.checkingResults,
                programCalculationType: this.programCalculationType
            });
        }
    }, {
        key: 'progressLastUpdatedCode',
        get: function get() {
            return 'programInfoHeader.dataThrough';
        }
    }, {
        key: 'onTrackPercentage',
        get: function get() {
            return (0, _get3.default)(this.props, 'goal.paxProgressResponse.onTrackPercentage', 0);
        }
    }, {
        key: 'progressPercentage',
        get: function get() {
            return (0, _get3.default)(this.props, 'goal.paxProgressResponse.progressPercentage', 0);
        }
    }, {
        key: 'dateThroughPercentage',
        get: function get() {
            return (0, _get3.default)(this.props, 'goal.paxProgressResponse.dateThroughPercentage', 0);
        }
    }, {
        key: 'programCalculationType',
        get: function get() {
            return (0, _get3.default)(this.props, 'goal.programCalculationType', '');
        }
    }, {
        key: 'dataThrough',
        get: function get() {
            return (0, _get3.default)(this.props, 'goal.paxProgressResponse.dataThrough');
        }
    }, {
        key: 'endDate',
        get: function get() {
            return (0, _get3.default)(this.props, 'program.endDate', Date.now());
        }
    }, {
        key: 'startDate',
        get: function get() {
            return (0, _get3.default)(this.props, 'program.startDate', Date.now());
        }
    }, {
        key: 'goalSelectionToDate',
        get: function get() {
            return (0, _get3.default)(this.props, 'program.goalSelectionToDate', Date.now());
        }
    }, {
        key: 'programType',
        get: function get() {
            return (0, _get3.default)(this.props, 'program.programType');
        }
    }, {
        key: 'programViewStatus',
        get: function get() {
            return (0, _get3.default)(this.props, 'program.programViewStatus');
        }
    }, {
        key: 'getProgressStatusTreatment',
        get: function get() {
            return (0, _contestsUtils.getProgressStatusTreatment)(this.onTrackPercentage, this.programViewStatus, this.progressPercentage, this.programCalculationType, (0, _get3.default)(this.props, 'finalResult.isMinQualifierAchieved', false));
        }
    }, {
        key: 'getDaysLeft',
        get: function get() {
            return (0, _difference_in_days2.default)(new Date(), (0, _parse2.default)(this.endDate));
        }
    }, {
        key: 'calendarLabel',
        get: function get() {
            return this.checkingResults ? 'dashboard.tile.common.waitingForFinal' : 'progress.daysToEarnLabel';
        }
    }, {
        key: 'footerMessageCode',
        get: function get() {
            if ((0, _is_after2.default)(new Date(), (0, _parse2.default)(this.endDate))) {
                return KEY_CODES.checkingResults;
            } else {
                return this.getProgressStatusTreatment.label;
            }
        }
    }, {
        key: 'checkingResults',
        get: function get() {
            return (0, _is_after2.default)(new Date(), (0, _parse2.default)(this.endDate));
        }
    }]);

    return ProgressEnhancer;
}(_react2.default.Component);

ProgressEnhancer.propTypes = {
    programHasEnded: _propTypes2.default.bool.isRequired,
    programHeaderImage: _propTypes2.default.string,
    progressLastUpdated: _propTypes2.default.number,
    program: _propTypes2.default.shape({
        programName: _propTypes2.default.string.isRequired,
        role: _propTypes2.default.string,
        programViewId: _propTypes2.default.string.isRequired,
        programViewStatus: _propTypes2.default.string,
        goalSelectionToDate: _propTypes2.default.string,
        endDate: _propTypes2.default.string,
        isOwnerEligibleForAward: _propTypes2.default.bool
    }).isRequired,
    TileComponent: _propTypes2.default.func.isRequired
};
ProgressEnhancer.defaultProps = {
    goal: {},
    program: {}
};
exports.default = ProgressEnhancer;
module.exports = exports['default'];
//# sourceMappingURL=progress-enhancer.js.map
