'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = getManagerNoProgressProps;

var _contestsProgressDux = require('contests-progress-dux');

var _selectors = require('../selectors');

var _contestsUtils = require('contests-utils');

var KEY_CODES = _contestsUtils.constants.KEY_CODES;
function getManagerNoProgressProps(state, program) {
    var programViewId = program.programViewId;

    var goal = _contestsProgressDux.selectors.getProgramViewGoal(state, programViewId);
    var daysDifference = (0, _selectors.getDaysDifference)(program);

    var selectionCount = goal.selectionCount || {};
    var count = selectionCount.goalSelectedCount || 0;
    var total = selectionCount.paxCount || 0;
    var percent = selectionCount.goalSelectedPercentage || 0;

    return {
        tileType: 'manager-no-progress',

        total: total,
        count: count,
        percent: percent,
        daysLeft: daysDifference.programEndDate || 0,

        statusCode: KEY_CODES.selectedGoal,
        footerMessageCode: KEY_CODES.timeToMotivate,
        buttonTextCode: KEY_CODES.viewDetails,
        calendarLabel: 'progress.daysToEarnLabel'
    };
}
module.exports = exports['default'];
//# sourceMappingURL=no-progress-props.js.map
