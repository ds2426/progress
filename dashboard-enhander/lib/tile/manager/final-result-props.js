'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = getManagerFinalResultProps;

var _get = require('lodash/get');

var _get2 = _interopRequireDefault(_get);

var _contestsProgressDux = require('contests-progress-dux');

var _contestsUtils = require('contests-utils');

var _selectors = require('../selectors');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var KEY_CODES = _contestsUtils.constants.KEY_CODES;


var EARNED_OVERRIDE_CODES = ['YNN', 'YYN', 'YNN', 'YYN', 'YNY', 'YYY'];

function getManagerFinalResultProps(state, program) {
    var programViewId = program.programViewId;

    var finalResult = _contestsProgressDux.selectors.getProgramViewFinalResults(state, programViewId);
    var finalResultCode = _contestsProgressDux.selectors.getResultCode(state, programViewId);
    var goal = _contestsProgressDux.selectors.getProgramViewGoal(state, programViewId);

    var count = finalResult.totalNumberofPaxWhoAchievedGoal || 0;
    var total = (0, _get2.default)(goal, 'participantOnTrack.paxCount') || 0;

    return {
        tileType: 'manager-final-result',
        total: total,
        count: count,
        percent: (0, _selectors.getPercent)(count, total),
        buttonTextCode: KEY_CODES.seeResults,
        totalNumberOfPoints: finalResult.totalNumberOfPoints || 0,
        mediaLabel: goal.mediaLabel || 'AwardPoints',
        earnedOverride: EARNED_OVERRIDE_CODES.indexOf(finalResultCode) !== -1
    };
}
module.exports = exports['default'];
//# sourceMappingURL=final-result-props.js.map
