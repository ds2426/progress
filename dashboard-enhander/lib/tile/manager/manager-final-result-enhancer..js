'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _get2 = require('lodash/get');

var _get3 = _interopRequireDefault(_get2);

var _is_after = require('date-fns/is_after');

var _is_after2 = _interopRequireDefault(_is_after);

var _parse = require('date-fns/parse');

var _parse2 = _interopRequireDefault(_parse);

var _contestsUtils = require('contests-utils');

var _messageCodes = require('../final-result/message-codes');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } // libs

// shared


var PVS__GOAL_SELECTION = _contestsUtils.constants.PVS__GOAL_SELECTION,
    PVS__GOAL_SELECTED = _contestsUtils.constants.PVS__GOAL_SELECTED,
    PVS__NO_PROGRESS = _contestsUtils.constants.PVS__NO_PROGRESS,
    PVS__PROGRESS_LOADED_NO_ISSUANCE = _contestsUtils.constants.PVS__PROGRESS_LOADED_NO_ISSUANCE,
    PVS__FINAL_ISSUANCE = _contestsUtils.constants.PVS__FINAL_ISSUANCE,
    PVS__NO_GOAL_SELECTED = _contestsUtils.constants.PVS__NO_GOAL_SELECTED,
    PVS__OBJ_OVERVIEW = _contestsUtils.constants.PVS__OBJ_OVERVIEW,
    PVS__WAITING_FOR_PROGRESS = _contestsUtils.constants.PVS__WAITING_FOR_PROGRESS,
    KEY_CODES = _contestsUtils.constants.KEY_CODES;

var SHOULD_UPDATE_PROPS = ['program', 'goal', 'finalResult', 'progressLoading'];
var GOAL_SELECTION_STATES = [PVS__GOAL_SELECTED, PVS__GOAL_SELECTION, PVS__NO_PROGRESS, PVS__NO_GOAL_SELECTED];

var ManagerFinalResultEnhancer = function (_React$Component) {
    _inherits(ManagerFinalResultEnhancer, _React$Component);

    function ManagerFinalResultEnhancer() {
        _classCallCheck(this, ManagerFinalResultEnhancer);

        return _possibleConstructorReturn(this, (ManagerFinalResultEnhancer.__proto__ || Object.getPrototypeOf(ManagerFinalResultEnhancer)).apply(this, arguments));
    }

    _createClass(ManagerFinalResultEnhancer, [{
        key: 'shouldComponentUpdate',
        value: function shouldComponentUpdate(nextProps, nextState) {
            var _this2 = this;

            return SHOULD_UPDATE_PROPS.reduce(function (shouldUpdate, prop) {
                return shouldUpdate || nextProps[prop] !== _this2.props[prop];
            }, false);
        }
    }, {
        key: 'render',
        value: function render() {
            var _props = this.props,
                program = _props.program,
                programHasEnded = _props.programHasEnded,
                programHeaderImage = _props.programHeaderImage,
                TileComponent = _props.TileComponent,
                progressLastUpdated = _props.progressLastUpdated,
                progressLastUpdatedCode = _props.progressLastUpdatedCode;

            return _react2.default.createElement(TileComponent, {
                tileType: 'manager',
                program: program,
                programName: program.programName,
                programViewId: program.programViewId,
                programHasEnded: programHasEnded,
                programLogo: program.programLogo,
                programHeaderImage: programHeaderImage,
                programViewStatus: this.props.programViewStatus,
                isManager: this.isManager,
                isFinalResults: this.props.programViewStatus === PVS__FINAL_ISSUANCE,
                checkingResults: this.programViewStatus === PVS__PROGRESS_LOADED_NO_ISSUANCE && (0, _is_after2.default)(new Date(), (0, _parse2.default)(this.endDate)),
                statusCode: this.statusCode,
                progressLastUpdated: progressLastUpdated,
                progressLastUpdatedCode: progressLastUpdatedCode,
                total: this.paxCount,
                count: this.count,
                percent: this.percent,
                footerMessageCode: this.footerMessageCode,
                buttonTextCode: this.buttonTextCode,
                daysLeft: this.daysLeft,
                calendarLabel: this.calendarLabel,
                lastDayLeftDate: this.endDate,
                resultMessageCode: this.resultMessageCode,
                resultSubmessageCode: this.resultSubmessageCode,
                showFinalResult: this.showFinalResult,
                totalNumberOfPoints: this.totalNumberOfPoints,
                totalNumberofPaxWhoAchievedGoal: this.totalNumberofPaxWhoAchievedGoal,
                mediaLabel: this.mediaLabel
            });
        }
    }, {
        key: 'buttonTextCode',
        get: function get() {
            if (this.props.programViewStatus === PVS__OBJ_OVERVIEW) {
                return KEY_CODES.learnAboutProgram;
            } else if (this.props.programViewStatus === PVS__FINAL_ISSUANCE) {
                return KEY_CODES.seeResults;
            } else {
                return KEY_CODES.viewDetails;
            }
        }
    }, {
        key: 'footerMessageCode',
        get: function get() {
            switch (this.programViewStatus) {
                case PVS__OBJ_OVERVIEW:
                    return KEY_CODES.getReady;
                case PVS__WAITING_FOR_PROGRESS:
                    return KEY_CODES.timeToMotivate;
                case PVS__GOAL_SELECTION:
                case PVS__GOAL_SELECTED:
                    return KEY_CODES.getEveryoneToSelect;
                case PVS__NO_PROGRESS:
                    return KEY_CODES.timeToMotivate;
                case PVS__PROGRESS_LOADED_NO_ISSUANCE:
                    return (0, _is_after2.default)(new Date(), (0, _parse2.default)(this.endDate)) ? KEY_CODES.checkingResults : KEY_CODES.timeToMotivate;
                case PVS__FINAL_ISSUANCE:
                    return '';
            }

            return '';
        }
    }, {
        key: 'showProgress',
        get: function get() {
            return this.props.programViewStatus === PVS__PROGRESS_LOADED_NO_ISSUANCE;
        }
    }, {
        key: 'showFinalResult',
        get: function get() {
            return this.totalNumberOfPoints > 0 && this.isManagerAvailableForAward;
        }
    }, {
        key: 'totalNumberOfPoints',
        get: function get() {
            return (0, _get3.default)(this.props, 'finalResult.totalNumberOfPoints', 0);
        }
    }, {
        key: 'isManagerAvailableForAward',
        get: function get() {
            return (0, _get3.default)(this.props, 'finalResult.isManagerEligibleForAward', false);
        }
    }, {
        key: 'totalNumberofPaxWhoAchievedGoal',
        get: function get() {
            return (0, _get3.default)(this.props, 'finalResult.totalNumberofPaxWhoAchievedGoal', 0);
        }
    }, {
        key: 'paxCount',
        get: function get() {
            var goal = this.props.goal;

            return (0, _get3.default)(goal, 'selectionCount.paxCount');
        }
    }, {
        key: 'isGoalSelectionPeriod',
        get: function get() {
            var program = this.props.program;

            return GOAL_SELECTION_STATES.includes(program.programViewStatus);
        }
    }, {
        key: 'statusCode',
        get: function get() {
            if (this.programViewStatus === PVS__FINAL_ISSUANCE) {
                return KEY_CODES.participantsAchieved;
            }
            return this.isGoalSelectionPeriod ? KEY_CODES.selectedGoal : KEY_CODES.participantsOnTrack;
        }
    }, {
        key: 'count',
        get: function get() {
            var goal = this.props.goal;

            return this.isGoalSelectionPeriod ? (0, _get3.default)(goal, 'selectionCount.goalSelectedCount') : (0, _get3.default)(goal, 'participantOnTrack.onTrackCount');
        }
    }, {
        key: 'percent',
        get: function get() {
            var goal = this.props.goal;

            return this.isGoalSelectionPeriod ? (0, _get3.default)(goal, 'selectionCount.goalSelectedPercentage') : (0, _get3.default)(goal, 'participantOnTrack.onTrackPct');
        }
    }, {
        key: 'endDate',
        get: function get() {
            return (0, _get3.default)(this.props, 'program.endDate', Date.now());
        }
    }, {
        key: 'startDate',
        get: function get() {
            return (0, _get3.default)(this.props, 'program.startDate', Date.now());
        }
    }, {
        key: 'goalSelectionToDate',
        get: function get() {
            return (0, _get3.default)(this.props, 'program.goalSelectionToDate', Date.now());
        }
    }, {
        key: 'programType',
        get: function get() {
            return (0, _get3.default)(this.props, 'program.programType');
        }
    }, {
        key: 'programViewStatus',
        get: function get() {
            return this.props.programViewStatus;
        }
    }, {
        key: 'daysLeft',
        get: function get() {
            return 10; ///asdfasdf
        }
    }, {
        key: 'overviewMessageCode',
        get: function get() {
            return this.programViewStatus === PVS__WAITING_FOR_PROGRESS ? KEY_CODES.waitingForProgress : KEY_CODES.checkoutYourTeamsGoals;
        }
    }, {
        key: 'mediaLabel',
        get: function get() {
            return (0, _get3.default)(this.props, 'goal.mediaLabel', 'AwardPoints');
        }
    }, {
        key: 'resultMessageCode',
        get: function get() {
            return (0, _messageCodes.getResultMessageCode)('manager', this.props.finalResultCode);
        }
    }, {
        key: 'resultSubmessageCode',
        get: function get() {
            return (0, _messageCodes.getResultSubmessageCode)('manager', this.props.finalResultCode);
        }
    }]);

    return ManagerFinalResultEnhancer;
}(_react2.default.Component);

ManagerFinalResultEnhancer.propTypes = {
    programHasEnded: _propTypes2.default.bool.isRequired,
    progressLastUpdated: _propTypes2.default.number,
    progressLastUpdatedCode: _propTypes2.default.string,
    programHeaderImage: _propTypes2.default.string,
    programViewStatus: _propTypes2.default.string,
    program: _propTypes2.default.shape({
        programName: _propTypes2.default.string,
        programViewId: _propTypes2.default.string,
        role: _propTypes2.default.string,
        programViewStatus: _propTypes2.default.string,
        goalSelectionToDate: _propTypes2.default.string,
        isOwnerEligibleForAward: _propTypes2.default.bool
    }).isRequired,
    goal: _propTypes2.default.shape({
        paxCount: _propTypes2.default.number,
        onTrackCount: _propTypes2.default.number,
        onTrackPct: _propTypes2.default.number
    }),
    finalResultCode: _propTypes2.default.string,
    TileComponent: _propTypes2.default.func.isRequired
};
ManagerFinalResultEnhancer.defaultProps = {
    program: {},
    goal: {}
};
exports.default = ManagerFinalResultEnhancer;
module.exports = exports['default'];
//# sourceMappingURL=manager-final-result-enhancer..js.map
