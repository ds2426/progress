'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = getManagerGoalSelectionProps;

var _contestsProgressDux = require('contests-progress-dux');

var _selectors = require('../selectors');

var _contestsUtils = require('contests-utils');

var KEY_CODES = _contestsUtils.constants.KEY_CODES;
function getManagerGoalSelectionProps(state, program) {
    var programViewId = program.programViewId;

    var goal = _contestsProgressDux.selectors.getProgramViewGoal(state, programViewId);
    var hasEnded = (0, _selectors.getProgramHasEnded)(program);
    var daysDifference = (0, _selectors.getDaysDifference)(program);
    var isCheckingResults = (0, _selectors.getIsCheckingResults)(program);

    var participantOnTrack = goal.participantOnTrack || {};
    var count = participantOnTrack.onTrackCount || 0;
    var total = participantOnTrack.paxCount || 0;
    var percent = participantOnTrack.onTrackPct || 0;

    return {
        tileType: 'manager-progress',
        statusCode: KEY_CODES.participantsOnTrack,

        total: total,
        count: count,
        percent: percent,
        footerMessageCode: isCheckingResults ? KEY_CODES.checkingResults : KEY_CODES.timeToMotivate,
        buttonTextCode: KEY_CODES.viewDetails,

        daysLeft: Math.abs(daysDifference.programEndDate || 0),
        calendarLabel: hasEnded ? 'dashboard.tile.common.waitingForFinal' : 'progress.daysToEarnLabel'
    };
}
module.exports = exports['default'];
//# sourceMappingURL=progress-props.js.map
