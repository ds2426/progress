'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = getManagerGoalSelectionProps;

var _contestsProgressDux = require('contests-progress-dux');

var _selectors = require('../selectors');

var _contestsUtils = require('contests-utils');

var KEY_CODES = _contestsUtils.constants.KEY_CODES;
function getManagerGoalSelectionProps(state, program) {
    var programViewId = program.programViewId;

    var goal = _contestsProgressDux.selectors.getProgramViewGoal(state, programViewId);
    var hasStarted = (0, _selectors.getProgramHasStarted)(program);
    var daysDifference = (0, _selectors.getDaysDifference)(program);
    var objectivesNotStarted = !hasStarted && (0, _selectors.isObjectives)(program);
    var objectiveHasStarted = hasStarted && (0, _selectors.isObjectives)(program);

    var selectionCount = goal.selectionCount || {};
    var count = selectionCount.goalSelectedCount || 0;
    var total = selectionCount.paxCount || 0;

    return {
        tileType: 'manager-goal-selection',
        lastDayLeftDate: program.startDate,

        statusCode: hasStarted ? KEY_CODES.participantsOnTrack : KEY_CODES.selectedGoal,

        total: total,
        count: count,
        percent: (0, _selectors.getPercent)(count, total),

        footerMessageCode: hasStarted ? KEY_CODES.getReady : KEY_CODES.getEveryoneToSelect,
        buttonTextCode: objectivesNotStarted ? KEY_CODES.learnAboutProgram : KEY_CODES.viewDetails,

        daysLeft: (objectiveHasStarted ? daysDifference.programEndDate : daysDifference.programStartDate) || 0,
        calendarLabel: 'goalSelection.toSelectGoal'
    };
}
module.exports = exports['default'];
//# sourceMappingURL=goal-selection-props.js.map
