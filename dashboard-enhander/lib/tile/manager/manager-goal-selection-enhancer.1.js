'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _get2 = require('lodash/get');

var _get3 = _interopRequireDefault(_get2);

var _contestsUtils = require('contests-utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } // libs

// shared


var KEY_CODES = _contestsUtils.constants.KEY_CODES;

var SHOULD_UPDATE_PROPS = ['program', 'goal', 'finalResult', 'progressLoading'];

var ManagerGoalSelectionEnhancer = function (_React$Component) {
    _inherits(ManagerGoalSelectionEnhancer, _React$Component);

    function ManagerGoalSelectionEnhancer() {
        _classCallCheck(this, ManagerGoalSelectionEnhancer);

        return _possibleConstructorReturn(this, (ManagerGoalSelectionEnhancer.__proto__ || Object.getPrototypeOf(ManagerGoalSelectionEnhancer)).apply(this, arguments));
    }

    _createClass(ManagerGoalSelectionEnhancer, [{
        key: 'shouldComponentUpdate',
        value: function shouldComponentUpdate(nextProps, nextState) {
            var _this2 = this;

            return SHOULD_UPDATE_PROPS.reduce(function (shouldUpdate, prop) {
                return shouldUpdate || nextProps[prop] !== _this2.props[prop];
            }, false);
        }
    }, {
        key: 'render',
        value: function render() {
            var _props = this.props,
                program = _props.program,
                programHeaderImage = _props.programHeaderImage,
                progressLastUpdated = _props.progressLastUpdated,
                progressLastUpdatedCode = _props.progressLastUpdatedCode,
                TileComponent = _props.TileComponent;

            return _react2.default.createElement(TileComponent, _extends({}, this.props, {
                tileType: 'manager-goal-selection',
                programName: program.programName,
                programViewId: program.programViewId,
                programLogo: program.programLogo,
                programHeaderImage: programHeaderImage,

                statusCode: this.statusCode,

                progressLastUpdated: progressLastUpdated,
                progressLastUpdatedCode: progressLastUpdatedCode,
                total: this.paxCount,
                count: this.count,
                percent: this.percent,

                footerMessageCode: this.footerMessageCode,
                buttonTextCode: this.buttonTextCode,

                daysLeft: this.getDaysLeft,
                calendarLabel: this.calendarLabel,
                lastDayLeftDate: this.props.startDate
            }));
        }
    }, {
        key: 'buttonTextCode',
        get: function get() {
            return this.props.programNotStarted && this.props.isObjectives ? KEY_CODES.learnAboutProgram : KEY_CODES.viewDetails;
        }
    }, {
        key: 'footerMessageCode',
        get: function get() {
            return this.programNotStarted ? KEY_CODES.getReady : KEY_CODES.getEveryoneToSelect;
        }
    }, {
        key: 'statusCode',
        get: function get() {
            return this.props.programNotStarted ? KEY_CODES.selectedGoal : KEY_CODES.participantsOnTrack;
        }
    }, {
        key: 'paxCount',
        get: function get() {
            return (0, _get3.default)(this.props, 'goal.selectionCount.paxCount');
        }
    }, {
        key: 'count',
        get: function get() {
            return (0, _get3.default)(this.props, 'goal.selectionCount.goalSelectedCount');
        }
    }, {
        key: 'percent',
        get: function get() {
            return (0, _get3.default)(this.props, 'goal.selectionCount.goalSelectedPercentage');
        }
    }, {
        key: 'getDaysLeft',
        get: function get() {
            return this.props.programHasStarted && this.props.isObjectives ? (0, _get3.default)(this.props, 'daysDifference.programEndDate', 0) : (0, _get3.default)(this.props, 'daysDifference.programStartDate', 0);
        }
    }, {
        key: 'calendarLabel',
        get: function get() {
            return 'goalSelection.toSelectGoal';
        }
    }]);

    return ManagerGoalSelectionEnhancer;
}(_react2.default.Component);

ManagerGoalSelectionEnhancer.propTypes = {
    progressLastUpdated: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]),
    progressLastUpdatedCode: _propTypes2.default.string,
    programHeaderImage: _propTypes2.default.string,
    program: _propTypes2.default.shape({
        programName: _propTypes2.default.string,
        programViewId: _propTypes2.default.string,
        role: _propTypes2.default.string,
        programViewStatus: _propTypes2.default.string,
        goalSelectionToDate: _propTypes2.default.string,
        isOwnerEligibleForAward: _propTypes2.default.bool
    }).isRequired,
    startDate: _propTypes2.default.string,
    isObjectives: _propTypes2.default.bool,
    programHasStarted: _propTypes2.default.bool,
    programNotStarted: _propTypes2.default.bool,
    TileComponent: _propTypes2.default.func.isRequired
};
ManagerGoalSelectionEnhancer.defaultProps = {
    program: {},
    goal: {}
};
exports.default = ManagerGoalSelectionEnhancer;
module.exports = exports['default'];
//# sourceMappingURL=manager-goal-selection-enhancer.1.js.map
