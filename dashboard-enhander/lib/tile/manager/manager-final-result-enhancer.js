'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _get2 = require('lodash/get');

var _get3 = _interopRequireDefault(_get2);

var _contestsUtils = require('contests-utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } // libs

// shared


var KEY_CODES = _contestsUtils.constants.KEY_CODES;

var SHOULD_UPDATE_PROPS = ['program', 'goal', 'finalResult', 'progressLoading'];
var EARNED_OVERRIDE_CODES = ['YNN', 'YYN', 'YNN', 'YYN', 'YNY', 'YYY'];

var ManagerFinalResultEnhancer = function (_React$Component) {
    _inherits(ManagerFinalResultEnhancer, _React$Component);

    function ManagerFinalResultEnhancer() {
        _classCallCheck(this, ManagerFinalResultEnhancer);

        return _possibleConstructorReturn(this, (ManagerFinalResultEnhancer.__proto__ || Object.getPrototypeOf(ManagerFinalResultEnhancer)).apply(this, arguments));
    }

    _createClass(ManagerFinalResultEnhancer, [{
        key: 'shouldComponentUpdate',
        value: function shouldComponentUpdate(nextProps, nextState) {
            var _this2 = this;

            return SHOULD_UPDATE_PROPS.reduce(function (shouldUpdate, prop) {
                return shouldUpdate || nextProps[prop] !== _this2.props[prop];
            }, false);
        }
    }, {
        key: 'render',
        value: function render() {
            var _props = this.props,
                program = _props.program,
                programHeaderImage = _props.programHeaderImage,
                TileComponent = _props.TileComponent,
                progressLastUpdated = _props.progressLastUpdated,
                progressLastUpdatedCode = _props.progressLastUpdatedCode;

            return _react2.default.createElement(TileComponent, {
                tileType: 'manager-final-result',
                programName: program.programName,
                programViewId: program.programViewId,
                programLogo: program.programLogo,
                programHeaderImage: programHeaderImage,
                progressLastUpdated: progressLastUpdated,
                progressLastUpdatedCode: progressLastUpdatedCode,
                total: this.paxCount,
                count: this.totalNumberofPaxWhoAchievedGoal,
                percent: this.percent,
                buttonTextCode: this.buttonTextCode,
                totalNumberOfPoints: this.totalNumberOfPoints,
                totalNumberofPaxWhoAchievedGoal: this.totalNumberofPaxWhoAchievedGoal,
                mediaLabel: this.mediaLabel,
                earnedOverride: this.earnedOverride
            });
        }
    }, {
        key: 'buttonTextCode',
        get: function get() {
            return KEY_CODES.seeResults;
        }
    }, {
        key: 'totalNumberOfPoints',
        get: function get() {
            return (0, _get3.default)(this.props.finalResult, 'totalNumberOfPoints') || 0;
        }
    }, {
        key: 'totalNumberofPaxWhoAchievedGoal',
        get: function get() {
            return (0, _get3.default)(this.props.finalResult, 'totalNumberofPaxWhoAchievedGoal') || 0;
        }
    }, {
        key: 'mediaLabel',
        get: function get() {
            return (0, _get3.default)(this.props.goal, 'mediaLabel', 'AwardPoints');
        }
    }, {
        key: 'paxCount',
        get: function get() {
            return (0, _get3.default)(this.props.goal, 'participantOnTrack.paxCount') || 0;
        }
    }, {
        key: 'earnedOverride',
        get: function get() {
            return EARNED_OVERRIDE_CODES.indexOf(this.props.finalResultCode) !== -1;
        }
    }, {
        key: 'percent',
        get: function get() {
            var percent = this.totalNumberofPaxWhoAchievedGoal / this.paxCount;
            return isNaN(percent) ? 0 : percent;
        }
    }]);

    return ManagerFinalResultEnhancer;
}(_react2.default.Component);

ManagerFinalResultEnhancer.propTypes = {
    progressLastUpdated: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]),
    progressLastUpdatedCode: _propTypes2.default.string,
    programHeaderImage: _propTypes2.default.string,
    program: _propTypes2.default.shape({
        programName: _propTypes2.default.string,
        programViewId: _propTypes2.default.string,
        role: _propTypes2.default.string,
        isOwnerEligibleForAward: _propTypes2.default.bool
    }).isRequired,
    finalResultCode: _propTypes2.default.string,
    TileComponent: _propTypes2.default.func.isRequired,
    goal: _propTypes2.default.shape({
        mediaLabel: _propTypes2.default.string,
        participantOnTrack: _propTypes2.default.shape({
            paxCount: _propTypes2.default.number
        })
    }),
    finalResult: _propTypes2.default.shape({
        totalNumberOfPoints: _propTypes2.default.number,
        totalNumberofPaxWhoAchievedGoal: _propTypes2.default.number
    })
};
ManagerFinalResultEnhancer.defaultProps = {
    program: {},
    goal: {},
    finalResult: {}
};
exports.default = ManagerFinalResultEnhancer;
module.exports = exports['default'];
//# sourceMappingURL=manager-final-result-enhancer.js.map
