'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = getManagerOverviewProps;

var _selectors = require('../selectors');

var _contestsUtils = require('contests-utils');

var KEY_CODES = _contestsUtils.constants.KEY_CODES;
function getManagerOverviewProps(state, program) {
    var hasStarted = (0, _selectors.getProgramHasStarted)(program);
    var daysDifference = (0, _selectors.getDaysDifference)(program);
    var objectiveHasStarted = hasStarted && (0, _selectors.isObjectives)(program);

    return {
        tileType: 'manager-overview',
        programOverviewCode: hasStarted ? KEY_CODES.waitingForProgress : KEY_CODES.checkoutYourTeamsGoals,

        footerMessageCode: hasStarted ? KEY_CODES.timeToMotivate : KEY_CODES.getReady,
        buttonTextCode: hasStarted ? KEY_CODES.viewDetails : KEY_CODES.learnAboutProgram,

        daysLeft: (objectiveHasStarted ? daysDifference.programEndDate : daysDifference.programStartDate) || 0,
        calendarLabel: objectiveHasStarted ? 'progress.daysToEarnLabel' : 'progress.daysToProgramStartLabel',
        lastDayLeftDate: hasStarted ? program.endDate : program.startDate
    };
}
module.exports = exports['default'];
//# sourceMappingURL=overview-props.js.map
