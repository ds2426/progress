'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getGoalLabelSelector = exports.getPercent = exports.getDaysDifference = exports.getIsWaitingForProgress = exports.getIsCheckingResults = exports.getProgramHasStarted = exports.getProgramHasEnded = exports.isManager = exports.isObjectives = undefined;

var _get = require('lodash/get');

var _get2 = _interopRequireDefault(_get);

var _memoize = require('lodash/memoize');

var _memoize2 = _interopRequireDefault(_memoize);

var _reselect = require('reselect');

var _is_after = require('date-fns/is_after');

var _is_after2 = _interopRequireDefault(_is_after);

var _parse = require('date-fns/parse');

var _parse2 = _interopRequireDefault(_parse);

var _difference_in_days = require('date-fns/difference_in_days');

var _difference_in_days2 = _interopRequireDefault(_difference_in_days);

var _contestsProgressDux = require('contests-progress-dux');

var _contestsUtils = require('contests-utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// actionCreators
var PVS__PROGRESS_LOADED_NO_ISSUANCE = _contestsUtils.constants.PVS__PROGRESS_LOADED_NO_ISSUANCE,
    PVS__NO_PROGRESS = _contestsUtils.constants.PVS__NO_PROGRESS,
    ROLE__SELECTOR = _contestsUtils.constants.ROLE__SELECTOR,
    PT__OBJ = _contestsUtils.constants.PT__OBJ;


var selectorRoleRegex = new RegExp('^' + ROLE__SELECTOR + '$', 'i');

var isObjectives = exports.isObjectives = function isObjectives(_ref) {
    var programType = _ref.programType;
    return programType === PT__OBJ;
};
var isManager = exports.isManager = function isManager(_ref2) {
    var role = _ref2.role;
    return !selectorRoleRegex.test(role);
};
var getProgramHasEnded = exports.getProgramHasEnded = function getProgramHasEnded(program) {
    return (0, _is_after2.default)(new Date(), (0, _parse2.default)(program.endDate || Date.now()));
};
var getProgramHasStarted = exports.getProgramHasStarted = function getProgramHasStarted(program) {
    return (0, _is_after2.default)(new Date(), (0, _parse2.default)(program.startDate || Date.now()));
};
var getIsCheckingResults = exports.getIsCheckingResults = function getIsCheckingResults(program) {
    return program.programViewStatus === PVS__PROGRESS_LOADED_NO_ISSUANCE && getProgramHasEnded(program);
};
var getIsWaitingForProgress = exports.getIsWaitingForProgress = function getIsWaitingForProgress(program) {
    return program.programViewStatus === PVS__NO_PROGRESS && getProgramHasStarted(program);
};
var getDaysDifference = exports.getDaysDifference = function getDaysDifference(program) {
    return {
        goalSelectionToDate: (0, _difference_in_days2.default)((0, _parse2.default)(program.goalSelectionToDate), new Date()) + 1,
        programStartDate: (0, _difference_in_days2.default)((0, _parse2.default)(program.startDate), new Date()) + 1,
        programEndDate: (0, _difference_in_days2.default)((0, _parse2.default)(program.endDate), new Date()) + 1
    };
};

var getPercent = exports.getPercent = function getPercent(count, total) {
    var percent = Math.round(count / total * 100);
    return isNaN(percent) ? 0 : percent;
};

var getGoalLabelSelector = exports.getGoalLabelSelector = (0, _memoize2.default)(function (programViewId) {
    return (0, _reselect.createSelector)(_contestsProgressDux.selectors.getGoalSelector(programViewId), function (goal) {
        return {
            amount: (0, _get2.default)(goal, 'paxGoalSelection.goalAchieveAmount', 0),
            labelType: (0, _get2.default)(goal, 'goalLabel.labelType', ''),
            labelText: (0, _get2.default)(goal, 'goalLabel.labelText', ''),
            labelPosition: (0, _get2.default)(goal, 'goalLabel.labelPosition', ''),
            currencyCode: (0, _get2.default)(goal, 'goalLabel.currencyCode', ''),
            currencySymbol: (0, _get2.default)(goal, 'goalLabel.currencySymbol', ''),
            currencyPosition: (0, _get2.default)(goal, 'goalLabel.currencyPosition', ''),
            precision: (0, _get2.default)(goal, 'goalLabel.precision', 0),
            rouningMethod: (0, _get2.default)(goal, 'goalLabel.roundingMethod', '')
        };
    });
});
//# sourceMappingURL=selectors.js.map
