'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _enhancer = require('./enhancer');

var _enhancer2 = _interopRequireDefault(_enhancer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _enhancer2.default;
module.exports = exports['default'];
//# sourceMappingURL=index.js.map
