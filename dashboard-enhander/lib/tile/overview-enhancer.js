'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _get2 = require('lodash/get');

var _get3 = _interopRequireDefault(_get2);

var _parse = require('date-fns/parse');

var _parse2 = _interopRequireDefault(_parse);

var _is_before = require('date-fns/is_before');

var _is_before2 = _interopRequireDefault(_is_before);

var _contestsUtils = require('contests-utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
// shared


var KEY_CODES = _contestsUtils.constants.KEY_CODES;


var SHOULD_UPDATE_PROPS = ['program', 'goal', 'progressLoading'];

var OverviewEnhancer = function (_React$Component) {
    _inherits(OverviewEnhancer, _React$Component);

    function OverviewEnhancer() {
        _classCallCheck(this, OverviewEnhancer);

        return _possibleConstructorReturn(this, (OverviewEnhancer.__proto__ || Object.getPrototypeOf(OverviewEnhancer)).apply(this, arguments));
    }

    _createClass(OverviewEnhancer, [{
        key: 'shouldComponentUpdate',
        value: function shouldComponentUpdate(nextProps, nextState) {
            var _this2 = this;

            return SHOULD_UPDATE_PROPS.reduce(function (shouldUpdate, prop) {
                return shouldUpdate || nextProps[prop] !== _this2.props[prop];
            }, false);
        }
    }, {
        key: 'render',
        value: function render() {
            var _props = this.props,
                TileComponent = _props.TileComponent,
                program = _props.program,
                programHasEnded = _props.programHasEnded;


            return _react2.default.createElement(TileComponent, {
                tileType: 'goal-overview',
                programName: program.programName,
                programLogo: program.programLogo,
                programViewId: program.programViewId,
                programHeaderImage: program.programHeaderImage,
                programHasEnded: programHasEnded,
                goalSelectionToDate: program.goalSelectionToDate,
                endDate: program.endDate,
                startDate: program.startDate,
                programType: program.programType,
                checkingResults: false,
                daysLeft: this.getDaysLeft,
                calendarLabel: this.calendarLabel,
                lastDayLeftDate: this.lastDayLeftDate,
                overviewMessageCode: this.overviewMessageCode,
                buttonTextCode: this.buttonTextCode,
                footerMessageCode: this.footerMessageCode
            });
        }
    }, {
        key: 'buttonTextCode',
        get: function get() {
            return this.props.isManager ? this.managerButtonCode : KEY_CODES.selectagoal;
        }
    }, {
        key: 'footerMessageCode',
        get: function get() {
            return this.props.isManager ? this.managerFooterCode : this.footerCode;
        }
    }, {
        key: 'getDaysLeft',
        get: function get() {
            return (0, _get3.default)(this.props, 'daysDifference.programStartDate', 0);
        }
    }, {
        key: 'calendarLabel',
        get: function get() {
            return this.waitingForProgress ? 'progress.daysToEarnLabel' : 'progress.daysToProgramStartLabel';
        }
    }, {
        key: 'lastDayLeftDate',
        get: function get() {
            return this.waitingForProgress ? this.props.endDate : this.props.startDate;
        }
    }, {
        key: 'overviewMessageCode',
        get: function get() {
            return this.props.isManager ? this.overviewManagerCode : this.overViewCode;
        }
    }, {
        key: 'waitingForProgress',
        get: function get() {
            var _props2 = this.props,
                program = _props2.program,
                isManager = _props2.isManager,
                programHasStarted = _props2.programHasStarted;

            return isManager && program.programViewStatus === _contestsUtils.constants.PVS__NO_PROGRESS && programHasStarted;
        }
    }, {
        key: 'overviewManagerCode',
        get: function get() {
            return this.waitingForProgress ? KEY_CODES.waitingForProgress : KEY_CODES.checkoutYourTeamsGoals;
        }
    }, {
        key: 'overViewCode',
        get: function get() {
            return this.props.isObjectives ? KEY_CODES.achieveyourGoalEarnRewards : KEY_CODES.goalrightforyou;
        }
    }, {
        key: 'managerButtonCode',
        get: function get() {
            return this.props.programHasStarted ? KEY_CODES.viewDetails : KEY_CODES.learnAboutProgram;
        }
    }, {
        key: 'managerFooterCode',
        get: function get() {
            return this.waitingForProgress ? KEY_CODES.timeToMotivate : KEY_CODES.getReady;
        }
    }, {
        key: 'footerCode',
        get: function get() {
            return this.programViewStatus === _contestsUtils.constants.PVS__NO_PROGRESS && (0, _is_before2.default)(new Date(), (0, _parse2.default)(this.props.startDate)) ? KEY_CODES.onyourmark : KEY_CODES.upforchallenge;
        }
    }]);

    return OverviewEnhancer;
}(_react2.default.Component);

OverviewEnhancer.propTypes = {
    program: _propTypes2.default.shape({
        programName: _propTypes2.default.string.isRequired,
        programLogo: _propTypes2.default.string,
        programType: _propTypes2.default.string,
        programHeaderImage: _propTypes2.default.string,
        programId: _propTypes2.default.number,
        programViewId: _propTypes2.default.string.isRequired,
        programViewStatus: _propTypes2.default.string,
        role: _propTypes2.default.string,
        endDate: _propTypes2.default.string,
        startDate: _propTypes2.default.string,
        goalSelectionToDate: _propTypes2.default.string,
        isOwnerEligibleForAward: _propTypes2.default.bool
    }).isRequired,
    isManager: _propTypes2.default.bool,
    isObjectives: _propTypes2.default.bool,
    programHasEnded: _propTypes2.default.bool,
    programHasStarted: _propTypes2.default.bool,
    startDate: _propTypes2.default.string,
    endDate: _propTypes2.default.string,
    TileComponent: _propTypes2.default.func.isRequired
};
OverviewEnhancer.defaultProps = {
    goal: {},
    program: {}
};
exports.default = OverviewEnhancer;
module.exports = exports['default'];
//# sourceMappingURL=overview-enhancer.js.map
