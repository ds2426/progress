'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getStatusClassNameAndLabel = undefined;

var _contestsUtils = require('contests-utils');

var PVS__FINAL_ISSUANCE = _contestsUtils.constants.PVS__FINAL_ISSUANCE;
var getStatusClassNameAndLabel = exports.getStatusClassNameAndLabel = function getStatusClassNameAndLabel(onTrackPercentage, programViewStatus, progressPercentage, programCalculationType, isMinQualifierAchieved) {
    switch (true) {
        case onTrackPercentage < 60 && programViewStatus !== PVS__FINAL_ISSUANCE:
            return { className: 'get-going', label: 'managerProgress.chart.xAxisLabel.reqSeq1.label' };
        case onTrackPercentage > 59 && onTrackPercentage < 80 && programViewStatus !== PVS__FINAL_ISSUANCE:
            return { className: 'stay-focused', label: 'managerProgress.chart.xAxisLabel.reqSeq2.label' };
        case onTrackPercentage > 79 && onTrackPercentage < 100 && programViewStatus !== PVS__FINAL_ISSUANCE:
            return { className: 'sorta', label: 'managerProgress.chart.xAxisLabel.reqSeq3.label' };
        case onTrackPercentage > 99 && progressPercentage < 100 && programViewStatus !== PVS__FINAL_ISSUANCE:
        case onTrackPercentage > 99 && progressPercentage > 99 && programViewStatus !== PVS__FINAL_ISSUANCE && programCalculationType === 'average':
        case programViewStatus !== PVS__FINAL_ISSUANCE && programCalculationType !== 'average' && isMinQualifierAchieved === false:
            return { className: 'on-track', label: 'managerProgress.chart.xAxisLabel.reqSeq1.label' };
        case onTrackPercentage > 99 && progressPercentage > 99 && programViewStatus !== PVS__FINAL_ISSUANCE && programCalculationType !== 'average':
            return { className: 'made-it', label: 'Made It!' };
        default:
            return {};
    }
};
//# sourceMappingURL=utils.js.map
