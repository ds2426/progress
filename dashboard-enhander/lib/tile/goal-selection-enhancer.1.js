'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _get2 = require('lodash/get');

var _get3 = _interopRequireDefault(_get2);

var _contestsUtils = require('contests-utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
// shared


var PVS__GOAL_SELECTED = _contestsUtils.constants.PVS__GOAL_SELECTED,
    PVS__NO_GOAL_SELECTED = _contestsUtils.constants.PVS__NO_GOAL_SELECTED,
    PVS__NO_PROGRESS = _contestsUtils.constants.PVS__NO_PROGRESS,
    PVS__WAITING_FOR_PROGRESS = _contestsUtils.constants.PVS__WAITING_FOR_PROGRESS,
    KEY_CODES = _contestsUtils.constants.KEY_CODES,
    PT__OBJ = _contestsUtils.constants.PT__OBJ;

var SHOULD_UPDATE_PROPS = ['program', 'goal', 'progressLoading'];

var GoalSelectionEnhancer = function (_React$Component) {
    _inherits(GoalSelectionEnhancer, _React$Component);

    function GoalSelectionEnhancer() {
        _classCallCheck(this, GoalSelectionEnhancer);

        return _possibleConstructorReturn(this, (GoalSelectionEnhancer.__proto__ || Object.getPrototypeOf(GoalSelectionEnhancer)).apply(this, arguments));
    }

    _createClass(GoalSelectionEnhancer, [{
        key: 'shouldComponentUpdate',
        value: function shouldComponentUpdate(nextProps, nextState) {
            var _this2 = this;

            return SHOULD_UPDATE_PROPS.reduce(function (shouldUpdate, prop) {
                return shouldUpdate || nextProps[prop] !== _this2.props[prop];
            }, false);
        }
    }, {
        key: 'render',
        value: function render() {
            var _props = this.props,
                TileComponent = _props.TileComponent,
                program = _props.program,
                goal = _props.goal,
                programViewStatus = _props.programViewStatus,
                goalSelectionOver = _props.goalSelectionOver,
                programHasEnded = _props.programHasEnded,
                programHeaderImage = _props.programHeaderImage;

            return _react2.default.createElement(TileComponent, {
                tileType: 'goal-selection',
                goal: goal,
                programType: program.programType,
                programName: program.programName,
                programLogo: program.programLogo,
                programViewId: program.programViewId,
                programViewStatus: programViewStatus,
                programHasEnded: programHasEnded,
                programHeaderImage: programHeaderImage,
                programEndDate: program.endDate,
                goalSelectionOver: goalSelectionOver,
                goalAchieveAmount: (0, _get3.default)(goal, 'paxGoalSelection.goalAchieveAmount', 0),
                isGoalSelected: program.isGoalSelected,
                isObjectives: program.programType === PT__OBJ,
                isManager: this.props.isManager
                /* Goal Label */
                , mediaLabel: (0, _get3.default)(goal, 'mediaLabel', ''),
                levelName: (0, _get3.default)(goal, 'paxGoalSelection.name', ''),
                levelDescription: (0, _get3.default)(goal, 'paxGoalSelection.description', ''),
                labelType: (0, _get3.default)(goal, 'goalLabel.labelType', ''),
                labelText: (0, _get3.default)(goal, 'goalLabel.labelText', ''),
                labelPosition: (0, _get3.default)(goal, 'goalLabel.labelPosition', ''),
                currencyCode: (0, _get3.default)(goal, 'goalLabel.currencyCode', ''),
                currencySymbol: (0, _get3.default)(goal, 'goalLabel.currencySymbol', ''),
                currencyPosition: (0, _get3.default)(goal, 'goalLabel.currencyPosition', ''),
                precision: (0, _get3.default)(goal, 'goalLabel.precision', ''),
                roundingMethod: (0, _get3.default)(goal, 'goalLabel.roundingMethod', '')
                /* footer*/
                , buttonTextCode: this.buttonTextCode,
                footerMessageCode: this.footerMessageCode,

                daysLeftTreatment: this.getDaysLeftTreatment
            });
        }
    }, {
        key: 'isGoalSelected',
        get: function get() {
            return (0, _get3.default)(this.props, 'program.isGoalSelected', false);
        }
    }, {
        key: 'buttonTextCode',
        get: function get() {
            switch (this.props.programViewStatus) {
                case PVS__NO_GOAL_SELECTED:
                    return KEY_CODES.viewDetails;
                case PVS__GOAL_SELECTED:
                    return KEY_CODES.changegoal;
                case PVS__NO_PROGRESS:
                    return KEY_CODES.setyourplan;
                default:
                    return KEY_CODES.selectagoal;
            }
        }
    }, {
        key: 'footerMessageCode',
        get: function get() {
            switch (this.props.programViewStatus) {
                case PVS__NO_GOAL_SELECTED:
                    return '';
                case PVS__GOAL_SELECTED:
                    return KEY_CODES.pushyourself;
                case PVS__WAITING_FOR_PROGRESS:
                case PVS__NO_PROGRESS:
                    return KEY_CODES.onyourmark;
                default:
                    return KEY_CODES.upforchallenge;
            }
        }
    }, {
        key: 'endDate',
        get: function get() {
            return (0, _get3.default)(this.props, 'program.endDate', Date.now());
        }
    }, {
        key: 'startDate',
        get: function get() {
            return (0, _get3.default)(this.props, 'program.startDate', Date.now());
        }
    }, {
        key: 'goalSelectionToDate',
        get: function get() {
            return (0, _get3.default)(this.props, 'program.goalSelectionToDate', Date.now());
        }
    }, {
        key: 'programType',
        get: function get() {
            return (0, _get3.default)(this.props, 'program.programType');
        }
    }, {
        key: 'programViewStatus',
        get: function get() {
            var programViewStatus = (0, _get3.default)(this.props, 'program.programViewStatus');
            return (0, _contestsUtils.getlabelsProgramViewStatus)(programViewStatus, this.programType, this.startDate);
        }
    }, {
        key: 'getDaysLeftTreatment',
        get: function get() {
            return (0, _contestsUtils.getDaysLeftTreatment)(this.endDate, this.startDate, this.goalSelectionToDate, this.programViewStatus, this.programType, false);
        }
    }]);

    return GoalSelectionEnhancer;
}(_react2.default.Component);

GoalSelectionEnhancer.propTypes = {
    program: _propTypes2.default.shape({
        programName: _propTypes2.default.string.isRequired,
        role: _propTypes2.default.string,
        programViewStatus: _propTypes2.default.string,
        goalSelectionToDate: _propTypes2.default.string,
        endDate: _propTypes2.default.string,
        isOwnerEligibleForAward: _propTypes2.default.bool
    }).isRequired,
    goal: _propTypes2.default.shape({
        paxCount: _propTypes2.default.number,
        onTrackCount: _propTypes2.default.number,
        onTrackPct: _propTypes2.default.number,
        levelDescription: _propTypes2.default.string,
        levelName: _propTypes2.default.string,
        goalLabel: _propTypes2.default.shape({
            labelType: _propTypes2.default.string,
            labelText: _propTypes2.default.string,
            labelPosition: _propTypes2.default.string,
            currencyCode: _propTypes2.default.string,
            currencySymbol: _propTypes2.default.string,
            currencyPosition: _propTypes2.default.string,
            precision: _propTypes2.default.number,
            roundingMethod: _propTypes2.default.string
        })
    }),
    programHasEnded: _propTypes2.default.bool.isRequired,
    programHeaderImage: _propTypes2.default.string,
    programViewStatus: _propTypes2.default.string,
    goalSelectionOver: _propTypes2.default.bool,
    isManager: _propTypes2.default.bool,
    TileComponent: _propTypes2.default.func.isRequired
};
GoalSelectionEnhancer.defaultProps = {
    goal: {},
    program: {}
};
exports.default = GoalSelectionEnhancer;
module.exports = exports['default'];
//# sourceMappingURL=goal-selection-enhancer.1.js.map
