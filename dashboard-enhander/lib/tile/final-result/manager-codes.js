'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getResultMessageCode = getResultMessageCode;
exports.getResultSubmessageCode = getResultSubmessageCode;
function getResultMessageCode(resultCode) {
    switch (resultCode) {
        case 'YYN':
        case 'YYY':
        case 'YNY':
        case 'YNN':
            return {
                key: 'finalResults.selector.dashboard.message.earned',
                cssClass: 'you-earned'
            };
        case 'YXN':
        case 'YXY':
            return {
                key: 'finalResults.manager.noAwardSubhead',
                cssClass: 'did-not-earn'
            };
        default:
            return {
                key: ''
            };
    }
}

function getResultSubmessageCode(resultCode) {
    switch (resultCode) {
        case 'YYN':
        case 'YNN':
        case 'YYY':
        case 'YNY':
            return {
                key: 'dashboard.tile.common.participantsAchieved',
                cssClass: 'sub-message-green'
            };

        default:
            return {
                key: ''
            };
    }
}

// key: 'finalResults.selector.dashboard.message.earned',
// key: 'dashboard.tile.common.participantsAchieved',
//# sourceMappingURL=manager-codes.js.map
