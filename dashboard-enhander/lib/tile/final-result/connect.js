'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.mapStateToProps = undefined;

var _reactRedux = require('react-redux');

var _get = require('lodash/get');

var _get2 = _interopRequireDefault(_get);

var _contestsProgressDux = require('contests-progress-dux');

var _contestsUtils = require('contests-utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// actionCreators
var mapStateToProps = exports.mapStateToProps = function mapStateToProps(state, ownProps) {
    var programViewId = (0, _get2.default)(ownProps, 'program.programViewId');
    var finalResultCode = (0, _get2.default)(ownProps, 'program.programViewStatus') === _contestsUtils.constants.PVS__FINAL_ISSUANCE ? _contestsProgressDux.selectors.getResultCode(state, programViewId) : null;
    return {
        finalResultCode: finalResultCode,
        finalResult: _contestsProgressDux.selectors.getProgramViewFinalResults(state, programViewId),
        goal: _contestsProgressDux.selectors.getProgramViewGoal(state, programViewId),
        progressLastUpdated: _contestsProgressDux.selectors.getProgramViewProgressLastUpdated(state, programViewId),
        progressLoading: _contestsProgressDux.selectors.getBusyLoadingProgramView(state, programViewId)
    };
};

exports.default = (0, _reactRedux.connect)(mapStateToProps);
//# sourceMappingURL=connect.js.map
