'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getResultMessageCode = getResultMessageCode;
exports.getResultSubmessageCode = getResultSubmessageCode;
exports.getFooterMessageCode = getFooterMessageCode;

var _constants = require('./constants');

var MESSAGE_EARNED = { key: 'finalResults.selector.dashboard.message.earned', cssClass: 'you-earned' };
var MESSAGE_NOT_EARNED = { key: 'finalResults.selector.dashboard.message.goalNotAchieved', cssClass: 'did-not-earn' };
function getResultMessageCode(resultCode) {
    switch (resultCode) {
        case 'YYXX':
        case 'YYYN':
        case 'YYYY':
        case 'YYNY':
        case 'YYNN':
            return MESSAGE_EARNED;
        case 'NYXX':
        case 'NYNN':
        case 'NYNY':
        case 'NYYY':
        case 'NYYN':
        case 'NNYN':
        case 'NNXX':
        case 'NNNN':
        case 'NNNY':
        case 'NNYY':
            return MESSAGE_NOT_EARNED;
        default:
            return _constants.NO_MESSAGE_CODE;
    }
}

var SUB_NOT_ACHIEVE = { key: 'finalResults.selector.dashboard.subMessage.notAchieveGoal', cssClass: 'sub-message-black' };
var SUB_EARN_POINTS = { key: 'finalResults.selector.dashboard.subMessage.notAchieveGoalButGotPoints', cssClass: 'sub-message-green' };
var SUB_EARN_BASE = { key: 'finalResults.selector.dashboard.subMessage.missGoalEarnBase', cssClass: 'sub-message-green' };
function getResultSubmessageCode(resultCode) {
    switch (resultCode) {
        case 'NNYN':
        case 'NNYY':
        case 'NNNY':
        case 'NNNN':
        case 'NNXX':
            return SUB_NOT_ACHIEVE;
        case 'NYNN':
        case 'NYXX':
        case 'NYNY':
        case 'NYYY':
            return SUB_EARN_POINTS;
        case 'NYYN':
            return SUB_EARN_BASE;
        default:
            return _constants.NO_MESSAGE_CODE;
    }
}

function getFooterMessageCode(resultCode) {
    switch (resultCode) {
        case 'NYYN':
        case 'NYYY':
        case 'NYNY':
        case 'NYNN':
        case 'NYXX':
            return 'finalResults.selector.dashboard.message.goodEffort';

        case 'NNYN':
            return 'finalResults.selector.dashboard.message.bonusSuccess';

        case 'NNYY':
        case 'NNNY':
        case 'NNNN':
        case 'NNXX':
            return 'finalResults.selector.dashboard.message.betterLuck';

        case 'YYNY':
        case 'YYNN':
        case 'YYYY':
        case 'YYYN':
        case 'YYXX':
            return 'finalResults.selector.dashboard.message.goalSuccess';

        default:
            return _constants.NO_MESSAGE_CODE;
    }
}
//# sourceMappingURL=challengepoint-messages.js.map
