'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getFinalResultTextKey = getFinalResultTextKey;

var _goalquestMessages = require('./goalquest-messages');

var codes = _interopRequireWildcard(_goalquestMessages);

var _contestsProgressDux = require('contests-progress-dux');

var _finalResultEnhancer = require('./final-result-enhancer');

var _finalResultEnhancer2 = _interopRequireDefault(_finalResultEnhancer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

exports.default = _finalResultEnhancer2.default;
function getFinalResultTextKey(state, resultCode) {
    var resultType = _contestsProgressDux.selectors.getResultType(state);
    var messageCode = '';
    switch (resultType) {
        case 'goalquest':
            messageCode = codes.getFooterMessageCode(resultCode);
            break;
    }
    return messageCode;
}
//# sourceMappingURL=index.js.map
