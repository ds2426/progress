'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getResultMessageCode = getResultMessageCode;
exports.getResultSubmessageCode = getResultSubmessageCode;
exports.getFooterMessageCode = getFooterMessageCode;

var _constants = require('./constants');

var MESSAGE_EARNED = { key: 'finalResults.selector.dashboard.message.earned', cssClass: 'you-earned' };
var MESSAGE_NOT_EARNED = { key: 'finalResults.selector.dashboard.message.goalNotAchieved', cssClass: 'did-not-earn' };
var MESSAGE_MISSED_MQ = { key: 'finalResults.selector.dashboard.message.missedMQ', cssClass: 'did-not-earn' };
function getResultMessageCode(resultCode) {
    switch (resultCode) {
        case 'YXXX': // Achieved Goal
        case 'YYXX': // Achieved Goal, Met MQ
        case 'YXYY': // Achieved Goal, Achieved Bonus
        case 'YXYN': // Achieved Goal, Achieved Bonus  2
        case 'YXNY': // Achieved Goal, Did not Achieve Bonus
        case 'YXNN': // Achieved Goal, Did not Achieve Bonus  2
        case 'YYYN': // Achieved Goal, Met MQ, Achieved Bonus
        case 'YYYY': // Achieved Goal, Met MQ, Achieved Bonus  2
        case 'YNYN': // Achieved Goal, Missed MQ, Achieved Bonus (bonus not coupled with goal)
        case 'YYNY': // Achieved Goal, Met MQ, Did not achieve Bonus
        case 'YYNN': // Achieved Goal, Met MQ, Did not achieve Bonus 2
        case 'NXYN': // Did not achieve Goal, Achieved Bonus (bonus not coupled with goal)
        case 'NYYN': // Did Not Achieve Goal, Met MQ, Achieved Bonus (bonus not coupled with goal)
        case 'NNYN':
            // Did Not Achieve Goal, Missed MQ, Achieved Bonus (bonus not coupled with goal)
            return MESSAGE_EARNED;
        case 'NXXX': // Goal Not Achieved
        case 'NNXX': // Did not achieve Goal, Missed MQ
        case 'NYXX': // Did not achieve Goal, Missed MQ
        case 'NYYY': // Did Not Achieve Goal, Met MQ, Achieved Bonus (bonus coupled with goal) does not earn
        case 'NNYY': // Did Not Achieve Goal, Missed MQ, Achieved Bonus (bonus coupled with goal) does not earn
        case 'NYNN': // Did Not Achieve Goal, Met MQ, Did Not Achieve Bonus
        case 'NYNY': // Did Not Achieve Goal, Met MQ, Did Not Achieve Bonus 2
        case 'NNNY':
        case 'NNNN':
        case 'NXNN':
        case 'NXNY':
        case 'NXYY':
            return MESSAGE_NOT_EARNED;
        case 'YNXX': // Achieved Goal, Missed MQ
        case 'YNYY': // Achieved Goal, Missed MQ, Achieved Bonus (bonus coupled with goal)does not earn
        case 'YNNN': // Achieved Goal, Missed MQ, Did not Achieve Bonus
        case 'YNNY':
            // Achieved Goal, Missed MQ, Did not Achieve Bonus 2
            return MESSAGE_MISSED_MQ;
        default:
            return _constants.NO_MESSAGE_CODE;
    }
}

var SUB_NOT_ACHIEVED = { key: 'finalResults.selector.dashboard.subMessage.notAchieveGoal', cssClass: 'sub-message-black' };
var SUB_ACHIEVE_GOAL_MISS_MQ = { key: 'finalResults.selector.dashboard.subMessage.achieveGoalMissMQ', cssClass: 'sub-message-black' };
var SUB_MISS_GOAL_EARN_BONUS = { key: 'finalResults.selector.dashboard.subMessage.missGoalEarnBonus', cssClass: 'sub-message-green' };
var SUB_MISS_MQ_EARN_BONUS = { key: 'finalResults.selector.dashboard.subMessage.missMQEarnBonus', cssClass: 'sub-message-green' };
var SUB_MISS_GOAL_MISS_BONUS = { key: 'finalResults.selector.dashboard.subMessage.missGoalMissBonus', cssClass: 'sub-message-black' };
function getResultSubmessageCode(resultCode) {
    switch (resultCode) {
        case 'NXXX': // Goal Not Achieved
        case 'NNXX': // Did not Achieve Goal, Missed MQ 1
        case 'NYXX': // Did not Achieve Goal, Missed MQ 2
        case 'NYYY': // Did Not Achieve Goal, Met MQ, Achieved Bonus (bonus coupled with goal) does not earn
        case 'NNYY': // Did Not Achieve Goal, Missed MQ, Achieved Bonus (bonus coupled with goal) does not earn
        case 'NXYY': // Did not achieve Goal, Achieved Bonus (bonus coupled with goal) does not earn
        case 'NNNN': // Did Not Achieve Goal, Missed MQ, Did Not Achieve Bonus
        case 'NNNY': //Did Not Achieve Goal, Missed MQ, Did Not Achieve Bonus 2
        case 'NYNY':
        case 'NYNN':
            return SUB_NOT_ACHIEVED;
        case 'YNXX': // Achieved Goal, Missed MQ
        case 'YNNN': // Achieved Goal, Missed MQ, Did not Achieve Bonus
        case 'YNNY': // Achieved Goal, Missed MQ, Did not Achieve Bonus 2
        case 'YNYY':
            // Achieved Goal, Missed MQ, Achieved Bonus (bonus coupled with goal)does not earn
            return SUB_ACHIEVE_GOAL_MISS_MQ;
        case 'NXYN': // Did not achieve Goal, Achieved Bonus (bonus not coupled with goal)
        case 'NYYN':
        case 'NNYN':
            // Did Not Achieve Goal, Missed MQ, Achieved Bonus (bonus not coupled with goal)
            return SUB_MISS_GOAL_EARN_BONUS;
        case 'YNYN':
            return SUB_MISS_MQ_EARN_BONUS;
        case 'NXNY': // Did not achieve Goal, Did not achieve Bonus1
        case 'NXNN':
            // Did not achieve Goal, Did not achieve Bonus2
            return SUB_MISS_GOAL_MISS_BONUS;
        default:
            return _constants.NO_MESSAGE_CODE;
    }
}

function getFooterMessageCode(resultCode) {
    switch (resultCode) {
        case 'YXXX': //Did not achieve Goal
        case 'YYXX': // Achieved Goal, Met MQ
        case 'YYNN':
        case 'YYNY':
        case 'YYYY':
        case 'YYYN':
        case 'YXNN':
        case 'YXNY':
        case 'YXYN':
        case 'YXYY':
            return { key: 'finalResults.selector.dashboard.message.goalSuccess' };

        case 'NXXX': // Goal Not Achieved
        case 'YNXX': // Achieved Goal, Missed MQ
        case 'YNYY': // Achieved Goal, Missed MQ, Achieved Bonus (bonus coupled with goal)does not earn
        case 'NYYY': // Did Not Achieve Goal, Met MQ, Achieved Bonus (bonus coupled with goal) does not earn
        case 'NNYY': // Did Not Achieve Goal, Missed MQ, Achieved Bonus (bonus coupled with goal) does not earn
        case 'NNNN': // Did Not Achieve Goal, Missed MQ, Did Not Achieve Bonus
        case 'NNNY': //Did Not Achieve Goal, Missed MQ, Did Not Achieve Bonus 2
        case 'NYNN': // Did Not Achieve Goal, Met MQ, Did Not Achieve Bonus
        case 'NYNY': // Did Not Achieve Goal, Met MQ, Did Not Achieve Bonus 2
        case 'YNNN':
        case 'YNNY':
        case 'NXNN':
        case 'NXNY':
        case 'NXYY': // Did not achieve Goal, Achieved Bonus (bonus coupled with goal) does not earn
        case 'NYXX':
        case 'NNXX':
            return { key: 'finalResults.selector.dashboard.message.betterLuck' };

        case 'NXYN': // Did not achieve Goal, Achieved Bonus (bonus not coupled with goal)
        case 'NYYN': // Did Not Achieve Goal, Met MQ, Achieved Bonus (bonus not coupled with goal)
        case 'NNYN': // Did Not Achieve Goal, Missed MQ, Achieved Bonus (bonus not coupled with goal)
        case 'YNYN':
            return { key: 'finalResults.selector.dashboard.message.bonusSuccess' };

        default:
            return _constants.NO_MESSAGE_CODE;
    }
}
//# sourceMappingURL=goalquest-messages.js.map
