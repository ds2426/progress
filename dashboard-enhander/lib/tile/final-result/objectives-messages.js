'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getResultMessageCode = getResultMessageCode;
exports.getResultSubmessageCode = getResultSubmessageCode;
exports.getFooterMessageCode = getFooterMessageCode;

var _constants = require('./constants');

var MESSAGE_EARNED = { key: 'finalResults.selector.dashboard.message.earned', cssClass: 'you-earned' };
var MESSAGE_NOT_ACHIEVED = { key: 'finalResults.selector.dashboard.message.goalNotAchieved', cssClass: 'did-not-earn' };
var MESSAGE_MISS_MQ = { key: 'finalResults.selector.dashboard.message.missedMQ', cssClass: 'did-not-earn' };
function getResultMessageCode(resultCode) {
    switch (resultCode) {
        case 'YX': // goal achieved, no mq
        case 'YY':
            // goal achieved, met mq
            return MESSAGE_EARNED;
        case 'NX': // did not achieve goal, no mq
        case 'NY': // goal unachieved, met mq
        case 'NN':
            // goal unachieved, missed mq
            return MESSAGE_NOT_ACHIEVED;
        case 'YN':
            // goal achieved, missed mq
            return MESSAGE_MISS_MQ;
        default:
            return _constants.NO_MESSAGE_CODE;
    }
}

var SUB_NOT_ACHIEVED = { key: 'finalResults.selector.dashboard.subMessage.notAchieveGoal', cssClass: 'sub-message-black' };
var SUB_MISS_MQ = { key: 'finalResults.selector.dashboard.subMessage.achieveGoalMissMQ', cssClass: 'sub-message-black' };
function getResultSubmessageCode(resultCode) {
    switch (resultCode) {
        case 'NY': // goal unachieved, met mq
        case 'NN': // goal unachieved, missed mq
        case 'NX':
            // did not achieve goal, no mq
            return SUB_NOT_ACHIEVED;
        case 'YN':
            // goal achieved, missed mq
            return SUB_MISS_MQ;
        default:
            return _constants.NO_MESSAGE_CODE;
    }
}

var FOOTER_SUCCESS = { key: 'finalResults.selector.dashboard.message.goalSuccess' };
var FOOTER_BETTER_LUCK = { key: 'finalResults.selector.dashboard.message.betterLuck' };
function getFooterMessageCode(resultCode) {
    switch (resultCode) {
        case 'YX': // goal achieved, no mq
        case 'YY':
            // goal achieved, met mq
            return FOOTER_SUCCESS;
        case 'NX': // goal unachieved, no mq
        case 'YN': // goal achieved, missed mq
        case 'NN': // goal unachieved, missed mq
        case 'NY':
            // goal unachieved, met mq
            return FOOTER_BETTER_LUCK;
        default:
            return _constants.NO_MESSAGE_CODE;
    }
}
//# sourceMappingURL=objectives-messages.js.map
