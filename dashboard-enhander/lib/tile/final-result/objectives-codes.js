'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getResultMessageCode = getResultMessageCode;
exports.getResultSubmessageCode = getResultSubmessageCode;
exports.getFooterMessageCode = getFooterMessageCode;
function getResultMessageCode(resultCode) {
    var status = {
        key: '',
        cssClass: ''
    };
    switch (resultCode) {
        case 'YX': // goal achieved, no mq
        case 'YY':
            // goal achieved, met mq
            status = {
                key: 'finalResults.selector.dashboard.message.earned',
                cssClass: 'you-earned'
            };
            return status;
        case 'NX': // did not achieve goal, no mq
        case 'NY': // goal unachieved, met mq
        case 'NN':
            // goal unachieved, missed mq
            status = {
                key: 'finalResults.selector.dashboard.message.goalNotAchieved',
                cssClass: 'did-not-earn'
            };
            return status;
        case 'YN':
            // goal achieved, missed mq
            status = {
                key: 'finalResults.selector.dashboard.message.missedMQ',
                cssClass: 'did-not-earn'
            };
            return status;
        default:
            return '';
    }
}

function getResultSubmessageCode(resultCode) {
    var status = {
        key: '',
        cssClass: ''
    };
    switch (resultCode) {
        case 'NY': // goal unachieved, met mq
        case 'NN': // goal unachieved, missed mq
        case 'NX':
            // did not achieve goal, no mq
            status = {
                key: 'finalResults.selector.dashboard.subMessage.notAchieveGoal',
                cssClass: 'sub-message-black'
            };
            return status; // You did not achieve your goal.
        case 'YN':
            // goal achieved, missed mq
            status = {
                key: 'finalResults.selector.dashboard.subMessage.achieveGoalMissMQ',
                cssClass: 'sub-message-black'
            };
            return status; // You achieved your goal, but missed the minimum qualifier.
        default:
            return '';
    }
}

function getFooterMessageCode(resultCode) {
    switch (resultCode) {
        case 'YX': // goal achieved, no mq
        case 'YY':
            // goal achieved, met mq
            return 'finalResults.selector.dashboard.message.goalSuccess';

        case 'NX': // goal unachieved, no mq
        case 'YN': // goal achieved, missed mq
        case 'NN': // goal unachieved, missed mq
        case 'NY':
            // goal unachieved, met mq
            return 'finalResults.selector.dashboard.message.betterLuck';
        default:
            return '';
    }
}
//# sourceMappingURL=objectives-codes.js.map
