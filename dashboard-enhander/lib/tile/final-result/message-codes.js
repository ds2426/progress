'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getResultMessageCode = getResultMessageCode;
exports.getResultSubmessageCode = getResultSubmessageCode;
exports.getFooterMessageCode = getFooterMessageCode;

var _contestsUtils = require('contests-utils');

var _goalquestMessages = require('./goalquest-messages');

var goalquestCodes = _interopRequireWildcard(_goalquestMessages);

var _challengepointMessages = require('./challengepoint-messages');

var challengepointCodes = _interopRequireWildcard(_challengepointMessages);

var _objectivesMessages = require('./objectives-messages');

var objectivesCodes = _interopRequireWildcard(_objectivesMessages);

var _constants = require('./constants');

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var PT__GQ = _contestsUtils.constants.PT__GQ,
    PT__CP = _contestsUtils.constants.PT__CP,
    PT__OBJ = _contestsUtils.constants.PT__OBJ;
function getResultMessageCode(programType, resultCode) {

    switch (programType) {
        case PT__GQ:
            return goalquestCodes.getResultMessageCode(resultCode);
        case PT__CP:
            return challengepointCodes.getResultMessageCode(resultCode);
        case PT__OBJ:
            return objectivesCodes.getResultMessageCode(resultCode);
    }

    return _constants.NO_MESSAGE_CODE;
}

function getResultSubmessageCode(programType, resultCode) {
    switch (programType) {
        case PT__GQ:
            return goalquestCodes.getResultSubmessageCode(resultCode);
        case PT__CP:
            return challengepointCodes.getResultSubmessageCode(resultCode);
        case PT__OBJ:
            return objectivesCodes.getResultSubmessageCode(resultCode);
    }

    return _constants.NO_MESSAGE_CODE;
}

function getFooterMessageCode(programType, resultCode) {
    switch (programType) {
        case PT__GQ:
            return goalquestCodes.getFooterMessageCode(resultCode);
        case PT__CP:
            return challengepointCodes.getFooterMessageCode(resultCode);
        case PT__OBJ:
            return objectivesCodes.getFooterMessageCode(resultCode);
    }

    return _constants.NO_MESSAGE_CODE;
}
//# sourceMappingURL=message-codes.js.map
