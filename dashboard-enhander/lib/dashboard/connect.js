'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.mapDispatchToProps = exports.mapStateToProps = undefined;

var _reactRedux = require('react-redux');

var _programsDux = require('@biw/programs-dux');

var _contestsProgressDux = require('contests-progress-dux');

// actionCreators
var fetchProgramViewProgress = _contestsProgressDux.actions.fetchProgramViewProgress;
var mapStateToProps = exports.mapStateToProps = function mapStateToProps(state) {
    return {
        programs: _programsDux.selectors.getProgramViews(state)
    };
};

var mapDispatchToProps = exports.mapDispatchToProps = { fetchProgramViewProgress: fetchProgramViewProgress };

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps);
//# sourceMappingURL=connect.js.map
