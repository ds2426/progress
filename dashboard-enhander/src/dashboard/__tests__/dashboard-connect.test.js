import { mapStateToProps, mapDispatchToProps } from '../connect';

describe( 'dashboard connect', () => {
    it ( 'gets passed the required props when connected with react-redux.', () => {
        const result = mapStateToProps( {} );

        expect( result.programs ).toBeDefined();
    } );

    it( 'has the appropriate props for mapDispatchToProps', () => {
        expect( mapDispatchToProps.fetchProgramViewProgress ).toBeDefined();
    } );
} );
