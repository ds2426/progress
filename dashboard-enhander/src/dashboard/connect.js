import { connect }  from 'react-redux';
// actionCreators
import { selectors as programSelectors } from '@biw/programs-dux';
import { actions as progressActions } from 'contests-progress-dux';
const { fetchProgramViewProgress } = progressActions;

export const mapStateToProps = state => ( {
    programs: programSelectors.getProgramViews( state )
} );

export const mapDispatchToProps = { fetchProgramViewProgress };

export default connect( mapStateToProps, mapDispatchToProps );
