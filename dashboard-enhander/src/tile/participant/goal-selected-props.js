import get from 'lodash/get';
import { selectors as progressSelectors } from 'contests-progress-dux';
import { constants } from 'contests-utils';
import isBefore from 'date-fns/is_before';
import parseDate from 'date-fns/parse';

const { KEY_CODES } = constants;

import {
    isObjectives,
    getGoalLabelSelector,
    getProgramHasStarted,
    getDaysDifference
} from '../selectors';
const EMPTY_ARRAY = [];

const canSelectGoal = program => !isObjectives( program ) && isBefore( new Date(), parseDate( program.goalSelectionToDate ) );
const getDaysLeft = ( program ) => {
    const daysDifference = getDaysDifference( program );
    if ( canSelectGoal( program ) ) {
        return daysDifference.goalSelectionToDate;
    } else if ( !getProgramHasStarted( program ) ) {
        return daysDifference.programStartDate;
    }
    return daysDifference.programEndDate;
};
const getCalendarLabel = ( program ) => {
    if ( canSelectGoal( program ) ) {
        return 'goalSelection.toChangeGoal';
    }
    return 'progress.daysToEarnLabel';
};
const getLastDayLeftDate = ( program ) => {
    if ( canSelectGoal( program ) ) {
        return program.goalSelectionToDate || Date.now();
    } else if ( !getProgramHasStarted( program ) ) {
        return program.startDate || Date.now();
    }
    return program.endDate || Date.now();
};

export default function getGoalSelectedProps( state, program ) {
    const { programViewId } = program;
    const goal = progressSelectors.getProgramViewGoal( state, programViewId );

    const selectedGoalId = get( goal, 'paxGoalSelection.payoutTierId', 0 );
    const goalLevels = goal.goalLevels || EMPTY_ARRAY;
    const [ selectedGoal ] = goalLevels.filter( g => g.payoutTierId === selectedGoalId );
    const isHighestGoal = goalLevels.length === goalLevels.indexOf( selectedGoal ) + 1;
    const footerMessageCode = isHighestGoal ? KEY_CODES.waytopush : KEY_CODES.pushyourself;

    return {
        tileType: 'goal-selected',
        /* footer*/
        buttonTextCode: isObjectives( program ) ? KEY_CODES.setyourplan : KEY_CODES.changegoal,
        footerMessageCode: footerMessageCode,
        levelName: get( goal, 'paxGoalSelection.name' ),
        goalLabel: getGoalLabelSelector( programViewId )( state ),
        /*countdown*/
        daysLeft: getDaysLeft( program ),
        calendarLabel: getCalendarLabel( program ),
        lastDayLeftDate: getLastDayLeftDate( program ),
    };
}
