import { constants } from 'contests-utils';
const { KEY_CODES } = constants;

export default function getNoGoalSelectedProps( state, program ) {

    return {
        tileType: 'no-goal-selected',
        calendarLabel: '',
        lastDayLeftDate: program.goalSelectionToDate,

        /* footer*/
        buttonTextCode: KEY_CODES.viewDetails,
        footerMessageCode: '',

        daysLeft: 0,
    };
}
