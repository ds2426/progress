import { constants } from 'contests-utils';
const {
    PVS__NO_PROGRESS,
    KEY_CODES
 } = constants;

import {
    isObjectives,
    getProgramHasStarted,
    getDaysDifference
} from '../selectors';

export default function getProgramOverviewProps( state, program ) {
    const { programViewStatus } = program;
    const daysDifference = getDaysDifference( program );
    const programIsObjectives = isObjectives( program );

    return {
        tileType: 'program-overview',
        checkingResults: false,
        calendarLabel: programIsObjectives ? 'progress.daysToProgramStartLabel' : 'goalSelection.toSelectGoal',
        daysLeft: daysDifference[ programIsObjectives ? 'programStartDate' : 'goalSelectionToDate' ] || 0,
        lastDayLeftDate: ( programIsObjectives ? program.startDate : program.goalSelectionToDate ) || Date.now(),
        programOverviewCode: programIsObjectives ? KEY_CODES.achieveyourGoalEarnRewards : KEY_CODES.goalrightforyou,
        buttonTextCode: programIsObjectives && !getProgramHasStarted( program ) ? KEY_CODES.learnAboutProgram : KEY_CODES.selectagoal,
        footerMessageCode: programViewStatus === PVS__NO_PROGRESS && getProgramHasStarted( program ) ? KEY_CODES.onyourmark : KEY_CODES.upforchallenge,
    };
}
