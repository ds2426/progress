import memoize from 'lodash/memoize';
import { selectors as progressSelectors } from 'contests-progress-dux';
import { constants } from 'contests-utils';

import {
    getResultMessageCode,
    getResultSubmessageCode,
    getFooterMessageCode,
} from '../final-result/message-codes';

const { KEY_CODES } = constants;
const getMediaLabelObj = memoize( mediaLabel => ( { mediaLabel } ) );

export default function getFinalResultProps( state, program ) {
    const { programViewId, programType } = program;
    const finalResult = progressSelectors.getProgramViewFinalResults( state, programViewId );
    const finalResultCode = progressSelectors.getResultCode( state, programViewId );
    const goal = progressSelectors.getProgramViewGoal( state, programViewId );

    const mediaLabelObj = getMediaLabelObj( goal.mediaLabel || 'Points' );

    const achievedGoal = typeof finalResult.acheivedGoal !== 'undefined' ? finalResult.acheivedGoal : false;
    const achievedMQ = typeof finalResult.isMQAchieved !== 'undefined' ? finalResult.isMQAchieved : null;
    const isMQEnabled = goal.isMinimumQualifierEnabled;
    const footerMessageCode = getFooterMessageCode( programType, finalResultCode );

    return {
        tileType: 'final-result',
        showSuccess: achievedGoal && ( isMQEnabled && achievedMQ ) || achievedGoal && !isMQEnabled,
        pointsEarned: finalResult.totalNumberOfPoints || 0,
        pointsMediaLabel: mediaLabelObj.mediaLabel,

        resultMessageCode: getResultMessageCode( programType, finalResultCode ),
        resultSubmessageCode: getResultSubmessageCode( programType, finalResultCode ),
        footerMessageCode: footerMessageCode.key || footerMessageCode,
        buttonTextCode: KEY_CODES.seeResults,
        replaceOptions: mediaLabelObj,
    };
}
