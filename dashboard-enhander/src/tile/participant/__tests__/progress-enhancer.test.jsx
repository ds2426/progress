import addDays from 'date-fns/add_days';
import addHours from 'date-fns/add_hours';
import forEach from 'lodash/forEach';

import p5fixture from '@biw/test-utils/fixtures/p5';
import p6fixture from '@biw/test-utils/fixtures/p6';
import p7fixture from '@biw/test-utils/fixtures/p7';
import p8fixture from '@biw/test-utils/fixtures/p8';
import p9fixture from '@biw/test-utils/fixtures/p9';
import p10fixture from '@biw/test-utils/fixtures/p10';
import p3objfixture from '@biw/test-utils/fixtures/p3obj';
import p8objfixture from '@biw/test-utils/fixtures/p8obj';

import createTestStore from '@biw/test-utils/test-store';
import progressReducer, {
    ROOT_KEY as PROGRESS,
    actions as progressActions
} from 'contests-progress-dux';
import programsReducer, {
    ROOT_KEY as PROGRAMS,
    actions as programsActions
} from '@biw/programs-dux';

import { mapStateToProps } from '../../enhancer';

const dateNow = new Date( Date.now() );
function dateFromNow( days ) {
    return addHours( addDays( dateNow, ( days ) ), days > 0 ? 1 : -1 ).toISOString();
}

describe( 'Progress Tile: ', () => {
    describe( 'Situation #5: Participant Progress with Progress - Standard Calculation Type', () => {
        const endDate = dateFromNow( 3 );
        const goalSelectionToDate = dateFromNow( -1 );

        const store = createTestStore( {
            [ PROGRESS ]: progressReducer,
            [ PROGRAMS ]: programsReducer
        } );
        store.dispatch( progressActions.setProgramViewProgressData( p5fixture.programViewId, p5fixture.progress ) );
        const program = {
            ...p5fixture.program,
            endDate,
            goalSelectionToDate
        };
        store.dispatch( programsActions.setProgramViews( [ program ] ) );

        const componentProps = mapStateToProps( store.getState(), { program } );

        forEach( {
            tileType: 'progress',
            programViewId: '5041---Selector',
            programName: 'Sales Race #4 Accelerator and Bonus',

            heroImageUrl: null,
            onTrackPct: 38,
            currentGoalValue: 3058.53,
            goalAchieveAmount: 36362.55,
            statusTreatment: { className: 'get-going', label: 'progress.getGoing' },
            isReduction: false,
            isStandard: true,
            goalLabel: expect.objectContaining( {
                amount: 36362.55,
                currencyCode: 'usd',
                currencyPosition: 'before',
                currencySymbol: '$',
                labelText: 'in sales dollars',
                labelType: 'currency',
                labelPosition: 'after',
                precision: 2,
                rouningMethod: 'standard'
            } ),

            programHasEnded: false,
            progressPercent: 8,
            dateThroughPercentage: 22,
            programLogo: expect.any( String ),
            calendarLabel: 'progress.daysToEarnLabel',
            daysLeft: 4,
            lastDayLeftDate: endDate,
            footerMessageCode: 'progress.getGoing',
            buttonTextCode: 'dashboard.tile.common.viewDetails',
            progressLastUpdated: '2017-01-20T00:00:00-0600',
            progressLastUpdatedCode: 'programInfoHeader.dataThrough'
        }, ( val, key ) => {
            it( `should have a prop called '${key}' with a value of '${val}'`, () => {
                expect( componentProps[ key ] ).toEqual( val );
            } );
        } );
    } );

    describe( 'Situation #6: Participant Progress with Progress - Reduction Average Calculation Type', () => {
        const endDate = dateFromNow( 19 );
        const goalSelectionToDate = dateFromNow( -1 );

        const store = createTestStore( {
            [ PROGRESS ]: progressReducer,
            [ PROGRAMS ]: programsReducer
        } );
        store.dispatch( progressActions.setProgramViewProgressData( p6fixture.programViewId, p6fixture.progress ) );
        const program = {
            ...p6fixture.program,
            endDate,
            goalSelectionToDate
        };
        store.dispatch( programsActions.setProgramViews( [ program ] ) );

        const componentProps = mapStateToProps( store.getState(), { program } );

        forEach( {
            programViewId: '5050---Selector',
            programName: 'Make the Grade #2',
            tileType: 'progress',
            programHasEnded: false,

            heroImageUrl: 'https://apiqa.farm1.honeycombsvc.com/hcapi/participantMedia/linda/logo/1524676108166_logo.png',
            onTrackPct: 75,
            currentGoalValue: 2.27,
            goalAchieveAmount: 1.8,
            statusTreatment: { className: 'stay-focused', label: 'progress.stayFocused' },
            isReduction: true,
            isStandard: false,
            goalLabel: expect.objectContaining( {
                amount: 1.8,
                currencyCode: null,
                currencyPosition: null,
                currencySymbol: null,
                labelText: 'Average Handle Time',
                labelType: 'none',
                labelPosition: 'after',
                precision: 2,
                rouningMethod: 'standard'
            } ),

            progressPercent: 75,
            dateThroughPercentage: 32,

            programLogo: expect.any( String ),
            calendarLabel: 'progress.daysToEarnLabel',
            daysLeft: 20,
            lastDayLeftDate: dateFromNow( 19 ),
            footerMessageCode: 'progress.stayFocused',
            buttonTextCode: 'dashboard.tile.common.viewDetails',
            progressLastUpdated: '2017-03-10T00:00:00-0600',
            progressLastUpdatedCode: 'programInfoHeader.dataThrough',
        }, ( val, key ) => {
            it( `should have a prop called '${key}' with a value of '${val}'`, () => {
                expect( componentProps[ key ] ).toEqual( val );
            } );
        } );
    } );

    describe( 'Situation #7: Participant Progress with Progress - Average Calculation Type', () => {
        const endDate = dateFromNow( 19 );
        const goalSelectionToDate = dateFromNow( -1 );

        const store = createTestStore( {
            [ PROGRESS ]: progressReducer,
            [ PROGRAMS ]: programsReducer
        } );
        store.dispatch( progressActions.setProgramViewProgressData( p7fixture.programViewId, p7fixture.progress ) );
        const program = {
            ...p7fixture.program,
            endDate,
            goalSelectionToDate
        };
        store.dispatch( programsActions.setProgramViews( [ program ] ) );

        const componentProps = mapStateToProps( store.getState(), { program } );

        forEach( {
            programViewId: '5044---Selector',
            programName: 'Sell One More! #2',
            tileType: 'progress',
            programHasEnded: false,
            progressPercent: 186,
            dateThroughPercentage: 51,

            heroImageUrl: null,
            onTrackPct: 186,
            currentGoalValue: 5.7,
            goalAchieveAmount: 3.2,
            statusTreatment: { className: 'on-track', label: 'progress.onTrackHeader' },
            isReduction: false,
            isStandard: false,
            isAverage: true,
            goalLabel: expect.objectContaining( {
                amount: 3.2,
                currencyCode: null,
                currencyPosition: null,
                currencySymbol: null,
                labelText: 'Units per Customer',
                labelType: 'none',
                labelPosition: 'after',
                precision: 2,
                rouningMethod: 'standard'
            } ),

            programLogo: expect.any( String ),
            calendarLabel: 'progress.daysToEarnLabel',
            daysLeft: 20,
            lastDayLeftDate: dateFromNow( 19 ),
            footerMessageCode: 'progress.onTrackHeader',
            buttonTextCode: 'dashboard.tile.common.viewDetails',
            progressLastUpdated: '2017-03-31T00:00:00-0500',
            progressLastUpdatedCode: 'programInfoHeader.dataThrough',
        }, ( val, key ) => {
            it( `should have a prop called '${key}' with a value of '${val}'`, () => {
                expect( componentProps[ key ] ).toEqual( val );
            } );
        } );
    } );

    describe( 'Situation #8: Participant Progress Waiting on Final Results - Standard Calculation Type', () => {
        const endDate = dateFromNow( -1 );
        const goalSelectionToDate = dateFromNow( -10 );

        const store = createTestStore( {
            [ PROGRESS ]: progressReducer,
            [ PROGRAMS ]: programsReducer
        } );
        store.dispatch( progressActions.setProgramViewProgressData( p8fixture.programViewId, p8fixture.progress ) );
        const program = {
            ...p8fixture.program,
            endDate,
            goalSelectionToDate
        };
        store.dispatch( programsActions.setProgramViews( [ program ] ) );

        const componentProps = mapStateToProps( store.getState(), { program } );

        forEach( {
            programViewId: '5022---Selector',
            programName: 'Sales Race #2',
            tileType: 'waiting-for-final-result',
            programHasEnded: true,

            progressPercent: 51,
            dateThroughPercentage: 51,

            checkingResults: true,
            programCalculationType: 'standard',
            heroImageUrl: null,
            onTrackPct: 103,
            statusTreatment: { className: 'on-track', label: 'progress.onTrackHeader' },
            isReduction: false,
            isStandard: true,
            goalLabel: expect.objectContaining( {
                amount: 36362.55,
                currencyCode: 'usd',
                currencyPosition: 'before',
                currencySymbol: '$',
                labelText: 'in sales dollars',
                labelType: 'currency',
                labelPosition: 'after',
                precision: 2,
                rouningMethod: 'standard'
            } ),

            programLogo: expect.any( String ),
            calendarLabel: 'dashboard.tile.common.waitingForFinal',
            daysLeft: 0,
            lastDayLeftDate: dateFromNow( -1 ),
            footerMessageCode: 'dashboard.tile.common.checkingResults',

            buttonTextCode: 'dashboard.tile.common.viewDetails',
            progressLastUpdated: '2017-02-15T00:00:00-0600',
            progressLastUpdatedCode: 'programInfoHeader.dataThrough',
        }, ( val, key ) => {
            it( `should have a prop called '${key}' with a value of '${val}'`, () => {
                expect( componentProps[ key ] ).toEqual( val );
            } );
        } );
    } );

    describe( 'Situation #9: Participant Progress Waiting on Final Results - Reduction Averaage Calculation Type', () => {
        const endDate = dateFromNow( -1 );
        const goalSelectionToDate = dateFromNow( -10 );

        const store = createTestStore( {
            [ PROGRESS ]: progressReducer,
            [ PROGRAMS ]: programsReducer
        } );
        store.dispatch( progressActions.setProgramViewProgressData( p9fixture.programViewId, p9fixture.progress ) );
        const program = {
            ...p9fixture.program,
            endDate,
            goalSelectionToDate
        };
        store.dispatch( programsActions.setProgramViews( [ program ] ) );

        const componentProps = mapStateToProps( store.getState(), { program } );

        forEach( {
            programViewId: '5052---Selector',
            programName: 'Make the Grade #3',
            tileType: 'waiting-for-final-result',
            programHasEnded: true,
            progressPercent: 129,
            dateThroughPercentage: 50,

            checkingResults: true,
            programCalculationType: 'reduction',
            heroImageUrl: 'https://apiqa.farm1.honeycombsvc.com/hcapi/participantMedia/linda/logo/1524676108166_logo.png',
            onTrackPct: 129,
            statusTreatment: { className: 'on-track', label: 'progress.onTrackHeader' },
            isReduction: true,
            isStandard: false,
            goalLabel: expect.objectContaining( {
                amount: 1.62,
                currencyCode: null,
                currencyPosition: null,
                currencySymbol: null,
                labelText: 'Average Handle Time',
                labelType: 'none',
                labelPosition: 'after',
                precision: 2,
                rouningMethod: 'standard'
            } ),

            programLogo: expect.any( String ),
            calendarLabel: 'dashboard.tile.common.waitingForFinal',
            daysLeft: 0,
            lastDayLeftDate: dateFromNow( -1 ),
            footerMessageCode: 'dashboard.tile.common.checkingResults',
            buttonTextCode: 'dashboard.tile.common.viewDetails',
            progressLastUpdated: '2017-02-14T00:00:00-0600',
            progressLastUpdatedCode: 'programInfoHeader.dataThrough',
        }, ( val, key ) => {
            it( `should have a prop called '${key}' with a value of '${val}'`, () => {
                expect( componentProps[ key ] ).toEqual( val );
            } );
        } );
    } );

    describe( 'Situation #10: Participant Progress Waiting on Final Results - Average Calculation Type', () => {
        const endDate = dateFromNow( -1 );
        const goalSelectionToDate = dateFromNow( -10 );

        const store = createTestStore( {
            [ PROGRESS ]: progressReducer,
            [ PROGRAMS ]: programsReducer
        } );
        store.dispatch( progressActions.setProgramViewProgressData( p10fixture.programViewId, p10fixture.progress ) );
        const program = {
            ...p10fixture.program,
            endDate,
            goalSelectionToDate
        };
        store.dispatch( programsActions.setProgramViews( [ program ] ) );

        const componentProps = mapStateToProps( store.getState(), { program } );

        forEach( {
            programViewId: '5044---Selector',
            programName: 'Sell One More! #2',
            tileType: 'waiting-for-final-result',
            programHasEnded: true,
            progressPercent: 186,
            dateThroughPercentage: 51,

            checkingResults: true,
            programCalculationType: 'average',
            heroImageUrl: null,
            onTrackPct: 186,
            statusTreatment: { className: 'on-track', label: 'progress.onTrackHeader' },
            isReduction: false,
            isStandard: false,
            goalLabel: expect.objectContaining( {
                amount: 3.2,
                currencyCode: null,
                currencyPosition: null,
                currencySymbol: null,
                labelText: 'Units per Customer',
                labelType: 'none',
                labelPosition: 'after',
                precision: 2,
                rouningMethod: 'standard'
            } ),

            programLogo: expect.any( String ),
            calendarLabel: 'dashboard.tile.common.waitingForFinal',
            daysLeft: 0,
            lastDayLeftDate: dateFromNow( -1 ),
            footerMessageCode: 'dashboard.tile.common.checkingResults',
            buttonTextCode: 'dashboard.tile.common.viewDetails',
            progressLastUpdated: '2017-03-31T00:00:00-0500',
            progressLastUpdatedCode: 'programInfoHeader.dataThrough',
        }, ( val, key ) => {
            it( `should have a prop called '${key}' with a value of '${val}'`, () => {
                expect( componentProps[ key ] ).toEqual( val );
            } );
        } );
    } );
} );


describe( 'Objective Progress Tile: ', () => {
    describe( 'Situation #3 Tile: Participant Progress with Progress - Standard Calculation Type', () => {
        const endDate = dateFromNow( 3 );
        const goalSelectionToDate = dateFromNow( -1 );

        const store = createTestStore( {
            [ PROGRESS ]: progressReducer,
            [ PROGRAMS ]: programsReducer
        } );
        store.dispatch( progressActions.setProgramViewProgressData( p3objfixture.programViewId, p3objfixture.progress ) );
        const program = {
            ...p3objfixture.program,
            endDate,
            goalSelectionToDate
        };
        store.dispatch( programsActions.setProgramViews( [ program ] ) );

        const componentProps = mapStateToProps( store.getState(), { program } );

        forEach( {
            programViewId: '11268---Selector',
            programName: 'Objectives Reduction Average – progress',
            tileType: 'progress',
            programHasEnded: false,

            progressPercent: 4,
            dateThroughPercentage: 36,
            statusTreatment: {
                className: 'get-going',
                label: 'progress.getGoing',
            },

            heroImageUrl: null,
            onTrackPct: 4,
            currentGoalValue: 5.94,
            goalAchieveAmount: 3.5,
            isReduction: true,
            isStandard: false,
            goalLabel: expect.objectContaining( {
                amount: 3.5,
                currencyCode: null,
                currencyPosition: null,
                currencySymbol: null,
                labelText: 'Average Handle Time',
                labelType: 'none',
                labelPosition: 'after',
                precision: 2,
                rouningMethod: 'down'
            } ),

            programLogo: expect.any( String ),
            calendarLabel: 'progress.daysToEarnLabel',
            daysLeft: 4,
            lastDayLeftDate: dateFromNow( 3 ),
            footerMessageCode: 'progress.getGoing',
            buttonTextCode: 'dashboard.tile.common.viewDetails',
            progressLastUpdated: '2018-02-01T00:00:00-0600',
            progressLastUpdatedCode: 'programInfoHeader.dataThrough'
        }, ( val, key ) => {
            it( `should have a prop called '${key}' with a value of '${val}'`, () => {
                expect( componentProps[ key ] ).toEqual( val );
            } );
        } );
    } );

    describe( 'Situation #8 Tile: Participant Progress Waiting on Final Results - Average Calculation Type', () => {
        const endDate = dateFromNow( -1 );
        const goalSelectionToDate = dateFromNow( -1 );

        const store = createTestStore( {
            [ PROGRESS ]: progressReducer,
            [ PROGRAMS ]: programsReducer
        } );
        store.dispatch( progressActions.setProgramViewProgressData( p8objfixture.programViewId, p8objfixture.progress ) );
        const program = {
            ...p8objfixture.program,
            endDate,
            goalSelectionToDate
        };
        store.dispatch( programsActions.setProgramViews( [ program ] ) );

        const componentProps = mapStateToProps( store.getState(), { program } );

        forEach( {
            programViewId: '11327---Selector',
            programName: 'Program #2: Objectives Average – progress 001',
            tileType: 'waiting-for-final-result',
            programHasEnded: true,

            progressPercent: 40,
            dateThroughPercentage: 7,
            statusTreatment: {
                className: 'on-track',
                label: 'progress.onTrackHeader',
            },

            heroImageUrl: null,
            onTrackPct: 200,
            isReduction: false,
            isStandard: true,
            goalLabel: expect.objectContaining( {
                amount: 21.09,
                currencyCode: null,
                currencyPosition: null,
                currencySymbol: null,
                labelText: 'average per work order',
                labelType: 'none',
                labelPosition: 'after',
                precision: 2,
                rouningMethod: 'standard'
            } ),

            checkingResults: true,
            programCalculationType: 'standard',

            programLogo: expect.any( String ),
            calendarLabel: 'dashboard.tile.common.waitingForFinal',
            daysLeft: 0,
            lastDayLeftDate: dateFromNow( -1 ),
            footerMessageCode: 'dashboard.tile.common.checkingResults',
            buttonTextCode: 'dashboard.tile.common.viewDetails',
            progressLastUpdated: '2018-01-06T00:00:00-0600',
            progressLastUpdatedCode: 'programInfoHeader.dataThrough',

        }, ( val, key ) => {
            it( `should have a prop called '${key}' with a value of '${val}'`, () => {
                expect( componentProps[ key ] ).toEqual( val );
            } );
        } );
    } );
} );
