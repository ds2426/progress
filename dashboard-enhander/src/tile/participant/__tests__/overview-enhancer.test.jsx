
import addDays from 'date-fns/add_days';
import addHours from 'date-fns/add_hours';
import forEach from 'lodash/forEach';
import p1objfixture from '@biw/test-utils/fixtures/p1obj';
import p2objfixture from '@biw/test-utils/fixtures/p2obj';

import createTestStore from '@biw/test-utils/test-store';
import progressReducer, {
    ROOT_KEY as PROGRESS,
    actions as progressActions
} from 'contests-progress-dux';
import programsReducer, {
    ROOT_KEY as PROGRAMS,
    actions as programsActions
} from '@biw/programs-dux';

import { mapStateToProps } from '../../enhancer';

const dateNow = new Date( Date.now() );
function dateFromNow( days ) {
    return addHours( addDays( dateNow, ( days ) ), days > 0 ? 1 : -1 ).toISOString();
}

describe( 'Objectives Overview Tile: ', () => {
    describe( 'Program #1: Objectives Standard - Not Started', () => {
        const startDate = dateFromNow( 60 );
        const endDate = dateFromNow( 90 );
        const store = createTestStore( {
            [ PROGRESS ]: progressReducer,
            [ PROGRAMS ]: programsReducer
        } );
        store.dispatch( progressActions.setProgramViewProgressData( p1objfixture.programViewId, p1objfixture.progress ) );
        const program = {
            ...p1objfixture.program,
            endDate,
            startDate
        };
        store.dispatch( programsActions.setProgramViews( [ program ] ) );

        const componentProps = mapStateToProps( store.getState(), { program } );

        forEach( {
            heroImageUrl: null,
            programHasEnded: false,
            goalSelectionToDate: null,
            endDate: endDate,
            startDate: startDate,
            isObjectives: true,
            programHasStarted: false,
            checkingResults: false,
            programOverviewCode: 'dashboard.tile.common.achieveyourGoalEarnRewards',

            programViewId: '11249---Selector',
            programName: 'Program #1: Objectives Standard - Not Started',
            tileType: 'program-overview',

            programLogo: 'https://apipprd.farm1.honeycombsvc.com/hcapi/participantMedia/bugathon/logo/1516289129065_logo.png',


            calendarLabel: 'progress.daysToProgramStartLabel',
            daysLeft: 61,
            lastDayLeftDate: dateFromNow( 60 ),
            footerMessageCode: 'dashboard.tile.common.upforchallenge',
            buttonTextCode: 'dashboard.tile.common.learnAboutProgram'
        }, ( val, key ) => {
            it( `should have a prop called '${key}' with a value of '${val}'`, () => {
                expect( componentProps[ key ] ).toEqual( val );
            } );
        } );
    } );

    describe( 'Program #1: Objectives Standard - no progress', () => {
        const startDate = dateFromNow( 40 );
        const endDate = dateFromNow( 60 );
        const store = createTestStore( {
            [ PROGRESS ]: progressReducer,
            [ PROGRAMS ]: programsReducer
        } );
        store.dispatch( progressActions.setProgramViewProgressData( p2objfixture.programViewId, p2objfixture.progress ) );
        const program = {
            ...p2objfixture.program,
            endDate,
            startDate
        };
        store.dispatch( programsActions.setProgramViews( [ program ] ) );

        const componentProps = mapStateToProps( store.getState(), { program } );

        forEach( {
            tileType: 'program-overview',
            heroImageUrl: null,
            programHasEnded: false,
            goalSelectionToDate: null,
            endDate: endDate,
            startDate: startDate,
            isObjectives: true,
            programHasStarted: false,
            checkingResults: false,
            programOverviewCode: 'dashboard.tile.common.achieveyourGoalEarnRewards',

            programViewId: '11692---Selector',
            programName: 'Program #2: Objectives Standard - No Progress',
            programLogo: 'https://apipprd.farm1.honeycombsvc.com/hcapi/participantMedia/bugathon/logo/1516289129065_logo.png',

            calendarLabel: 'progress.daysToProgramStartLabel',
            daysLeft: 41,
            lastDayLeftDate: startDate,
            footerMessageCode: 'dashboard.tile.common.upforchallenge',
            buttonTextCode: 'dashboard.tile.common.learnAboutProgram'
        }, ( val, key ) => {
            it( `should have a prop called '${key}' with a value of '${val}'`, () => {
                expect( componentProps[ key ] ).toEqual( val );
            } );
        } );
    } );//


} );


