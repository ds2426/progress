import addDays from 'date-fns/add_days';
import addHours from 'date-fns/add_hours';
import forEach from 'lodash/forEach';

import p1fixture from '@biw/test-utils/fixtures/p1';
import p2fixture from '@biw/test-utils/fixtures/p2';
import p3fixture from '@biw/test-utils/fixtures/p3';
import p4fixture from '@biw/test-utils/fixtures/p4';

import createTestStore from '@biw/test-utils/test-store';
import progressReducer, {
    ROOT_KEY as PROGRESS,
    actions as progressActions
} from 'contests-progress-dux';
import programsReducer, {
    ROOT_KEY as PROGRAMS,
    actions as programsActions
} from '@biw/programs-dux';

import { mapStateToProps } from '../../enhancer';

const dateNow = new Date();

function dateFromNow( days ) {
    return addHours( addDays( dateNow, days ), days > 0 ? 1 : -1 ).toISOString();
}

describe( 'Goal Selection Tile: ', () => {

    describe( 'Situation #1: Participant During Goal Selection, No Goal Selected', () => {
        const endDate = dateFromNow( 40 );
        const goalSelectionToDate = dateFromNow( 20 );

        const store = createTestStore( {
            [ PROGRESS ]: progressReducer,
            [ PROGRAMS ]: programsReducer
        } );
        store.dispatch( progressActions.setProgramViewProgressData( p1fixture.programViewId, p1fixture.progress ) );
        const program = {
            ...p1fixture.program,
            endDate,
            goalSelectionToDate
        };
        store.dispatch( programsActions.setProgramViews( [ program ] ) );

        const componentProps = mapStateToProps( store.getState(), { program } );

        forEach( {
            programViewId: '13425---Selector',
            programName: 'PillNavTest',
            tileType: 'program-overview',
            programLogo: null,

            footerMessageCode: 'dashboard.tile.common.upforchallenge',
            buttonTextCode: 'dashboard.tile.common.selectagoal',
            calendarLabel: 'goalSelection.toSelectGoal',
            daysLeft: 21,
            lastDayLeftDate: goalSelectionToDate,

            heroImageUrl: null,
            programHasEnded: false,
            goalSelectionToDate: goalSelectionToDate,
            endDate: endDate,
            startDate: '2017-12-01T00:00:00-0600',
            isObjectives: false,
            programHasStarted: true,
            checkingResults: false,
            programOverviewCode: 'dashboard.tile.common.goalrightforyou',
        }, ( val, key ) => {
            it( `should have a prop called '${key}' with a value of '${val}'`, () => {
                expect( componentProps[ key ] ).toEqual( val );
            } );
        } );
    } );

    describe( 'Situation #2: Participant During Goal Selection, Goal Selected', () => {
        const endDate = dateFromNow( 40 );
        const startDate = dateFromNow( 40 );
        const goalSelectionToDate = dateFromNow( 30 );

        const store = createTestStore( {
            [ PROGRESS ]: progressReducer,
            [ PROGRAMS ]: programsReducer
        } );
        store.dispatch( progressActions.setProgramViewProgressData( p2fixture.programViewId, p2fixture.progress ) );
        const program = {
            ...p2fixture.program,
            endDate,
            goalSelectionToDate,
            startDate

        };
        store.dispatch( programsActions.setProgramViews( [ program ] ) );

        const componentProps = mapStateToProps( store.getState(), { program } );

        forEach( {
            programViewId: '5021---Selector',
            programName: 'Sales Race #1',
            tileType: 'goal-selected',

            footerMessageCode: 'dashboard.tile.common.pushyourself',
            buttonTextCode: 'dashboard.tile.common.changegoal',
            calendarLabel: 'goalSelection.toChangeGoal',
            daysLeft: 31,
            lastDayLeftDate: goalSelectionToDate,

            programType: 'goalquest',
            programLogo: expect.any( String ),
            heroImageUrl: null,
            endDate: endDate,
            isObjectives: false,
            goalLabel: expect.objectContaining( {
                amount: 36362.55,
                currencyCode: 'usd',
                currencyPosition: 'before',
                currencySymbol: '$',
                labelText: 'in sales dollars',
                labelType: 'currency',
                labelPosition: 'after',
                precision: 2,
                rouningMethod: 'standard'
            } ),
        }, ( val, key ) => {
            it( `should have a prop called '${key}' with a value of '${val}'`, () => {
                expect( componentProps[ key ] ).toEqual( val );
            } );
        } );
    } );

    describe( 'Situation #2: Participant During Goal Selection, Goal Selected at highest level', () => {
        const endDate = dateFromNow( 40 );
        const goalSelectionToDate = dateFromNow( 20 );
        const goalLevels = p2fixture.progress.goal.goalLevels;

        const store = createTestStore( {
            [ PROGRESS ]: progressReducer,
            [ PROGRAMS ]: programsReducer
        } );
        store.dispatch( progressActions.setProgramViewProgressData( p2fixture.programViewId, {
            ...p2fixture.progress,
            goal: {
                ...p2fixture.progress.goal,
                paxGoalSelection: goalLevels[ goalLevels.length - 1 ]
            },
        } ) );
        const program = {
            ...p2fixture.program,
            endDate,
            goalSelectionToDate
        };
        store.dispatch( programsActions.setProgramViews( [ program ] ) );

        const componentProps = mapStateToProps( store.getState(), { program } );

        forEach( {
            programViewId: '5021---Selector',
            programName: 'Sales Race #1',
            tileType: 'goal-selected',

            footerMessageCode: 'dashboard.tile.common.waytopush',
            buttonTextCode: 'dashboard.tile.common.changegoal',
            calendarLabel: 'goalSelection.toChangeGoal',
            daysLeft: 21,
            lastDayLeftDate: goalSelectionToDate,

            programType: 'goalquest',
            programLogo: expect.any( String ),
            heroImageUrl: null,
            endDate: endDate,
            isObjectives: false,
            goalLabel: expect.objectContaining( {
                amount: 41557.2,
                currencyCode: 'usd',
                currencyPosition: 'before',
                currencySymbol: '$',
                labelText: 'in sales dollars',
                labelType: 'currency',
                labelPosition: 'after',
                precision: 2,
                rouningMethod: 'standard'
            } ),
        }, ( val, key ) => {
            it( `should have a prop called '${key}' with a value of '${val}'`, () => {
                expect( componentProps[ key ] ).toEqual( val );
            } );
        } );
    } );

    describe( 'Situation #3: Participant No Goal Selected During Goal Selection', () => {
        const store = createTestStore( {
            [ PROGRESS ]: progressReducer,
            [ PROGRAMS ]: programsReducer
        } );
        store.dispatch( progressActions.setProgramViewProgressData( p3fixture.programViewId, p3fixture.progress ) );
        const program = {
            ...p3fixture.program,
            endDate: dateFromNow( -1 ),
            goalSelectionToDate: dateFromNow( -1 )
        };
        store.dispatch( programsActions.setProgramViews( [ program ] ) );

        const componentProps = mapStateToProps( store.getState(), { program } );

        forEach( {
            programViewId: '5049---Selector',
            programName: 'Make the Grade #1',
            tileType: 'no-goal-selected',

            programLogo: 'https://api.farm1.honeycombsvc.com/hcapi/participantMedia/demo0531/logo/1497647161239_logo.png',

            footerMessageCode: '',
            buttonTextCode: 'dashboard.tile.common.viewDetails',

            heroImageUrl: null,
            calendarLabel: '',
            lastDayLeftDate: program.goalSelectionToDate,
            daysLeft: 0,
        }, ( val, key ) => {
            it( `should have a prop called '${key}' with a value of '${val}'`, () => {
                expect( componentProps[ key ] ).toEqual( val );
            } );
        } );
    } );

    describe( 'Situation #4: Participant Progress with No Progress', () => {
        const store = createTestStore( {
            [ PROGRESS ]: progressReducer,
            [ PROGRAMS ]: programsReducer
        } );
        store.dispatch( progressActions.setProgramViewProgressData( p4fixture.programViewId, p4fixture.progress ) );
        const program = {
            ...p4fixture.program,
            endDate: dateFromNow( 20 ),
            goalSelectionToDate: dateFromNow( -1 )
        };
        store.dispatch( programsActions.setProgramViews( [ program ] ) );

        const componentProps = mapStateToProps( store.getState(), { program } );

        forEach( {
            programViewId: '5049---Selector',
            programName: 'Make the Grade #1',
            tileType: 'no-progress',

            programLogo: 'https://api.farm1.honeycombsvc.com/hcapi/participantMedia/demo0531/logo/1497647161239_logo.png',

            levelName: 'Level 3',
            levelDescription: 'Reduce your average handle time by 10%',

            footerMessageCode: 'dashboard.tile.common.onyourmark',
            buttonTextCode: 'dashboard.tile.common.setyourplan',
            calendarLabel: 'progress.daysToEarnLabel',
            daysLeft: 21,
            lastDayLeftDate: dateFromNow( 20 ),

            goal: p4fixture.progress.goal,
            programHasEnded: false,
            heroImageUrl: 'https://apiqa.farm1.honeycombsvc.com/hcapi/participantMedia/linda/logo/1524676108166_logo.png',
            endDate: program.endDate,
            startDate: program.startDate,
            isObjectives: false,
            goalLabel: {
                amount: 1.75,
                currencyCode: null,
                currencyPosition: null,
                currencySymbol: null,
                labelText: 'Average Handle Time',
                labelType: 'none',
                labelPosition: 'after',
                precision: 2,
                rouningMethod: 'standard'
            },
        }, ( val, key ) => {
            it( `should have a prop called '${key}' with a value of '${val}'`, () => {
                expect( componentProps[ key ] ).toEqual( val );
            } );
        } );
    } );
} );
