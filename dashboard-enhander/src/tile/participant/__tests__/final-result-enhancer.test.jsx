import addDays from 'date-fns/add_days';
import addHours from 'date-fns/add_hours';
import forEach from 'lodash/forEach';

import p11fixture from '@biw/test-utils/fixtures/p11';
import createTestStore from '@biw/test-utils/test-store';
import progressReducer, {
    ROOT_KEY as PROGRESS,
    actions as progressActions
} from 'contests-progress-dux';
import programsReducer, {
    ROOT_KEY as PROGRAMS,
    actions as programsActions
} from '@biw/programs-dux';

import { mapStateToProps } from '../../enhancer';

function getNow() {
    return new Date( Date.now() );
}

function dateFromNow( days ) {
    return addHours( addDays( getNow(), days ), days > 0 ? 1 : -1 ).toISOString();
}

describe( 'Final Result Tile: ', () => {
    describe( 'Situation #11: Final Results', () => {
        const store = createTestStore( {
            [ PROGRESS ]: progressReducer,
            [ PROGRAMS ]: programsReducer
        } );
        store.dispatch( progressActions.setProgramViewProgressData( p11fixture.programViewId, p11fixture.progress ) );
        const program = {
            ...p11fixture.program,
            endDate: dateFromNow( -1 ),
            goalSelectionToDate: dateFromNow( -1 )
        };
        store.dispatch( programsActions.setProgramViews( [ program ] ) );

        const componentProps = mapStateToProps( store.getState(), { program } );

        forEach( {
            programViewId: '5023---Selector',
            programName: 'Sales Race #3',
            tileType: 'final-result',
            programLogo: expect.any( String ),
            resultMessageCode: { cssClass: 'did-not-earn', key: 'finalResults.selector.dashboard.message.goalNotAchieved' },
            resultSubmessageCode: { 'cssClass': 'sub-message-black', 'key': 'finalResults.selector.dashboard.subMessage.notAchieveGoal' },
            footerMessageCode: 'finalResults.selector.dashboard.message.betterLuck',
            buttonTextCode: 'dashboard.tile.common.seeResults',
            progressLastUpdatedCode: 'programInfoHeader.dataThrough',
            pointsMediaLabel: 'Points',
            pointsEarned: 0,
            progressLastUpdated: '2016-12-31T00:00:00-0600',
            heroImageUrl: null,
            showSuccess: false,
            replaceOptions: { mediaLabel: 'Points' },
         }, ( val, key ) => {
            it( `should have a prop called '${key}' with a value of '${val}'`, () => {
                expect( componentProps[ key ] ).toEqual( val );
            } );
        } );
    } );
} );
