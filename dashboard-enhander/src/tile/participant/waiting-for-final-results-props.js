import { selectors as progressSelectors } from 'contests-progress-dux';

import { constants, getProgressStatusTreatment } from 'contests-utils';
const { KEY_CODES } = constants;

import {
    getProgramHasEnded,
    getDaysDifference,
    getGoalLabelSelector
} from '../selectors';

export default function getProgressProps( state, program ) {
    const { programViewStatus, programViewId } = program;
    const goal = progressSelectors.getProgramViewGoal( state, programViewId );
    const progress = goal.paxProgressResponse || {};
    const goalSelection = goal.paxGoalSelection || {};

    const programHasEnded = getProgramHasEnded( program );
    const daysDifference = getDaysDifference( program );
    const programCalculationType = goal.programCalculationType || 'standard';
    const statusTreatment = getProgressStatusTreatment(
        progress.onTrackPercentage || 0,
        programViewStatus,
        progress.progressPercentage || 0,
        programCalculationType,
        progress.isMinQualifierAchieved
    );

    return {
        tileType: 'waiting-for-final-result',
        checkingResults: programHasEnded,
        goalLabel: getGoalLabelSelector( programViewId )( state ),
        programCalculationType,

        isReduction: programCalculationType === 'reduction',
        isStandard: programCalculationType === 'standard',
        isAverage: programCalculationType === 'average',

        progressPercent: progress.progressPercentage || 0,
        onTrackPct: progress.onTrackPercentage || 0,
        currentGoalValue: progress.currentGoalValue,
        goalAchieveAmount: goalSelection.goalAchieveAmount,
        dateThroughPercentage: progress.dateThroughPercentage || 0,
        statusTreatment,
        daysLeft: daysDifference.programEndDate,

        calendarLabel: programHasEnded ? 'dashboard.tile.common.waitingForFinal' : 'progress.daysToEarnLabel',
        footerMessageCode: programHasEnded ? KEY_CODES.checkingResults : statusTreatment.label,
        buttonTextCode: KEY_CODES.viewDetails,
    };
}
