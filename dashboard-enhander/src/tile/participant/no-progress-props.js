import get from 'lodash/get';
import { selectors as progressSelectors } from 'contests-progress-dux';
import { constants } from 'contests-utils';
const { KEY_CODES } = constants;

import {
    getDaysDifference,
    getGoalLabelSelector
} from '../selectors';

export default function getNoProgressProps( state, program ) {
    const { programViewId } = program;
    const goal = progressSelectors.getProgramViewGoal( state, programViewId );

    const daysDifference = getDaysDifference( program );

    return {
        tileType: 'no-progress',

        /* Goal Label */
        levelName: get( goal, 'paxGoalSelection.name', '' ),
        levelDescription: get( goal, 'paxGoalSelection.description', '' ),
        goalLabel: getGoalLabelSelector( programViewId )( state ),
        /* footer*/
        buttonTextCode: KEY_CODES.setyourplan,
        footerMessageCode: KEY_CODES.onyourmark,

        daysLeft: daysDifference.programEndDate,
        calendarLabel: 'progress.daysToEarnLabel'
    };
}
