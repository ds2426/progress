import { NO_MESSAGE_CODE } from './constants';

const MESSAGE_EARNED = { key: 'finalResults.selector.dashboard.message.earned', cssClass: 'you-earned' };
const MESSAGE_NOT_ACHIEVED = { key: 'finalResults.selector.dashboard.message.goalNotAchieved', cssClass: 'did-not-earn' };
const MESSAGE_MISS_MQ = { key: 'finalResults.selector.dashboard.message.missedMQ', cssClass: 'did-not-earn' };
export function getResultMessageCode( resultCode ) {
    switch ( resultCode ) {
        case 'YX': // goal achieved, no mq
        case 'YY': // goal achieved, met mq
            return MESSAGE_EARNED;
        case 'NX': // did not achieve goal, no mq
        case 'NY': // goal unachieved, met mq
        case 'NN': // goal unachieved, missed mq
            return MESSAGE_NOT_ACHIEVED;
        case 'YN': // goal achieved, missed mq
            return MESSAGE_MISS_MQ;
        default:
            return NO_MESSAGE_CODE;
    }
}

const SUB_NOT_ACHIEVED = { key: 'finalResults.selector.dashboard.subMessage.notAchieveGoal', cssClass: 'sub-message-black' };
const SUB_MISS_MQ = { key: 'finalResults.selector.dashboard.subMessage.achieveGoalMissMQ', cssClass: 'sub-message-black' };
export function getResultSubmessageCode( resultCode ) {
    switch ( resultCode ) {
        case 'NY': // goal unachieved, met mq
        case 'NN': // goal unachieved, missed mq
        case 'NX': // did not achieve goal, no mq
            return SUB_NOT_ACHIEVED;
        case 'YN': // goal achieved, missed mq
            return SUB_MISS_MQ;
        default:
            return NO_MESSAGE_CODE;
    }
}

const FOOTER_SUCCESS = { key: 'finalResults.selector.dashboard.message.goalSuccess' };
const FOOTER_BETTER_LUCK = { key: 'finalResults.selector.dashboard.message.betterLuck' };
export function getFooterMessageCode( resultCode ) {
    switch ( resultCode ) {
        case 'YX': // goal achieved, no mq
        case 'YY': // goal achieved, met mq
            return FOOTER_SUCCESS;
        case 'NX': // goal unachieved, no mq
        case 'YN': // goal achieved, missed mq
        case 'NN': // goal unachieved, missed mq
        case 'NY': // goal unachieved, met mq
            return FOOTER_BETTER_LUCK;
        default:
            return NO_MESSAGE_CODE;
    }
}
