import { selectors as progressSelectors } from 'contests-progress-dux';
import getFinalResultProps from '../../participant/final-result-props';

describe( 'Participant final result messaging',  () => {
    describe( 'goalquest results', () => {
        let props;
        let componentProps;
        let caseCount = 0;
        beforeEach( () => {
            props = {
                program: {
                    programType: 'goalquest',
                    programName: 'foo',
                    programViewId: 'something'
                }
            };
        } );

        describe( 'Case: Achieved Goal', () => {
            [ 'YXXX' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'you-earned', key: 'finalResults.selector.dashboard.message.earned' },
                        resultSubmessageCode: { key: '' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.goalSuccess'
                    } ) );
                } );
            } );
        } );

        describe( 'Case: Did not achieve Goal', () => {
            [ 'NXXX' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'did-not-earn', key: 'finalResults.selector.dashboard.message.goalNotAchieved' },
                        resultSubmessageCode: { cssClass: 'sub-message-black', key: 'finalResults.selector.dashboard.subMessage.notAchieveGoal' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.betterLuck'
                    } ) );
                } );
            } );
        } );

        describe( 'Case: Achieved Goal, Missed MQ', () => {
            [ 'YNXX' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'did-not-earn', key: 'finalResults.selector.dashboard.message.missedMQ' },
                        resultSubmessageCode: { cssClass: 'sub-message-black', key: 'finalResults.selector.dashboard.subMessage.achieveGoalMissMQ' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.betterLuck'
                    } ) );
                } );
            } );
        } );

        describe( 'Case: Achieved Goal, Met MQ', () => {
            [ 'YYXX' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'you-earned', key: 'finalResults.selector.dashboard.message.earned' },
                        resultSubmessageCode: { key: '' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.goalSuccess'
                    } ) );
                } );
            } );
        } );

        describe( 'Case: Did not achieve Goal, Missed MQ', () => {
            [ 'NNXX', 'NYXX' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'did-not-earn', key: 'finalResults.selector.dashboard.message.goalNotAchieved' },
                        resultSubmessageCode: { cssClass: 'sub-message-black', key: 'finalResults.selector.dashboard.subMessage.notAchieveGoal' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.betterLuck'
                    } ) );
                } );
            } );
        } );

        describe( 'Case: Achieved Goal, Achieved Bonus', () => {
            [ 'YXYY', 'YXYN' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'you-earned', key: 'finalResults.selector.dashboard.message.earned' },
                        resultSubmessageCode: { key: '' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.goalSuccess'
                    } ) );
                } );
            } );
        } );

        describe( 'Case: Achieved Goal, Did not Achieve Bonus', () => {
            [ 'YXNY', 'YXNN' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'you-earned', key: 'finalResults.selector.dashboard.message.earned' },
                        resultSubmessageCode: { key: '' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.goalSuccess'
                    } ) );
                } );
            } );
        } );

        describe( 'Case: Did not achieve Goal, Achieved Bonus (bonus not coupled with goal)', () => {
            [ 'NXYN' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'you-earned', key: 'finalResults.selector.dashboard.message.earned' },
                        resultSubmessageCode: { cssClass: 'sub-message-green', key: 'finalResults.selector.dashboard.subMessage.missGoalEarnBonus' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.bonusSuccess'
                    } ) );
                } );
            } );
        } );

        describe( 'Case: Did not achieve Goal, Achieved Bonus (bonus coupled with goal) does not earn', () => {
            [ 'NXYY' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'did-not-earn', key: 'finalResults.selector.dashboard.message.goalNotAchieved' },
                        resultSubmessageCode: { cssClass: 'sub-message-black', key: 'finalResults.selector.dashboard.subMessage.notAchieveGoal' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.betterLuck'
                    } ) );
                } );
            } );
        } );

        describe( 'Case: Did not achieve Goal, Did not achieve Bonus', () => {
            [ 'NXNY', 'NXNN' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'did-not-earn', key: 'finalResults.selector.dashboard.message.goalNotAchieved' },
                        resultSubmessageCode: { cssClass: 'sub-message-black', key: 'finalResults.selector.dashboard.subMessage.missGoalMissBonus' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.betterLuck'
                    } ) );
                } );
            } );
        } );

        describe( 'Case: Achieved Goal, Met MQ, Achieved Bonus', () => {
            [ 'YYYN', 'YYYY' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'you-earned', key: 'finalResults.selector.dashboard.message.earned' },
                        resultSubmessageCode: { key: '' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.goalSuccess'
                    } ) );
                } );
            } );
        } );

        describe( 'Case: Achieved Goal, Missed MQ, Achieved Bonus (bonus not coupled with goal)', () => {
            [ 'YNYN' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'you-earned', key: 'finalResults.selector.dashboard.message.earned' },
                        resultSubmessageCode: { cssClass: 'sub-message-green', key: 'finalResults.selector.dashboard.subMessage.missMQEarnBonus' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.bonusSuccess'
                    } ) );
                } );
            } );
        } );

        describe( 'Case: Achieved Goal, Missed MQ, Achieved Bonus (bonus coupled with goal)does not earn', () => {
            [ 'YNYY' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'did-not-earn', key: 'finalResults.selector.dashboard.message.missedMQ' },
                        resultSubmessageCode: { cssClass: 'sub-message-black', key: 'finalResults.selector.dashboard.subMessage.achieveGoalMissMQ' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.betterLuck'
                    } ) );
                } );
            } );
        } );

        describe( 'Case: Achieved Goal, Met MQ, Did not achieve Bonus', () => {
            [ 'YYNY', 'YYNN' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'you-earned', key: 'finalResults.selector.dashboard.message.earned' },
                        resultSubmessageCode: { key: '' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.goalSuccess'
                    } ) );
                } );
            } );
        } );

        describe( 'Case: Achieved Goal, Missed MQ, Did not Achieve Bonus', () => {
            [ 'YNNY', 'YNNN' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'did-not-earn', key: 'finalResults.selector.dashboard.message.missedMQ' },
                        resultSubmessageCode: { cssClass: 'sub-message-black', key: 'finalResults.selector.dashboard.subMessage.achieveGoalMissMQ' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.betterLuck'
                    } ) );
                } );
            } );
        } );

        describe( 'Case: Did Not Achieve Goal, Met MQ, Achieved Bonus (bonus not coupled with goal)', () => {
            [ 'NYYN' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'you-earned', key: 'finalResults.selector.dashboard.message.earned' },
                        resultSubmessageCode: { cssClass: 'sub-message-green', key: 'finalResults.selector.dashboard.subMessage.missGoalEarnBonus' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.bonusSuccess'
                    } ) );
                } );
            } );
        } );

        describe( 'Case: Did Not Achieve Goal, Met MQ, Achieved Bonus (bonus coupled with goal) does not earn', () => {
            [ 'NYYY' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'did-not-earn', key: 'finalResults.selector.dashboard.message.goalNotAchieved' },
                        resultSubmessageCode: { cssClass: 'sub-message-black', key: 'finalResults.selector.dashboard.subMessage.notAchieveGoal' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.betterLuck'
                    } ) );
                } );
            } );
        } );

        describe( 'Case: Did Not Achieve Goal, Missed MQ, Achieved Bonus (bonus not coupled with goal)', () => {
            [ 'NNYN' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'you-earned', key: 'finalResults.selector.dashboard.message.earned' },
                        resultSubmessageCode: { cssClass: 'sub-message-green', key: 'finalResults.selector.dashboard.subMessage.missGoalEarnBonus' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.bonusSuccess'
                    } ) );
                } );
            } );
        } );

        describe( 'Case: Did Not Achieve Goal, Missed MQ, Achieved Bonus (bonus coupled with goal) does not earn', () => {
            [ 'NNYY' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'did-not-earn', key: 'finalResults.selector.dashboard.message.goalNotAchieved' },
                        resultSubmessageCode: { cssClass: 'sub-message-black', key: 'finalResults.selector.dashboard.subMessage.notAchieveGoal' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.betterLuck'
                    } ) );
                } );
            } );
        } );

        describe( 'Case: Did Not Achieve Goal, Missed MQ, Did Not Achieve Bonus', () => {
            [ 'NNNN', 'NNNY' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'did-not-earn', key: 'finalResults.selector.dashboard.message.goalNotAchieved' },
                        resultSubmessageCode: { cssClass: 'sub-message-black', key: 'finalResults.selector.dashboard.subMessage.notAchieveGoal' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.betterLuck'
                    } ) );
                } );
            } );
        } );

        describe( 'Case: Did Not Achieve Goal, Met MQ, Did Not Achieve Bonus', () => {
            [ 'NYNN', 'NYNY' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'did-not-earn', key: 'finalResults.selector.dashboard.message.goalNotAchieved' },
                        resultSubmessageCode: { cssClass: 'sub-message-black', key: 'finalResults.selector.dashboard.subMessage.notAchieveGoal' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.betterLuck'
                    } ) );
                } );
            } );
        } );

        it( 'should have tested 30 goalquest result codes', () => {
            expect( caseCount ).toEqual( 30 );
        } );
    } );

    describe( 'challengepoint results', () => {
        let props;
        let componentProps;
        let caseCount = 0;
        beforeEach( () => {
            props = {
                program: {
                    programType: 'challengepoint',
                    programName: 'foo',
                    programViewId: 'something'
                }
            };
        } );

        describe( 'Case: Achieved Goal, Earned Base Earnings', () => {
            [ 'YYXX' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'you-earned', key: 'finalResults.selector.dashboard.message.earned' },
                        resultSubmessageCode: { key: '' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.goalSuccess'
                    } ) );
                } );
            } );
        } );

        describe( 'Case: Did not Achieve Goal, Earned Base Earnings', () => {
            [ 'NYXX' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'did-not-earn', key: 'finalResults.selector.dashboard.message.goalNotAchieved' },
                        resultSubmessageCode: { cssClass: 'sub-message-green', key: 'finalResults.selector.dashboard.subMessage.notAchieveGoalButGotPoints' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.goodEffort'
                    } ) );
                } );
            } );
        } );

        describe( 'Case: Did not Achieve Goal, Did not Earn Base Earnings', () => {
            [ 'NNXX' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'did-not-earn', key: 'finalResults.selector.dashboard.message.goalNotAchieved' },
                        resultSubmessageCode: { cssClass: 'sub-message-black', key: 'finalResults.selector.dashboard.subMessage.notAchieveGoal' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.betterLuck'
                    } ) );
                } );
            } );
        } );

        describe( 'Case: Achieved Goal, Earned Base Earnings, Achieved Bonus', () => {
            [ 'YYYN', 'YYYY' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'you-earned', key: 'finalResults.selector.dashboard.message.earned' },
                        resultSubmessageCode: { key: '' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.goalSuccess'
                    } ) );
                } );
            } );
        } );

        describe( 'Case: Achieved Goal, Earned Base Earnings, Did not Achieve Bonus', () => {
            [ 'YYNN', 'YYNY' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'you-earned', key: 'finalResults.selector.dashboard.message.earned' },
                        resultSubmessageCode: { key: '' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.goalSuccess'
                    } ) );
                } );
            } );
        } );
       describe( 'Case: Did not Achieve Goal, Did not Earn Base Earnings, Did not achieve Bonus', () => {
            [ 'NNNN', 'NNNY' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'did-not-earn', key: 'finalResults.selector.dashboard.message.goalNotAchieved' },
                        resultSubmessageCode: { cssClass: 'sub-message-black', key: 'finalResults.selector.dashboard.subMessage.notAchieveGoal' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.betterLuck'
                    } ) );
                } );
            } );
        } );

        describe( 'Case: Did not Achieve Goal, Earned Base Earnings, did not Achieve Bonus', () => {
            [ 'NYNN', 'NYNY' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'did-not-earn', key: 'finalResults.selector.dashboard.message.goalNotAchieved' },
                        resultSubmessageCode: { cssClass: 'sub-message-green', key: 'finalResults.selector.dashboard.subMessage.notAchieveGoalButGotPoints' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.goodEffort'
                    } ) );
                } );
            } );
        } );

        describe( 'Case: Did not Achieve Goal, Earned Base Earnings, Achieve Bonus (bonus coupled with goal) does not earn', () => {
            [ 'NYYY' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'did-not-earn', key: 'finalResults.selector.dashboard.message.goalNotAchieved' },
                        resultSubmessageCode: { cssClass: 'sub-message-green', key: 'finalResults.selector.dashboard.subMessage.notAchieveGoalButGotPoints' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.goodEffort'
                    } ) );
                } );
            } );
        } );

        describe( 'Case: Did not Achieve Goal, Earned Base Earnings, Achieve Bonus (bonus not coupled with goal)', () => {
            [ 'NYYN' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'did-not-earn', key: 'finalResults.selector.dashboard.message.goalNotAchieved' },
                        resultSubmessageCode: { cssClass: 'sub-message-green', key: 'finalResults.selector.dashboard.subMessage.missGoalEarnBase' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.goodEffort'
                    } ) );
                } );
            } );
        } );

        describe( 'Case: Did not Achieve Goal, Did not earn Base Earnings, Achieve Bonus (bonus coupled with goal) does not earn', () => {
            [ 'NNYY' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'did-not-earn', key: 'finalResults.selector.dashboard.message.goalNotAchieved' },
                        resultSubmessageCode: { cssClass: 'sub-message-black', key: 'finalResults.selector.dashboard.subMessage.notAchieveGoal' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.betterLuck'
                    } ) );
                } );
            } );
        } );

        describe( 'Case: Did not Achieve Goal, Did not earn Base Earnings, Achieve Bonus (bonus not coupled with goal)', () => {
            [ 'NNYN' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'did-not-earn', key: 'finalResults.selector.dashboard.message.goalNotAchieved' },
                        resultSubmessageCode: { cssClass: 'sub-message-black', key: 'finalResults.selector.dashboard.subMessage.notAchieveGoal' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.bonusSuccess'
                    } ) );
                } );
            } );
        } );

        it( 'should have tested 15 challengepoint result codes', () => {
            expect( caseCount ).toEqual( 15 );
        } );
    } );

    describe( 'objective results', () => {
        let props;
        let componentProps;
        let caseCount = 0;
        beforeEach( () => {
            props = {
                program: {
                    programType: 'objective',
                    programName: 'foo',
                    programViewId: 'something'
                }
            };
        } );

        describe( 'Case: Achieved Goal', () => {
            [ 'YX' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'you-earned', key: 'finalResults.selector.dashboard.message.earned' },
                        resultSubmessageCode: { key: '' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.goalSuccess'
                    } ) );
                } );
            } );
        } );

        describe( 'Case: Goal Achieved, Met Minimum Qualifier', () => {
            [ 'YY' ].forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'you-earned', key: 'finalResults.selector.dashboard.message.earned' },
                        resultSubmessageCode: { key: '' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.goalSuccess'
                    } ) );
                } );
            } );
        } );

        describe( 'Case: Did Not Achieve Goal', () => {
                [ 'NN' ].forEach( ( code ) => {
                    caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'did-not-earn', key: 'finalResults.selector.dashboard.message.goalNotAchieved' },
                        resultSubmessageCode: { cssClass: 'sub-message-black', key: 'finalResults.selector.dashboard.subMessage.notAchieveGoal' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.betterLuck'
                    } ) );
                } );
            } );
        } );

        describe( 'Case: Goal Achieved, Missed Minimum Qualifier', () => {
            const POSSIBLE_CODES = [ 'YN' ];
            POSSIBLE_CODES.forEach( ( code ) => {
                caseCount = caseCount + 1;
                it( `should return expected values for a code of ${code}`, () => {
                    spyOn( progressSelectors, 'getResultCode' ).and.returnValue( code );
                    componentProps = getFinalResultProps( {}, props.program );
                    expect( componentProps ).toEqual( expect.objectContaining( {
                        resultMessageCode: { cssClass: 'did-not-earn', key: 'finalResults.selector.dashboard.message.missedMQ' },
                        resultSubmessageCode: { cssClass: 'sub-message-black', key: 'finalResults.selector.dashboard.subMessage.achieveGoalMissMQ' },
                        footerMessageCode: 'finalResults.selector.dashboard.message.betterLuck'
                    } ) );
                } );
            } );
        } );

        it( 'should have tested 4 challengepoint result codes', () => {
            expect( caseCount ).toEqual( 4 );
        } );
    } );
} );
