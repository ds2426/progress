import { constants } from 'contests-utils';
import * as goalquestCodes from './goalquest-messages';
import * as challengepointCodes from './challengepoint-messages';
import * as objectivesCodes from './objectives-messages';
import { NO_MESSAGE_CODE } from './constants';


const {
    PT__GQ,
    PT__CP,
    PT__OBJ
} = constants;

export function getResultMessageCode( programType, resultCode ) {

    switch ( programType ) {
        case PT__GQ:
            return goalquestCodes.getResultMessageCode( resultCode );
        case PT__CP:
            return challengepointCodes.getResultMessageCode( resultCode );
        case PT__OBJ:
            return objectivesCodes.getResultMessageCode( resultCode );
    }

    return NO_MESSAGE_CODE;
}

export function getResultSubmessageCode( programType, resultCode ) {
    switch ( programType ) {
        case PT__GQ:
            return goalquestCodes.getResultSubmessageCode( resultCode );
        case PT__CP:
            return challengepointCodes.getResultSubmessageCode( resultCode );
        case PT__OBJ:
            return objectivesCodes.getResultSubmessageCode( resultCode );
    }

    return NO_MESSAGE_CODE;
}

export function getFooterMessageCode( programType, resultCode ) {
    switch ( programType ) {
        case PT__GQ:
            return goalquestCodes.getFooterMessageCode( resultCode );
        case PT__CP:
            return challengepointCodes.getFooterMessageCode( resultCode );
        case PT__OBJ:
            return objectivesCodes.getFooterMessageCode( resultCode );
    }

    return NO_MESSAGE_CODE;
}
