import get from 'lodash/get';
import memoize from 'lodash/memoize';
import { createSelector } from 'reselect';
import isAfter from 'date-fns/is_after';
import parseDate from 'date-fns/parse';
import differenceInDays from 'date-fns/difference_in_days';
// actionCreators
import { selectors as progressSelectors } from 'contests-progress-dux';
import { constants } from 'contests-utils';
const {
    PVS__PROGRESS_LOADED_NO_ISSUANCE,
    PVS__NO_PROGRESS,
    ROLE__SELECTOR,
    PT__OBJ
} = constants;


const selectorRoleRegex = new RegExp( `^${ROLE__SELECTOR}$`, 'i' );

export const isObjectives = ( { programType } ) => programType === PT__OBJ;
export const isManager = ( { role } ) => !selectorRoleRegex.test( role );
export const getProgramHasEnded = program => isAfter( new Date(), parseDate( program.endDate || Date.now() ) );
export const getProgramHasStarted = program => isAfter( new Date(), parseDate( program.startDate || Date.now() ) );
export const getIsCheckingResults = program => program.programViewStatus === PVS__PROGRESS_LOADED_NO_ISSUANCE && getProgramHasEnded( program );
export const getIsWaitingForProgress = program => program.programViewStatus === PVS__NO_PROGRESS && getProgramHasStarted( program );
export const getDaysDifference = program => ( {
    goalSelectionToDate: differenceInDays( parseDate( program.goalSelectionToDate ), new Date() ) + 1,
    programStartDate: differenceInDays( parseDate ( program.startDate ), new Date() ) + 1,
    programEndDate: differenceInDays( parseDate( program.endDate ), new Date() ) + 1
} );

export const getPercent = ( count, total ) => {
    const percent = Math.round( ( count / total ) * 100 );
    return isNaN( percent ) ? 0 : percent;
};

export const getGoalLabelSelector = memoize( programViewId => createSelector(
    progressSelectors.getGoalSelector( programViewId ),
    goal => ( {
        amount: get( goal, 'paxGoalSelection.goalAchieveAmount', 0 ),
        labelType: get( goal, 'goalLabel.labelType', '' ),
        labelText: get( goal, 'goalLabel.labelText', '' ),
        labelPosition: get( goal, 'goalLabel.labelPosition', '' ),
        currencyCode: get( goal, 'goalLabel.currencyCode', '' ),
        currencySymbol: get( goal, 'goalLabel.currencySymbol', '' ),
        currencyPosition: get( goal, 'goalLabel.currencyPosition', '' ),
        precision: get( goal, 'goalLabel.precision', 0 ),
        rouningMethod: get( goal, 'goalLabel.roundingMethod', '' )
    } )
) );
