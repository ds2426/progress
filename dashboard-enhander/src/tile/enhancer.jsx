import { connect }  from 'react-redux';
import isBefore from 'date-fns/is_before';
import parseDate from 'date-fns/parse';
// actionCreators
import { selectors as progressSelectors } from 'contests-progress-dux';
import { constants } from 'contests-utils';
const {
    PVS__GOAL_SELECTION,
    PVS__NO_PROGRESS,
    PVS__NO_GOAL_SELECTED,
    PVS__GOAL_SELECTED,
    PVS__PROGRESS_LOADED_NO_ISSUANCE,
    PVS__FINAL_ISSUANCE,
} = constants;

import getManagerFinalResultProps from './manager/final-result-props';
import getManagerGoalSelectionProps from './manager/goal-selection-props';
import getManagerNoProgressProps from './manager/no-progress-props';
import getManagerOverviewProps from './manager/overview-props';
import getManagerProgressProps from './manager/progress-props';

import getFinalResultProps from './participant/final-result-props';
import getProgramOverviewProps from './participant/program-overview-props';
import getGoalSelectedProps from './participant/goal-selected-props';
import getNoGoalSelectedProps from './participant/no-goal-selected-props';
import getNoProgressProps from './participant/no-progress-props';
import getProgressProps from './participant/progress-props';
import getWaitingForFinalResultsProps from './participant/waiting-for-final-results-props';

import {
    isObjectives,
    isManager,
    getProgramHasEnded,
    getProgramHasStarted,
    getIsCheckingResults
} from './selectors';

const getTileProps = ( state, program ) => {
    const now = Date.now();
    const {
        programViewStatus = '',
        startDate = now,
    } = program;

    const managerGoalSelectionStates = [ PVS__GOAL_SELECTION, PVS__GOAL_SELECTED ];
    if ( isManager( program ) ) {
        if ( isObjectives( program ) && ( programViewStatus === PVS__NO_PROGRESS || isBefore( now, parseDate( startDate ) ) ) ) {
            return getManagerOverviewProps( state, program );
        } else if ( managerGoalSelectionStates.includes( programViewStatus ) ) {
            return getManagerGoalSelectionProps( state, program );
        } else if ( programViewStatus === PVS__NO_PROGRESS ) {
            return getManagerNoProgressProps( state, program );
        } else if ( programViewStatus === PVS__FINAL_ISSUANCE ) {
            return getManagerFinalResultProps( state, program );
        } else {
            return getManagerProgressProps( state, program );
        }
    }

    switch ( programViewStatus ) {
        case PVS__GOAL_SELECTION:
            return getProgramOverviewProps( state, program );
        case PVS__NO_GOAL_SELECTED:
            return getNoGoalSelectedProps( state, program );
        case PVS__GOAL_SELECTED:
            return getGoalSelectedProps( state, program );
        case PVS__NO_PROGRESS:
            if ( isObjectives( program ) && isBefore( now, parseDate ( startDate ) ) )  {
                return getProgramOverviewProps ( state, program );
            }
            return getNoProgressProps( state, program );
        case PVS__PROGRESS_LOADED_NO_ISSUANCE:
            return getIsCheckingResults( program ) ?
                getWaitingForFinalResultsProps( state, program ) :
                getProgressProps( state, program );
        case PVS__FINAL_ISSUANCE:
            return getFinalResultProps( state, program );
    }

    // default if nothing is found so that we show *something*
    return getProgramOverviewProps( state, program );
};

export const mapStateToProps = ( state, { program = {} } ) => {
    const { programViewId } = program;
    return {
        program: program,
        programType: program.programType,
        programName: program.programName,
        programLogo: program.programLogo,
        programViewId: program.programViewId,
        programViewStatus: program.programViewStatus,
        heroImageUrl: program.heroImageUrl,
        endDate: program.endDate,
        startDate: program.startDate,
        goalSelectionToDate: program.goalSelectionToDate,
        lastDayLeftDate: program.endDate,
        isOwnerEligibleForAward: program.isOwnerEligibleForAward,
        isObjectives: isObjectives( program ),
        isManager: isManager( program ),

        programHasEnded: getProgramHasEnded( program ),
        programHasStarted: getProgramHasStarted( program ),
        checkingResults: getIsCheckingResults( program ),

        finalResult: progressSelectors.getProgramViewFinalResults( state, programViewId ),
        goal: progressSelectors.getProgramViewGoal( state, programViewId ),
        progressLastUpdated: progressSelectors.getProgramViewProgressLastUpdated( state, programViewId ),
        progressLoading: progressSelectors.getProgressIsFetching( state, programViewId ),
        hasProgressData: Object.keys( progressSelectors.getProgramViewProgressData( state, programViewId ) ).length > 0,
        finalResultCode: progressSelectors.getResultCode( state, programViewId ),
        progressLastUpdatedCode: 'programInfoHeader.dataThrough',

        ...getTileProps( state, program )
    };
};

export default connect( mapStateToProps );
