import {
    isObjectives,
    getProgramHasStarted,
    getDaysDifference,
} from '../selectors';

import { constants } from 'contests-utils';
const { KEY_CODES } = constants;

export default function getManagerOverviewProps( state, program ) {
    const hasStarted = getProgramHasStarted( program );
    const daysDifference = getDaysDifference( program );
    const objectiveHasStarted = hasStarted && isObjectives( program );

    return {
        tileType: 'manager-overview',
        programOverviewCode: hasStarted ? KEY_CODES.waitingForProgress : KEY_CODES.checkoutYourTeamsGoals,

        footerMessageCode: hasStarted ? KEY_CODES.timeToMotivate : KEY_CODES.getReady,
        buttonTextCode: hasStarted ? KEY_CODES.viewDetails : KEY_CODES.learnAboutProgram,

        daysLeft: ( objectiveHasStarted ? daysDifference.programEndDate : daysDifference.programStartDate ) || 0,
        calendarLabel: objectiveHasStarted ? 'progress.daysToEarnLabel' : 'progress.daysToProgramStartLabel',
        lastDayLeftDate: hasStarted ? program.endDate : program.startDate,
    };
}
