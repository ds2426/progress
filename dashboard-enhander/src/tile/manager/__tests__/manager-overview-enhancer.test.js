import addDays from 'date-fns/add_days';
import addHours from 'date-fns/add_hours';
import forEach from 'lodash/forEach';

import m1Obj from '@biw/test-utils/fixtures/m1obj';
import m2Obj from '@biw/test-utils/fixtures/m2obj';

import createTestStore from '@biw/test-utils/test-store';
import progressReducer, {
    ROOT_KEY as PROGRESS,
    actions as progressActions
} from 'contests-progress-dux';
import programsReducer, {
    ROOT_KEY as PROGRAMS,
    actions as programsActions
} from '@biw/programs-dux';

import { mapStateToProps } from '../../enhancer';

const dateNow = new Date( Date.now() );
const dateFromNow = days => addHours( addDays( dateNow, days ), days > 0 ? 1 : -1 ).toISOString();

//Objectives
describe( 'Objective Manager Tile: ', () => {

    describe( 'Situation #1 Manager Tile: Program Not Started', () => {
        const endDate = dateFromNow( 30 );
        const startDate = dateFromNow( 20 );

        const store = createTestStore( {
            [ PROGRESS ]: progressReducer,
            [ PROGRAMS ]: programsReducer
        } );
        store.dispatch( progressActions.setProgramViewProgressData( m1Obj.programViewId, m1Obj.progress ) );
        const program = {
            ...m1Obj.program,
            heroImageUrl: null,
            startDate,
            endDate
        };
        store.dispatch( programsActions.setProgramViews( [ program ] ) );

        const componentProps = mapStateToProps( store.getState(), { program } );
        forEach( {
            programViewId: '11249---multiowner',
            programName: 'Program #1: Objectives Standard - Not Started',
            tileType: 'manager-overview',
            programLogo: 'https://apipprd.farm1.honeycombsvc.com/hcapi/participantMedia/bugathon/logo/1516289129065_logo.png',
            footerMessageCode: 'dashboard.tile.common.getReady',
            buttonTextCode: 'dashboard.tile.common.learnAboutProgram',

            calendarLabel: 'progress.daysToProgramStartLabel',
            daysLeft: 21,
            lastDayLeftDate: startDate,

            programHasEnded: false,
            heroImageUrl: null,

            programOverviewCode: 'dashboard.tile.common.checkoutYourTeamsGoals',

            progressLastUpdated: expect.anything(),
            progressLastUpdatedCode: 'programInfoHeader.dataThrough',
        }, ( val, key ) => {
            it( `should have a prop called '${key}' with a value of '${val}'`, () => {
                expect( componentProps[ key ] ).toEqual( val );
            } );
        } );
    } );

    describe( 'Situation #2 Tile: Manager Progress with No Progress', () => {
        const store = createTestStore( {
            [ PROGRESS ]: progressReducer,
            [ PROGRAMS ]: programsReducer
        } );
        store.dispatch( progressActions.setProgramViewProgressData( m2Obj.programViewId, m2Obj.progress ) );
        const program = {
            ...m2Obj.program,
            heroImageUrl: null,
            endDate: dateFromNow( 20 ),
            programName: 'Program #2: Objectives Standard - No Progress',
            programViewId: '11692---multiowner',
        };
        store.dispatch( programsActions.setProgramViews( [ program ] ) );

        const componentProps = mapStateToProps( store.getState(), { program } );
        forEach( {
            tileType: 'manager-overview',
            programHasEnded: false,

            programViewId: '11692---multiowner',
            programLogo: 'https://apipprd.farm1.honeycombsvc.com/hcapi/participantMedia/bugathon/logo/1516289129065_logo.png',
            footerMessageCode: 'dashboard.tile.manager.timeToMotivate',
            buttonTextCode: 'dashboard.tile.common.viewDetails',
            progressLastUpdated: expect.anything(),

            calendarLabel: 'progress.daysToEarnLabel',
            daysLeft: 21,
            lastDayLeftDate: dateFromNow( 20 ),

            programName: 'Program #2: Objectives Standard - No Progress',
            heroImageUrl: null,
            programOverviewCode: 'dashboard.tile.common.waitingForProgress',

            progressLastUpdatedCode: 'programInfoHeader.dataThrough',
        }, ( val, key ) => {
            it( `should have a prop called '${key}' with a value of '${val}'`, () => {
                expect( componentProps[ key ] ).toEqual( val );
            } );
        } );
    } );
} );
