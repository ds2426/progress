import addDays from 'date-fns/add_days';
import addHours from 'date-fns/add_hours';
import forEach from 'lodash/forEach';

import m5fixture from '@biw/test-utils/fixtures/m5';
import m5Objfixture from '@biw/test-utils/fixtures/m5obj';

import { constants } from 'contests-utils';
const { KEY_CODES } = constants;

import createTestStore from '@biw/test-utils/test-store';
import progressReducer, {
    ROOT_KEY as PROGRESS,
    actions as progressActions
} from 'contests-progress-dux';
import programsReducer, {
    ROOT_KEY as PROGRAMS,
    actions as programsActions
} from '@biw/programs-dux';

import { mapStateToProps } from '../../enhancer';

const dateNow = new Date( Date.now() );
const dateFromNow = days => addHours( addDays( dateNow, days ), days > 0 ? 1 : -1 ).toISOString();

describe( 'Manager Tile: ', () => {
    describe( 'Situation #5: Manager Final Results', () => {
        const endDate = dateFromNow( -1 );
        const goalSelectionToDate = dateFromNow( -10 );

        const store = createTestStore( {
            [ PROGRESS ]: progressReducer,
            [ PROGRAMS ]: programsReducer
        } );
        store.dispatch( progressActions.setProgramViewProgressData( m5fixture.programViewId, m5fixture.progress ) );
        const program = {
            ...m5fixture.program,
            endDate,
            goalSelectionToDate
        };
        store.dispatch( programsActions.setProgramViews( [ program ] ) );

        const componentProps = mapStateToProps( store.getState(), { program } );
        forEach( {
            tileType: 'manager-final-result',
            programViewId: '5023---Owner',
            buttonTextCode: KEY_CODES.seeResults,
            programName: 'Sales Race #3',
            count: 3,
            total: 12,
            percent: 25,
            progressLastUpdated: expect.anything(),
            progressLastUpdatedCode: 'programInfoHeader.dataThrough',
            programLogo: 'https://api.farm1.honeycombsvc.com/hcapi/participantMedia/demo0322/logo/1497031430795_logo.png',
            heroImageUrl: null,
            totalNumberOfPoints: 600,
            mediaLabel: 'AwardPoints',
            earnedOverride: true,
        }, ( val, key ) => {
            it( `should have a prop called '${key}' with a value of '${val}'`, () => {
                expect( componentProps[ key ] ).toEqual( val );
            } );
        } );
    } );
} );

//Objectives
describe( 'Objective Manager Tile: ', () => {
    describe( 'Situation #5: Manager Final Results', () => {
        const endDate = dateFromNow( -1 );
        const goalSelectionToDate = dateFromNow( -10 );

        const store = createTestStore( {
            [ PROGRESS ]: progressReducer,
            [ PROGRAMS ]: programsReducer
        } );
        store.dispatch( progressActions.setProgramViewProgressData( m5Objfixture.programViewId, m5Objfixture.progress ) );
        const program = {
            ...m5Objfixture.program,
            endDate,
            goalSelectionToDate
        };
        store.dispatch( programsActions.setProgramViews( [ program ] ) );

        const componentProps = mapStateToProps( store.getState(), { program } );
        forEach( {
            tileType: 'manager-final-result',
            programViewId: '11314---multiowner',
            buttonTextCode: KEY_CODES.seeResults,
            programName: 'Program #3: Objectives Standard - Final',
            count: 0,
            total: 125,
            percent: 0,
            progressLastUpdated: expect.anything(),
            progressLastUpdatedCode: 'programInfoHeader.dataThrough',
            programLogo: expect.any( String ),
            heroImageUrl: 'https://apiqa.farm1.honeycombsvc.com/hcapi/participantMedia/linda/logo/1524676108166_logo.png',
            totalNumberOfPoints: 0,
            mediaLabel: 'AwardPoints',
            earnedOverride: false,
        }, ( val, key ) => {
            it( `should have a prop called '${key}' with a value of '${val}'`, () => {
                expect( componentProps[ key ] ).toEqual( val );
            } );
        } );
    } );
} );
