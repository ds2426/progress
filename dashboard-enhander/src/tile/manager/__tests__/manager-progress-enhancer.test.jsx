import addDays from 'date-fns/add_days';
import addHours from 'date-fns/add_hours';
import forEach from 'lodash/forEach';

import m3fixture from '@biw/test-utils/fixtures/m3';
import m3Obj from '@biw/test-utils/fixtures/m3obj';

import { constants } from 'contests-utils';
const { KEY_CODES } = constants;

import createTestStore from '@biw/test-utils/test-store';
import progressReducer, {
    ROOT_KEY as PROGRESS,
    actions as progressActions
} from 'contests-progress-dux';
import programsReducer, {
    ROOT_KEY as PROGRAMS,
    actions as programsActions
} from '@biw/programs-dux';

import { mapStateToProps } from '../../enhancer';

const dateNow = new Date( Date.now() );
const dateFromNow = days => addHours( addDays( dateNow, days ), days > 0 ? 1 : -1 ).toISOString();

describe( 'Manager Tile: ', () => {
    describe( 'Situation #3: Manager Progress with Progress Loaded', () => {
        const endDate = dateFromNow( 10 );

        const store = createTestStore( {
            [ PROGRESS ]: progressReducer,
            [ PROGRAMS ]: programsReducer
        } );
        store.dispatch( progressActions.setProgramViewProgressData( m3fixture.programViewId, m3fixture.progress ) );
        const program = {
            ...m3fixture.program,
            endDate
        };
        store.dispatch( programsActions.setProgramViews( [ program ] ) );

        const componentProps = mapStateToProps( store.getState(), { program } );

        forEach( {
            tileType: 'manager-progress',
            programViewId: '5022---Owner',
            heroImageUrl: null,
            programHasEnded: false,
            statusCode: KEY_CODES.participantsOnTrack,
            footerMessageCode: KEY_CODES.timeToMotivate,
            progressLastUpdatedCode: 'programInfoHeader.dataThrough',
            buttonTextCode: KEY_CODES.viewDetails,
            programName: 'Sales Race #2',
            count: 7,
            total: 12,
            percent: 58,
            progressLastUpdated: expect.anything(),
            programLogo: 'https://api.farm1.honeycombsvc.com/hcapi/participantMedia/demo0322/logo/1497031387759_logo.png',
            calendarLabel: 'progress.daysToEarnLabel',
            daysLeft: 11,
            lastDayLeftDate: endDate
        }, ( val, key ) => {
            it( `should have a prop called '${key}' with a value of '${val}'`, () => {
                expect( componentProps[ key ] ).toEqual( val );
            } );
        } );
    } );

    describe( 'Situation #4: Manager Progress Waiting on Final Results', () => {
        const endDate = dateFromNow( -1 );
        const goalSelectionToDate = dateFromNow( -10 );

        const store = createTestStore( {
            [ PROGRESS ]: progressReducer,
            [ PROGRAMS ]: programsReducer
        } );
        store.dispatch( progressActions.setProgramViewProgressData( m3fixture.programViewId, m3fixture.progress ) );
        const program = {
            ...m3fixture.program,
            endDate,
            goalSelectionToDate
        };
        store.dispatch( programsActions.setProgramViews( [ program ] ) );

        const componentProps = mapStateToProps( store.getState(), { program } );

        forEach( {
            tileType: 'manager-progress',
            programViewId: '5022---Owner',
            heroImageUrl: null,
            programHasEnded: true,
            statusCode: KEY_CODES.participantsOnTrack,
            footerMessageCode: KEY_CODES.checkingResults,
            progressLastUpdatedCode: 'programInfoHeader.dataThrough',
            buttonTextCode: KEY_CODES.viewDetails,
            programName: 'Sales Race #2',
            count: 7,
            total: 12,
            percent: 58,
            progressLastUpdated: expect.anything(),
            programLogo: 'https://api.farm1.honeycombsvc.com/hcapi/participantMedia/demo0322/logo/1497031387759_logo.png',

            calendarLabel: 'dashboard.tile.common.waitingForFinal',
            daysLeft: 0,
            lastDayLeftDate: endDate
        }, ( val, key ) => {
            it( `should have a prop called '${key}' with a value of '${val}'`, () => {
                expect( componentProps[ key ] ).toEqual( val );
            } );
        } );
    } );
} );

//Objectives
describe( 'Objective Manager Tile: ', () => {

    describe( 'Situation #3 Tile: Manager Progress - Progress', () => {
        const endDate = dateFromNow( 20 );

        const store = createTestStore( {
            [ PROGRESS ]: progressReducer,
            [ PROGRAMS ]: programsReducer
        } );
        store.dispatch( progressActions.setProgramViewProgressData( m3Obj.programViewId, m3Obj.progress ) );
        const program = {
            ...m3Obj.program,
            endDate,
            heroImageUrl: null
        };
        store.dispatch( programsActions.setProgramViews( [ program ] ) );

        const componentProps = mapStateToProps( store.getState(), { program } );

        forEach( {
            tileType: 'manager-progress',
            programName: 'Program #2: Objectives Standard - progress',
            programViewId: '11292---multiowner',
            programHasEnded: false,

            programLogo: 'https://apipprd.farm1.honeycombsvc.com/hcapi/participantMedia/bugathon/logo/1515146171853_logo.png',
            footerMessageCode: 'dashboard.tile.manager.timeToMotivate',
            buttonTextCode: 'dashboard.tile.common.viewDetails',

            calendarLabel: 'progress.daysToEarnLabel',
            daysLeft: 21,
            lastDayLeftDate: dateFromNow( 20 ),

            count: 82,
            total: 125,
            percent: 66,
            progressLastUpdatedCode: 'programInfoHeader.dataThrough',
            progressLastUpdated: expect.anything(),
            statusCode: 'dashboard.tile.manager.participantsOnTrack',
            heroImageUrl: null,
            checkingResults: false,
        }, ( val, key ) => {
            it( `should have a prop called '${key}' with a value of '${val}'`, () => {
                expect( componentProps[ key ] ).toEqual( val );
            } );
        } );
    } );
} );
