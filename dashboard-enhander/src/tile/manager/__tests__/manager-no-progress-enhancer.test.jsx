import addDays from 'date-fns/add_days';
import addHours from 'date-fns/add_hours';
import forEach from 'lodash/forEach';

import m2fixture from '@biw/test-utils/fixtures/m2';
import { constants } from 'contests-utils';
const { KEY_CODES } = constants;

import createTestStore from '@biw/test-utils/test-store';
import progressReducer, {
    ROOT_KEY as PROGRESS,
    actions as progressActions
} from 'contests-progress-dux';
import programsReducer, {
    ROOT_KEY as PROGRAMS,
    actions as programsActions
} from '@biw/programs-dux';

import { mapStateToProps } from '../../enhancer';

const dateNow = new Date( Date.now() );
const dateFromNow = days => addHours( addDays( dateNow, days ), days > 0 ? 1 : -1 ).toISOString();

describe( 'Manager Tile: ', () => {
    describe( 'Situation #2: Manager Progress with No Progress', () => {
        const startDate = dateFromNow( -2 );
        const endDate = dateFromNow( 20 );

        const store = createTestStore( {
            [ PROGRESS ]: progressReducer,
            [ PROGRAMS ]: programsReducer
        } );
        store.dispatch( progressActions.setProgramViewProgressData( m2fixture.programViewId, m2fixture.progress ) );
        const program = {
            ...m2fixture.program,
            startDate,
            endDate
        };
        store.dispatch( programsActions.setProgramViews( [ program ] ) );

        const componentProps = mapStateToProps( store.getState(), { program } );

        forEach( {
            programViewId: '5049---Owner',
            tileType: 'manager-no-progress',
            heroImageUrl: null,
            programHasEnded: false,
            statusCode: 'dashboard.tile.common.selectedGoal',
            footerMessageCode: KEY_CODES.timeToMotivate,
            progressLastUpdatedCode: 'programInfoHeader.dataThrough',
            buttonTextCode: KEY_CODES.viewDetails,
            programName: 'Make the Grade #1',
            count: 1,
            total: 12,
            percent: 8,
            progressLastUpdated: expect.anything(),
            programLogo: 'https://api.farm1.honeycombsvc.com/hcapi/participantMedia/demo0531/logo/1497647161239_logo.png',
            calendarLabel: 'progress.daysToEarnLabel',
            daysLeft: 21,
            lastDayLeftDate: dateFromNow( 20 ),
        }, ( val, key ) => {
            it( `should have a prop called '${key}' with a value of '${val}'`, () => {
                expect( componentProps[ key ] ).toEqual( val );
            } );
        } );
    } );
} );
