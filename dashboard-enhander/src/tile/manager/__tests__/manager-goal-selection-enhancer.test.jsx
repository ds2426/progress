import addDays from 'date-fns/add_days';
import addHours from 'date-fns/add_hours';
import forEach from 'lodash/forEach';

import m1fixture from '@biw/test-utils/fixtures/m1';

import { constants } from 'contests-utils';
const { KEY_CODES } = constants;

import createTestStore from '@biw/test-utils/test-store';
import progressReducer, {
    ROOT_KEY as PROGRESS,
    actions as progressActions
} from 'contests-progress-dux';
import programsReducer, {
    ROOT_KEY as PROGRAMS,
    actions as programsActions
} from '@biw/programs-dux';

import { mapStateToProps } from '../../enhancer';

const dateNow = new Date( Date.now() );
const dateFromNow = days => addHours( addDays( dateNow, days ), days > 0 ? 1 : -1 ).toISOString();

describe( 'Manager Tile: ', () => {
    describe( 'Situation #1: Manager During Goal Selection', () => {
        const startDate = dateFromNow( 20 );
        const goalSelectionToDate = dateFromNow( 20 );

        const store = createTestStore( {
            [ PROGRESS ]: progressReducer,
            [ PROGRAMS ]: programsReducer
        } );
        store.dispatch( progressActions.setProgramViewProgressData( m1fixture.programViewId, m1fixture.progress ) );
        const program = {
            ...m1fixture.program,
            startDate,
            goalSelectionToDate
        };
        store.dispatch( programsActions.setProgramViews( [ program ] ) );

        const componentProps = mapStateToProps( store.getState(), { program } );
        forEach( {
            programViewId: '5021---Owner',
            heroImageUrl: null,
            tileType: 'manager-goal-selection',
            statusCode: KEY_CODES.selectedGoal,
            footerMessageCode: KEY_CODES.getEveryoneToSelect,
            progressLastUpdatedCode: 'programInfoHeader.dataThrough',
            buttonTextCode: KEY_CODES.viewDetails,
            programName: 'Sales Race #1',
            count: 3,
            total: 12,
            percent: 25,
            progressLastUpdated: expect.anything(),
            programLogo: 'https://api.farm1.honeycombsvc.com/hcapi/participantMedia/demo0322/logo/1497030437575_logo.png',

            calendarLabel: 'goalSelection.toSelectGoal',
            daysLeft: 21,
            lastDayLeftDate: dateFromNow( 20 ),
        }, ( val, key ) => {
            it( `should have a prop called '${key}' with a value of '${val}'`, () => {
                expect( componentProps[ key ] ).toEqual( val );
            } );
        } );
    } );
} );
