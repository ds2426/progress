import { selectors as progressSelectors } from 'contests-progress-dux';

import {
    isObjectives,
    getProgramHasStarted,
    getDaysDifference,
    getPercent
} from '../selectors';

import { constants } from 'contests-utils';
const { KEY_CODES } = constants;

export default function getManagerGoalSelectionProps( state, program ) {
    const { programViewId } = program;
    const goal = progressSelectors.getProgramViewGoal( state, programViewId );
    const hasStarted = getProgramHasStarted( program );
    const daysDifference = getDaysDifference( program );
    const objectivesNotStarted = !hasStarted && isObjectives( program );
    const objectiveHasStarted = hasStarted && isObjectives( program );

    const selectionCount = goal.selectionCount || {};
    const count = selectionCount.goalSelectedCount || 0;
    const total = selectionCount.paxCount || 0;

    return {
        tileType: 'manager-goal-selection',
        lastDayLeftDate: program.startDate,

        statusCode: hasStarted ? KEY_CODES.participantsOnTrack : KEY_CODES.selectedGoal,

        total,
        count,
        percent: getPercent( count, total ),

        footerMessageCode: hasStarted ? KEY_CODES.getReady : KEY_CODES.getEveryoneToSelect,
        buttonTextCode: objectivesNotStarted ? KEY_CODES.learnAboutProgram : KEY_CODES.viewDetails,

        daysLeft: ( objectiveHasStarted ? daysDifference.programEndDate : daysDifference.programStartDate ) || 0,
        calendarLabel: 'goalSelection.toSelectGoal',
    };
}
