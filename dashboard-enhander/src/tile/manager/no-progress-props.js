import { selectors as progressSelectors } from 'contests-progress-dux';

import { getDaysDifference } from '../selectors';

import { constants } from 'contests-utils';
const { KEY_CODES } = constants;

export default function getManagerNoProgressProps( state, program ) {
    const { programViewId } = program;
    const goal = progressSelectors.getProgramViewGoal( state, programViewId );
    const daysDifference = getDaysDifference( program );

    const selectionCount = goal.selectionCount || {};
    const count = selectionCount.goalSelectedCount || 0;
    const total = selectionCount.paxCount || 0;
    const percent = selectionCount.goalSelectedPercentage || 0;

    return {
        tileType: 'manager-no-progress',

        total,
        count,
        percent,
        daysLeft: daysDifference.programEndDate || 0,

        statusCode: KEY_CODES.selectedGoal,
        footerMessageCode: KEY_CODES.timeToMotivate,
        buttonTextCode: KEY_CODES.viewDetails,
        calendarLabel: 'progress.daysToEarnLabel',
    };
}
