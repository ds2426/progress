import get from 'lodash/get';
import { selectors as progressSelectors } from 'contests-progress-dux';
import { constants } from 'contests-utils';
const { KEY_CODES } = constants;

import { getPercent } from '../selectors';

const EARNED_OVERRIDE_CODES = [ 'YNN', 'YYN', 'YNN', 'YYN', 'YNY', 'YYY' ];

export default function getManagerFinalResultProps( state, program ) {
    const { programViewId } = program;
    const finalResult = progressSelectors.getProgramViewFinalResults( state, programViewId );
    const finalResultCode = progressSelectors.getResultCode( state, programViewId );
    const goal = progressSelectors.getProgramViewGoal( state, programViewId );

    const count = finalResult.totalNumberofPaxWhoAchievedGoal || 0;
    const total = get( goal, 'participantOnTrack.paxCount' ) || 0;

    return {
        tileType: 'manager-final-result',
        total,
        count,
        percent: getPercent( count, total ),
        buttonTextCode: KEY_CODES.seeResults,
        totalNumberOfPoints: finalResult.totalNumberOfPoints || 0,
        mediaLabel: goal.mediaLabel || 'AwardPoints',
        earnedOverride: EARNED_OVERRIDE_CODES.indexOf( finalResultCode ) !== -1,
    };
}
