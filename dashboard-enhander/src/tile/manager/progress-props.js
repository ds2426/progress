import { selectors as progressSelectors } from 'contests-progress-dux';

import {
    getProgramHasEnded,
    getDaysDifference,
    getIsCheckingResults
} from '../selectors';

import { constants } from 'contests-utils';
const { KEY_CODES } = constants;

export default function getManagerGoalSelectionProps( state, program ) {
    const { programViewId } = program;
    const goal = progressSelectors.getProgramViewGoal( state, programViewId );
    const hasEnded = getProgramHasEnded( program );
    const daysDifference = getDaysDifference( program );
    const isCheckingResults = getIsCheckingResults( program );

    const participantOnTrack = goal.participantOnTrack || {};
    const count = participantOnTrack.onTrackCount || 0;
    const total = participantOnTrack.paxCount || 0;
    const percent = participantOnTrack.onTrackPct || 0;

    return {
        tileType: 'manager-progress',
        statusCode: KEY_CODES.participantsOnTrack,

        total,
        count,
        percent,
        footerMessageCode: isCheckingResults ? KEY_CODES.checkingResults : KEY_CODES.timeToMotivate,
        buttonTextCode: KEY_CODES.viewDetails,

        daysLeft: Math.abs( daysDifference.programEndDate || 0 ),
        calendarLabel: hasEnded ? 'dashboard.tile.common.waitingForFinal' : 'progress.daysToEarnLabel',
    };
}
