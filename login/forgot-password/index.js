// libs
import { connect } from 'react-redux';
// selectors
import { selectors as clientSelectors } from '@biw/client-dux';
// dux
import { forgotPasswordDux, forgotPasswordSelectors } from 'biw-login-dux';
import { fetchSelectors } from 'biw-client-fetch';
// component
import ForgotPassword from './forgot-password';

const { requestPassword, setMessage, setSuccess } = forgotPasswordDux;
const { getForgotPasswordErrorMessage, getForgotPasswordSuccess, getForgotPasswordSuccessMessage } = forgotPasswordSelectors;
const { getFetching } = fetchSelectors;

export const mapStateToProps = state => ( {
    busy: getFetching( state, 'forgotPassword' ),
    clientLogo: clientSelectors.getClientLogo( state ),
    errorMessage: getForgotPasswordErrorMessage( state ),
    successMessage: getForgotPasswordSuccessMessage( state ),
    success: getForgotPasswordSuccess( state )
} );

export const mapDispatchToProps = {
    requestPassword,
    setMessage,
    setSuccess
};

export default connect( mapStateToProps, mapDispatchToProps )( ForgotPassword );
