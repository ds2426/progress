import { mapStateToProps, mapDispatchToProps } from '../index';

describe( 'Forgot Password Connect', () => {
    it ( 'gets passed the required props when connected with react-redux.', () => {
        const result = mapStateToProps( {} );

        expect( result.busy ).toBeDefined();
        expect( result.clientLogo ).toBeDefined();
        expect( result.success ).toBeDefined();
        expect( result.errorMessage ).toBeDefined();
        expect( result.successMessage ).toBeDefined();
    } );

    it( 'has the appropriate props for mapDispatchToProps', () => {
        expect( mapDispatchToProps.requestPassword ).toBeDefined();
        expect( mapDispatchToProps.setMessage ).toBeDefined();
        expect( mapDispatchToProps.setSuccess ).toBeDefined();
    } );
} );


