import React from 'react';
import { mount, shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import { LoaderButton } from 'shared';
import ForgotPassword from '../forgot-password';

const defaultProps = {
    busy: false,
    success: false,
    errorMessage: false,
    successMessage: '',
    requestPassword: jest.fn(),
    setMessage: jest.fn(),
    requestPassword: jest.fn(),
    setSuccess: jest.fn(),
};

describe( 'Forgot Password Component', () => {

    it( 'renders the dom like the snapshot when the props have default values.', () => {
        const props = {
            ...defaultProps
        };

        const component = renderer.create( <ForgotPassword { ...props } /> );
        const tree = component.toJSON();

        expect( tree ).toMatchSnapshot();
    } );

    it ( 'disables the request password button when a username and valid email address have not been entered.', () => {
        const props = {
            ...defaultProps
        };

        const tree = shallow( <ForgotPassword { ...props } /> );

        const button = tree.find( LoaderButton );

        expect ( button.props().disabled ).toBe( true );

        tree.setState( { emailAddress: 'test' } );

        expect ( button.props().disabled ).toBe( true );

        tree.setState( { username: 'test' } );

        expect ( button.props().disabled ).toBe( true );
    } );

    it ( 'enables the request password button when a username and valid email address have been entered.', () => {
        const props = {
            ...defaultProps
        };

        const tree = mount( <ForgotPassword { ...props } /> );

        expect ( tree.find( LoaderButton ).props().disabled ).toBe( true );

        tree.setState( { emailAddress: 'test@test.com' } );

        expect ( tree.find( LoaderButton ).props().disabled ).toBe( true );

        tree.setState( { username: 'test' } );

        expect ( tree.find( LoaderButton ).props().disabled ).toBe( false );
    } );

    it ( 'changes emailAddress in state when the user changes something in the email address input ', () => {
        const props = {
            ...defaultProps
        };

        const tree = shallow( <ForgotPassword { ...props } /> );

        const input = tree.find( 'input[name="emailAddress"]' );

        input.simulate( 'change', { target: { name: input.props().name, value: 'test' } } );

        expect ( tree.state( 'emailAddress' ) ).toBe( 'test' );
    } );

    it ( 'changes username in state when the user changes something in the username input ', () => {
        const props = {
            ...defaultProps
        };

        const tree = shallow( <ForgotPassword { ...props } /> );

        const input = tree.find( 'input[name="username"]' );

        input.simulate( 'change', { target: { name: input.props().name, value: 'test' } } );

        expect ( tree.state( 'username' ) ).toBe( 'test' );
    } );

    it ( 'reset fields when componentWillReceiveProps is called  ', () => {
        const props = {
            ...defaultProps
        };

        const tree = shallow( <ForgotPassword { ...props } /> );

        tree.setState( { emailAddress: 'test@test.com', username: 'test-111' } );

        tree.setProps( { success: true } );

        expect( tree.state().emailAddress ).toBe( '' );

        expect( tree.state().username ).toBe( '' );

    } );

    it ( 'does not reset fields when componentWillReceiveProps is called with false value ', () => {
        const props = {
            ...defaultProps,
            success: true
        };


        const tree = shallow( <ForgotPassword { ...props } /> );

        tree.setState( { emailAddress: 'test@test.com', username: 'test-123' } );

        tree.setProps( { success: false } );

        expect( tree.state().emailAddress ).toBe( 'test@test.com' );

        expect( tree.state().username ).toBe( 'test-123' );

    } );

    it ( 'executes requestPassword prop when the request username button is clicked.', () => {
        const props = {
            ...defaultProps,
            requestPassword: jest.fn()
        };

        const tree = mount( <ForgotPassword { ...props } /> );

        const button = tree.find( LoaderButton );

        tree.setState( { emailAddress: 'test@test.com', username: 'test' } );

        button.simulate( 'click' );

        expect ( props.requestPassword.mock.calls.length ).toBe( 1 );
    } );

    it ( 'executes requestPassword prop when the user presses enter in the email address input with a valid email address.', () => {
        const props = {
            ...defaultProps,

            requestPassword: jest.fn()
        };

        const tree = shallow( <ForgotPassword { ...props } /> );

        tree.setState( { emailAddress: 'test@test.com', 'username': 'test' } );

        const emailInput = tree.find( 'input[name="emailAddress"]' );
        const usernameInput = tree.find( 'input[name="username"]' );

        emailInput.simulate( 'keyPress', { charCode: 13 } );

        expect ( props.requestPassword.mock.calls.length ).toBe( 1 );

        usernameInput.simulate( 'keyPress', { charCode: 13 } );

        expect ( props.requestPassword.mock.calls.length ).toBe( 2 );
    } );

    it ( 'does not execute requestPassword prop when the user presses enter in the email address input with an empty username or invalid email address.', () => {
        const props = {
            ...defaultProps,

            requestPassword: jest.fn()
        };

        const tree = shallow( <ForgotPassword { ...props } /> );

        const input = tree.find( 'input[name="emailAddress"]' );

        tree.setState( { emailAddress: 'test@test.com' } );

        input.simulate( 'keyPress', { charCode: 13 } );

        expect ( props.requestPassword.mock.calls.length ).toBe( 0 );

        tree.setState( { emailAddress: 'test', username: 'test' } );

        input.simulate( 'keyPress', { charCode: 13 } );

        expect ( props.requestPassword.mock.calls.length ).toBe( 0 );
    } );

    it ( 'shows error alerts for all errors.', () => {
        const props = {
            ...defaultProps,
            errorMessage: { text: 'error' },
            setMessage: jest.fn()
        };

        const tree = mount( <ForgotPassword { ...props } /> );

        const alerts = tree.find( '.alert-error' );

        expect ( alerts.length ).toBe( 1 );
    } );

    it ( 'executes setMessage prop for all errors when the component is unmounted.', () => {
        const props = {
            ...defaultProps,
            errorMessage: { text: 'test' },
            setMessage: jest.fn()
        };

        const tree = mount( <ForgotPassword { ...props } /> );

        const button = tree.find( '.alert-error .close-btn' );

        button.simulate( 'click' );

        expect ( props.setMessage.mock.calls.length ).toBe( 1 );
    } );

    it ( 'executes setMessage and setSuccess props for success when the component is unmounted.', () => {
        const props = {
            ...defaultProps,
            successMessage: 'test',
            success: true,
            setMessage: jest.fn(),
            setSuccess: jest.fn()
        };

        const tree = shallow( <ForgotPassword { ...props } /> );

        tree.unmount();

        expect ( props.setMessage.mock.calls.length ).toBe( 1 );
        expect ( props.setSuccess.mock.calls.length ).toBe( 1 );
    } );

    it ( 'executes setMessage prop for all errors when request password button is clicked.', () => {
        const props = {
            ...defaultProps,
            errorMessage: { text: 'test' },
            setMessage: jest.fn(),
            requestPassword: jest.fn()
        };

        const tree = mount( <ForgotPassword { ...props } /> );

        const button = tree.find( LoaderButton );

        tree.setState( { emailAddress: 'test@test.com', username: 'test' } );

        button.simulate( 'click' );

        expect ( props.setMessage.mock.calls.length ).toBe( 1 );
    } );

    it ( 'executes setMessage prop when an error alert is closed.', () => {
        const props = {
            ...defaultProps,
            errorMessage: { text: 'test' },
            setMessage: jest.fn()
        };

        const tree = mount( <ForgotPassword { ...props } /> );

        const button = tree.find( '.alert-error .close-btn' );

        button.simulate( 'click' );

        expect ( props.setMessage.mock.calls.length ).toBe( 1 );
    } );

    it ( 'shows a success alert when there is a success message.', () => {
        const props = {
            ...defaultProps,
            successMessage: 'test',
            success: true,
            setMessage: jest.fn()
        };

        const tree = mount( <ForgotPassword { ...props } /> );

        const alerts = tree.find( '.alert-success' );

        expect ( alerts.length ).toBe( 1 );
    } );

    it ( 'executes setMessage prop when an success alert is closed.', () => {
        const props = {
            ...defaultProps,
            success: true,
            successMessage: 'test',
            setMessage: jest.fn(),
            setSuccess: jest.fn()
        };

        const tree = mount( <ForgotPassword { ...props } /> );

        const button = tree.find( '.close-btn' );

        button.simulate( 'click' );

        expect ( props.setMessage.mock.calls.length ).toBe( 1 );
    } );

    it ( 'doesnot executes setMessage and setSuccess prop when success props is false ', () => {
        const props = {
            ...defaultProps,
            setMessage: jest.fn(),
            success: false
        };

        const tree = shallow( <ForgotPassword { ...props } /> );

        const input = tree.find( 'input[name="emailAddress"]' );

        input.simulate( 'change', { target: { name: input.props().name, value: 'Test' } } );

        expect ( props.setMessage.mock.calls.length ).toBe( 0 );

        expect ( tree.state( 'emailAddress' ) ).toBe( 'Test' );
    } );

    it ( 'executes setMessage and setSuccess props when success props is true ', () => {
        const props = {
            ...defaultProps,
            setMessage: jest.fn(),
            setSuccess: jest.fn(),
            success: true
        };

        const tree = shallow( <ForgotPassword { ...props } /> );

        const input = tree.find( 'input[name="emailAddress"]' );

        input.simulate( 'change', { target: { name: input.props().name, value: 'test' } } );

        expect ( props.setMessage.mock.calls.length ).toBe( 1 );
        expect ( props.setSuccess.mock.calls.length ).toBe( 1 );

        expect ( tree.state( 'emailAddress' ) ).toBe( 'test' );
    } );

} );
