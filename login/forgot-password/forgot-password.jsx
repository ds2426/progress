// libs
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import isEmailValid from 'utils/email-validator';
import { LocaleString } from '@biw/intl';
// shared components
import { Alert } from 'component-library';
import { LoaderButton, NarrowForm, CancelButton } from 'shared';
import ErrorMessage from 'routes/login/shared/error-message';
import './forgot-password.scss';

export default class ForgotPassword extends React.Component {

    static propTypes = {
        busy: PropTypes.bool.isRequired,
        clientLogo: PropTypes.string,
        errorMessage: PropTypes.oneOfType( [ PropTypes.object, PropTypes.bool ] ).isRequired,
        successMessage: PropTypes.oneOfType( [ PropTypes.string, PropTypes.bool ] ).isRequired,
        success: PropTypes.bool.isRequired,
        requestPassword: PropTypes.func.isRequired,
        setMessage: PropTypes.func.isRequired,
        setSuccess: PropTypes.func.isRequired
    }

    state = {
        emailAddress: '',
        username: '',
        focusedField: ''
    }

    componentDidUpdate( prevProps ) {
        if ( !prevProps.success && this.props.success ) {
            this.setState( { emailAddress: '', username: '' } );
        }
    }

    componentWillUnmount() {
        this.dismissSuccess();
        this.dismissError();
    }

    handleInputChange = event => {
        const name = event.target.name;
        this.setState( { [ name ]: event.target.value } );

        this.dismissSuccess();
    };

    submit = () => {
        const { emailAddress, username } = this.state;
        this.props.requestPassword( { emailAddress, userName: username } );

        this.dismissError();
    };

    checkEnter = event => {
        const enterPressed = event.charCode === 13;
        const { emailAddress, username } = this.state;

        if ( enterPressed && username.length && isEmailValid( emailAddress )  ) {
            this.submit();
        }
    };

    dismissError = () => {
        if ( this.props.errorMessage ) {
            this.props.setMessage( 'error', false );
        }
    }

    dismissSuccess = () => {
        if ( this.props.success === true ) {
            this.props.setMessage( 'success', false );
            this.props.setSuccess( false );
        }
    }

    render() {
        const {
            emailAddress,
            focusedField,
            username
        } = this.state;
        const {
            busy,
            clientLogo,
            errorMessage,
            successMessage,
            success
        } = this.props;

        const emailFieldClasses = classNames( {
            'email-address-field': true,
            'input-wrap': true,
            'focused': focusedField === 'emailAddress' || !!emailAddress
        } );

        const userFieldClasses = classNames( {
            'username-container': true,
            'input-wrap': true,
            'focused': focusedField === 'username' || !!username
        } );

        return (
            <NarrowForm page="forgot-password" headerTxt="forgotPassword.form.headerText" clientLogo={ clientLogo } errorsExist={ !!errorMessage }>
                <div className={ emailFieldClasses }>
                    <label htmlFor="emailAddress" className="control-label">
                        <LocaleString code="common.emailAddressLabel" />
                    </label>
                    <input
                        type="text"
                        name="emailAddress"
                        tabIndex="1"
                        id="emailAddress"
                        className="gq-input"
                        value={ emailAddress }
                        autoComplete="off"
                        onChange={ this.handleInputChange }
                        onFocus={ this.handleInputFocus }
                        onBlur={ this.handleInputBlur }
                        onKeyPress={ this.checkEnter } />
                </div>

                <div className={ userFieldClasses }>
                    <label htmlFor="username" className="control-label">
                        <LocaleString code="common.userIdLabel" />
                    </label>
                    <input
                        type="text"
                        name="username"
                        id="username"
                        className="gq-input"
                        value={ username }
                        autoComplete="off"
                        onChange={ this.handleInputChange }
                        onFocus={ this.handleInputFocus }
                        onBlur={ this.handleInputBlur }
                        onKeyPress={ this.checkEnter } />
                </div>

                {
                    errorMessage
                    &&
                    <Alert type="error" onDismiss={ () => this.props.setMessage( 'error', false ) }>
                        <p>
                            <ErrorMessage errorMessage={ errorMessage } localeCode="errors.generic.global" />
                        </p>
                    </Alert>
                }

                {
                    successMessage
                    &&
                    <Alert type="success" onDismiss={ this.dismissSuccess }>
                        <p><LocaleString code={ successMessage } /></p>
                    </Alert>
                }

                <LoaderButton
                    fetching={ busy }
                    handleClick={ this.submit }
                    disabled={ !username.length || !isEmailValid( emailAddress ) }
                    done={ success }
                    clientBranding
                    >

                    <LocaleString code="forgotPassword.form.sendEmailButtonLabel" />
                </LoaderButton>
                <CancelButton />
            </NarrowForm>
        );
    }
}
