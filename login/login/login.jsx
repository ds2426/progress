// libs
import React from 'react';
import PropTypes from 'prop-types';
import Link from 'redux-first-router-link';
import classNames from 'classnames';
import { LocaleString } from '@biw/intl';
// shared components
import { Alert } from 'component-library';
import { LoaderButton, NarrowForm } from 'shared';
import PasswordInput from 'shared/password-input';
import LanguageSelector from 'shared/language-selector';
//utils
import { getNewSignedRequest, inSalesforce } from 'utils/salesforce-util';
// scss
import './login.scss';
import ErrorMessage from 'routes/login/shared/error-message';

const localeShape = PropTypes.shape( {
    code: PropTypes.string
} );

export default class Login extends React.Component {
    static propTypes = {
        localeOptions: PropTypes.arrayOf( localeShape ),
        locale: localeShape,
        overrideUserLanguage: PropTypes.bool, // eslint-disable-line react/no-unused-prop-types
        userLocaleSetting: PropTypes.string, // eslint-disable-line react/no-unused-prop-types
        busyFetchingLocaleBundle: PropTypes.bool,
        selectLanguage: PropTypes.func.isRequired,
        clientLogo: PropTypes.string.isRequired,
        errorMessage: PropTypes.oneOfType( [ PropTypes.bool, PropTypes.object ] ),
        clearErrors: PropTypes.func.isRequired,
        userIdLabel: PropTypes.string.isRequired,
        busy: PropTypes.bool.isRequired,
        success: PropTypes.bool.isRequired,
        login: PropTypes.func.isRequired,
        setSuccess: PropTypes.func.isRequired,
        setMessage: PropTypes.func.isRequired,
        accountLocked: PropTypes.bool
    }

    static defaultProps = {
        busyFetchingLocaleBundle: false,
        overrideUserLanguage: false,
        accountLocked: false
    }

    state = {
        username: '',
        password: '',
        passwordVisible: false
    }

    componentWillUnmount() {
        this.dismissSuccess();
        this.props.clearErrors();
    }

    handleInputChange = event => {
        const name = event.target.name;
        this.setState( { [ name ]: event.target.value } );
    }

    onLoginSubmit = () => {
        const { username, password } = this.state;
        if ( !this.props.accountLocked && username.length && password.length ) {
            // If we are manually logging in and we're inside a Canvas app, then we
            // need to submit a signed request for mapping the SF user to the GQ account
            // currently logging in.
            if ( inSalesforce( window ) ) {
                return getNewSignedRequest( window )
                    .then( signedRequest => {
                        const mapping = { type: 'salesforce',  signedRequest };
                        this.props.login( { username, password, mapping } );
                    } );
            }

            this.props.login( { username, password } );
            this.props.clearErrors();
        }
    }

    handleSubmit = e => {
        e.preventDefault();
        this.onLoginSubmit();
    }

    dismissSuccess = () => {
        if ( this.props.success ) {
            this.props.setMessage( 'success', false );
            this.props.setSuccess( false );
        }
    }

    handleChangeLocale = locale => this.props.selectLanguage( locale.code );

    render() {
        const { username, password } = this.state;
        const {
            accountLocked,
            locale,
            localeOptions,
            clientLogo,
            success,
            errorMessage,
            busy,
            userIdLabel,
            busyFetchingLocaleBundle,
            clearErrors
        } = this.props;

        const userLabelClassName = classNames( 'control-label', { 'has-value': !!username } );

        return (
            <NarrowForm page="login" headerTxt="header.login" clientLogo={ clientLogo } errorsExist={ !!errorMessage }>
                <form onSubmit={this.handleSubmit}>
                    <div className="input-wrap locale-selector">
                        <LanguageSelector
                            id="language"
                            key="languageSelectorMenu"
                            options = { localeOptions }
                            selectedLocale={ locale }
                            onChange={ this.handleChangeLocale }
                            disabled={busyFetchingLocaleBundle}
                             />
                            <LocaleString key="languageSelectorLabel" TextComponent="label" className="control-label has-value" htmlFor="language" code="profile.languageLabel" />
                    </div>
                    <div className="input-wrap username-field">
                        <Link to="/forgot-user-id" className="forgot-link login-input-label client-link">
                            <LocaleString code="login.forgotIdLabel" replaceOptions={ { userIdLabel } } />
                        </Link>
                        <input
                            type="text"
                            name="username"
                            id="formUserId"
                            className="gq-input"
                            value={ username }
                            autoComplete="off"
                            onChange={ this.handleInputChange }
                        />
                        <label htmlFor="formUserId" className={userLabelClassName}>
                            <LocaleString code="common.userIdLabel" />
                        </label>
                    </div>

                    <div className="input-wrap password-field">
                        <Link to="/forgot-password" className="forgot-link login-input-label client-link">
                            <LocaleString code="login.forgotPwdLabel" />
                        </Link>
                        <PasswordInput
                            autoComplete="off"
                            id="formPassword"
                            disabled={accountLocked}
                            name="password"
                            value={ password }
                            onChange={ this.handleInputChange }
                            label="login.pwdLabel"
                        />
                    </div>

                    { errorMessage &&
                        <Alert type="error" onDismiss={ clearErrors } >
                            <ErrorMessage errorMessage={ errorMessage } localeCode="errors.generic.login" />
                        </Alert>
                    }

                    <p className="activation-line">
                        <LocaleString code="onload.noPasswordLabel" />
                        <Link className="client-link" to="/activation">&nbsp;<strong><LocaleString code="onload.activateLabel" /></strong></Link>
                    </p>

                    <input type="submit" hidden className="hidden" />

                    <LoaderButton
                        fetching={ busy }
                        handleClick={ this.onLoginSubmit }
                        disabled={ !username.length || !password.length || accountLocked }
                        done={ success }
                        clientBranding
                    >
                        <LocaleString code="login.loginBtnLabel" />
                    </LoaderButton>
                </form>
            </NarrowForm>
        );
    }
}
