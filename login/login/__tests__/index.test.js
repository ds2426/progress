import AppStore from 'app/store';
import { mapStateToProps } from '../index';

describe( 'Login connect', () => {

    describe( 'mapStateToProps', () => {
        const result = mapStateToProps( AppStore().getState() );
        [
            'success',
            'busy',
            'localeOptions',
            'locale',
            'busyFetchingLocaleBundle',
            'clientLogo',
            'errorMessage',
            'userIdLabel',
            'firstTimeSetupDone',
        ].forEach( ( name ) => {
            it( `should return something for ${name}`, () => {
                expect( result[ name ] ).toBeDefined();
            } );
        } );
    } );
} );


