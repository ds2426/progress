import React from 'react';
import { shallow, mount } from 'enzyme';
import renderer from 'react-test-renderer';
import Login from '../login';
import { LoaderButton } from 'shared';

const defaultProps = {
    authenticated: false,
    success: false,
    localeOptions: [],
    locale: { code: 'en-US', intlCode: 'en-US' },
    errorMessage: false,
    busy: false,
    clientLogo: '',
    preActivationLoginAttemptFlag: false,
    login: jest.fn(),
    setUserData: jest.fn(),
    setSuccess: jest.fn(),
    setMessage: jest.fn(),
    clearErrors: jest.fn(),
    routeTo: jest.fn(),
    selectLanguage: jest.fn(),
    userIdLabel: 'foo',
};

describe( 'Login', () => {

    it( 'disables the login button after 5 unsuccessful attempts', () => {
        const props = {
            ...defaultProps,
            errorCode: 10009
        };
        const tree = shallow( <Login { ...props }  /> );

        const button = tree.find( LoaderButton );

        tree.setState( { locked: true } );

        expect( button.props().disabled ).toBe( true );
    } );

    it( 'renders the dom like the snapshot when the props have default values.', () => {
        const props = {
            ...defaultProps
        };

        const component = renderer.create( <Login { ...props } /> );
        const tree = component.toJSON();

        expect( tree ).toMatchSnapshot();
    } );

    it ( 'executes login prop when the Login button is clicked.', () => {
        const props = {
            ...defaultProps,
            login: jest.fn()
        };

        const tree = mount( <Login { ...props } /> );

        const button = tree.find( LoaderButton );

        tree.setState( { username: 'test', password: 'Testing123!' } );

        button.simulate( 'click' );

        expect ( props.login.mock.calls.length ).toBe( 1 );
    } );

    it ( 'changes username in state when the user changes something in the user name input ', () => {
        const props = {
            ...defaultProps
        };

        const tree = mount( <Login { ...props } /> );

        const input = tree.find( 'input[name="username"]' );

        input.simulate( 'change', { target: { name: input.props().name, value: 'test' } } );

        expect ( tree.state( 'username' ) ).toBe( 'test' );
    } );

    it ( 'changes password in state when the user changes something in the password input ', () => {
        const props = {
            ...defaultProps
        };

        const tree = mount( <Login { ...props } /> );

        const input = tree.find( 'input[name="password"]' );

        input.simulate( 'change', { target: { name: input.props().name, value: 'Testing123!' } } );

        expect ( tree.state( 'password' ) ).toBe( 'Testing123!' );
    } );

    it ( 'disables the login button when username or password field has not been entered.', () => {
        const props = {
            ...defaultProps
        };

        const tree = mount( <Login { ...props } /> );

        const button = tree.find( LoaderButton );

        expect ( button.props().disabled ).toBe( true );

        tree.setState( { username: 'test' } );

        expect ( button.props().disabled ).toBe( true );
    } );

    it ( 'enables the login button when user enter user name and password fields.', () => {
        const props = {
            ...defaultProps
        };

        const tree = mount( <Login { ...props } /> );

        expect ( tree.find( LoaderButton ).props().disabled ).toBe( true );

        tree.setState( { username: 'test', password: 'Testing123!' } );

        expect ( tree.find( LoaderButton ).props().disabled ).toBe( false );
    } );

    it ( 'shows error alerts for all errors.', () => {
        const props = {
            ...defaultProps,
            errorMessage: { text: 'test' }
        };

        const tree = mount( <Login { ...props } /> );

        const alerts = tree.find( '.alert-error' );

        expect ( alerts.length ).toBe( 1 );
    } );

    it ( 'executes clearErrors prop when login error alert is closed.', () => {
        const props = {
            ...defaultProps,
            errorMessage: { text: 'test' },
            clearErrors: jest.fn()
        };

        const tree = mount( <Login { ...props } /> );

        const button = tree.find( '.alert-error .close-btn' );
        button.simulate( 'click' );

        expect ( props.clearErrors.mock.calls.length ).toBe( 1 );
    } );

    it ( 'dismiss all errors when the login button is clicked.', () => {
        const props = {
            ...defaultProps,
            errorMessage: { text: 'test' },
            clearErrors: jest.fn(),
            login: jest.fn()
        };

        const tree = mount( <Login { ...props } /> );

        const button = tree.find( LoaderButton );

        tree.setState( { username: 'test', password: 'Testing123!' } );

        button.simulate( 'click' );

        expect ( props.clearErrors.mock.calls.length ).toBe( 1 );

        expect ( props.login.mock.calls.length ).toBe( 1 );
    } );

    it ( 'execute login prop when the user presses enter in the password input.', () => {
        const props = {
            ...defaultProps,
            login: jest.fn()
        };

        const tree = mount( <Login { ...props } /> );

        tree.setState( { username: 'test', password: 'Testing123!' } );

        const form = tree.find( 'form' );
        form.simulate( 'submit' );

        expect ( props.login.mock.calls.length ).toBe( 1 );
    } );

    it ( 'does not execute login prop when the user presses enter in the username input with out text in password input.', () => {
        const props = {
            ...defaultProps,
            login: jest.fn()
        };

        const tree = mount( <Login { ...props } /> );

        tree.setState( { username: 'test' } );

        const form = tree.find( 'form' );
        form.simulate( 'submit' );

        expect ( props.login.mock.calls.length ).toBe( 0 );
    } );

    it ( 'does not execute login prop when the user presses enter in the password input with out text in user name input.', () => {
        const props = {
            ...defaultProps,
            login: jest.fn()
        };

        const tree = mount( <Login { ...props } /> );

        tree.setState( { password: 'Testing123!' } );

        const form = tree.find( 'form' );
        form.simulate( 'submit' );

        expect ( props.login.mock.calls.length ).toBe( 0 );
    } );

    it ( 'executes clearErrors prop when the component is unmounted.', () => {
        const props = {
            ...defaultProps,
            errorMessage: { text: 'test' },
            clearErrors: jest.fn()
        };

        const tree = mount( <Login { ...props } /> );

        tree.unmount();

        expect ( props.clearErrors.mock.calls.length ).toBe( 1 );
    } );

} );
