// libs
import React from 'react';
// shared components
import LoadingOverlay from 'components/shared/loading-overlay';

const LaunchAs = () => <LoadingOverlay loading />;

export default LaunchAs;
