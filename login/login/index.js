// libs
import { connect } from 'react-redux';
import get from 'lodash/get';
import { getString } from '@biw/intl';
// selectors
import {
    selectors as clientSelectors,
    types as clientTypes
} from '@biw/client-dux';
import { fetchSelectors } from 'biw-client-fetch';
// actionCreators
import { loginDux, loginSelectors } from 'biw-login-dux';
// selectors
import { getLanguageOverride, getLocaleCode } from 'app/dux/user/selectors';
import { setUserLocale } from 'routes/profile/profile-dux';
// main component
import Login from './login';

const { login, setSuccess, setMessage } = loginDux;
const { getLoginErrorMessage, getLoginSuccess, getLaunchAsErrorMessage, getLoginErrorCode } = loginSelectors;
const { getFetching } = fetchSelectors;

const clearErrors = () => dispatch => {
    dispatch( setMessage( 'loginError', false ) );
    dispatch( setMessage( 'launchAsError', false ) );
};

export const mapStateToProps = state => ( {
    localeOptions: clientSelectors.getLocaleList( state ),
    locale: clientSelectors.getUserLocale( state ),
    userLocaleSetting: getLocaleCode( state ),
    overrideUserLanguage: getLanguageOverride( state ),

    busyFetchingLocaleBundle: get( state, `data.fetching.${clientTypes.CLIENT_LOCALE_BUNDLE}`, false ),
    clientLogo: clientSelectors.getClientLogo( state ),

    errorMessage: getLoginErrorMessage( state ) || getLaunchAsErrorMessage( state ),
    userIdLabel: getString( state, 'common.userIdSentenceLabel' ),

    busy: getFetching( state, 'login' ),
    success: getLoginSuccess( state ),
    firstTimeSetupDone: get( state, 'user.firstTimeSetupDone', true ),
    accountLocked: getLoginErrorCode( state ) === 10009
} );

export const mapDispatchToProps = {
    login,
    clearErrors,
    setSuccess,
    setMessage,
    selectLanguage: setUserLocale
};

export default connect( mapStateToProps, mapDispatchToProps )( Login );
