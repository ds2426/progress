import React from 'react';
import { mount, shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import ForgotUserId from '../forgot-user-id';
import { LoaderButton } from 'shared';

const defaultProps = {
    busy: false,
    success: false,
    errorMessage: false,
    successMessage: '',
    getString: jest.fn(),
    setMessage: jest.fn(),
    setSuccess: jest.fn(),
    requestUsername: jest.fn()
};

describe( 'Forgot User Id Component', () => {

    it( 'renders the dom like the snapshot when the props have default values.', () => {
        const props = {
            ...defaultProps
        };

        const component = renderer.create( <ForgotUserId { ...props } /> );
        const tree = component.toJSON();

        expect( tree ).toMatchSnapshot();
    } );

    it ( 'disables the request username button when a valid email address has not been entered.', () => {
        const props = {
            ...defaultProps
        };

        const tree = shallow( <ForgotUserId { ...props } /> );

        const button = tree.find( LoaderButton );

        expect ( button.props().disabled ).toBe( true );

        tree.setState( { emailAddress: 'test' } );

        expect ( button.props().disabled ).toBe( true );
    } );

    it ( 'enables the request username button when a valid email address has been entered.', () => {
        const props = {
            ...defaultProps
        };

        const tree = mount( <ForgotUserId { ...props } /> );

        expect ( tree.find( LoaderButton ).props().disabled ).toBe( true );

        tree.setState( { emailAddress: 'test@test.com' } );

        expect ( tree.find( LoaderButton ).props().disabled ).toBe( false );
    } );

    it ( 'changes emailAddress in state when the user changes something in the email address input ', () => {
        const props = {
            ...defaultProps
        };

        const tree = shallow( <ForgotUserId { ...props } /> );

        const input = tree.find( 'input[name="emailAddress"]' );

        input.simulate( 'change', { target: { name: input.props().name, value: 'test' } } );

        expect ( tree.state( 'emailAddress' ) ).toBe( 'test' );
    } );

    it ( 'reset fields when componentWillReceiveProps is called  ', () => {
        const props = {
            ...defaultProps
        };

        const tree = shallow( <ForgotUserId { ...props } /> );

        tree.setState( { emailAddress: 'test@test.com' } );

        tree.setProps( { success: true } );

        expect( tree.state().emailAddress ).toBe( '' );
    } );

    it ( 'does not reset fields when componentWillReceiveProps is called with false value ', () => {
        const props = {
            ...defaultProps,
            success: true
        };

        const tree = shallow( <ForgotUserId { ...props } /> );

        tree.setState( { emailAddress: 'test@test.com' } );

        tree.setProps( { success: false } );

        expect( tree.state().emailAddress ).toBe( 'test@test.com' );
    } );

    it ( 'executes requestUsername prop when the request username button is clicked.', () => {
        const props = {
            ...defaultProps,
            requestUsername: jest.fn()
        };

        const tree = mount( <ForgotUserId { ...props } /> );

        const button = tree.find( LoaderButton );

        tree.setState( { emailAddress: 'test@test.com' } );

        button.simulate( 'click' );

        expect ( props.requestUsername.mock.calls.length ).toBe( 1 );
    } );

    it ( 'executes requestUsername prop when the user presses enter in the email address input with a valid email address.', () => {
        const props = {
            ...defaultProps,
            requestUsername: jest.fn()
        };

        const tree = shallow( <ForgotUserId { ...props } /> );

        tree.setState( { emailAddress: 'test@test.com' } );

        const input = tree.find( 'input[name="emailAddress"]' );

        input.simulate( 'keyPress', { charCode: 13 } );

        expect ( props.requestUsername.mock.calls.length ).toBe( 1 );
    } );

    it ( 'does not execute requestUsername prop when the user presses enter in the email address input with an invalid email address.', () => {
        const props = {
            ...defaultProps,
            requestUsername: jest.fn()
        };

        const tree = shallow( <ForgotUserId { ...props } /> );

        tree.setState( { emailAddress: 'test' } );

        const input = tree.find( 'input[name="emailAddress"]' );

        input.simulate( 'keyPress', { charCode: 13 } );

        expect ( props.requestUsername.mock.calls.length ).toBe( 0 );
    } );

    it ( 'shows error alerts for all errors.', () => {
        const props = {
            ...defaultProps,
            errorMessage: { text: 'test' }
        };

        const tree = mount( <ForgotUserId { ...props } /> );

        const alerts = tree.find( '.alert-error' );

        expect ( alerts.length ).toBe( 1 );
    } );

    it ( 'executes setMessage prop for all errors when the component is unmounted.', () => {
        const props = {
            ...defaultProps,
            errorMessage: { text: 'test' },
            setMessage: jest.fn()
        };

        const tree = shallow( <ForgotUserId { ...props } /> );

        tree.unmount();

        expect ( props.setMessage.mock.calls.length ).toBe( 1 );
    } );

    it ( 'executes setMessage and setSuccess props for all success and success messages when the component is unmounted.', () => {
        const props = {
            ...defaultProps,
            successMessage: 'success',
            setMessage: jest.fn(),
            setSuccess: jest.fn(),
            success: true
        };

        const tree = shallow( <ForgotUserId { ...props } /> );

        tree.unmount();

        expect ( props.setMessage.mock.calls.length ).toBe( 1 );
        expect ( props.setSuccess.mock.calls.length ).toBe( 1 );
    } );

    it ( 'executes setMessage prop for all errors when request username button is clicked.', () => {
        const props = {
            ...defaultProps,
            errorMessage: { text: 'test' },
            requestUsername: jest.fn(),
            setMessage: jest.fn()
        };

        const tree = mount( <ForgotUserId { ...props } /> );

        const button = tree.find( LoaderButton );

        tree.setState( { emailAddress: 'test@test.com' } );

        button.simulate( 'click' );

        expect ( props.setMessage.mock.calls.length ).toBe( 1 );
    } );

    it ( 'executes setMessage prop when an error alert is closed.', () => {
        const props = {
            ...defaultProps,
            errorMessage: { text: 'error' },
            setMessage: jest.fn()
        };

        const tree = mount( <ForgotUserId { ...props } /> );

        const button = tree.find( '.alert-error .close-btn' );

        button.simulate( 'click' );

        expect ( props.setMessage.mock.calls.length ).toBe( 1 );
    } );

    it ( 'shows a success alert when there is a success message.', () => {
        const props = {
            ...defaultProps,
            successMessage: 'test',
        };

        const tree = mount( <ForgotUserId { ...props } /> );

        const alerts = tree.find( '.alert-success' );

        expect ( alerts.length ).toBe( 1 );
    } );

    it ( 'executes setMessage prop when an success alert is closed.', () => {
        const props = {
            ...defaultProps,
            success: true,
            successMessage: 'test',
            setMessage: jest.fn(),
            setSuccess: jest.fn()
        };

        const tree = mount( <ForgotUserId { ...props } /> );

        const button = tree.find( '.close-btn' );

        button.simulate( 'click' );

        expect ( props.setMessage.mock.calls.length ).toBe( 1 );
    } );

    it ( 'doesnot executes setMessage prop when success props is false ', () => {
        const props = {
            ...defaultProps,
            setMessage: jest.fn(),
            success: false
        };

        const tree = shallow( <ForgotUserId { ...props } /> );

        const input = tree.find( 'input[name="emailAddress"]' );

        input.simulate( 'change', { target: { name: input.props().name, value: 'Test' } } );

        expect ( props.setMessage.mock.calls.length ).toBe( 0 );

        expect ( tree.state( 'emailAddress' ) ).toBe( 'Test' );
    } );

    it ( 'executes setMessage prop when success props is true ', () => {
        const props = {
            ...defaultProps,
            setMessage: jest.fn(),
            setSuccess: jest.fn(),
            success: true
        };

        const tree = shallow( <ForgotUserId { ...props } /> );

        const input = tree.find( 'input[name="emailAddress"]' );

        input.simulate( 'change', { target: { name: input.props().name, value: 'test' } } );

        expect ( props.setMessage.mock.calls.length ).toBe( 1 );

        expect ( tree.state( 'emailAddress' ) ).toBe( 'test' );
    } );

} );
