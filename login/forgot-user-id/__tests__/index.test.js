import { mapStateToProps, mapDispatchToProps } from '../index';

describe( 'ForgotUserId connect', () => {

    it ( 'gets passed the required props when connected with react-redux.', () => {
        const result = mapStateToProps( {} );

        expect( result.busy ).toBeDefined();
        expect( result.clientLogo ).toBeDefined();
        expect( result.success ).toBeDefined();
        expect( result.successMessage ).toBeDefined();
        expect( result.errorMessage ).toBeDefined();

    } );

    it( 'has the appropriate props for mapDispatchToProps', () => {
        expect( mapDispatchToProps.requestUsername ).toBeDefined();
        expect( mapDispatchToProps.setMessage ).toBeDefined();
        expect( mapDispatchToProps.setSuccess ).toBeDefined();
    } );

} );


