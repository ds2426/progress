// libs
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import isEmailValid from 'utils/email-validator';
// shared components
import { LocaleString } from '@biw/intl';
import { Alert } from 'component-library';
import { LoaderButton, NarrowForm, CancelButton } from 'shared';
import ErrorMessage from 'routes/login/shared/error-message';
import './forgot-user-id.scss';

export default class ForgotUserId extends React.Component {
    static propTypes = {
        busy: PropTypes.bool.isRequired,
        clientLogo: PropTypes.string,
        errorMessage: PropTypes.oneOfType( [ PropTypes.object, PropTypes.bool ] ).isRequired,
        getString: PropTypes.func.isRequired,
        successMessage: PropTypes.oneOfType( [ PropTypes.string, PropTypes.bool ] ).isRequired,
        success: PropTypes.bool.isRequired,
        requestUsername: PropTypes.func.isRequired,
        setMessage: PropTypes.func.isRequired,
        setSuccess: PropTypes.func.isRequired
    }

    state = {
        emailAddress: '',
        focusedField: ''
    }

    componentDidUpdate( prevProps ) {
        if ( !prevProps.success && this.props.success ) {
            this.setState( { emailAddress: '' } );
        }
    }

    componentWillUnmount() {
        this.dismissSuccess();
        this.dismissError();
    }

    handleInputChange = ( event ) => {
        const name = event.target.name;
        this.setState( { [ name ]: event.target.value } );

        this.dismissSuccess();
    }

    handleInputFocus = ( event ) => {
        this.setState( { 'focusedField': event.target.name } );
    }

    handleInputBlur = ( event ) => {
        this.setState( { 'focusedField': '' } );
    }

    submit = () => {
        const { emailAddress } = this.state;
        this.props.requestUsername( { emailAddress } );

        this.dismissError();
    }

    checkEnter = ( event ) => {
        const enterPressed = event.charCode === 13;
        const { emailAddress } = this.state;

        if ( enterPressed && isEmailValid( emailAddress )  ) {
            this.submit();
        }
    }

    dismissError = () => {
        if ( this.props.errorMessage ) {
            this.props.setMessage( 'error', false );
        }
    }

    dismissSuccess = () => {
        if ( this.props.success === true ) {
            this.props.setMessage( 'success', false );
            this.props.setSuccess( false );
        }
    }

    render() {
        const {
            emailAddress,
            focusedField
        } = this.state;

        const {
            busy,
            clientLogo,
            errorMessage,
            success,
            successMessage,
            getString
        } = this.props;

        const emailFieldClasses = classNames( {
            'email-address-field': true,
            'input-wrap': true,
            'error': this.state.errorMsg,
            'focused': focusedField === 'emailAddress' || !!emailAddress
        } );

        return (
            <NarrowForm page="forgot-user-id" clientLogo={ clientLogo } errorsExist={ !!errorMessage }>
                <h1 className="sub-title">
                    <LocaleString code="forgotUserId.form.headerText" replaceOptions={ { userIdLabel: getString( 'common.userIdLabel' ) } } />
                </h1>
                <div className={ emailFieldClasses }>
                    <label htmlFor="emailAddress" className="control-label">
                        <LocaleString code="common.emailAddressLabel" />
                    </label>
                    <input
                        type="text"
                        name="emailAddress"
                        id="emailAddress"
                        className="gq-input"
                        value={ emailAddress }
                        autoComplete="off"
                        onChange={ this.handleInputChange }
                        onFocus={ this.handleInputFocus }
                        onBlur={ this.handleInputBlur }
                        onKeyPress={ this.checkEnter } />
                </div>

                {
                    errorMessage
                    &&
                    <Alert type="error" onDismiss={ () => this.props.setMessage( 'error', false ) }>
                        <p>
                            <ErrorMessage errorMessage={ errorMessage } localeCode="errors.generic.global" />
                        </p>
                    </Alert>
                }

                {
                    successMessage
                    &&
                    <Alert type="success" onDismiss={ this.dismissSuccess }>
                        <p><LocaleString code={ successMessage } /></p>
                    </Alert>
                }

                <LoaderButton
                    fetching={ busy }
                    handleClick={ this.submit }
                    disabled={ !isEmailValid( emailAddress ) }
                    done={ success }
                    clientBranding
                    >
                    <LocaleString
                        code="forgotUserId.form.sendEmailButtonLabel"
                        replaceOptions={ { userIdLabel: getString( 'common.userIdLabel' ) } }
                         />
                </LoaderButton>
                <CancelButton />

            </NarrowForm>
        );
    }
}
