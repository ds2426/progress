// libs
import { connect } from 'react-redux';
import { getString } from '@biw/intl';
// selectors
import { selectors as clientSelectors } from '@biw/client-dux';
// actionCreators
import { forgotUserIdDux, forgotUserIdSelectors } from 'biw-login-dux';
import { fetchSelectors } from 'biw-client-fetch';
// component
import ForgotUserId from './forgot-user-id';

const { requestUsername, setMessage, setSuccess } = forgotUserIdDux;
const { getForgotUserIdErrorMessage, getForgotUserIdSuccess, getForgotUserIdSuccessMessage } = forgotUserIdSelectors;
const { getFetching } = fetchSelectors;

export const mapStateToProps = state => ( {
    busy: getFetching( state, 'forgotUserId' ),
    clientLogo: clientSelectors.getClientLogo( state ),
    errorMessage: getForgotUserIdErrorMessage( state ),
    getString: ( key, replaceOptions ) => getString( state, key, replaceOptions ),
    successMessage: getForgotUserIdSuccessMessage( state ),
    success: getForgotUserIdSuccess( state )
} );

export const mapDispatchToProps = {
    requestUsername,
    setMessage,
    setSuccess
};

export default connect( mapStateToProps, mapDispatchToProps )( ForgotUserId );
