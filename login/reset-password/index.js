// libs
import { connect } from 'react-redux';
import get from 'lodash/get';
import { getString } from '@biw/intl';
// actionCreators
import { resetPasswordDux, resetPasswordSelectors } from 'biw-login-dux';
// selectors
import { selectors as clientSelectors } from '@biw/client-dux';
import { fetchSelectors } from 'biw-client-fetch';
// component
import ResetPassword from './reset-password';

const { savePassword, setMessage, setSuccess } = resetPasswordDux;
const { getResetPasswordSuccess, getResetPasswordErrorMessage, getResetPasswordSuccessMessage } = resetPasswordSelectors;
const { getFetching } = fetchSelectors;

export const mapStateToProps = state => ( {
    busy: getFetching( state, 'savePassword' ),
    clientLogo: clientSelectors.getClientLogo( state ),
    success: getResetPasswordSuccess( state ),
    passwordRules: clientSelectors.getClientPasswordRules( state ),
    passwordGuid: get( state, 'location.query.k', '' ),
    passwordLabels: {
        isLowerCaseRequired: getString( state, 'passwordHelper.rules.lowercaseRequiredLabel' ),
        isUpperCaseRequired: getString( state, 'passwordHelper.rules.uppercaseRequiredLabel' ),
        isSpecialCharRequired: getString( state, 'passwordHelper.rules.specialCharRequiredLabel' ),
        isNumberRequired: getString( state, 'passwordHelper.rules.numberRequiredLabel' ),
        length: getString( state, 'passwordHelper.rules.lenghRequirement', {
            minLength: clientSelectors.getClientPasswordRules( state ).minLength,
            maxLength: clientSelectors.getClientPasswordRules( state ).maxLength
        } )
    },
    errorMessage: getResetPasswordErrorMessage( state ),
    successMessage: getResetPasswordSuccessMessage( state )
} );

export const mapDispatchToProps = {
    savePassword,
    setMessage,
    setSuccess
};

export default connect( mapStateToProps, mapDispatchToProps )( ResetPassword );
