// libs
import React from 'react';
import PropTypes from 'prop-types';
import classNames  from 'classnames';
import Link from 'redux-first-router-link';
import { LocaleString } from '@biw/intl';
// shared components
import { Alert, PasswordHelper } from 'component-library';
import { LoaderButton, NarrowForm, BackButton } from 'shared';
import ErrorMessage from 'routes/login/shared/error-message';


export default class ResetPassword extends React.Component {
    static propTypes = {
        busy: PropTypes.bool.isRequired,
        clientLogo: PropTypes.string,
        success: PropTypes.bool,
        passwordRules: PropTypes.object.isRequired,
        passwordGuid: PropTypes.string,
        passwordLabels: PropTypes.object.isRequired,
        errorMessage: PropTypes.oneOfType( [ PropTypes.object, PropTypes.string ] ),
        successMessage: PropTypes.oneOfType( [ PropTypes.bool, PropTypes.string ] ),
        savePassword: PropTypes.func.isRequired,
        setMessage: PropTypes.func.isRequired,
        setSuccess: PropTypes.func.isRequired
    }

    state = {
        focusedField: '',
        password: '',
        success: false,
        clientLogo: ''
    }

    componentWillUnmount() {
        this.dismissError();
        this.dismissSuccess();
    }

    handleInputChange = event => {
        const name = event.target.name;
        this.setState( { [ name ]: event.target.value } );

        this.dismissSuccess();
    };

    onResetPasswordSubmit = () => {
        const password = this.state.password;
        const passwordGuid = this.props.passwordGuid;

        this.props.savePassword( { password, passwordGuid } );

        this.dismissError();
    };

    checkEnter = event => {
        const enterPressed = event.charCode === 13;
        const { password } = this.state;

        if ( enterPressed && password.length ) {
            this.onResetPasswordSubmit();
        }
    };

    dismissError = () => {
        if ( this.props.errorMessage ) {
            this.props.setMessage( 'error', false );
        }
    }

    dismissSuccess = () => {
        if ( this.props.success === true ) {
            this.props.setMessage( 'success', false );
            this.props.setSuccess( false );
        }
    }

    render() {

        const {
            focusedField,
            password
        } = this.state;
        const {
            busy,
            clientLogo,
            errorMessage,
            passwordRules,
            passwordLabels,
            success,
            successMessage
        } = this.props;

        const passwordFieldClasses = classNames( {
            'password-container': true,
            'input-wrap': true,
            'focused': focusedField === 'password' || !!password
        } );

        return (
            <NarrowForm page="reset-password" clientLogo={ clientLogo } errorsExist={ !!errorMessage }>

                <BackButton to="/login" />

                <h1 className="sub-title text-center">
                    <LocaleString code="resetPassword.resetLabel" />
                </h1>

                <div className={ passwordFieldClasses }>
                    <label htmlFor="password" className="control-label">
                        <LocaleString code="common.createPwdLabel" />
                    </label>
                    <PasswordHelper
                        pwdInputOpts={ {
                            className: 'gq-input',
                            autoComplete: 'off'
                        } }
                        name= "password"
                        id="password"
                        value= { password }
                        disable= { success }
                        onChange={ this.handleInputChange }
                        onFocus={ this.handleInputFocus }
                        onBlur={ this.handleInputBlur }
                        onKeyPress={ this.checkEnter }
                        passwordRules={ passwordRules }
                        passwordLabels={ passwordLabels } />
                </div>

                { errorMessage &&
                    <Alert type="error" onDismiss={ () => this.props.setMessage( 'error', false ) }>
                        <p>
                            <ErrorMessage errorMessage={ errorMessage } localeCode="errors.generic.global" />
                        </p>
                    </Alert>
                }

                { successMessage &&
                    <Alert type="success" onDismiss={ () => this.dismissSuccess() }>
                        <Link to="/login" style={ { color: 'white' } }>
                            <LocaleString code={ successMessage } />
                        </Link>
                    </Alert>
                }

                <LoaderButton
                    fetching={ busy }
                    handleClick={ this.onResetPasswordSubmit }
                    disabled={ !password.length || success }
                    done={ success }
                    clientBranding>
                    <LocaleString code="resetPassword.form.resetPasswordButtonLabel" />
                </LoaderButton>

            </NarrowForm>
        );
    }
}
