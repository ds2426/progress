import React from 'react';
import { mount } from 'enzyme';
import renderer from 'react-test-renderer';
import ResetPassword from '../reset-password';
import { LoaderButton } from 'shared';

const defaultProps = {
    busy: false,
    disabled: false,
    passwordRules: {},
    passwordLabels: {},
    passwordGuid: 'test-guid',
    errorMessage: '',
    successMessage: '',
    routeTo: jest.fn(),
    savePassword: jest.fn(),
    setMessage: jest.fn(),
    setSuccess: jest.fn()
};

describe( 'Reset Password Component', () => {

    it( 'renders the dom like the snapshot when the props have default values.', () => {
        const props = {
            ...defaultProps
        };

        const component = renderer.create( <ResetPassword { ...props } /> );
        const tree = component.toJSON();

        expect( tree ).toMatchSnapshot();
    } );

    it( 'renders the dom like the snapshot when the prop success is true.', () => {
        const props = {
            ...defaultProps,
            success: true
        };

        const component = renderer.create( <ResetPassword { ...props } /> );
        const tree = component.toJSON();

        expect( tree ).toMatchSnapshot();
    } );

    it( 'renders the dom like the snapshot when the prop passwordGuid is empty.', () => {
        const props = {
            ...defaultProps,
            passwordGuid: null
        };

        const component = renderer.create( <ResetPassword { ...props } /> );
        const tree = component.toJSON();

        expect( tree ).toMatchSnapshot();
    } );

    it ( 'disables the reset password button when a valid password have not been entered.', () => {
        const props = {
            ...defaultProps
        };

        const tree = mount( <ResetPassword { ...props } /> );

        const button = tree.find( LoaderButton );

        expect ( button.props().disabled ).toBe( true );
    } );

    it ( 'the loader button should have client branding styles applied', () => {
        const props = {
            ...defaultProps
        };

        const tree = mount( <ResetPassword { ...props } /> );

        const button = tree.find( LoaderButton );

        expect ( button.props().clientBranding ).toBe( true );
    } );

    it ( 'disables the reset password button when a valid password has been entered and wehn success flag return true.', () => {
        const props = {
            ...defaultProps,
            success: true
        };

        const tree = mount( <ResetPassword { ...props } /> );

        const button = tree.find( LoaderButton );

        expect ( button.props().disabled ).toBe( true );
    } );

    it ( 'enables the reset password button when a valid password has been entered.', () => {
        const props = {
            ...defaultProps,
            success: false
        };

        const tree = mount( <ResetPassword { ...props } /> );

        expect ( tree.find( LoaderButton ).props().disabled ).toBe( true );

        tree.setState( { password: 'test' } );

        expect ( tree.find( LoaderButton ).props().disabled ).toBe( false );
    } );

    it ( 'changes password in state when the user changes something in the password input ', () => {
        const props = {
            ...defaultProps
        };

        const tree = mount( <ResetPassword { ...props } /> );

        const input = tree.find( 'input[name="password"]' );

        input.simulate( 'change', { target: { name: input.props().name, value: 'test' } } );

        expect ( tree.state( 'password' ) ).toBe( 'test' );
    } );

    it ( 'executes savePassword prop when the reset password button is clicked.', () => {
        const props = {
            ...defaultProps,
            savePassword: jest.fn()
        };

        const tree = mount( <ResetPassword { ...props } /> );

        const button = tree.find( LoaderButton );

        tree.setState( { passwordGuid: '35345', password: 'test' } );

        button.simulate( 'click' );

        expect ( props.savePassword.mock.calls.length ).toBe( 1 );
    } );

    it ( 'executes savePassword prop when the user presses enter in the password field.', () => {
        const props = {
            ...defaultProps,
            savePassword: jest.fn()
        };

        const tree = mount( <ResetPassword { ...props } /> );

        tree.setState( { 'password': 'test' } );

        const passwordInput = tree.find( 'input[name="password"]' );

        passwordInput.simulate( 'keyPress', { charCode: 13 } );

        expect ( props.savePassword.mock.calls.length ).toBe( 1 );
    } );

    it ( 'does not execute savePassword prop when the user presses enter in the password input with an invalid password.', () => {
        const props = {
            ...defaultProps,
            savePassword: jest.fn()
        };

        const tree = mount( <ResetPassword { ...props } /> );

        const passwordInput = tree.find( 'input[name="password"]' );

        passwordInput.simulate( 'keyPress', { charCode: 13 } );

        expect ( props.savePassword.mock.calls.length ).toBe( 0 );
    } );

    it ( 'shows error alerts for all errors.', () => {
        const props = {
            ...defaultProps,
            errorMessage: 'test'
        };

        const tree = mount( <ResetPassword { ...props } /> );

        const alerts = tree.find( '.alert-error' );

        expect ( alerts.length ).toBe( 1 );
    } );

    it ( 'executes setMessage prop for error when the component is unmounted.', () => {
        const props = {
            ...defaultProps,
            errorMessage: 'test',
            setMessage: jest.fn()
        };

        const tree = mount( <ResetPassword { ...props } /> );

        tree.unmount();

        expect ( props.setMessage.mock.calls.length ).toBe( 1 );
    } );

    it ( 'executes setMessage prop for error when reset password button is clicked.', () => {
        const props = {
            ...defaultProps,
            errorMessage: 'test',
            savePassword: jest.fn(),
            setMessage: jest.fn()
        };

        const tree = mount( <ResetPassword { ...props } /> );

        const button = tree.find( LoaderButton );

        tree.setState( { passwordGuid: 'test', password: 'test' } );

        button.simulate( 'click' );

        expect ( props.setMessage.mock.calls.length ).toBe( 1 );
    } );

    it ( 'executes setMessage prop when an error alert is closed.', () => {
        const props = {
            ...defaultProps,
            errorMessage: 'test',
            setMessage: jest.fn()
        };

        const tree = mount( <ResetPassword { ...props } /> );

        const button = tree.find( '.close-btn' );

        button.simulate( 'click' );

        expect ( props.setMessage.mock.calls.length ).toBe( 1 );
    } );

    it ( 'shows a success alert when there is a success message.', () => {
        const props = {
            ...defaultProps,
            successMessage: 'test'
        };

        const tree = mount( <ResetPassword { ...props } /> );

        const alerts = tree.find( '.alert-success' );

        expect ( alerts.length ).toBe( 1 );
    } );

} );
