import { mapStateToProps, mapDispatchToProps } from '../index';

describe( 'Reset Password Connect', () => {
    it ( 'gets passed the required props when connected with react-redux.', () => {
        const result = mapStateToProps( { location: { query: { k: 'test' } } } );

        expect( result.clientLogo ).toBeDefined();
        expect( result.errorMessage ).toBeDefined();
        expect( result.busy ).toBeDefined();
        expect( result.success ).toBeDefined();
        expect( result.passwordRules ).toBeDefined();
        expect( result.passwordLabels ).toBeDefined();
        expect( result.passwordGuid ).toBe( 'test' );
        expect( result.successMessage ).toBeDefined();

    } );

    it( 'has the appropriate props for mapDispatchToProps', () => {
        expect( mapDispatchToProps.savePassword ).toBeDefined();
        expect( mapDispatchToProps.setMessage ).toBeDefined();
        expect( mapDispatchToProps.setSuccess ).toBeDefined();
    } );
} );


