import React from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';
import ErrorMessage, { ActivationErrorMessage } from '../index';

const defaultProps = {
    errorMessage: { test: 'test' },
    localeCode: 'errors.generic.global'
};

describe( 'ErrorMessage', () => {
    it( 'renders without errorMessage errors', () => {
        const renderer = new ShallowRenderer();
        const wrapper = renderer.render(
            <ErrorMessage { ...defaultProps } />
        );

        expect( wrapper ).toMatchSnapshot();
    } );
    it( 'renders without localeCode errors', () => {
        const props = {
            ...defaultProps,
            errorMessage: { test: null }
        };

        const renderer = new ShallowRenderer();
        const wrapper = renderer.render(
            <ErrorMessage { ...props } />
        );

        expect( wrapper ).toMatchSnapshot();
    } );
    it( 'renders without localeCode errors', () => {
        const renderer = new ShallowRenderer();
        const wrapper = renderer.render(
            <ActivationErrorMessage { ...defaultProps } />
        );

        expect( wrapper ).toMatchSnapshot();
    } );
} );
