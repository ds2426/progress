import React from 'react';
import get from 'lodash/get';
import Link from 'redux-first-router-link';
import PropTypes from 'prop-types';
// shared
import { LocaleString } from '@biw/intl';

const ErrorMessage = ( { errorMessage, localeCode } ) => {
    const message = typeof errorMessage === 'string' ? errorMessage : errorMessage.text;
    if ( message ) {
        return <span>{ message }</span>;
    } else {
        return <LocaleString code={ localeCode } />;
    }
};

ErrorMessage.propTypes = {
    errorMessage: PropTypes.oneOfType( [ PropTypes.object, PropTypes.string ] ),
    localeCode: PropTypes.string
};

export const ActivationErrorMessage = ( props ) => {
    if ( get( props, 'errorMessage.text' ) ) {
        return (
            <Link to="/login" style={ { color: 'white' } }>
                <ErrorMessage { ...props } />
            </Link>
        );
    } else {
        return <ErrorMessage { ...props } />;
    }
};

ActivationErrorMessage.propTypes = {
    errorMessage: PropTypes.object
};

export default ErrorMessage;
