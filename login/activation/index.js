// libs
import { connect } from 'react-redux';
// utils
import { getString, localeEnhancer } from '@biw/intl';
// selectors
import { selectors as clientSelectors } from '@biw/client-dux';
// dux
import { activationDux, activationSelectors } from 'biw-login-dux';
import { fetchSelectors } from 'biw-client-fetch';
// component
import Activation from './activation';

const {
    activate,
    setMessage,
    preActivationLoginAttempt
} = activationDux;
const { getActivationSuccess, getActivationWarningMessage, getActivationErrorMessage, getActivationLoginAttemptFlag } = activationSelectors;
const { getFetching } = fetchSelectors;

export const mapStateToProps = ( state ) => {
    const passwordRules = clientSelectors.getClientPasswordRules( state );
    return {
        passwordRules,
        clientLogo: clientSelectors.getClientLogo( state ),
        preActivationLoginAttemptFlag: getActivationLoginAttemptFlag( state ),
        warningMessage: getActivationWarningMessage( state ),
        errorMessage: getActivationErrorMessage( state ),
        activating: getFetching( state, 'activate' ),
        activationFields: clientSelectors.getClientBranding( state ).activationFields || [],
        success: getActivationSuccess( state ),
        passwordLabels: {
            isLowerCaseRequired: getString( state, 'passwordHelper.rules.lowercaseRequiredLabel' ),
            isUpperCaseRequired: getString( state, 'passwordHelper.rules.uppercaseRequiredLabel' ),
            isSpecialCharRequired: getString( state, 'passwordHelper.rules.specialCharRequiredLabel' ),
            isNumberRequired: getString( state, 'passwordHelper.rules.numberRequiredLabel' ),
            length: getString( state, 'passwordHelper.rules.lenghRequirement', {
                minLength: passwordRules.minLength,
                maxLength: passwordRules.maxLength
            } )
        }
    };
};

export const mapDispatchToProps = {
    activate,
    preActivationLoginAttempt,
    setMessage
};

const activationConnect = connect( mapStateToProps, mapDispatchToProps );

export default localeEnhancer( activationConnect( Activation ) );
