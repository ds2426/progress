import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';
import Activation from '../activation';
import { LoaderButton } from 'shared';

const defaultProps = {
    activating: true,
    activationFields: [],
    done: false,
    clientLogo: '',
    preActivationLoginAttemptFlag: false,
    locale: { code: 'en_US', 'intlCode': 'en-US' },
    disabled: false,
    passwordRules: {},
    passwordLabels: {},
    activateErrors: [],
    activate: jest.fn(),
    preActivationLoginAttempt: jest.fn(),
    routeTo: jest.fn(),
    setMessage: jest.fn(),
    errorMessage: false,
    warningMessage: 'Welcome! Before logging in, you need to activate your account.'
};

describe( 'Activation component', () => {

    it( 'renders the dom like snapshot when the props have default values.', () => {
        const props = {
            ...defaultProps
        };

        const component = renderer.create( <Activation { ...props } /> );
        const tree = component.toJSON();

        expect( tree ).toMatchSnapshot();
    } );

    it( 'renders the dom like snapshot when the props have activation fields', () => {
        const props = {
            ...defaultProps,

            activationFields: [
                {
                    id: 5042,
                    characteristic: {
                        code: 'email',
                        label: 'Email'
                    },
                    characteristicDataType: 'text',
                    fieldLabel: [
                        {
                            locale: 'en_US',
                            displayName: 'English (United States)',
                            cmText: 'Email'
                        }
                    ]
                }
            ]
        };

        const component = renderer.create( <Activation { ...props } /> );
        const tree = component.toJSON();

        expect( tree ).toMatchSnapshot();
    } );


    it( 'renders the No Label Provided label when the locale value does not match with locale value in activation fields', () => {
        const props = {
            ...defaultProps,

            activationFields: [
                {
                    id: 5042,
                    characteristic: {
                        code: 'email',
                        label: 'Email'
                    },
                    characteristicDataType: 'text',
                    fieldLabel: [
                        {
                            locale: 'ZH_tr',
                            displayName: 'Chinesse (United States)',
                            cmText: 'Email'
                        }
                    ]
                }
            ]
        };

        const component = renderer.create( <Activation { ...props } /> );
        const tree = component.toJSON();

        expect( tree ).toMatchSnapshot();
    } );

    it( 'renders this dom like snapshot when done flag is true.', () => {
        const props = {
            ...defaultProps,

            done: true,
            locale: { code: 'en_US', 'intlCode': 'en-US' }
        };

        const component = renderer.create( <Activation { ...props } /> );
        const tree = component.toJSON();

        expect( tree ).toMatchSnapshot();
    } );

    it ( 'does not execute activate prop when the user presses enter in the username input with out text in other inputs.', () => {
        const props = {
            ...defaultProps,
            activate: jest.fn()
        };

        const tree = mount( <Activation { ...props } /> );

        tree.setState( { username: 'test' } );

        const input = tree.find( 'input[name="username"]' );

        input.simulate( 'keyPress', { charCode: 13 } );

        expect ( props.activate.mock.calls.length ).toBe( 0 );
    } );

    it ( 'does not execute activate prop when the user presses enter in the password input with out text in other inputs.', () => {
        const props = {
            ...defaultProps,
            activationFields: [
                {
                    id: 5042,
                    characteristic: {
                        code: 'email',
                        label: 'Email'
                    },
                    characteristicDataType: 'text',
                    fieldLabel: [
                        {
                            locale: 'en_US',
                            displayName: 'English (United States)',
                            cmText: 'Email'
                        }
                    ]
                }
            ],

            activate: jest.fn()
        };

        const tree = mount( <Activation { ...props } /> );

        tree.setState( { username: 'test', password: 'Testing123!', email: '' } );

        const input = tree.find( 'input[name="password"]' );

        input.simulate( 'keyPress', { charCode: 13 } );

        expect ( props.activate.mock.calls.length ).toBe( 0 );
    } );

    it ( 'execute activate prop when the user presses enter in the password input if you dont have any activation fields.', () => {
        const props = {
            ...defaultProps,
            activate: jest.fn()
        };

        const tree = mount( <Activation { ...props } /> );

        tree.setState( { username: 'test', password: 'Testing123!' } );

        const input = tree.find( 'input[name="password"]' );

        input.simulate( 'keyPress', { charCode: 13 } );

        expect ( props.activate.mock.calls.length ).toBe( 1 );
    } );

    it ( 'executes activate prop when the user presses enter in the last activation input.', () => {
        const props = {
            ...defaultProps,
            activationFields: [
                {
                    id: 5042,
                    characteristic: {
                        code: 'email',
                        label: 'Email'
                    },
                    characteristicDataType: 'text',
                    fieldLabel: [
                        {
                            locale: 'en_US',
                            displayName: 'English (United States)',
                            cmText: 'Email'
                        }
                    ]
                }
            ],

            activate: jest.fn()
        };

        const tree = mount( <Activation { ...props } /> );

        tree.setState( { username: 'test', password: 'Testing123!', characteristics: { email: 'test@test.com ' } } );

        const input = tree.find( 'input[name="email"]' );

        input.simulate( 'keyPress', { charCode: 13 } );

        expect ( props.activate.mock.calls.length ).toBe( 1 );
    } );

    it ( 'shows error alerts for all errors.', () => {
        const props = {
            ...defaultProps,
            errorMessage: {},
            setMessage: jest.fn()
        };

        const tree = mount( <Activation { ...props } /> );

        const alerts = tree.find( '.alert-error' );

        expect ( alerts.length ).toBe( 1 );
    } );

    it ( 'shows error alert when errorCode is 20013 for all errors.', () => {
        const props = {
            ...defaultProps,
            errorMessage: {},
            setMessage: jest.fn()
        };

        const tree = mount( <Activation { ...props } /> );

        const alerts = tree.find( '.alert-error' );

        expect ( alerts.length ).toBe( 1 );
    } );

    it ( 'shows warning alert panel if preActivationLoginAttemptFlag is true.', () => {
        const props = {
            ...defaultProps,
            preActivationLoginAttemptFlag: true,
            warningMessage: 'Warning message'
        };

        const tree = mount( <Activation { ...props } /> );

        const alerts = tree.find( '.alert-warning' );

        expect ( alerts.length ).toBe( 1 );
    } );

    it ( 'executes setMessage prop for all errors when the component is unmounted.', () => {
        const props = {
            ...defaultProps,
            errorMessage: {},
            setMessage: jest.fn()
        };

        const tree = mount( <Activation { ...props } /> );

        const button = tree.find( '.close-btn' );

        button.simulate( 'click' );

        expect ( props.setMessage.mock.calls.length ).toBe( 1 );
    } );

    it ( 'executes preActivationLoginAttempt prop when component is unmounted.', () => {
        const props = {
            ...defaultProps,

            preActivationLoginAttempt: jest.fn()
        };

        const tree = mount( <Activation { ...props } /> );

        tree.unmount();

        expect ( props.preActivationLoginAttempt.mock.calls.length ).toBe( 1 );
    } );

    it ( 'changes username in state when the user changes something in the user name input ', () => {
        const props = {
            ...defaultProps
        };

        const tree = mount( <Activation { ...props } /> );

        const input = tree.find( 'input[name="username"]' );

        input.simulate( 'change', { target: { name: input.props().name, value: 'test' } } );

        expect ( tree.state( 'username' ) ).toBe( 'test' );
    } );

    it ( 'changes password in state when the user changes something in the password input ', () => {
        const props = {
            ...defaultProps
        };

        const tree = mount( <Activation { ...props } /> );

        const input = tree.find( 'input[name="password"]' );

        input.simulate( 'change', { target: { name: input.props().name, value: 'Testing123!' } } );

        expect ( tree.state( 'password' ) ).toBe( 'Testing123!' );
    } );

    it ( 'changes email in state when the user changes something in the email input ', () => {
        const props = {
            ...defaultProps,

            activationFields: [
                {
                    id: 5042,
                    characteristic: {
                        code: 'email',
                        label: 'Email'
                    },
                    characteristicDataType: 'text',
                    fieldLabel: [
                        {
                            locale: 'en_US',
                            displayName: 'English (United States)',
                            cmText: 'Email'
                        }
                    ]
                }
            ]
        };

        const tree = mount( <Activation { ...props } /> );

        const input = tree.find( 'input[name="email"]' );

        input.simulate( 'change', { target: { name: input.props().type, value: 'Test@test.com' } } );

        expect ( tree.state( 'characteristics' ).text ).toBe( 'Test@test.com' );
    } );

    it ( 'disables the activate button when any field has not been entered.', () => {
        const props = {
            ...defaultProps,

            validateFields: jest.fn()
        };

        const tree = mount( <Activation { ...props } /> );

        const button = tree.find( LoaderButton );

        expect ( button.props().disabled ).toBe( true );

        tree.setState( { username: 'test' } );

        expect ( button.props().disabled ).toBe( true );
    } );

    it ( 'disables the activate button when email field has not been entered.', () => {
        const props = {
            ...defaultProps,
            activationFields: [
                {
                    id: 5042,
                    characteristic: {
                        code: 'email',
                        label: 'Email'
                    },
                    characteristicDataType: 'text',
                    fieldLabel: [
                        {
                            locale: 'en_US',
                            displayName: 'English (United States)',
                            cmText: 'Email'
                        }
                    ]
                }
            ]
        };

        const tree = mount( <Activation { ...props } /> );

        expect ( tree.find( LoaderButton ).props().disabled ).toBe( true );

        tree.setState( { username: 'test', password: 'Testing123!', email: '' } );

        expect ( tree.find( LoaderButton ).props().disabled ).toBe( true );
    } );

    it ( 'enables the activate button when user enter all fields.', () => {
        const props = {
            ...defaultProps
        };

        const tree = mount( <Activation { ...props } /> );

        expect ( tree.find( LoaderButton ).props().disabled ).toBe( true );

        tree.setState( { username: 'test', password: 'Testing123!', email: 'test@test.com' } );

        expect ( tree.find( LoaderButton ).props().disabled ).toBe( false );
    } );
} );
