import AppStore from 'app/store';
import { mapStateToProps, mapDispatchToProps } from '../index';

describe( 'Activation connect', () => {
    it ( 'gets passed the required props when connected with react-redux.', () => {
        const result = mapStateToProps( AppStore().getState() );

        expect( result.passwordRules ).toBeDefined();
        expect( result.passwordLabels ).toBeDefined();
        expect( result.preActivationLoginAttemptFlag ).toBeDefined();
        expect( result.warningMessage ).toBeDefined();
        expect( result.errorMessage ).toBeDefined();
        expect( result.activating ).toBeDefined();
        expect( result.activationFields ).toBeDefined();
        expect( result.success ).toBeDefined();
    } );

    it( 'should have the proper mapDispatchToProps', () => {
        expect( mapDispatchToProps.activate ).toBeDefined();
        expect( mapDispatchToProps.setMessage ).toBeDefined();
        expect( mapDispatchToProps.preActivationLoginAttempt ).toBeDefined();
    } );

} );



