// libs
import React from 'react';
import PropTypes from 'prop-types';
import Link from 'redux-first-router-link';
import classNames from 'classnames';
// shared components
import { Alert, PasswordHelper } from 'component-library';
import { LoaderButton, NarrowForm } from 'shared';
import { LocaleString } from '@biw/intl';
import { ActivationErrorMessage } from 'routes/login/shared/error-message';
import * as types from 'app/router/types';
import './activation.scss';

const LOGOUT = { type: types.LOGOUT };
export default class Activation extends React.Component {

    static propTypes = {
        clientLogo: PropTypes.string.isRequired,
        passwordRules: PropTypes.object.isRequired,
        preActivationLoginAttemptFlag: PropTypes.bool,
        warningMessage: PropTypes.oneOfType( [ PropTypes.bool, PropTypes.string ] ),
        errorMessage: PropTypes.oneOfType( [ PropTypes.bool, PropTypes.object ] ),
        activating: PropTypes.bool,
        activationFields: PropTypes.array.isRequired,
        locale: PropTypes.shape( {
            code: PropTypes.string
        } ).isRequired,
        success: PropTypes.bool,
        passwordLabels: PropTypes.object.isRequired,
        activate: PropTypes.func.isRequired,
        preActivationLoginAttempt: PropTypes.func.isRequired,
        setMessage: PropTypes.func.isRequired
    }

    state = {
        username: '',
        password: '',
        focusedField: '',
        characteristics: {},
        activating: false,
        success: false,
        preActivationLoginAttemptFlag: false
    }

    componentWillUnmount() {
        this.props.preActivationLoginAttempt( false, '' );
    }

    onActivateSubmit = () => {
        const characteristics = [];

        const {
            activationFields,
        } = this.props;

        const {
            username,
            password
        } = this.state;

        activationFields.forEach( field => {
            // Service that sends characteristics sends data in a different format than the service
            // that receives the characteristic data.
            const { characteristic, id } = field;
            characteristic.label = this.state.characteristics[ field.characteristic.code ];
            characteristic.code = id.toString();
            characteristics.push( characteristic );
        } );

        this.props.activate( { characteristics, password, username } );
    }

    handleField = event => {
        const { name, value } = event.target;

        if ( name === 'username' || name === 'password' ) {
            this.setState( { [ name ]: value } );
        } else {
            const characteristics = { ...this.state.characteristics };
            characteristics[ name ] = value;
            this.setState( { characteristics } );
        }
    }

    validateFields = () => {
        const { activationFields } = this.props;

        const {
            username,
            password
        } = this.state;

        if ( !username.length || !password.length ) {
            return false;
        }

        return activationFields.every( field => {
            return this.state.characteristics[ field.characteristic.code ] && this.state.characteristics[ field.characteristic.code ].length > 0;
        } );
    }

    checkEnter = event => {
        const enterPressed = event.charCode === 13;

        if ( enterPressed && this.validateFields() ) {
            this.onActivateSubmit();
        }
    }

    handleInputFocus = ( event ) => {
        this.setState( { 'focusedField': event.target.name } );
    }

    handleInputBlur = ( event ) => {
        this.setState( { 'focusedField': '' } );
    }

    activationFieldClasses = ( code, focusedField ) => {
        return classNames( {
            [ code + '-field' ]: true,
            'input-wrap': true,
            'focused': focusedField === code || !!this.state.characteristics[ code ]
        } );
    }

    render() {

        const {
            activationFields,
            clientLogo,
            locale,
            activating,
            preActivationLoginAttemptFlag,
            errorMessage,
            passwordRules,
            passwordLabels,
            success,
            warningMessage
        } = this.props;

        const {
            username,
            password,
            focusedField
        } = this.state;

        const userFieldClasses = classNames( {
            'username-field': true,
            'input-wrap': true,
            'focused': focusedField === 'username' || !!username
        } );

        const passwordFieldClasses = classNames( {
            'password-container': true,
            'input-wrap': true,
            'focusedText': focusedField === 'password' || !!password,
            'focused': focusedField === 'password'
        } );

        return (
            <NarrowForm page="activation" clientLogo={ clientLogo } errorsExist={ !!errorMessage }>
                <h1 className="sub-title text-center">
                    <LocaleString code="activate.activateLabel" />
                </h1>

                {
                    preActivationLoginAttemptFlag
                    &&
                    <Alert type={ 'warning' }>
                        <p>{ warningMessage }</p>
                    </Alert>
                }

                <div className={ userFieldClasses }>
                    <label htmlFor="formUserId" className="control-label">
                        <LocaleString code="common.userIdLabel" />
                    </label>
                    <input
                        type="text"
                        name="username"
                        id="formUserId"
                        className="gq-input"
                        value={ username }
                        autoComplete="off"
                        onChange={ this.handleField }
                        onFocus={ this.handleInputFocus }
                        onBlur={ this.handleInputBlur }/>
                </div>

                <div className={ passwordFieldClasses }>
                    <label htmlFor="password" className="control-label">
                        <LocaleString code="common.createPwdLabel" />
                    </label>
                     <PasswordHelper
                        pwdInputOpts={ {
                            className: 'gq-input',
                            autoComplete: 'off'
                        } }
                        name= "password"
                        id="password"
                        value= { password }
                        disable= { success }
                        onChange={ this.handleField }
                        onFocus={ this.handleInputFocus }
                        onBlur={ this.handleInputBlur }
                        onKeyPress={ this.checkEnter }
                        passwordRules={ passwordRules }
                        passwordLabels={ passwordLabels } />
                </div>

                {
                    activationFields.map( ( field, index ) => {
                        const {
                            characteristic: { code },
                            fieldLabel
                        } = field;

                        const label = fieldLabel.find( label => label.locale == locale.code )
                            && fieldLabel.find( label => label.locale == locale.code ).cmText || 'No Label Provided';

                        return (
                            <div key={ code } className={ this.activationFieldClasses( code, focusedField ) }>
                                <label htmlFor={ code } className="control-label">{ label }</label>
                                <input
                                    type="text"
                                    name={ code }
                                    tabIndex={ index + 3 }
                                    id={ code }
                                    autoComplete="off"
                                    onChange={ this.handleField }
                                    onKeyPress={ this.checkEnter }
                                    onFocus={ this.handleInputFocus }
                                    onBlur={ this.handleInputBlur }
                                /><br/>
                            </div>
                        );
                    } )
                }

                {
                    errorMessage
                    &&
                    <Alert type="error" onDismiss={ () => this.props.setMessage( 'error', false ) }>
                        <p>
                            <ActivationErrorMessage  errorMessage={ errorMessage } localeCode="errors.generic.global" />
                        </p>
                    </Alert>
                }


                <LoaderButton
                    fetching={ activating }
                    handleClick={ this.onActivateSubmit }
                    disabled={ this.validateFields() ? false : true }
                    tabIndex={ activationFields.length + 3 }
                    done={ success }
                    clientBranding>
                    <LocaleString code="activate.activateBtnLabel" />
                </LoaderButton>


                <div className="cancel-button-container">
                    <Link to={LOGOUT} className="btn-cancel client-link">
                        <LocaleString code="activate.cancelBtnLabel" />
                    </Link>
                </div>

            </NarrowForm>
        );
    }
}
