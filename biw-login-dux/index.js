module.exports = {
    activationDux: require( './lib/activation' ),
    activationSelectors: require( './lib/activation/selectors' ),
    forgotPasswordDux: require( './lib/forgot-password' ),
    forgotPasswordSelectors: require( './lib/forgot-password/selectors' ),
    forgotUserIdDux: require( './lib/forgot-user-id' ),
    forgotUserIdSelectors: require( './lib/forgot-user-id/selectors' ),
    loginDux: require( './lib/login' ),
    loginSelectors: require( './lib/login/selectors' ),
    resetPasswordDux: require( './lib/reset-password' ),
    resetPasswordSelectors: require( './lib/reset-password/selectors' ),
    setAny: require( './lib/set-any' )
};