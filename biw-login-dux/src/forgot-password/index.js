import { clientFetch } from 'biw-client-fetch';

/*******************************************************************************
 * Initial state
 */

export const forgotPasswordState = {
    successMessage: false,
    errorMessage: false,
    success: false
};

/*******************************************************************************
 * Non-async action types
 */

export const FORGOT_PASSWORD = 'forgotPassword';
export const SET_FORGOT_PASSWORD_MESSAGE = 'setForgotPasswordMessage';
export const SET_FORGOT_PASSWORD_SUCCESS = 'setForgotPasswordSuccess';

/*******************************************************************************
 * Action creators
 */

export const setMessage = ( messageType, message ) => {
    return{
        type: SET_FORGOT_PASSWORD_MESSAGE,
        messageType,
        message
    };
 };

export const setSuccess = ( bool ) => {
    return{
        type: SET_FORGOT_PASSWORD_SUCCESS,
        success: bool
    };
};

export const requestPassword = ( body ) => {
    const successMessage = 'forgotPassword.form.successMessage';
    return dispatch => {
        return clientFetch( {
            url: '/api/v1.0/forgot-password',
            type: FORGOT_PASSWORD,
            method: 'post',
            body: body
        } ).then( ( response ) => {

            if ( !response || !Object.keys( response ).length ) {
                dispatch( setSuccess( false ) );
                dispatch( setMessage( 'error', { text: null, errorCode: null } ) );

                return;
            }
            
            if( response.errorCode ) {
                dispatch( setSuccess( false ) );
                dispatch( setMessage( 'error', { text: response.responseMessage, errorCode: response.errorCode } ) );
            } else {
                dispatch( setSuccess( true ) );
                dispatch( setMessage( 'success', successMessage ) );
            }
        } );
    };
};

/*******************************************************************************
 * Reducer
 */
 
export const forgotPasswordReducer = ( state = forgotPasswordState, action ) => {
    switch ( action.type ) {

        case SET_FORGOT_PASSWORD_MESSAGE:
            return {
                ...state,
                [ action.messageType + 'Message' ]: action.message 
            };

        case SET_FORGOT_PASSWORD_SUCCESS:
            return{
                ...state,
                success: action.success
            };
            
        default:
            return state;
            
    }
};