import { forgotPasswordState } from '../';
import * as selectors from '../selectors';

describe( 'Forgot Password Selectors ::', () => {

    describe( 'getForgotPasswordSuccess()', () => {
        it( 'should return false if the state is incomplete', () => {
            expect( selectors.getForgotPasswordSuccess( {} ) ).toEqual( false );
        } );

        it( 'should return true if success is true', () => {
            const newState = {
                forgotPassword: {
                    ...forgotPasswordState,
                    success: true
                }
            };
            expect( selectors.getForgotPasswordSuccess( newState ) ).toEqual( true );          
        } );
    } );

    describe( 'getForgotPasswordSuccessMessage()', () => {
        it( 'should return false if the state is incomplete', () => {
            expect( selectors.getForgotPasswordSuccessMessage( {} ) ).toEqual( false );
        } );

        it( 'should return string', () => {
            const newState = {
                forgotPassword: {
                    ...forgotPasswordState,
                    successMessage: 'success' 
                }
            };
            expect( selectors.getForgotPasswordSuccessMessage( newState ) ).toEqual( 'success' );          
        } );
    } );

    describe( 'getForgotPasswordErrorMessage()', () => {
        it( 'should return false if the state is incomplete', () => {
            expect( selectors.getForgotPasswordErrorMessage( {} ) ).toEqual( false );
        } );

        it( 'should return string', () => {
            const newState = {
                forgotPassword: {
                    ...forgotPasswordState,
                    errorMessage: 'error'
                }
            };
            expect( selectors.getForgotPasswordErrorMessage( newState ) ).toEqual( 'error' );          
        } );
    } );

} );