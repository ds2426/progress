import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import { ponyfill, setOptions } from 'biw-client-fetch';

import {
    forgotPasswordReducer,
    forgotPasswordState,
    SET_FORGOT_PASSWORD_MESSAGE,
    SET_FORGOT_PASSWORD_SUCCESS,
    requestPassword
} from '../';

describe( 'Forgot password dux', () => {

    setOptions( { baseURL: 'http://www.test.com', headers: {} } );

    let sandbox;
    let mockStore;
    let url;

    beforeEach( () => {
        sandbox = fetchMock.sandbox();
        spyOn( ponyfill, 'fetch' ).and.callFake( sandbox );
        const middlewares = [ thunk ];
        mockStore = configureStore( middlewares );
        url = 'http://www.test.com/api/v1.0/forgot-password';
    } );

    it( 'should send password email', async () => {

        const response = { response: 'Email sent Successfully' };

        const expectedAction = [
            {
                type: SET_FORGOT_PASSWORD_SUCCESS,
                success: true
            },
            {
                type: SET_FORGOT_PASSWORD_MESSAGE,
                messageType: 'success',
                message: 'forgotPassword.form.successMessage'
            }
        ];
        const expectedStateOne = {
            ...forgotPasswordState,
            success: true
        };
        const expectedStateTwo = {
            ...forgotPasswordState,
            successMessage: 'forgotPassword.form.successMessage'
        };

        sandbox.once( url, response );

        const store = mockStore( {} );

        return store.dispatch( requestPassword( {} ) ).then(
            () => {
                const actions = store.getActions();
                expect( actions ).toEqual( expectedAction );
                return actions;
            }

        ).then( ( actions ) => {
            expect( forgotPasswordReducer( forgotPasswordState, actions[ 0 ] ) ).toEqual( expectedStateOne );
            expect( forgotPasswordReducer( forgotPasswordState, actions[ 1 ] ) ).toEqual( expectedStateTwo );
        } );

    } );

    it( 'should log error', async () => {

        const response = { errorCode: 'error', responseMessage: 'error' };
        const expectedAction = [
            {
                type: SET_FORGOT_PASSWORD_SUCCESS,
                success: false
            },
            {
                type: SET_FORGOT_PASSWORD_MESSAGE,
                messageType: 'error',
                message: { text: 'error', errorCode: 'error' }
            }
        ];
        const expectedStateOne = {
            ...forgotPasswordState,
            success: false
        };
        const expectedStateTwo = {
            ...forgotPasswordState,
            errorMessage: { text: 'error', errorCode: 'error' }
        };

        sandbox.once( url, response );

        const store = mockStore( {} );

        return store.dispatch( requestPassword( {} ) ).then(
            () => {
                const actions = store.getActions();
                expect( actions ).toEqual( expectedAction );
                return actions;
            }
        ).then( ( actions ) => {
            expect( forgotPasswordReducer( forgotPasswordState, actions[ 0 ] ) ).toEqual( expectedStateOne );
            expect( forgotPasswordReducer( forgotPasswordState, actions[ 1 ] ) ).toEqual( expectedStateTwo );
        } );

    } );

} );
