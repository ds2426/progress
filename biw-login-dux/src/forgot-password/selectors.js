import get from 'lodash/get';

export const getForgotPasswordErrorMessage = ( state ) => {
    return get( state, 'forgotPassword.errorMessage', false );
};

export const getForgotPasswordSuccess = ( state ) => {
    return get( state, 'forgotPassword.success', false );
};

export const getForgotPasswordSuccessMessage = ( state ) => {
    return get( state, 'forgotPassword.successMessage', false );
};