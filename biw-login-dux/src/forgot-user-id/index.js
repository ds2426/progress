import { clientFetch } from 'biw-client-fetch';

/*******************************************************************************
 * Initial state
 */

export const forgotUserIdState = {
    successMessage: false,
    errorMessage: false,
    success: false
};

/*******************************************************************************
 * Non-async action types
 */

export const FORGOT_USER_ID = 'forgotUserId';
export const SET_FORGOT_USER_ID_MESSAGE = 'setForgotUserIdMessage';
export const SET_FORGOT_USER_ID_SUCCESS = 'setForgotUserIdSuccess';

/*******************************************************************************
 * Action creators
 */

export const setMessage = ( messageType, message ) => {
    return{
        type: SET_FORGOT_USER_ID_MESSAGE,
        messageType,
        message
    };
};

export const setSuccess = ( bool ) => {
    return{
        type: SET_FORGOT_USER_ID_SUCCESS,
        success: bool
    };
};

export const requestUsername = ( body ) => {
    const successMessage = 'forgotUserId.form.successMessage';
    return dispatch => {

        return clientFetch( {
            url: '/api/v1.0/forgot-username',
            type: FORGOT_USER_ID,
            method: 'post',
            body: body
        } ).then( ( response ) => {

            if ( !response || !Object.keys( response ).length ) {
                dispatch( setSuccess( false ) );
                dispatch( setMessage( 'error', { text: null, errorCode: null } ) );

                return;
            }
            
            if( response.errorCode ) {
                dispatch( setSuccess( false ) );
                dispatch( setMessage( 'error', { text: response.responseMessage, errorCode: response.errorCode } ) );
            } else {
                dispatch( setSuccess( true ) );
                dispatch( setMessage( 'success', successMessage ) );
            }
        } );
    };
};

/*******************************************************************************
 * Reducer
 */
 
export const forgotUserIdReducer = ( state = forgotUserIdState, action ) => {
    switch ( action.type ) {

        case SET_FORGOT_USER_ID_MESSAGE:
            return{
                ...state,
                [ action.messageType + 'Message' ]: action.message
            };
        case SET_FORGOT_USER_ID_SUCCESS:
            return {
                ...state,
                success: action.success
            };
        default:
            return state;
            
    }
};