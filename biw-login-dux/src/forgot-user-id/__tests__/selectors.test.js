import { forgotUserIdState } from '../';
import * as selectors from '../selectors';

describe( 'Forgot User Id Selectors ::', () => {

    describe( 'getForgotUserIdSuccess()', () => {
        it( 'should return false if the state is incomplete', () => {
            expect( selectors.getForgotUserIdSuccess( {} ) ).toEqual( false );
        } );

        it( 'should return true if success is true', () => {
            const newState = {
                forgotUserId: {
                    ...forgotUserIdState,
                    success: true
                }
            };
            expect( selectors.getForgotUserIdSuccess( newState ) ).toEqual( true );          
        } );
    } );

    describe( 'getForgotUserIdSuccessMessage()', () => {
        it( 'should return false if the state is incomplete', () => {
            expect( selectors.getForgotUserIdSuccessMessage( {} ) ).toEqual( false );
        } );

        it( 'should return string', () => {
            const newState = {
                forgotUserId: {
                    ...forgotUserIdState,
                    successMessage: 'success'
                }
            };
            expect( selectors.getForgotUserIdSuccessMessage( newState ) ).toEqual( 'success' );          
        } );
    } );

    describe( 'getForgotUserIdErrorMessage()', () => {
        it( 'should return false if the state is incomplete', () => {
            expect( selectors.getForgotUserIdErrorMessage( {} ) ).toEqual( false );
        } );

        it( 'should return string', () => {
            const newState = {
                forgotUserId: {
                    ...forgotUserIdState,
                    errorMessage: 'error'
                }
            };
            expect( selectors.getForgotUserIdErrorMessage( newState ) ).toEqual( 'error' );          
        } );
    } );

} );