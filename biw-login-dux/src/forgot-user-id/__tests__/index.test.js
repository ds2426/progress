import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import { ponyfill, setOptions } from 'biw-client-fetch';

import {
    forgotUserIdReducer,
    forgotUserIdState,
    SET_FORGOT_USER_ID_MESSAGE,
    SET_FORGOT_USER_ID_SUCCESS,
    requestUsername
} from '../';

describe( 'Forgot user id dux', () => {

    setOptions( { baseURL: 'http://www.test.com', headers: {} } );

    let sandbox;
    let mockStore;
    let url;

    beforeEach( () => {
        sandbox = fetchMock.sandbox();
        spyOn( ponyfill, 'fetch' ).and.callFake( sandbox );
        const middlewares = [ thunk ];
        mockStore = configureStore( middlewares );
        url = 'http://www.test.com/api/v1.0/forgot-username';
    } );

    it( 'should send user id email', () => {

        const response = { response: 'Email sent Successfully' };

        const expectedAction = [
            {
                type: SET_FORGOT_USER_ID_SUCCESS,
                success: true
            },
            {
                type: SET_FORGOT_USER_ID_MESSAGE,
                messageType: 'success',
                message: 'forgotUserId.form.successMessage'
            }
        ];
        const expectedStateOne = {
            ...forgotUserIdState,
            success: true
        };
        const expectedStateTwo = {
            ...forgotUserIdState,
            successMessage: 'forgotUserId.form.successMessage'
        };

        sandbox.once( url, response );

        const store = mockStore( {} );

        return store.dispatch( requestUsername( {} ) ).then(
            () => {
                const actions = store.getActions();
                expect( actions ).toEqual( expectedAction );
                return actions;
            }

        ).then( ( actions ) => {
            expect( forgotUserIdReducer( forgotUserIdState, actions[ 0 ] ) ).toEqual( expectedStateOne );
            expect( forgotUserIdReducer( forgotUserIdState, actions[ 1 ] ) ).toEqual( expectedStateTwo );
        } );

    } );

    it( 'should log error', async () => {

        const response = { errorCode: 'error', responseMessage: 'error' };
        const expectedAction = [
            {
                type: SET_FORGOT_USER_ID_SUCCESS,
                success: false
            },
            {
                type: SET_FORGOT_USER_ID_MESSAGE,
                messageType: 'error',
                message: { text: 'error', errorCode: 'error' }
            }
        ];
        const expectedStateOne = {
            ...forgotUserIdState,
            success: false
        };
        const expectedStateTwo = {
            ...forgotUserIdState,
            errorMessage: { text: 'error', errorCode: 'error' }
        };

        sandbox.once( url, response );

        const store = mockStore( {} );

        return store.dispatch( requestUsername( {} ) ).then(
            () => {
                const actions = store.getActions();
                expect( actions ).toEqual( expectedAction );
                return actions;
            }
        ).then( ( actions ) => {
            expect( forgotUserIdReducer( forgotUserIdState, actions[ 0 ] ) ).toEqual( expectedStateOne );
            expect( forgotUserIdReducer( forgotUserIdState, actions[ 1 ] ) ).toEqual( expectedStateTwo );
        } );

    } );

} );
