import get from 'lodash/get';

export const getForgotUserIdErrorMessage = ( state ) => {
    return get( state, 'forgotUserId.errorMessage', false );
};

export const getForgotUserIdSuccess = ( state ) => {
    return get( state, 'forgotUserId.success', false );
};

export const getForgotUserIdSuccessMessage = ( state ) => {
    return get( state, 'forgotUserId.successMessage', false );
};