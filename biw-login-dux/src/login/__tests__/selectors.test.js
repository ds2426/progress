import { loginState } from '../';
import * as selectors from '../selectors';

describe( 'Login Selectors ::', () => {

    describe( 'getLoginSuccess()', () => {
        it( 'should return false if the state is incomplete', () => {
            expect( selectors.getLoginSuccess( {} ) ).toEqual( false );
        } );

        it( 'should return true if success is true', () => {
            const newState = {
                login: {
                    ...loginState,
                    success: true
                }
            };
            expect( selectors.getLoginSuccess( newState ) ).toEqual( true );          
        } );
    } );

    describe( 'getLoginErrorMessage()', () => {
        it( 'should return string', () => {
            expect( selectors.getLoginErrorMessage( {} ) ).toEqual( false );
        } );

        it( 'should return string', () => {
            const newState = {
                login: {
                    ...loginState,
                    loginErrorMessage: 'error'
                }
            };
            expect( selectors.getLoginErrorMessage( newState ) ).toEqual( 'error' );          
        } );
    } );

    describe( 'getLaunchAsErrorMessage()', () => {
        it( 'should return string', () => {
            expect( selectors.getLaunchAsErrorMessage( {} ) ).toEqual( false );
        } );

        it( 'should return string', () => {
            const newState = {
                login: {
                    ...loginState,
                    launchAsErrorMessage: 'error'
                }
            };
            expect( selectors.getLaunchAsErrorMessage( newState ) ).toEqual( 'error' );          
        } );
    } );

    describe( 'getLoginErrorCode()', () => {
        it( 'should return string', () => {
            expect( selectors.getLoginErrorCode( {} ) ).toEqual( null );
        } );

        it( 'should return string', () => {
            const newState = {
                login: {
                    ...loginState,
                    errorCode: 'err23453or'
                }
            };
            expect( selectors.getLoginErrorCode( newState ) ).toEqual( 'err23453or' );          
        } );
    } );

} );