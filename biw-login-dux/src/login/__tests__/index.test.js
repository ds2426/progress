import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import { setOptions, ponyfill } from 'biw-client-fetch';

import {
    loginReducer,
    loginState,
    SET_USER_DATA,
    SET_LOGIN_SUCCESS,
    SET_LOGIN_MESSAGE,
    login,
    launchAs
} from '../';

setOptions( { baseURL: 'http://www.test.com', headers: {} } );
describe( 'Login action', () => {

    let sandbox;
    let mockStore;
    let loginUrl;
    let userData;

    beforeEach( () => {
        userData = {
            accountNonExpired: true,
            accountNonLocked: true,
            adminUser: {},
            clientCode: 'string',
            clientId: 0,
            credentialsNonExpired: true,
            emailAddress: 'string',
            enabled: true,
            firstName: 'string',
            firstTimeSetupDone: true,
            lastName: 'string',
            locale: 'string',
            loginToken: 'string',
            middleName: 'string',
            suffix: 'string',
            userId: 0,
            userName: 'string'
        };
        sandbox = fetchMock.sandbox();
        spyOn( ponyfill, 'fetch' ).and.callFake( sandbox );
        const middlewares = [ thunk ];
        mockStore = configureStore( middlewares );
        loginUrl = 'http://www.test.com/api/v1.0/login';
    } );

    it( 'should login', async () => {
        const expectedActions = [
            {
                type: SET_LOGIN_SUCCESS,
                success: true
            },
            {
                type: SET_USER_DATA,
                user: {
                    ...userData,
                    authenticated: true
                }
            }
        ];
        const expectedState = {
            ...loginState,
            success: true
        };

        sandbox.once( loginUrl, userData );

        const store = mockStore( {} );

        await store.dispatch( login( {} ) );

        const actions = store.getActions();
        expect( actions ).toEqual( expect.arrayContaining( expectedActions ) );

        const newState = actions.reduce( loginReducer, loginState );
        expect( newState ).toEqual( expectedState );
    } );

    it( 'should log login error', async () => {
        const response = { errorCode: 'error', responseMessage: 'error' };
        const expectedActions = [
            {
                type: SET_LOGIN_SUCCESS,
                success: false
            },
            {
                type: SET_LOGIN_MESSAGE,
                messageType: 'loginError',
                message: { text: 'error', errorCode: 'error' }
            }
        ];
        const expectedState = {
            ...loginState,
            success: false,
            loginErrorMessage: { text: 'error', errorCode: 'error' },
            errorCode: 'error'
        };

        sandbox.once( loginUrl, response );

        const store = mockStore( {} );

        await store.dispatch( login( {} ) );
        const actions = store.getActions();
        expect( actions ).toEqual( expect.arrayContaining( expectedActions ) );

        const newState = actions.reduce( loginReducer, loginState );
        expect( newState ).toEqual( expectedState );
    } );

    it( 'should log 500 error for login', async () => {
        const expectedActions = [
            {
                type: SET_LOGIN_SUCCESS,
                success: false
            },
            {
                type: SET_LOGIN_MESSAGE,
                messageType: 'loginError',
                message: { text: null, errorCode: null }
            }
        ];
        const expectedState = {
            ...loginState,
            success: false,
            loginErrorMessage: { text: null, errorCode: null },
            errorCode: null
        };

        sandbox.once( loginUrl, { status: 500, body: {} } );

        const store = mockStore( {} );
        await store.dispatch( login( {} ) );
        const actions = store.getActions();
        expect( actions ).toEqual( expect.arrayContaining( expectedActions ) );

        const newState = actions.reduce( loginReducer, loginState );
        expect( newState ).toEqual( expectedState );
    } );

    it( 'should accept an object as arguments', async () => {
        const store = mockStore( {} );
        sandbox.once( loginUrl, userData );

        await store.dispatch( login( { username: 'foo', password: 'bar', fhqwgads: 'to the limit' } ) );
        const result = JSON.parse( sandbox.calls().matched[ 0 ][ 1 ].body );
        expect( result ).toEqual( expect.objectContaining( {
            fhqwgads: 'to the limit',
            password: 'bar',
            username: 'foo'
        } ) );
    } );

    it( 'should trim the whitespace on the username property', async () => {
        const store = mockStore( {} );
        sandbox.once( loginUrl, userData );

        await store.dispatch( login( { username: '   foo   ', password: 'bar', fhqwgads: 'to the limit' } ) );
        const result = JSON.parse( sandbox.calls().matched[ 0 ][ 1 ].body );
        expect( result ).toEqual( expect.objectContaining( {
            fhqwgads: 'to the limit',
            password: 'bar',
            username: 'foo'
        } ) );
    } );
} );

describe( 'LaunchAs action', () => {
    let sandbox;
    let mockStore;
    let launchAsUrl;
    let userData;

    beforeEach( () => {
        userData = {
            accountNonExpired: true,
            accountNonLocked: true,
            adminUser: {},
            clientCode: 'string',
            clientId: 0,
            credentialsNonExpired: true,
            emailAddress: 'string',
            enabled: true,
            firstName: 'string',
            firstTimeSetupDone: true,
            lastName: 'string',
            locale: 'string',
            loginToken: 'string',
            middleName: 'string',
            suffix: 'string',
            userId: 0,
            userName: 'string'
        };
        sandbox = fetchMock.sandbox();
        spyOn( ponyfill, 'fetch' ).and.callFake( sandbox );
        const middlewares = [ thunk ];
        mockStore = configureStore( middlewares );
        launchAsUrl = 'http://www.test.com/api/v1.0/launch-as';
    } );

    it( 'should launch as', async () => {
        Object.defineProperty( window.location, 'search', {
          writable: true,
          value: 'testToken'
        } );

        const expectedActions = [
            {
                type: SET_LOGIN_SUCCESS,
                success: true
            },
            {
                type: SET_USER_DATA,
                user: {
                    ...userData,
                    authenticated: true,
                    isLaunchAs: true
                }
            }
        ];
        const expectedState = {
            ...loginState,
            success: true
        };

        sandbox.once( launchAsUrl, userData );

        const store = mockStore( {} );
        await store.dispatch( launchAs() );
        const actions = store.getActions();
        expect( actions ).toEqual( expect.arrayContaining( expectedActions ) );

        const newState = actions.reduce( loginReducer, loginState );
        expect( newState ).toEqual( expectedState );
    } );

    it( 'should log launch as error', async () => {
        const response = { errorCode: 'error', responseMessage: 'error' };
        const expectedActions = [
            {
                type: SET_LOGIN_SUCCESS,
                success: false
            },
            {
                type: SET_LOGIN_MESSAGE,
                messageType: 'launchAsError',
                message: { text: 'error', errorCode: 'error' }
            }
        ];
        const expectedState = {
            ...loginState,
            success: false,
            launchAsErrorMessage: { text: 'error', errorCode: 'error' },
            errorCode: 'error'
        };

        sandbox.once( launchAsUrl, response );

        const store = mockStore( {} );
        await store.dispatch( launchAs() );
        const actions = store.getActions();
        expect( actions ).toEqual( expect.arrayContaining( expectedActions ) );

        const newState = actions.reduce( loginReducer, loginState );
        expect( newState ).toEqual( expectedState );
    } );

    it( 'should log 500 error for launch as', async () => {
        const expectedActions = [
            {
                type: SET_LOGIN_SUCCESS,
                success: false
            },
            {
                type: SET_LOGIN_MESSAGE,
                messageType: 'launchAsError',
                message: { text: null, errorCode: null }
            }
        ];
        const expectedState = {
            ...loginState,
            success: false,
            launchAsErrorMessage: { text: null, errorCode: null },
            errorCode: null
        };

        sandbox.once( launchAsUrl, { status: 500, body: {} } );

        const store = mockStore( {} );
        await store.dispatch( launchAs() );
        const actions = store.getActions();
        expect( actions ).toEqual( expect.arrayContaining( expectedActions ) );

        const newState = actions.reduce( loginReducer, loginState );
        expect( newState ).toEqual( expectedState );
    } );

    it( 'should not launch as because no token', async () => {
        Object.defineProperty( window.location, 'search', {
          writable: true,
          value: null
        } );

        const expectedActions = [
            {
                type: SET_LOGIN_SUCCESS,
                success: false
            },
            {
                type: SET_LOGIN_MESSAGE,
                messageType: 'launchAsError',
                message: { text: 'No login token.' }
            }
        ];
        const expectedState = {
            ...loginState,
            success: false,
            launchAsErrorMessage: { text: 'No login token.' },
            errorCode: null
        };

        sandbox.once( launchAsUrl, userData );

        const store = mockStore( {} );
        await store.dispatch( launchAs() );
        const actions = store.getActions();
        expect( actions ).toEqual( expect.arrayContaining( expectedActions ) );

        const newState = actions.reduce( loginReducer, loginState );
        expect( newState ).toEqual( expectedState );
    } );
} );
