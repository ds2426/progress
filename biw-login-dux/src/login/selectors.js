import get from 'lodash/get';

export const getLoginErrorMessage = ( state ) => {
    return get( state, 'login.loginErrorMessage', false );
};

export const getLaunchAsErrorMessage = ( state ) => {
    return get( state, 'login.launchAsErrorMessage', false );
};

export const getLoginSuccess = ( state ) => {
    return get( state, 'login.success', false );
};

export const getLoginErrorCode = ( state ) => {
    return get( state, 'login.errorCode', null );
};