import { clientFetch } from 'biw-client-fetch';
import get from 'lodash/get';

/*******************************************************************************
 * Initial state
 */

export const loginState = {
    success: false,
    loginErrorMessage: false,
    launchAsErrorMessage: false,
    errorCode: null
};

/*******************************************************************************
 * Non-async action types
 */

export const LOGIN = 'login';
export const SET_LOGIN_MESSAGE = 'setLoginMessage';
export const SET_LOGIN_SUCCESS = 'setLoginSuccess';
export const LAUNCH_AS = 'launchAs';

export const SET_USER_DATA = 'setUserData';

/*******************************************************************************
 * Action creators
 */

export const setMessage = ( messageType, message ) => ( {
    type: SET_LOGIN_MESSAGE,
    messageType,
    message
} );

export const setSuccess = bool => ( {
    type: SET_LOGIN_SUCCESS,
    success: bool
} );

const isFailedResponse = response => !response || !Object.keys( response ).length || response.errorCode;

export const login = body => dispatch => clientFetch( {
    url: '/api/v1.0/login',
    type: LOGIN,
    method: 'post',
    body: {
        ...body,
        username: ( body.username || '' ).trim()
    }
} ).then( ( response ) => {
    const isSuccess = !isFailedResponse( response );
    if ( !isSuccess ) {
        dispatch( setMessage( 'loginError', {
            text: get( response, 'responseMessage', null ),
            errorCode: get( response, 'errorCode', null )
        } ) );
    } else {
        dispatch( {
            type: SET_USER_DATA,
            user: {
                ...response,
                authenticated: true
            }
        } );
    }

    dispatch( setSuccess( isSuccess ) );
    return isSuccess;
} );

export const launchAs = () => dispatch => {
    const searchString = window.location.search;
    if ( !searchString || searchString.length === 0 ) {
        dispatch( setMessage( 'launchAsError', { text: 'No login token.' } ) );
        dispatch( setSuccess( false ) );
        return Promise.resolve( false );
    }

    const token = window.location.search.substring( 1 );

    return clientFetch( {
        url: '/api/v1.0/launch-as',
        type: LAUNCH_AS,
        method: 'post',
        body: { token }
    } ).then( ( response ) => {
        let isSuccess = false;
        if ( isFailedResponse( response ) ) {
            dispatch( setMessage( 'launchAsError', {
                text: get( response, 'responseMessage', null ),
                errorCode: get( response, 'errorCode', null )
            } ) );
        } else {
            isSuccess = true;
            dispatch( {
                type: SET_USER_DATA,
                user: {
                    ...response,
                    authenticated: true,
                    isLaunchAs: true
                }
            } );
        }
        dispatch( setSuccess( isSuccess ) );
        return isSuccess;
    } );
};

/*******************************************************************************
 * Reducer
 */

export const loginReducer = ( state = loginState, action ) => {
    switch ( action.type ) {

        case SET_LOGIN_MESSAGE:
            return {
                ...state,
                [ action.messageType + 'Message' ]: action.message,
                errorCode: action.message.errorCode || null
            };
        case SET_LOGIN_SUCCESS:
            return {
                ...state,
                success: action.success
            };
        default:
            return state;

    }
};
