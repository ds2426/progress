export const message = ( type, messageType, message ) => {
    return{
        type,
        messageType,
        message
    };
 };

export const success = ( type, bool ) => {
    return{
        type,
        success: bool
    };
};