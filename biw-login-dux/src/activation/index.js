import { clientFetch } from 'biw-client-fetch';

/*******************************************************************************
 * Initial state
 */

export const activationState = {
    success: false,
    preActivationLoginAttemptFlag: false,
    warningMessage: null,
    errorMessage: false
};

/*******************************************************************************
 * Non-async action types
 */

export const ACTIVATE = 'activate';
export const SET_ACTIVATE_SUCCESS = 'setActivateSuccess';
export const SET_ACTIVATE_MESSAGE = 'setActivateError';
export const PRE_ACTIVATION_LOGIN_ATTEMPT = 'preActivationLoginAttempt';

export const SET_USER_DATA = 'setUserData';

/*******************************************************************************
 * Action creators
 */

export const setMessage = ( messageType, message ) => {
    return{
        type: SET_ACTIVATE_MESSAGE,
        messageType,
        message
    };
};

export const setSuccess = ( bool ) => {
    return{
        type: SET_ACTIVATE_SUCCESS,
        success: bool
    };
};

export const preActivationLoginAttempt = ( flag, message ) => {
    return {
        type: PRE_ACTIVATION_LOGIN_ATTEMPT,
        flag: flag,
        warningMessage: message
    };
};

export const activate = ( body ) => {
    return dispatch => {

        return clientFetch( {
            url: '/api/v1.0/activate',
            method: 'post',
            type: ACTIVATE,
            body: body
        } ).then( ( response ) => {

            if ( !response || !Object.keys( response ).length ) {
                dispatch( setSuccess( false ) );
                dispatch( setMessage( 'error', { text: null, errorCode: null } ) );

                return;
            }
            
            if( response.errorCode ) {
                dispatch( setSuccess( false ) );
                dispatch( setMessage( 'error', { text: response.responseMessage, errorCode: response.errorCode } ) );
            } else {
                
                dispatch( {
                    type: SET_USER_DATA,
                    user: {
                        ...response,
                        authenticated: true
                    }
                } );
                dispatch( setSuccess( true ) );
            }
        } );
        
    };
};

/*******************************************************************************
 * Reducer
 */
 
export const activationReducer = ( state = activationState, action ) => {
    switch ( action.type ) {

        case SET_ACTIVATE_MESSAGE:
            return {
                ...state,
                [ action.messageType + 'Message' ]: action.message 
            };
        case SET_ACTIVATE_SUCCESS:
            return {
                ...state,
                success: action.success
            };
        case PRE_ACTIVATION_LOGIN_ATTEMPT:
            return {
                ...state,
                preActivationLoginAttemptFlag: action.flag,
                warningMessage: action.warningMessage
            };
        default:
            return state;

    }
};