import get from 'lodash/get';

export const getActivationSuccess = ( state ) => {
    return get( state, 'activation.success', false );
};

export const getActivationWarningMessage = ( state ) => {
    return get( state, 'activation.warningMessage', false );
};

export const getActivationErrorMessage = ( state ) => {
    return get( state, 'activation.errorMessage', false );
};

export const getActivationLoginAttemptFlag = ( state ) => {
    return get( state, 'activation.preActivationLoginAttemptFlag', false );
};