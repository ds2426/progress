import { activationState } from '../';
import * as selectors from '../selectors';

describe( 'Activation Selectors ::', () => {

    describe( 'getActivationSuccess()', () => {
        it( 'should return false if the state is incomplete', () => {
            expect( selectors.getActivationSuccess( {} ) ).toEqual( false );
        } );

        it( 'should return true if success is true', () => {
            const newState = {
                activation: {
                    ...activationState,
                    success: true
                } 
            };
            expect( selectors.getActivationSuccess( newState ) ).toEqual( true );          
        } );
    } );

    describe( 'getActivationWarningMessage()', () => {
        it( 'should return false if the state is incomplete', () => {
            expect( selectors.getActivationWarningMessage( {} ) ).toEqual( false );
        } );

        it( 'should return string', () => {
            const newState = {
                activation: {
                    ...activationState,
                    warningMessage: 'warning'
                }
            };
            expect( selectors.getActivationWarningMessage( newState ) ).toEqual( 'warning' );          
        } );
    } );

    describe( 'getActivationErrorMessage()', () => {
        it( 'should return false if the state is incomplete', () => {
            expect( selectors.getActivationErrorMessage( {} ) ).toEqual( false );
        } );

        it( 'should return string', () => {
            const newState = {
                activation: {
                    ...activationState,
                    errorMessage: 'error'
                }
            };
            expect( selectors.getActivationErrorMessage( newState ) ).toEqual( 'error' );          
        } );
    } );

    describe( 'getActivationLoginAttemptFlag()', () => {
        it( 'should return false if the state is incomplete', () => {
            expect( selectors.getActivationLoginAttemptFlag( {} ) ).toEqual( false );
        } );

        it( 'should return true if preActivationLoginAttemptFlag is true', () => {
            const newState = {
                activation: {
                    ...activationState,
                    preActivationLoginAttemptFlag: true
                }
            };
            expect( selectors.getActivationLoginAttemptFlag( newState ) ).toEqual( true );          
        } );
    } );

} );