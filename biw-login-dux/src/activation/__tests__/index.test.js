import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import { ponyfill, setOptions } from 'biw-client-fetch';

import {
    activationReducer,
    activationState,
    SET_ACTIVATE_MESSAGE,
    SET_ACTIVATE_SUCCESS,
    SET_USER_DATA,
    activate
} from '../';

describe( 'Activation dux', () => {

    setOptions( { baseURL: 'http://www.test.com', headers: {} } );

    let sandbox;
    let mockStore;
    let url;

    beforeEach( () => {
        sandbox = fetchMock.sandbox();
        spyOn( ponyfill, 'fetch' ).and.callFake( sandbox );
        const middlewares = [ thunk ];
        mockStore = configureStore( middlewares );
        url = 'http://www.test.com/api/v1.0/activate';
    } );

    it( 'should activate', async () => {

        const userData = {
            accountNonExpired: true,
            accountNonLocked: true,
            adminUser: {},
            clientCode: 'string',
            clientId: 0,
            credentialsNonExpired: true,
            emailAddress: 'string',
            enabled: true,
            firstName: 'string',
            firstTimeSetupDone: true,
            lastName: 'string',
            locale: 'string',
            loginToken: 'string',
            middleName: 'string',
            suffix: 'string',
            userId: 0,
            userName: 'string'
        };

        const expectedAction = [
            {
                type: SET_USER_DATA,
                user: {
                    ...userData,
                    authenticated: true
                }
            },
            {
                type: SET_ACTIVATE_SUCCESS,
                success: true
            }
        ];
        const expectedStateOne = {
            ...activationState,
            success: true
        };

        sandbox.once( url, userData );

        const store = mockStore( {} );

        return store.dispatch( activate( {} ) ).then(
            () => {
                const actions = store.getActions();
                expect( actions ).toEqual( expectedAction );
                return actions;
            }

        ).then( ( actions ) => {
            expect( activationReducer( activationState, actions[ 0 ] ) ).toEqual( activationState );
            expect( activationReducer( activationState, actions[ 1 ] ) ).toEqual( expectedStateOne );
        } );

    } );

    it( 'should log error', async () => {

        const response = { errorCode: 'error', responseMessage: 'error' };
        const expectedAction = [
            {
                type: SET_ACTIVATE_SUCCESS,
                success: false
            },
            {
                type: SET_ACTIVATE_MESSAGE,
                messageType: 'error',
                message: { text: 'error', errorCode: 'error' }
            }
        ];
        const expectedStateOne = {
            ...activationState,
            success: false
        };
        const expectedStateTwo = {
            ...activationState,
            errorMessage: { text: 'error', errorCode: 'error' }
        };

        sandbox.once( url, response );

        const store = mockStore( {} );

        return store.dispatch( activate( {} ) ).then(
            () => {
                const actions = store.getActions();
                expect( actions ).toEqual( expectedAction );
                return actions;
            }
        ).then( ( actions ) => {
            expect( activationReducer( activationState, actions[ 0 ] ) ).toEqual( expectedStateOne );
            expect( activationReducer( activationState, actions[ 1 ] ) ).toEqual( expectedStateTwo );
        } );

    } );

} );
