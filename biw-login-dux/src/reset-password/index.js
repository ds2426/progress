import { clientFetch } from 'biw-client-fetch';

/*******************************************************************************
 * Initial state
 */

export const resetPasswordState = {
    successMessage: false,
    errorMessage: false,
    success: false
};

/*******************************************************************************
 * Non-async action types
 */

export const SAVE_PASSWORD = 'savePassword';
export const SET_SAVE_PASSWORD_SUCCESS = 'savePasswordSuccess';
export const SET_SAVE_PASSWORD_MESSAGE = 'setSavePasswordMessage';

/*******************************************************************************
 * Action creators
 */

export const setMessage = ( messageType, message ) => {
    return{
        type: SET_SAVE_PASSWORD_MESSAGE,
        messageType,
        message
    };
};

export const setSuccess = ( bool ) => {
    return{
        type: SET_SAVE_PASSWORD_SUCCESS,
        success: bool
    };
};

export const savePassword = ( body ) => {
    const successMessage = 'resetPassword.form.successMessage';
    return ( dispatch ) => {

        return clientFetch( {
            url: '/api/v1.0/savePassword',
            type: SAVE_PASSWORD,
            method: 'post',
            body: body
        } ).then( ( response ) => {

            if ( !response || !Object.keys( response ).length ) {
                dispatch( setSuccess( false ) );
                dispatch( setMessage( 'error', { text: null, errorCode: null } ) );

                return;
            }
            
            if( response.errorCode ) {
                dispatch( setSuccess( false ) );
                dispatch( setMessage( 'error', { text: response.responseMessage, errorCode: response.errorCode } ) );
            } else {
                dispatch( setSuccess( true ) );
                dispatch( setMessage( 'success', successMessage ) );
            }
        } );
    };
};

/*******************************************************************************
 * Reducer
 */
 
export const resetPasswordReducer = ( state = resetPasswordState, action ) => {
    switch ( action.type ) {

        case SET_SAVE_PASSWORD_MESSAGE:
            return {
                ...state,
                [ action.messageType + 'Message' ]: action.message
            };
        case SET_SAVE_PASSWORD_SUCCESS:
            return {
                ...state,
                success: action.success
            };
        default:
            return state;
    }
};