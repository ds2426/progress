import get from 'lodash/get';

export const getResetPasswordErrorMessage = ( state ) => {
    return get( state, 'resetPassword.errorMessage', false );
};

export const getResetPasswordSuccess = ( state ) => {
    return get( state, 'resetPassword.success', false );
};

export const getResetPasswordSuccessMessage = ( state ) => {
    return get( state, 'resetPassword.successMessage', false );
};