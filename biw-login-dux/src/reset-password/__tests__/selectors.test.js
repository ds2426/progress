import { resetPasswordState } from '../';
import * as selectors from '../selectors';

describe( 'Reset Password Selectors ::', () => {

    describe( 'getResetPasswordSuccess()', () => {
        it( 'should return false if the state is incomplete', () => {
            expect( selectors.getResetPasswordSuccess( {} ) ).toEqual( false );
        } );

        it( 'should return true if success is true', () => {
            const newState = {
                resetPassword: {
                    ...resetPasswordState,
                    success: true
                } 
            };
            expect( selectors.getResetPasswordSuccess( newState ) ).toEqual( true );          
        } );
    } );

    describe( 'getResetPasswordSuccessMessage()', () => {
        it( 'should return false if the state is incomplete', () => {
            expect( selectors.getResetPasswordSuccessMessage( {} ) ).toEqual( false );
        } );

        it( 'should return string', () => {
            const newState = {
                resetPassword: {
                    ...resetPasswordState,
                    successMessage: 'success' 
                }
            };
            expect( selectors.getResetPasswordSuccessMessage( newState ) ).toEqual( 'success' );          
        } );
    } );

    describe( 'getResetPasswordErrorMessage()', () => {
        it( 'should return false if the state is incomplete', () => {
            expect( selectors.getResetPasswordErrorMessage( {} ) ).toEqual( false );
        } );

        it( 'should return string', () => {
            const newState = {
                resetPassword: {
                    ...resetPasswordState,
                    errorMessage: 'error'  
                }
            };
            expect( selectors.getResetPasswordErrorMessage( newState ) ).toEqual( 'error' );          
        } );
    } );

} );