import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import { ponyfill, setOptions } from 'biw-client-fetch';

import {
    resetPasswordReducer,
    resetPasswordState,
    SET_SAVE_PASSWORD_MESSAGE,
    SET_SAVE_PASSWORD_SUCCESS,
    savePassword
} from '../';

describe( 'Activation dux', () => {

    setOptions( { baseURL: 'http://www.test.com', headers: {} } );

    let sandbox;
    let mockStore;
    let url;

    beforeEach( () => {
        sandbox = fetchMock.sandbox();
        spyOn( ponyfill, 'fetch' ).and.callFake( sandbox );
        const middlewares = [ thunk ];
        mockStore = configureStore( middlewares );
        url = 'http://www.test.com/api/v1.0/savePassword';
    } );

    it( 'should save password', async () => {

        const response = { response: 'Email sent Successfully' };

        const expectedAction = [
            {
                type: SET_SAVE_PASSWORD_SUCCESS,
                success: true
            },
            {
                type: SET_SAVE_PASSWORD_MESSAGE,
                messageType: 'success',
                message: 'resetPassword.form.successMessage'
            }
        ];
        const expectedStateOne = {
            ...resetPasswordState,
            success: true
        };
        const expectedStateTwo = {
            ...resetPasswordState,
            successMessage: 'resetPassword.form.successMessage'
        };

        sandbox.once( url, response );

        const store = mockStore( {} );

        return store.dispatch( savePassword( {} ) ).then(
            () => {
                const actions = store.getActions();
                expect( actions ).toEqual( expectedAction );
                return actions;
            }

        ).then( ( actions ) => {
            expect( resetPasswordReducer( resetPasswordState, actions[ 0 ] ) ).toEqual( expectedStateOne );
            expect( resetPasswordReducer( resetPasswordState, actions[ 1 ] ) ).toEqual( expectedStateTwo );
        } );

    } );

    it( 'should log error', async () => {

        const response = { errorCode: 'error', responseMessage: 'error' };
        const expectedAction = [
            {
                type: SET_SAVE_PASSWORD_SUCCESS,
                success: false
            },
            {
                type: SET_SAVE_PASSWORD_MESSAGE,
                messageType: 'error',
                message: { text: 'error', errorCode: 'error' }
            }
        ];
        const expectedStateOne = {
            ...resetPasswordState,
            success: false
        };
        const expectedStateTwo = {
            ...resetPasswordState,
            errorMessage: { text: 'error', errorCode: 'error' }
        };

        sandbox.once( url, response );

        const store = mockStore( {} );

        return store.dispatch( savePassword( {} ) ).then(
            () => {
                const actions = store.getActions();
                expect( actions ).toEqual( expectedAction );
                return actions;
            }
        ).then( ( actions ) => {
            expect( resetPasswordReducer( resetPasswordState, actions[ 0 ] ) ).toEqual( expectedStateOne );
            expect( resetPasswordReducer( resetPasswordState, actions[ 1 ] ) ).toEqual( expectedStateTwo );
        } );

    } );

} );
