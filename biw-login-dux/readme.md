# Login Dux

Our master login dux. 

## Implementation

##### Install

```bash
yarn add biw-login-dux
```

##### Import

```javascript
// individual dux files
import { activationDux } from 'biw-login-dux';
import { forgotPasswordDux } from 'biw-login-dux';
import { forgotUserIdDux } from 'biw-login-dux';
import { loginDux } from 'biw-login-dux';
import { resetPasswordDux } from 'biw-login-dux';
// all reducers combined
import { combinedLoginReducers } from 'biw-login-dux';
// setAny.message() and setAny.success() utils
import { setAny } from 'biw-client-fetch';
```

##### Use

Refer to [ contests-webapp login route ]( https://github.biworldwide.com/cpd/contests-webapp/tree/develop/src/app/components/routes/login ) for usage.

Enjoy!

:sweat_drops: