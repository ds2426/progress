'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.forgotUserIdReducer = exports.requestUsername = exports.setSuccess = exports.setMessage = exports.SET_FORGOT_USER_ID_SUCCESS = exports.SET_FORGOT_USER_ID_MESSAGE = exports.FORGOT_USER_ID = exports.forgotUserIdState = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _biwClientFetch = require('biw-client-fetch');

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*******************************************************************************
 * Initial state
 */

var forgotUserIdState = exports.forgotUserIdState = {
    successMessage: false,
    errorMessage: false,
    success: false
};

/*******************************************************************************
 * Non-async action types
 */

var FORGOT_USER_ID = exports.FORGOT_USER_ID = 'forgotUserId';
var SET_FORGOT_USER_ID_MESSAGE = exports.SET_FORGOT_USER_ID_MESSAGE = 'setForgotUserIdMessage';
var SET_FORGOT_USER_ID_SUCCESS = exports.SET_FORGOT_USER_ID_SUCCESS = 'setForgotUserIdSuccess';

/*******************************************************************************
 * Action creators
 */

var setMessage = exports.setMessage = function setMessage(messageType, message) {
    return {
        type: SET_FORGOT_USER_ID_MESSAGE,
        messageType: messageType,
        message: message
    };
};

var setSuccess = exports.setSuccess = function setSuccess(bool) {
    return {
        type: SET_FORGOT_USER_ID_SUCCESS,
        success: bool
    };
};

var requestUsername = exports.requestUsername = function requestUsername(body) {
    var successMessage = 'forgotUserId.form.successMessage';
    return function (dispatch) {

        return (0, _biwClientFetch.clientFetch)({
            url: '/api/v1.0/forgot-username',
            type: FORGOT_USER_ID,
            method: 'post',
            body: body
        }).then(function (response) {

            if (!response || !Object.keys(response).length) {
                dispatch(setSuccess(false));
                dispatch(setMessage('error', { text: null, errorCode: null }));

                return;
            }

            if (response.errorCode) {
                dispatch(setSuccess(false));
                dispatch(setMessage('error', { text: response.responseMessage, errorCode: response.errorCode }));
            } else {
                dispatch(setSuccess(true));
                dispatch(setMessage('success', successMessage));
            }
        });
    };
};

/*******************************************************************************
 * Reducer
 */

var forgotUserIdReducer = exports.forgotUserIdReducer = function forgotUserIdReducer() {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : forgotUserIdState;
    var action = arguments[1];

    switch (action.type) {

        case SET_FORGOT_USER_ID_MESSAGE:
            return _extends({}, state, _defineProperty({}, action.messageType + 'Message', action.message));
        case SET_FORGOT_USER_ID_SUCCESS:
            return _extends({}, state, {
                success: action.success
            });
        default:
            return state;

    }
};
//# sourceMappingURL=index.js.map
