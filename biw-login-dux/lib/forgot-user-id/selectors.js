'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getForgotUserIdSuccessMessage = exports.getForgotUserIdSuccess = exports.getForgotUserIdErrorMessage = undefined;

var _get = require('lodash/get');

var _get2 = _interopRequireDefault(_get);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var getForgotUserIdErrorMessage = exports.getForgotUserIdErrorMessage = function getForgotUserIdErrorMessage(state) {
    return (0, _get2.default)(state, 'forgotUserId.errorMessage', false);
};

var getForgotUserIdSuccess = exports.getForgotUserIdSuccess = function getForgotUserIdSuccess(state) {
    return (0, _get2.default)(state, 'forgotUserId.success', false);
};

var getForgotUserIdSuccessMessage = exports.getForgotUserIdSuccessMessage = function getForgotUserIdSuccessMessage(state) {
    return (0, _get2.default)(state, 'forgotUserId.successMessage', false);
};
//# sourceMappingURL=selectors.js.map
