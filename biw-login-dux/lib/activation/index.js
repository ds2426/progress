'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.activationReducer = exports.activate = exports.preActivationLoginAttempt = exports.setSuccess = exports.setMessage = exports.SET_USER_DATA = exports.PRE_ACTIVATION_LOGIN_ATTEMPT = exports.SET_ACTIVATE_MESSAGE = exports.SET_ACTIVATE_SUCCESS = exports.ACTIVATE = exports.activationState = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _biwClientFetch = require('biw-client-fetch');

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*******************************************************************************
 * Initial state
 */

var activationState = exports.activationState = {
    success: false,
    preActivationLoginAttemptFlag: false,
    warningMessage: null,
    errorMessage: false
};

/*******************************************************************************
 * Non-async action types
 */

var ACTIVATE = exports.ACTIVATE = 'activate';
var SET_ACTIVATE_SUCCESS = exports.SET_ACTIVATE_SUCCESS = 'setActivateSuccess';
var SET_ACTIVATE_MESSAGE = exports.SET_ACTIVATE_MESSAGE = 'setActivateError';
var PRE_ACTIVATION_LOGIN_ATTEMPT = exports.PRE_ACTIVATION_LOGIN_ATTEMPT = 'preActivationLoginAttempt';

var SET_USER_DATA = exports.SET_USER_DATA = 'setUserData';

/*******************************************************************************
 * Action creators
 */

var setMessage = exports.setMessage = function setMessage(messageType, message) {
    return {
        type: SET_ACTIVATE_MESSAGE,
        messageType: messageType,
        message: message
    };
};

var setSuccess = exports.setSuccess = function setSuccess(bool) {
    return {
        type: SET_ACTIVATE_SUCCESS,
        success: bool
    };
};

var preActivationLoginAttempt = exports.preActivationLoginAttempt = function preActivationLoginAttempt(flag, message) {
    return {
        type: PRE_ACTIVATION_LOGIN_ATTEMPT,
        flag: flag,
        warningMessage: message
    };
};

var activate = exports.activate = function activate(body) {
    return function (dispatch) {

        return (0, _biwClientFetch.clientFetch)({
            url: '/api/v1.0/activate',
            method: 'post',
            type: ACTIVATE,
            body: body
        }).then(function (response) {

            if (!response || !Object.keys(response).length) {
                dispatch(setSuccess(false));
                dispatch(setMessage('error', { text: null, errorCode: null }));

                return;
            }

            if (response.errorCode) {
                dispatch(setSuccess(false));
                dispatch(setMessage('error', { text: response.responseMessage, errorCode: response.errorCode }));
            } else {

                dispatch({
                    type: SET_USER_DATA,
                    user: _extends({}, response, {
                        authenticated: true
                    })
                });
                dispatch(setSuccess(true));
            }
        });
    };
};

/*******************************************************************************
 * Reducer
 */

var activationReducer = exports.activationReducer = function activationReducer() {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : activationState;
    var action = arguments[1];

    switch (action.type) {

        case SET_ACTIVATE_MESSAGE:
            return _extends({}, state, _defineProperty({}, action.messageType + 'Message', action.message));
        case SET_ACTIVATE_SUCCESS:
            return _extends({}, state, {
                success: action.success
            });
        case PRE_ACTIVATION_LOGIN_ATTEMPT:
            return _extends({}, state, {
                preActivationLoginAttemptFlag: action.flag,
                warningMessage: action.warningMessage
            });
        default:
            return state;

    }
};
//# sourceMappingURL=index.js.map
