'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getActivationLoginAttemptFlag = exports.getActivationErrorMessage = exports.getActivationWarningMessage = exports.getActivationSuccess = undefined;

var _get = require('lodash/get');

var _get2 = _interopRequireDefault(_get);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var getActivationSuccess = exports.getActivationSuccess = function getActivationSuccess(state) {
    return (0, _get2.default)(state, 'activation.success', false);
};

var getActivationWarningMessage = exports.getActivationWarningMessage = function getActivationWarningMessage(state) {
    return (0, _get2.default)(state, 'activation.warningMessage', false);
};

var getActivationErrorMessage = exports.getActivationErrorMessage = function getActivationErrorMessage(state) {
    return (0, _get2.default)(state, 'activation.errorMessage', false);
};

var getActivationLoginAttemptFlag = exports.getActivationLoginAttemptFlag = function getActivationLoginAttemptFlag(state) {
    return (0, _get2.default)(state, 'activation.preActivationLoginAttemptFlag', false);
};
//# sourceMappingURL=selectors.js.map
