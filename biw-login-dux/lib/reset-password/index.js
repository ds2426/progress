'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.resetPasswordReducer = exports.savePassword = exports.setSuccess = exports.setMessage = exports.SET_SAVE_PASSWORD_MESSAGE = exports.SET_SAVE_PASSWORD_SUCCESS = exports.SAVE_PASSWORD = exports.resetPasswordState = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _biwClientFetch = require('biw-client-fetch');

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*******************************************************************************
 * Initial state
 */

var resetPasswordState = exports.resetPasswordState = {
    successMessage: false,
    errorMessage: false,
    success: false
};

/*******************************************************************************
 * Non-async action types
 */

var SAVE_PASSWORD = exports.SAVE_PASSWORD = 'savePassword';
var SET_SAVE_PASSWORD_SUCCESS = exports.SET_SAVE_PASSWORD_SUCCESS = 'savePasswordSuccess';
var SET_SAVE_PASSWORD_MESSAGE = exports.SET_SAVE_PASSWORD_MESSAGE = 'setSavePasswordMessage';

/*******************************************************************************
 * Action creators
 */

var setMessage = exports.setMessage = function setMessage(messageType, message) {
    return {
        type: SET_SAVE_PASSWORD_MESSAGE,
        messageType: messageType,
        message: message
    };
};

var setSuccess = exports.setSuccess = function setSuccess(bool) {
    return {
        type: SET_SAVE_PASSWORD_SUCCESS,
        success: bool
    };
};

var savePassword = exports.savePassword = function savePassword(body) {
    var successMessage = 'resetPassword.form.successMessage';
    return function (dispatch) {

        return (0, _biwClientFetch.clientFetch)({
            url: '/api/v1.0/savePassword',
            type: SAVE_PASSWORD,
            method: 'post',
            body: body
        }).then(function (response) {

            if (!response || !Object.keys(response).length) {
                dispatch(setSuccess(false));
                dispatch(setMessage('error', { text: null, errorCode: null }));

                return;
            }

            if (response.errorCode) {
                dispatch(setSuccess(false));
                dispatch(setMessage('error', { text: response.responseMessage, errorCode: response.errorCode }));
            } else {
                dispatch(setSuccess(true));
                dispatch(setMessage('success', successMessage));
            }
        });
    };
};

/*******************************************************************************
 * Reducer
 */

var resetPasswordReducer = exports.resetPasswordReducer = function resetPasswordReducer() {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : resetPasswordState;
    var action = arguments[1];

    switch (action.type) {

        case SET_SAVE_PASSWORD_MESSAGE:
            return _extends({}, state, _defineProperty({}, action.messageType + 'Message', action.message));
        case SET_SAVE_PASSWORD_SUCCESS:
            return _extends({}, state, {
                success: action.success
            });
        default:
            return state;
    }
};
//# sourceMappingURL=index.js.map
