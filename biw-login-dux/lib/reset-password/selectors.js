'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getResetPasswordSuccessMessage = exports.getResetPasswordSuccess = exports.getResetPasswordErrorMessage = undefined;

var _get = require('lodash/get');

var _get2 = _interopRequireDefault(_get);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var getResetPasswordErrorMessage = exports.getResetPasswordErrorMessage = function getResetPasswordErrorMessage(state) {
    return (0, _get2.default)(state, 'resetPassword.errorMessage', false);
};

var getResetPasswordSuccess = exports.getResetPasswordSuccess = function getResetPasswordSuccess(state) {
    return (0, _get2.default)(state, 'resetPassword.success', false);
};

var getResetPasswordSuccessMessage = exports.getResetPasswordSuccessMessage = function getResetPasswordSuccessMessage(state) {
    return (0, _get2.default)(state, 'resetPassword.successMessage', false);
};
//# sourceMappingURL=selectors.js.map
