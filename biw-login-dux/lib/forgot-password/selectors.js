'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getForgotPasswordSuccessMessage = exports.getForgotPasswordSuccess = exports.getForgotPasswordErrorMessage = undefined;

var _get = require('lodash/get');

var _get2 = _interopRequireDefault(_get);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var getForgotPasswordErrorMessage = exports.getForgotPasswordErrorMessage = function getForgotPasswordErrorMessage(state) {
    return (0, _get2.default)(state, 'forgotPassword.errorMessage', false);
};

var getForgotPasswordSuccess = exports.getForgotPasswordSuccess = function getForgotPasswordSuccess(state) {
    return (0, _get2.default)(state, 'forgotPassword.success', false);
};

var getForgotPasswordSuccessMessage = exports.getForgotPasswordSuccessMessage = function getForgotPasswordSuccessMessage(state) {
    return (0, _get2.default)(state, 'forgotPassword.successMessage', false);
};
//# sourceMappingURL=selectors.js.map
