'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.forgotPasswordReducer = exports.requestPassword = exports.setSuccess = exports.setMessage = exports.SET_FORGOT_PASSWORD_SUCCESS = exports.SET_FORGOT_PASSWORD_MESSAGE = exports.FORGOT_PASSWORD = exports.forgotPasswordState = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _biwClientFetch = require('biw-client-fetch');

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*******************************************************************************
 * Initial state
 */

var forgotPasswordState = exports.forgotPasswordState = {
    successMessage: false,
    errorMessage: false,
    success: false
};

/*******************************************************************************
 * Non-async action types
 */

var FORGOT_PASSWORD = exports.FORGOT_PASSWORD = 'forgotPassword';
var SET_FORGOT_PASSWORD_MESSAGE = exports.SET_FORGOT_PASSWORD_MESSAGE = 'setForgotPasswordMessage';
var SET_FORGOT_PASSWORD_SUCCESS = exports.SET_FORGOT_PASSWORD_SUCCESS = 'setForgotPasswordSuccess';

/*******************************************************************************
 * Action creators
 */

var setMessage = exports.setMessage = function setMessage(messageType, message) {
    return {
        type: SET_FORGOT_PASSWORD_MESSAGE,
        messageType: messageType,
        message: message
    };
};

var setSuccess = exports.setSuccess = function setSuccess(bool) {
    return {
        type: SET_FORGOT_PASSWORD_SUCCESS,
        success: bool
    };
};

var requestPassword = exports.requestPassword = function requestPassword(body) {
    var successMessage = 'forgotPassword.form.successMessage';
    return function (dispatch) {
        return (0, _biwClientFetch.clientFetch)({
            url: '/api/v1.0/forgot-password',
            type: FORGOT_PASSWORD,
            method: 'post',
            body: body
        }).then(function (response) {

            if (!response || !Object.keys(response).length) {
                dispatch(setSuccess(false));
                dispatch(setMessage('error', { text: null, errorCode: null }));

                return;
            }

            if (response.errorCode) {
                dispatch(setSuccess(false));
                dispatch(setMessage('error', { text: response.responseMessage, errorCode: response.errorCode }));
            } else {
                dispatch(setSuccess(true));
                dispatch(setMessage('success', successMessage));
            }
        });
    };
};

/*******************************************************************************
 * Reducer
 */

var forgotPasswordReducer = exports.forgotPasswordReducer = function forgotPasswordReducer() {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : forgotPasswordState;
    var action = arguments[1];

    switch (action.type) {

        case SET_FORGOT_PASSWORD_MESSAGE:
            return _extends({}, state, _defineProperty({}, action.messageType + 'Message', action.message));

        case SET_FORGOT_PASSWORD_SUCCESS:
            return _extends({}, state, {
                success: action.success
            });

        default:
            return state;

    }
};
//# sourceMappingURL=index.js.map
