'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.loginReducer = exports.launchAs = exports.login = exports.setSuccess = exports.setMessage = exports.SET_USER_DATA = exports.LAUNCH_AS = exports.SET_LOGIN_SUCCESS = exports.SET_LOGIN_MESSAGE = exports.LOGIN = exports.loginState = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _biwClientFetch = require('biw-client-fetch');

var _get = require('lodash/get');

var _get2 = _interopRequireDefault(_get);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*******************************************************************************
 * Initial state
 */

var loginState = exports.loginState = {
    success: false,
    loginErrorMessage: false,
    launchAsErrorMessage: false,
    errorCode: null
};

/*******************************************************************************
 * Non-async action types
 */

var LOGIN = exports.LOGIN = 'login';
var SET_LOGIN_MESSAGE = exports.SET_LOGIN_MESSAGE = 'setLoginMessage';
var SET_LOGIN_SUCCESS = exports.SET_LOGIN_SUCCESS = 'setLoginSuccess';
var LAUNCH_AS = exports.LAUNCH_AS = 'launchAs';

var SET_USER_DATA = exports.SET_USER_DATA = 'setUserData';

/*******************************************************************************
 * Action creators
 */

var setMessage = exports.setMessage = function setMessage(messageType, message) {
    return {
        type: SET_LOGIN_MESSAGE,
        messageType: messageType,
        message: message
    };
};

var setSuccess = exports.setSuccess = function setSuccess(bool) {
    return {
        type: SET_LOGIN_SUCCESS,
        success: bool
    };
};

var isFailedResponse = function isFailedResponse(response) {
    return !response || !Object.keys(response).length || response.errorCode;
};

var login = exports.login = function login(body) {
    return function (dispatch) {
        return (0, _biwClientFetch.clientFetch)({
            url: '/api/v1.0/login',
            type: LOGIN,
            method: 'post',
            body: _extends({}, body, {
                username: (body.username || '').trim()
            })
        }).then(function (response) {
            var isSuccess = !isFailedResponse(response);
            if (!isSuccess) {
                dispatch(setMessage('loginError', {
                    text: (0, _get2.default)(response, 'responseMessage', null),
                    errorCode: (0, _get2.default)(response, 'errorCode', null)
                }));
            } else {
                dispatch({
                    type: SET_USER_DATA,
                    user: _extends({}, response, {
                        authenticated: true
                    })
                });
            }

            dispatch(setSuccess(isSuccess));
            return isSuccess;
        });
    };
};

var launchAs = exports.launchAs = function launchAs() {
    return function (dispatch) {
        var searchString = window.location.search;
        if (!searchString || searchString.length === 0) {
            dispatch(setMessage('launchAsError', { text: 'No login token.' }));
            dispatch(setSuccess(false));
            return Promise.resolve(false);
        }

        var token = window.location.search.substring(1);

        return (0, _biwClientFetch.clientFetch)({
            url: '/api/v1.0/launch-as',
            type: LAUNCH_AS,
            method: 'post',
            body: { token: token }
        }).then(function (response) {
            var isSuccess = false;
            if (isFailedResponse(response)) {
                dispatch(setMessage('launchAsError', {
                    text: (0, _get2.default)(response, 'responseMessage', null),
                    errorCode: (0, _get2.default)(response, 'errorCode', null)
                }));
            } else {
                isSuccess = true;
                dispatch({
                    type: SET_USER_DATA,
                    user: _extends({}, response, {
                        authenticated: true,
                        isLaunchAs: true
                    })
                });
            }
            dispatch(setSuccess(isSuccess));
            return isSuccess;
        });
    };
};

/*******************************************************************************
 * Reducer
 */

var loginReducer = exports.loginReducer = function loginReducer() {
    var _extends2;

    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : loginState;
    var action = arguments[1];

    switch (action.type) {

        case SET_LOGIN_MESSAGE:
            return _extends({}, state, (_extends2 = {}, _defineProperty(_extends2, action.messageType + 'Message', action.message), _defineProperty(_extends2, 'errorCode', action.message.errorCode || null), _extends2));
        case SET_LOGIN_SUCCESS:
            return _extends({}, state, {
                success: action.success
            });
        default:
            return state;

    }
};
//# sourceMappingURL=index.js.map
