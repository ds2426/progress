'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getLoginErrorCode = exports.getLoginSuccess = exports.getLaunchAsErrorMessage = exports.getLoginErrorMessage = undefined;

var _get = require('lodash/get');

var _get2 = _interopRequireDefault(_get);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var getLoginErrorMessage = exports.getLoginErrorMessage = function getLoginErrorMessage(state) {
    return (0, _get2.default)(state, 'login.loginErrorMessage', false);
};

var getLaunchAsErrorMessage = exports.getLaunchAsErrorMessage = function getLaunchAsErrorMessage(state) {
    return (0, _get2.default)(state, 'login.launchAsErrorMessage', false);
};

var getLoginSuccess = exports.getLoginSuccess = function getLoginSuccess(state) {
    return (0, _get2.default)(state, 'login.success', false);
};

var getLoginErrorCode = exports.getLoginErrorCode = function getLoginErrorCode(state) {
    return (0, _get2.default)(state, 'login.errorCode', null);
};
//# sourceMappingURL=selectors.js.map
