"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
var message = exports.message = function message(type, messageType, _message) {
    return {
        type: type,
        messageType: messageType,
        message: _message
    };
};

var success = exports.success = function success(type, bool) {
    return {
        type: type,
        success: bool
    };
};
//# sourceMappingURL=set-any.js.map
