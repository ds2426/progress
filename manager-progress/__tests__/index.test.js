import AppStore from 'app/store';
import { mapStateToProps } from '../index';

describe( 'manager-progress connect', () => {
    it ( 'gets passed the required props when connected with react-redux.', () => {
        const result = mapStateToProps( AppStore().getState() );

        expect( result.hasOts ).toBeDefined();
        expect( result.isLaunchAs ).toBeDefined();
        expect( result.program ).toBeDefined();
    } );
} );
