import React from 'react';
import { shallow } from 'enzyme';
import ManagerChart from '../manager-chart';
import { constants } from 'contests-utils';
import ManagerProgress from '../manager-progress';

const defaultProps = {
    program: {
        programId: 2345,
        programType: constants.PT__GQ
    },
    hasOts: false,
    isLaunchAs: false,
};

describe( 'Manager Progress', () => {

	it( 'can shallow mount manager progress', () => {

        const managerProgress = shallow( <ManagerProgress { ...defaultProps } /> );

        expect( managerProgress.find( '.manager-progress' ).length ).toEqual( 1 );

	} );

    it ( 'won\'t show Manager bar chart for Objective program and no_progress status ', () => { //obj-temp
        const props = {
            ...defaultProps,
            program: {
                ...defaultProps.program,
                programType: constants.PT__OBJ,
                programViewStatus: constants.PVS__NO_PROGRESS
            }
        };

        const managerProgress = shallow( <ManagerProgress { ...props } /> );
        expect( managerProgress.find( ManagerChart ) ).toHaveLength( 0 );
    } );

} );
