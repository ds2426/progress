// libs
import React from 'react';
import PropTypes from 'prop-types';
// shared components
import Chart from 'shared/bar-chart';

export default class ManagerChart extends React.Component {

    static propTypes = {
        programViewStatus: PropTypes.string,
        data: PropTypes.array,
        yAxisLabel: PropTypes.string,
        xAxisLabel: PropTypes.string,
        xAxisSubLabel: PropTypes.string,
        showHover: PropTypes.bool
    }

    static defaultProps = {
        showHover: false,
        data: []
    }

    render() {
        const {
            data,
            xAxisLabel,
            xAxisSubLabel,
            yAxisLabel,
            programViewStatus,
            showHover
        } = this.props;

        if ( !data.length ) {
            return false;
        }

        return (
            <Chart
                height={ 650 }
                width={ 1000 }
                data={ data }
                showHover={ showHover }
                xAxisLabel={ xAxisLabel || '' }
                xAxisSubLabel={ xAxisSubLabel }
                yAxisLabel={ yAxisLabel }
                programViewStatus={ programViewStatus }
            />
        );
    }
}
