import memoize from 'lodash/memoize';
import some from 'lodash/some';
import get from 'lodash/get';
import { createSelector } from 'reselect';
import { connect } from 'react-redux';
import { getActiveProgram } from 'app/router/selectors';
import { selectors as progressSelectors } from 'contests-progress-dux';
import { selectors as clientSelectors } from '@biw/client-dux';
import { getStringFromBundle } from '@biw/intl';

import { constants } from 'contests-utils';
const {
    PVS__PROGRESS_LOADED_NO_ISSUANCE,
    PVS__FINAL_ISSUANCE
} = constants;

const STATUS_MAP = {
    under60: { mq: 'red', notMq: 'darkRed', sublabel: 'managerProgress.chart.sublabel.under60', label: 'progress.getGoing' },
    '60plus': { mq: 'orange', notMq: 'darkOrange', sublabel: 'managerProgress.chart.sublabel.60plus', label: 'progress.stayFocused' },
    '80plus': { mq: 'blue', notMq: 'darkBlue', sublabel: 'managerProgress.chart.sublabel.80plus', label: 'progress.sorta' },
    '100plus': { mq: 'purple', notMq: 'darkPurple', sublabel: 'managerProgress.chart.sublabel.100plus', label: 'progress.onTrackHeader' },
    achieved: { mq: 'green', notMq: 'darkGreen', sublabel: 'managerProgress.chart.sublabel.achieved', label: 'progress.madeIt' }
};

const getGetString = createSelector(
    clientSelectors.getUserBundle,
    bundle => ( code, opts ) => getStringFromBundle( bundle, code, opts )
);

const getShowHoverSelector = memoize( programViewId => createSelector(
    progressSelectors.getProgressSelector( programViewId ),
    ( { filterOptions: filter } ) => {
        if ( !filter ) {
            return false;
        }
        return filter.isMqEnabled || some( filter.filterOptions, 'isMqEnabled' );
    }
) );

const mapValues = ( level, getString ) => {
    const levels = [];
    const COLORS = STATUS_MAP[ level.onTrackStatus ];
    if ( level.mqNotMetCount + level.mqMetCount < level.paxCount ) {
        level.mqMetCount += ( level.paxCount - ( level.mqNotMetCount + level.mqMetCount ) );
    }

    if ( level.mqNotMetCount != 0 ) {
        levels.push( {
            label: getString( 'managerProgress.mqNotMet' ),
            value: level.mqNotMetCount,
            color: COLORS.notMq
        } );
    }

    if ( level.mqMetCount != 0 ) {
        levels.push( {
            label: getString( 'managerProgress.mqMet' ),
            value: level.mqMetCount,
            color: COLORS.mq
        } );
    }

    return levels;
};

const getChartData = memoize( programViewId => createSelector(
    progressSelectors.getGoalSelector( programViewId ),
    getActiveProgram,
    getGetString,
    ( goal, program, getString ) => {
        if ( goal.progressOnTrackToGoalChart ) {
            const chartData = goal.progressOnTrackToGoalChart;
            const data = ( chartData.length == 6 && chartData[ 0 ].onTrackStatus == 'nogoal' )
                ? chartData.slice( 1, chartData.length ) : chartData;

            const tooltipLabelCode = ( program.programViewStatus === PVS__FINAL_ISSUANCE ?
                'managerProgress.chart.tooltipLabel.final' :
                'managerProgress.chart.tooltipLabel.progress' );
            const yAxisLabel = getString( 'managerProgress.chart.yAxisLabel' ).toLowerCase();

            return data.map( level => {
                const sublabel = getString( STATUS_MAP[ level.onTrackStatus ].sublabel );
                return {
                    label: getString( STATUS_MAP[ level.onTrackStatus ].label ),
                    tooltipLabel: getString( tooltipLabelCode, { yAxisLabel, sublabel } ),
                    sublabel,
                    reqSeq: level.recSeq,
                    values: mapValues( level, getString )
                };
            } );
        } else if ( get( goal, 'barChart.levelsBarChart' ) ) {
            const chartData = get( goal, 'barChart.levelsBarChart' );

            return chartData.map( level => ( {
                label: level.tierName,
                values: [ {
                    value: level.paxCount,
                    color: parseInt( level.payoutTierId, 10 ) === -1 ? 'red' : 'blue'
                } ]
            } ) );
        }
        return [];
    }
) );

const getXAxisLabelCode = createSelector(
    getActiveProgram,
    ( { programViewStatus } ) => {
        switch ( programViewStatus ) {
            case constants.PVS__FINAL_ISSUANCE:
                return 'managerProgress.chart.xAxisLabel.finalResults';

            case constants.PVS__GOAL_SELECTED:
            case constants.PVS__NO_PROGRESS:
            case constants.PVS__NO_GOAL_SELECTED:
            case constants.PVS__GOAL_SELECTION:
                return 'managerProgress.chart.xAxisLabel.inGoalSelection';

        }
        return 'managerProgress.chart.xAxisLabel.inProgress';
    }
);

export const mapStateToProps = ( state ) => {
    const { programViewId, programViewStatus } = getActiveProgram( state ) || {};
    const getString = getGetString( state );
    return {
        showHover: getShowHoverSelector( programViewId )( state ),
        data: getChartData( programViewId )( state ),
        xAxisLabel: getString( getXAxisLabelCode( state ) ),
        xAxisSubLabel: programViewStatus === PVS__PROGRESS_LOADED_NO_ISSUANCE ? getString( 'managerProgress.chart.xAxisSublabel' ) : '',
        yAxisLabel: getString( 'managerProgress.chart.yAxisLabel' ),

        programViewStatus,
    };
};

export default connect( mapStateToProps );
