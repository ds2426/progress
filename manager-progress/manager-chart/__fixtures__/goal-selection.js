module.exports = {
    'goal': {
        'barChart': {
        'levelsBarChart': [
            {
            'payoutTierId': '-1',
            'tierName': 'No Level Selected',
            'paxCount': 12,
            'recSeq': 1
            },
            {
            'payoutTierId': '25246,25248,25252',
            'tierName': 'Level 1',
            'paxCount': 0,
            'recSeq': 2
            },
            {
            'payoutTierId': '25244,25247,25250',
            'tierName': 'Level 2',
            'paxCount': 0,
            'recSeq': 3
            },
            {
            'payoutTierId': '25245,25249,25251',
            'tierName': 'Level 3',
            'paxCount': 2,
            'recSeq': 4
            }
        ]
        },
        'selectionCount': {
            'paxCount': 14,
            'goalSelectedCount': 2,
            'goalSelectedPercentage': 14
        }
    },
    'finalResult': null,
    'rules': {
        'overrideType': 'percentage_of_override',
        'text': 'Earn % from each member in your group. See My Rules for more details.'
    },
    'override': {
        'managerRules': [
        {
            'managerRule': 'gq.manager.rules.10251'
        }
        ],
        'participantRules': [
        {
            'groupId': 19826,
            'groupName': 'Group 1',
            'selectorRules': 'gq.group.selector.rules.12547'
        },
        {
            'groupId': 19827,
            'groupName': 'Group 2',
            'selectorRules': 'gq.group.selector.rules.12548'
        },
        {
            'groupId': 19828,
            'groupName': 'Group 3',
            'selectorRules': 'gq.group.selector.rules.12549'
        }
        ]
    },
    'detailsTable': {
        'goalSelectedDetailsByPax': [
        {
            'participantId': 246854,
            'avatarUrl': null,
            'firstName': 'Sal',
            'lastName': 'Limon',
            'orgUnitName': 'Peo',
            'orgUnitRole': 'mbr',
            'departmentType': null,
            'positionType': null,
            'groupName': 'Group 1',
            'tierName': null,
            'baseValue': 13.17,
            'baseValueStr': '$13.17 average per work order',
            'achieveAmount': 0,
            'achieveAmountStr': null,
            'goalLabelType': 'currency',
            'goalLabelText': 'average per work order',
            'goalLabelPosition': 'after',
            'goalCurrencyCode': 'usd',
            'goalCurrencySymbol': '$',
            'goalAchievementRule': 'baseline_plus_fixed_amount',
            'hasPlanSet': 0,
            'recSeq': null,
            'hasBaseline': 1
        },
        {
            'participantId': 246834,
            'avatarUrl': null,
            'firstName': 'Doug',
            'lastName': 'Gunn',
            'orgUnitName': 'Peo',
            'orgUnitRole': 'mbr',
            'departmentType': null,
            'positionType': null,
            'groupName': 'Group 1',
            'tierName': null,
            'baseValue': 11.39,
            'baseValueStr': '$11.39 average per work order',
            'achieveAmount': 0,
            'achieveAmountStr': null,
            'goalLabelType': 'currency',
            'goalLabelText': 'average per work order',
            'goalLabelPosition': 'after',
            'goalCurrencyCode': 'usd',
            'goalCurrencySymbol': '$',
            'goalAchievementRule': 'baseline_plus_fixed_amount',
            'hasPlanSet': 0,
            'recSeq': null,
            'hasBaseline': 1
        },
        {
            'participantId': 246807,
            'avatarUrl': null,
            'firstName': 'Jason',
            'lastName': 'Gutierrez',
            'orgUnitName': 'Peo',
            'orgUnitRole': 'mbr',
            'departmentType': null,
            'positionType': null,
            'groupName': 'Group 1',
            'tierName': null,
            'baseValue': 10.64,
            'baseValueStr': '$10.64 average per work order',
            'achieveAmount': 0,
            'achieveAmountStr': null,
            'goalLabelType': 'currency',
            'goalLabelText': 'average per work order',
            'goalLabelPosition': 'after',
            'goalCurrencyCode': 'usd',
            'goalCurrencySymbol': '$',
            'goalAchievementRule': 'baseline_plus_fixed_amount',
            'hasPlanSet': 0,
            'recSeq': null,
            'hasBaseline': 1
        },
        {
            'participantId': 246790,
            'avatarUrl': null,
            'firstName': 'James',
            'lastName': 'Zanine',
            'orgUnitName': 'Peo',
            'orgUnitRole': 'mbr',
            'departmentType': null,
            'positionType': null,
            'groupName': 'Group 1',
            'tierName': null,
            'baseValue': 11.21,
            'baseValueStr': '$11.21 average per work order',
            'achieveAmount': 0,
            'achieveAmountStr': null,
            'goalLabelType': 'currency',
            'goalLabelText': 'average per work order',
            'goalLabelPosition': 'after',
            'goalCurrencyCode': 'usd',
            'goalCurrencySymbol': '$',
            'goalAchievementRule': 'baseline_plus_fixed_amount',
            'hasPlanSet': 0,
            'recSeq': null,
            'hasBaseline': 1
        },
        {
            'participantId': 246782,
            'avatarUrl': null,
            'firstName': 'John',
            'lastName': 'Sturla',
            'orgUnitName': 'Peo',
            'orgUnitRole': 'mbr',
            'departmentType': null,
            'positionType': null,
            'groupName': 'Group 1',
            'tierName': null,
            'baseValue': 12.65,
            'baseValueStr': '$12.65 average per work order',
            'achieveAmount': 0,
            'achieveAmountStr': null,
            'goalLabelType': 'currency',
            'goalLabelText': 'average per work order',
            'goalLabelPosition': 'after',
            'goalCurrencyCode': 'usd',
            'goalCurrencySymbol': '$',
            'goalAchievementRule': 'baseline_plus_fixed_amount',
            'hasPlanSet': 0,
            'recSeq': null,
            'hasBaseline': 1
        },
        {
            'participantId': 246743,
            'avatarUrl': null,
            'firstName': 'Doug',
            'lastName': 'Gunn',
            'orgUnitName': 'Peo',
            'orgUnitRole': 'mbr',
            'departmentType': null,
            'positionType': null,
            'groupName': 'Group 1',
            'tierName': 'Level 3',
            'baseValue': 10.56,
            'baseValueStr': '$10.56 average per work order',
            'achieveAmount': 17.06,
            'achieveAmountStr': '$17.06 average per work order',
            'goalLabelType': 'currency',
            'goalLabelText': 'average per work order',
            'goalLabelPosition': 'after',
            'goalCurrencyCode': 'usd',
            'goalCurrencySymbol': '$',
            'goalAchievementRule': 'baseline_plus_fixed_amount',
            'hasPlanSet': 0,
            'recSeq': null,
            'hasBaseline': 1
        },
        {
            'participantId': 246881,
            'avatarUrl': null,
            'firstName': 'Jon',
            'lastName': 'Murawski',
            'orgUnitName': 'Peo',
            'orgUnitRole': 'mbr',
            'departmentType': null,
            'positionType': null,
            'groupName': 'Group 2',
            'tierName': null,
            'baseValue': 10.14,
            'baseValueStr': '$10.14 average per work order',
            'achieveAmount': 0,
            'achieveAmountStr': null,
            'goalLabelType': 'currency',
            'goalLabelText': 'average per work order',
            'goalLabelPosition': 'after',
            'goalCurrencyCode': 'usd',
            'goalCurrencySymbol': '$',
            'goalAchievementRule': 'fixed_amount',
            'hasPlanSet': 0,
            'recSeq': null,
            'hasBaseline': 1
        },
        {
            'participantId': 246842,
            'avatarUrl': null,
            'firstName': 'Paul',
            'lastName': 'Patterson',
            'orgUnitName': 'Peo',
            'orgUnitRole': 'mbr',
            'departmentType': null,
            'positionType': null,
            'groupName': 'Group 2',
            'tierName': null,
            'baseValue': 14.69,
            'baseValueStr': '$14.69 average per work order',
            'achieveAmount': 0,
            'achieveAmountStr': null,
            'goalLabelType': 'currency',
            'goalLabelText': 'average per work order',
            'goalLabelPosition': 'after',
            'goalCurrencyCode': 'usd',
            'goalCurrencySymbol': '$',
            'goalAchievementRule': 'fixed_amount',
            'hasPlanSet': 0,
            'recSeq': null,
            'hasBaseline': 1
        },
        {
            'participantId': 246817,
            'avatarUrl': null,
            'firstName': 'Charlie',
            'lastName': 'DeCastro',
            'orgUnitName': 'Peo',
            'orgUnitRole': 'mbr',
            'departmentType': null,
            'positionType': null,
            'groupName': 'Group 2',
            'tierName': null,
            'baseValue': 13.09,
            'baseValueStr': '$13.09 average per work order',
            'achieveAmount': 0,
            'achieveAmountStr': null,
            'goalLabelType': 'currency',
            'goalLabelText': 'average per work order',
            'goalLabelPosition': 'after',
            'goalCurrencyCode': 'usd',
            'goalCurrencySymbol': '$',
            'goalAchievementRule': 'fixed_amount',
            'hasPlanSet': 0,
            'recSeq': null,
            'hasBaseline': 1
        },
        {
            'participantId': 246801,
            'avatarUrl': null,
            'firstName': 'Donny',
            'lastName': 'Darling',
            'orgUnitName': 'Peo',
            'orgUnitRole': 'mbr',
            'departmentType': null,
            'positionType': null,
            'groupName': 'Group 2',
            'tierName': null,
            'baseValue': 13.96,
            'baseValueStr': '$13.96 average per work order',
            'achieveAmount': 0,
            'achieveAmountStr': null,
            'goalLabelType': 'currency',
            'goalLabelText': 'average per work order',
            'goalLabelPosition': 'after',
            'goalCurrencyCode': 'usd',
            'goalCurrencySymbol': '$',
            'goalAchievementRule': 'fixed_amount',
            'hasPlanSet': 0,
            'recSeq': null,
            'hasBaseline': 1
        },
        {
            'participantId': 246771,
            'avatarUrl': null,
            'firstName': 'Rob',
            'lastName': 'McAdams',
            'orgUnitName': 'Peo',
            'orgUnitRole': 'mbr',
            'departmentType': null,
            'positionType': null,
            'groupName': 'Group 2',
            'tierName': null,
            'baseValue': 11.76,
            'baseValueStr': '$11.76 average per work order',
            'achieveAmount': 0,
            'achieveAmountStr': null,
            'goalLabelType': 'currency',
            'goalLabelText': 'average per work order',
            'goalLabelPosition': 'after',
            'goalCurrencyCode': 'usd',
            'goalCurrencySymbol': '$',
            'goalAchievementRule': 'fixed_amount',
            'hasPlanSet': 0,
            'recSeq': null,
            'hasBaseline': 1
        },
        {
            'participantId': 246756,
            'avatarUrl': null,
            'firstName': 'Ralph',
            'lastName': 'Ott',
            'orgUnitName': 'Peo',
            'orgUnitRole': 'mbr',
            'departmentType': null,
            'positionType': null,
            'groupName': 'Group 2',
            'tierName': null,
            'baseValue': 13.41,
            'baseValueStr': '$13.41 average per work order',
            'achieveAmount': 0,
            'achieveAmountStr': null,
            'goalLabelType': 'currency',
            'goalLabelText': 'average per work order',
            'goalLabelPosition': 'after',
            'goalCurrencyCode': 'usd',
            'goalCurrencySymbol': '$',
            'goalAchievementRule': 'fixed_amount',
            'hasPlanSet': 0,
            'recSeq': null,
            'hasBaseline': 1
        },
        {
            'participantId': 246746,
            'avatarUrl': null,
            'firstName': 'Kathy',
            'lastName': 'Lynch',
            'orgUnitName': 'Peo',
            'orgUnitRole': 'mbr',
            'departmentType': null,
            'positionType': null,
            'groupName': 'Group 2',
            'tierName': null,
            'baseValue': 13.84,
            'baseValueStr': '$13.84 average per work order',
            'achieveAmount': 0,
            'achieveAmountStr': null,
            'goalLabelType': 'currency',
            'goalLabelText': 'average per work order',
            'goalLabelPosition': 'after',
            'goalCurrencyCode': 'usd',
            'goalCurrencySymbol': '$',
            'goalAchievementRule': 'fixed_amount',
            'hasPlanSet': 0,
            'recSeq': null,
            'hasBaseline': 1
        },
        {
            'participantId': 246785,
            'avatarUrl': null,
            'firstName': 'Mike',
            'lastName': 'Bozich',
            'orgUnitName': 'Peo',
            'orgUnitRole': 'mbr',
            'departmentType': null,
            'positionType': null,
            'groupName': 'Group 3',
            'tierName': 'Level 3',
            'baseValue': 12.67,
            'baseValueStr': '$12.67 average per work order',
            'achieveAmount': 16.47,
            'achieveAmountStr': '$16.47 average per work order',
            'goalLabelType': 'currency',
            'goalLabelText': 'average per work order',
            'goalLabelPosition': 'after',
            'goalCurrencyCode': 'usd',
            'goalCurrencySymbol': '$',
            'goalAchievementRule': 'percentage_of_baseline',
            'hasPlanSet': 0,
            'recSeq': null,
            'hasBaseline': 1
        }
        ],
        'hasBaseline': 1,
        'meta': {
        'page': null,
        'rowsPerPage': null,
        'rowCount': null,
        'totalPages': null,
        'sortedOn': null,
        'sortedOrder': null,
        'downloadLinks': null
        },
        'criteria': null
    },
    'filterOptions': null,
    'hierarchyTable': null
};
