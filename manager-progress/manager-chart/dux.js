import { clientFetch } from 'biw-client-fetch';
import { getActiveProgram } from 'app/router/selectors';

const initialState = {
    data: {
        hover: false
    },
    goal: {
        paxCount: 0,
        onTrackCount: 0,
        onTrackPct: 0,
        selectedGoalCount: 0,
        selectedGoalPct: 0
    },
    managerOverride: {
        overrideType: '',
        text: ''
    },
    managerRules: [],
    participantRules: [],
    filters: [],
    finalResult: {},
    loaded: false
};

// Action Types
const CHART_DATA = 'chartData';
const LOAD_CHART_DATA = 'LOAD_CHART_DATA';

// Reducer
export const managerChartReducer = ( state = initialState, action ) => {
    let newState = state;
    switch ( action.type ) {
        case LOAD_CHART_DATA:
            newState = {
                ...state,
                data: action.data.chart,
                goal: action.data.goal,
                managerOverride: action.data.managerOverride,
                managerRules: action.data.managerRules,
                participantRules: action.data.participantRules,
                filters: action.data.filters,
                finalResult: action.data.finalResult,
                loaded: true,
                showMq: action.data.showMq
            };
            break;
    }
    return newState;
};

// Action Creators
const loadChartData = ( data ) => {
    return { type: LOAD_CHART_DATA, data: data };
};

// Dispatch methods
export const fetchChartData = () => {
    return ( dispatch, getState ) => {
        const state = getState();
        const activeProgram = getActiveProgram( state );
        const url = `/api/v1.0/progress/manager/chart/${ activeProgram.programId }/${ activeProgram.role }/${ activeProgram.programViewStatus }/${ activeProgram.timePeriodId }`;
        return clientFetch( {
            url: url,
            type: CHART_DATA
        } ).then( ( data ) => {
            dispatch( loadChartData( data ) );
        } );

    };
};
