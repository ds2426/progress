import React from 'react';
import renderer from 'react-test-renderer';
import ManagerChart from '../manager-chart';

const defaultProps = {
    programViewStatus: 'programViewStatus',
    data: [],
    yAxisLabel: 'yAxisLabel',
    xAxisLabel: 'xAxisLabel',
    xAxisSubLabel: 'xAxisSubLabel',
    showHover: true
};

describe( 'Manager Chart', () => {

    it( 'can render error free manager chart', () => {
        const component = renderer.create( <ManagerChart { ...defaultProps } /> );
        const tree = component.toJSON();
        expect( tree ).toMatchSnapshot( 1 );
    } );
} );
