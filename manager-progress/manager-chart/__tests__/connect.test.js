import goalSelectionFixture from '../__fixtures__/goal-selection';
import goalSelectionProgram from '../__fixtures__/goal-selection-program';
import progressFixture from '../__fixtures__/progress';
import progressProgram from '../__fixtures__/progress-program';

import createTestStore from '@biw/test-utils/test-store';
import progressReducer, {
    ROOT_KEY as PROGRESS,
    actions as progressActions
} from 'contests-progress-dux';
import programsReducer, {
    ROOT_KEY as PROGRAMS,
    actions as programsActions,
    selectors as programsSelectors
} from '@biw/programs-dux';

import { mapStateToProps } from '../connect';

describe( 'manager-chart connect component', () => {
    describe( 'goal selection chart', () => {
        const store = createTestStore( {
            [ PROGRESS ]: progressReducer,
            [ PROGRAMS ]: programsReducer
        } );
        store.dispatch( progressActions.setProgramViewProgressData( goalSelectionProgram.programViewId, goalSelectionFixture ) );
        store.dispatch( programsActions.setProgramViews( [ goalSelectionProgram ] ) );
        it( 'should return the props for a manager chart in goal selection', () => {
            spyOn( programsSelectors, 'getProgram' ).and.returnValue( goalSelectionProgram );
            expect( mapStateToProps( store.getState() ) ).toEqual( {
                showHover: false,
                data: [
                    {
                        label: 'No Level Selected',
                        values: [
                            {
                                color: 'red',
                                value: 12,
                            },
                        ],
                    },
                    {
                        label: 'Level 1',
                        values: [
                            {
                                color: 'blue',
                                value: 0,
                            },
                        ],
                    },
                    {
                        label: 'Level 2',
                        values: [
                            {
                                color: 'blue',
                                value: 0,
                            },
                        ],
                    },
                    {
                        label: 'Level 3',
                        values: [
                            {
                                color: 'blue',
                                value: 2,
                            },
                        ],
                    },
                ],
                programViewStatus: 'goal_selected',
                xAxisLabel: 'managerProgress.chart.xAxisLabel.inGoalSelection',
                xAxisSubLabel: '',
                yAxisLabel: 'managerProgress.chart.yAxisLabel',
            } );
        } );
    } );

    describe( 'progress chart', () => {
        const store = createTestStore( {
            [ PROGRESS ]: progressReducer,
            [ PROGRAMS ]: programsReducer
        } );
        store.dispatch( progressActions.setProgramViewProgressData( progressProgram.programViewId, progressFixture ) );
        store.dispatch( programsActions.setProgramViews( [ progressProgram ] ) );
        it( 'should return the props for a manager chart with progress', () => {
            spyOn( programsSelectors, 'getProgram' ).and.returnValue( progressProgram );
            expect( mapStateToProps( store.getState() ) ).toEqual( {
                data: [
                    {
                        label: 'progress.getGoing',
                        reqSeq: 1,
                        sublabel: 'managerProgress.chart.sublabel.under60',
                        tooltipLabel: 'managerProgress.chart.tooltipLabel.progress',
                        values: [
                            {
                                color: 'darkRed',
                                label: 'managerProgress.mqNotMet',
                                value: 1,
                            },
                        ],
                    },
                    {
                        label: 'progress.stayFocused',
                        reqSeq: 2,
                        sublabel: 'managerProgress.chart.sublabel.60plus',
                        tooltipLabel: 'managerProgress.chart.tooltipLabel.progress',
                        values: [
                            {
                                color: 'darkOrange',
                                label: 'managerProgress.mqNotMet',
                                value: 1,
                            },
                            {
                                color: 'orange',
                                label: 'managerProgress.mqMet',
                                value: 4,
                            },
                        ],
                    },
                    {
                        label: 'progress.sorta',
                        reqSeq: 3,
                        sublabel: 'managerProgress.chart.sublabel.80plus',
                        tooltipLabel: 'managerProgress.chart.tooltipLabel.progress',
                        values: [
                            {
                                color: 'darkBlue',
                                label: 'managerProgress.mqNotMet',
                                value: 1,
                            },
                            {
                                color: 'blue',
                                label: 'managerProgress.mqMet',
                                value: 3,
                            },
                        ],
                    },
                    {
                        label: 'progress.onTrackHeader',
                        reqSeq: 4,
                        sublabel: 'managerProgress.chart.sublabel.100plus',
                        tooltipLabel: 'managerProgress.chart.tooltipLabel.progress',
                        values: [
                            {
                                color: 'darkPurple',
                                label: 'managerProgress.mqNotMet',
                                value: 1,
                            },
                        ],
                    },
                    {
                        label: 'progress.madeIt',
                        reqSeq: 5,
                        sublabel: 'managerProgress.chart.sublabel.achieved',
                        tooltipLabel: 'managerProgress.chart.tooltipLabel.progress',
                        values: [
                            {
                                color: 'green',
                                label: 'managerProgress.mqMet',
                                value: 3,
                            },
                        ],
                    },
                ],
                programViewStatus: 'progress_loaded_no_issuance',
                showHover: true,
                xAxisLabel: 'managerProgress.chart.xAxisLabel.inProgress',
                xAxisSubLabel: 'managerProgress.chart.xAxisSublabel',
                yAxisLabel: 'managerProgress.chart.yAxisLabel',
            } );
        } );
    } );
} );
