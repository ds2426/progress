/* eslint-disable prop-types */
import React from 'react';
// shared components
import { CheckmarkCircleIcon, Modal } from 'component-library';
import { LocaleString } from '@biw/intl';

export const PaxPlanModal = ( props ) => {
	const {
        busyFetchingPaxPlanDetails,
        goalQuestMyPlanVOs,
        paxPlanUser,
        paxPlanModalOpen
    } = props;

	return (
		<Modal
            className="pax-plan-modal"
            isOpen={ paxPlanModalOpen }
            onClose={ props.closeModal }>
            <h4>
                <div className="avatar">
                {
                    paxPlanUser.avatarUrl
                    ?
                    <img src={ paxPlanUser.avatarUrl } alt=""/>
                    :
                    <p>{ paxPlanUser.firstName.charAt( 0 ) + paxPlanUser.lastName.charAt( 0 ) }</p>
                }
                </div>
                <div className="pax-name branding-primary-text">
                    { `${ paxPlanUser.firstName } ${ paxPlanUser.lastName }` }
                </div>
            </h4>
            <div className="pax-plan-sets">
                {
                    busyFetchingPaxPlanDetails
                    ?
                    'Loading...'
                    :
                    (
                        goalQuestMyPlanVOs && goalQuestMyPlanVOs.length === 0
                        ?
                        <p className="title"> <LocaleString code="managerProgress.progressDetails.planSetTitle"/> </p>
                        :
                        (
                            goalQuestMyPlanVOs
                            &&
                            goalQuestMyPlanVOs.map( ( plan, i ) => {
                                const actionSteps = plan.actionSteps.sort( ( a, b ) => a.id - b.id ).map( ( step, i ) => (
                                    <ActionSteps key={ step.id } actionStepStatus={ step.actionStepStatus } actionStep={ step.actionStep } />
                                ) );
//
                                return (
                                    <PlanSet positive={ plan.positive } obstacle={ plan.obstacle }>
                                        { actionSteps }
                                    </PlanSet>
                                );
                            } )
                        )
                    )
                }
            </div>
        </Modal>
	);
};

export default PaxPlanModal;

const ActionSteps = ( { actionStepStatus, actionStep } ) => {
    return (
        <div>
            <p className="title">
                <LocaleString code="managerProgress.progressDetails.planSetAction"/>
                {
                    actionStepStatus
                    &&
                    <span className="icon"><CheckmarkCircleIcon /></span>
                }
            </p>
            <p className="description">{ actionStep }</p>
        </div>
    );
};

const PlanSet = ( { children, positive, obstacle } ) => {
  return (
      <div className="pax-plan-set">
          <p className="title"><LocaleString code="managerProgress.progressDetails.planSetActionItem1"/></p>
          <p className="description">{ positive }</p>

          <p className="title"><LocaleString code="managerProgress.progressDetails.planSetActionItem2"/></p>
          <p className="description">{ obstacle }</p>

          { children }
      </div>
  );
};
