import '@biw/intl/mock-package';
import React from 'react';
import renderer from 'react-test-renderer';
import { PaxPlanModal } from '../index';

const defaultProps = {
    goalQuestMyPlanVOs: [],
    busyFetchingPaxPlanDetails: false,
    paxPlanUser: { firstName: 'test', lastName: 'modal' },
    paxPlanModalOpen: true,
    actionSteps: [],
    getString: ( str ) => str,
    closeModal: jest.fn()
};

it( 'should render pax-plan-modal with no errors', () => {
    const component = renderer.create( <PaxPlanModal { ...defaultProps } /> );
    const tree = component.toJSON();
    expect( tree ).toMatchSnapshot( 1 );
} );
