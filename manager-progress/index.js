// libs
import { connect } from 'react-redux';
import get from 'lodash/get';
// shared and utils
import { getActiveProgram } from 'app/router/selectors';
// main component
import ExtendedManagerProgress from './manager-progress';

export const mapStateToProps = ( state ) => {
    const program = getActiveProgram( state ) || {};
    return {
        hasOts: get( state, 'user.participantFeatures.ots' ),
        isLaunchAs: get( state, 'user.isLaunchAs' ),
        program: program
    };
};

export default connect( mapStateToProps )( ExtendedManagerProgress );
