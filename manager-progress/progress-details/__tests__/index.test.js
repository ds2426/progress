import '@biw/intl/mock-package';
import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import { constants } from 'contests-utils';
import ProgressDetails from '../progress-details';
import OnTrackInfo from '../partials/on-track';

const defaultProps = {
    participantRulesError: false,
    role: constants.ROLE__MANAGER,
    isOwnerEligibleForAward: true,
    fetchAndDownloadRulesPdf: jest.fn(),
    managerRules: {},
    managerOverride: false,
    participantRules: {},
    programType: constants.PT__GQ
};

describe( 'ProgressDetails', () => {

    it( 'renders', () => {
        const wrapper = renderer.create(
            <ProgressDetails { ...defaultProps } />
        ).toJSON();
        expect( wrapper ).toMatchSnapshot();
    } );

    it( 'renders without OnTrack for objective no_progress', () => { //obj-temp
        const props = {
            ...defaultProps,
            programType: constants.PT__OBJ,
            programViewStatus: constants.PVS__NO_PROGRESS
        };
        const progressDetails = shallow( <ProgressDetails { ...props } /> );
        expect( progressDetails.find( OnTrackInfo ) ).toHaveLength( 0 );
    } );

    it( 'renders without OnTrack for objective no_progress', () => { //obj-temp
        const props = {
            ...defaultProps,
            programType: constants.PT__OBJ,
            programViewStatus: constants.PVS__GOAL_SELECTION
        };
        const progressDetails = shallow( <ProgressDetails { ...props } /> );
        expect( progressDetails.find( OnTrackInfo ) ).toHaveLength( 0 );
    } );

    it( 'renders without OnTrack for objective no_progress', () => { //obj-temp
        const props = {
            ...defaultProps,
            programType: constants.PT__OBJ,
            programViewStatus: constants.PVS__GOAL_SELECTED
        };
        const progressDetails = shallow( <ProgressDetails { ...props } /> );
        expect( progressDetails.find( OnTrackInfo ) ).toHaveLength( 0 );
    } );

    it( 'renders without OnTrack for objective no_goal_selected', () => { //obj-temp
        const props = {
            ...defaultProps,
            programType: constants.PT__OBJ,
            programViewStatus: constants.PVS__NO_GOAL_SELECTED
        };
        const progressDetails = shallow( <ProgressDetails { ...props } /> );
        expect( progressDetails.find( OnTrackInfo ) ).toHaveLength( 0 );
    } );

    it( 'renders with OnTrack for objective final_issuance', () => { //obj-temp
        const props = {
            ...defaultProps,
            programType: constants.PT__OBJ,
            programViewStatus: constants.PVS__FINAL_ISSUANCE
        };
        const progressDetails = shallow( <ProgressDetails { ...props } /> );
        expect( progressDetails.find( OnTrackInfo ) ).toHaveLength( 1 );
    } );

    it( 'renders with OnTrack for objective progress_loaded_no_issuance', () => { //obj-temp
        const props = {
            ...defaultProps,
            programType: constants.PT__OBJ,
            programViewStatus: constants.PVS__PROGRESS_LOADED_NO_ISSUANCE
        };
        const progressDetails = shallow( <ProgressDetails { ...props } /> );
        expect( progressDetails.find( OnTrackInfo ) ).toHaveLength( 1 );
    } );

    it( 'renders with OnTrack for goalquest', () => { //obj-temp
        const props = {
            ...defaultProps,
            programType: constants.PT__GQ
        };
        const progressDetails = shallow( <ProgressDetails { ...props } /> );
        expect( progressDetails.find( OnTrackInfo ) ).toHaveLength( 1 );
    } );

    it( 'renders with OnTrack for challengepoint', () => { //obj-temp
        const props = {
            ...defaultProps,
            programType: constants.PT__CP
        };
        const progressDetails = shallow( <ProgressDetails { ...props } /> );
        expect( progressDetails.find( OnTrackInfo ) ).toHaveLength( 1 );
    } );

} );
