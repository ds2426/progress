import { connect } from 'react-redux';
import { getActiveProgram } from 'app/router/selectors';
import { clientFetch, fetchSelectors } from 'biw-client-fetch';
import { selectors as progressSelectors } from 'contests-progress-dux';
import { downloadRulesPdf } from 'components/shared/rules/rules-dux';

export const fetchRulesPdf = cmKey => ( dispatch, getState ) => {
    clientFetch( {
        url: `/api/v1.0/cmmo/key/honeycombSystem/${ cmKey }`,
        type: 'downloadRules'
    } ).then( rules => {
        if ( !rules ) {
            return;
        }
        dispatch( downloadRulesPdf( rules.properties[ 0 ].value ) );
    } );
};

const EMPTY_ARRAY = [];
const EMPTY_OBJ = {};
export const mapStateToProps = ( state ) => {
    const { role, programType, programViewStatus, programViewId, isOwnerEligibleForAward } = getActiveProgram( state );
    const progressData = progressSelectors.getProgramViewProgressData( state, programViewId );
    const rules = progressData.rules || {};
    const goal = progressData.goal || {};
    return {
        participantRulesError: fetchSelectors.getFetchingError( state, 'participantRules' ),
        managerRules: rules.managerRules || EMPTY_ARRAY,
        managerOverride: progressData.override || EMPTY_OBJ,
        participantRules: rules.participantRules || EMPTY_ARRAY,
        isOwnerEligibleForAward,
        role,
        programType,
        programViewStatus,
        goal: goal.selectionCount || goal.participantOnTrack || EMPTY_OBJ
    };
};


export const mapDispatchToProps = {
    fetchAndDownloadRulesPdf: fetchRulesPdf
};

export default connect( mapStateToProps, mapDispatchToProps );
