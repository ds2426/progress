// libs
import React from 'react';
import PropTypes from 'prop-types';

// errors/locale/constants
import { constants } from 'contests-utils';
// manager progress child components
import RulesLink from './partials/rules-link';
import RulesDropDown from './partials/rules-dropdown';
import OnTrack from './partials/on-track';
// shared components
import { Alert } from 'component-library';
import { LocaleString } from '@biw/intl';

const propTypes = {
    participantRulesError: PropTypes.any,
    role: PropTypes.string,
    isOwnerEligibleForAward: PropTypes.bool,
    fetchAndDownloadRulesPdf: PropTypes.func,
    managerRules: PropTypes.any,
    managerOverride: PropTypes.any,
    participantRules: PropTypes.any,
    programType: PropTypes.string,
    programViewStatus: PropTypes.string,
    goal: PropTypes.shape( {
        selectedGoalCount: PropTypes.number,
        paxCount: PropTypes.number,
        selectedGoalPct: PropTypes.number,
        onTrackCount: PropTypes.number,
        onTrackPct: PropTypes.number,
        noGoalCount: PropTypes.number
    } )
};

const ProgressDetails = ( {
    role,
    isOwnerEligibleForAward,
    participantRulesError,
    fetchAndDownloadRulesPdf,
    managerRules,
    managerOverride,
    participantRules,
    programType,
    programViewStatus,
    goal
} ) => {
    const owner = role === constants.ROLE__OWNER || role === constants.ROLE__MULTIOWNER;
    const isGoalQuestOrChallengepoint = [ constants.PT__GQ, constants.PT__CP ].includes( programType );
    const isObjective = programType === constants.PT__OBJ;
    const showOnTrackForObjective = [ constants.PVS__FINAL_ISSUANCE, constants.PVS__PROGRESS_LOADED_NO_ISSUANCE ].includes( programViewStatus );
    const hasStatus = ( isGoalQuestOrChallengepoint || ( isObjective && showOnTrackForObjective ) );
    return (

        <div className="progress-details" id="progress">
            <div className="content-container progress-details-wrapper">

                {  hasStatus && //obj-temp
                    <OnTrack programViewStatus={programViewStatus} programType={programType} goal={goal} />
                }

                <div className={ `goal-progress-details ${hasStatus ? 'has-status' : ''}` }>
                    <div className="content-wrap">

                        {
                            owner &&
                            isOwnerEligibleForAward &&
                            managerOverride &&
                            managerOverride.overrideType !== constants.MO__NO_OVERRIDE &&
                            <div>
                                <h3><LocaleString code="managerProgress.progressDetails.managerOverrideHeader"/></h3>
                                <p>{ managerOverride.text }</p>
                            </div>
                        }

                        <h3><LocaleString code="managerProgress.progressDetails.rulesDocHeader"/></h3>

                        <div className="rules-dropdowns">

                            {
                                owner &&
                                isOwnerEligibleForAward &&
                                managerRules &&
                                managerRules.length !== 0 &&
                                <RulesLink
                                    fetchAndDownloadRulesPdf={ fetchAndDownloadRulesPdf }
                                    managerRules={ managerRules } />
                            }

                            { participantRules && participantRules.length > 0 ?
                                <RulesDropDown rules={ participantRules } label="managerProgress.progressDetails.participantRulesLabel" handleClick={ fetchAndDownloadRulesPdf } />
                            :
                                <p><LocaleString code="managerProgress.progressDetails.noParticipantRules"/></p>
                            }
                            { participantRulesError &&
                                <Alert type="error">
                                    <p dangerouslySetInnerHTML={ { __html: participantRulesError.message } } />
                                </Alert>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
ProgressDetails.propTypes = propTypes;

export default ProgressDetails;
