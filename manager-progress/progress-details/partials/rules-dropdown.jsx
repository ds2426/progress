// libs
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { LocaleString } from '@biw/intl';
// shared components
import { PlusIcon, MinusIcon } from 'component-library';
import Rule from './rule';

export default class RulesDropDown extends React.Component {
    static propTypes = {
        handleClick: PropTypes.func,
        label: PropTypes.string,
        rules: PropTypes.array
    }

    state = {
        isOpen: false
    };

    toggleMenu = ( bool ) => {
        if ( typeof bool === 'boolean' ) {
            this.setState( { isOpen: bool } );
        } else {
            this.setState( { isOpen: !this.state.isOpen } );
        }
    }

    handleClick = ( rule ) => {
        this.props.handleClick( rule.selectorRules || rule );
        this.toggleMenu();
    }

    render() {
        const { label, rules } = this.props;
        const { isOpen } = this.state;

        return (
            <div className="dropdown" tabIndex="0" onBlur={ () => this.toggleMenu( false ) }>
                <div className="dd-label" onClick={ this.toggleMenu }>
                    <LocaleString code={label} />
                    <div className="icon">{ isOpen ? <MinusIcon /> : <PlusIcon /> }</div>
                </div>
                <ul className={ classNames( 'dropdown-list', { 'is-open': isOpen } ) }>
                    { rules.map( ( rule, i ) => (
                        <Rule key={ i } rule={rule} onClick={this.handleClick} />
                    ) ) }
                </ul>
            </div>
        );
    }
}
