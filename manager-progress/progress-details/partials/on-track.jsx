// libs
import React from 'react';
import PropTypes from 'prop-types';
// shared components
import { LocaleString, FormattedNumber } from '@biw/intl';
import { constants } from 'contests-utils';

const PERCENT = { style: 'percent' };
const OnTrack = ( props ) => {

    const { programViewStatus, programType, goal } = props;

    if ( !props.goal ) {
        return <div />;
    }

    const renderHeader = () => {
        switch ( programViewStatus ) {
            case constants.PVS__GOAL_SELECTION:
            case constants.PVS__NO_PROGRESS:
            case constants.PVS__GOAL_SELECTED:
            case constants.PVS__NO_GOAL_SELECTED:
                return <LocaleString code="managerProgress.progressDetails.goalHeader"/>;
            case constants.PVS__FINAL_ISSUANCE:
                return <LocaleString code="managerProgress.progressDetails.finalResultsHeader"/>;
            default:
                return <LocaleString code="managerProgress.progressDetails.onTrackHeader"/>;
        }
    };

    const getProgressInfo = () => {
        switch ( programViewStatus ) {
            case constants.PVS__GOAL_SELECTION:
            case constants.PVS__NO_PROGRESS:
            case constants.PVS__GOAL_SELECTED:
            case constants.PVS__NO_GOAL_SELECTED:
                return {
                    numerator: goal.goalSelectedCount,
                    denominator: goal.paxCount,
                    percent: goal.goalSelectedPercentage
                };
            case constants.PVS__FINAL_ISSUANCE:
            default:
                return {
                    numerator: goal.onTrackCount,
                    denominator: goal.paxCount,
                    percent: goal.onTrackPct
                };
        }
    };

    const renderNoGoal = () => {
        switch ( programViewStatus ) {
            case constants.PVS__GOAL_SELECTION:
            case constants.PVS__NO_PROGRESS:
            case constants.PVS__GOAL_SELECTED:
            case constants.PVS__NO_GOAL_SELECTED:
                return;
            case constants.PVS__FINAL_ISSUANCE:
            default:
                return (
                    <div>
                        <h3><LocaleString code="managerProgress.progressDetails.noGoalHeader"/></h3>
                        <div className="no-goal-count">{ goal.noGoalCount }</div>
                    </div>
                );
        }
    };

    const progressInfo = getProgressInfo();

    return (
        <div className="goal-progress-details">
            <h3>{ renderHeader() }</h3>
            <div className="progress-info">
                <span className="progress-count branding-primary-text">{ progressInfo.numerator }&#47;{ progressInfo.denominator }</span>
                <FormattedNumber className="progress-pct" value={progressInfo.percent / 100} formatOptions={PERCENT} />
            </div>
            {
                programType !== constants.PT__OBJ && //obj-temp
                renderNoGoal()
            }
        </div>
    );
};

OnTrack.propTypes = {
    goal: PropTypes.shape( {
        goalSelectedCount: PropTypes.number,
        paxCount: PropTypes.number,
        goalSelectedPercentage: PropTypes.number,
        onTrackCount: PropTypes.number,
        onTrackPct: PropTypes.number,
        noGoalCount: PropTypes.number
    } ),
    programViewStatus: PropTypes.string,
    programType: PropTypes.string
};

export default OnTrack;
