// libs
import React from 'react';
import PropTypes from 'prop-types';

export default class Rule extends React.Component {
    static propTypes = {
        onClick: PropTypes.func,
        rule: PropTypes.shape( {
            groupName: PropTypes.string
        } )
    }

    handleClick = e => this.props.onClick( this.props.rule );
    render() {
        return (
            <li onClick={ this.handleClick }>
                { this.props.rule.groupName || 'No group name sent' }
            </li>
        );
    }
}
