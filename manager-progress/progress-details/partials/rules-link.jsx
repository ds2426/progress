// libs
import React from 'react';
import PropTypes from 'prop-types';
import { LocaleString } from '@biw/intl';
// shared components
import {
    DownloadIcon
} from 'component-library';

const RulesLink = ( { managerRules, fetchAndDownloadRulesPdf } ) => {
    return (
        <div className="rules-link" onClick={ event => {
                event.preventDefault();
                fetchAndDownloadRulesPdf( managerRules[ managerRules.length - 1 ].managerRule );
            }
        }>
            <LocaleString code="managerProgress.progressDetails.myRulesLabel"/>
            <div className="icon"><DownloadIcon /></div>
        </div>
    );
};

RulesLink.propTypes = {
    managerRules: PropTypes.array,
    fetchAndDownloadRulesPdf: PropTypes.func
};

export default RulesLink;
