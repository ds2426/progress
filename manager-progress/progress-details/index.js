import enhancer from './connect';
import ProgressDetails from './progress-details';

export default enhancer( ProgressDetails );
