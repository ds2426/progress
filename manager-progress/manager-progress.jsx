// libs
import React from 'react';
import PropTypes from 'prop-types';
import scrollToElement from 'scroll-to-element';
// manager progress child components
import ProgressTables from './manager-tables';
import ProgressDetails from './progress-details';
import ProgressBarChart from './manager-chart';
// shared components
import { LocaleString } from '@biw/intl';
import { constants } from 'contests-utils';
// shared
import {
    CatalogButton,
    Shop,
    ProgressNavBar,
    StatusTitle,
    SectionWaypoint,
    OnTheSpot,
    FinalResults,
    StickyHeader
} from 'shared';
// scss
import './scss/manager-progress.scss';

export default class ManagerProgress extends React.Component {

    static propTypes = {
        program: PropTypes.shape( {
            programType: PropTypes.string,
            startDate: PropTypes.string,
            programId: PropTypes.any.isRequired,
            isOwnerEligibleForAward: PropTypes.bool,
            programViewStatus: PropTypes.string,
            role: PropTypes.string,
        } ),
        hasOts: PropTypes.bool.isRequired,
        isLaunchAs: PropTypes.bool.isRequired
    }

    state = {
        currentSection: 'progress'
    };

    handleEnterWaypoint = section => this.setState( { currentSection: section } )

    handleNavBarClick = ( event ) => {
        event.preventDefault();
        const hash = event.currentTarget.hash;
        const section = hash.substring( 1 );

        if ( section == 'progress' ) {
            scrollToElement( document.body, { duration: 800 } );
        } else {
            scrollToElement( hash, { duration: 800, offset: -210 } );
        }

        setTimeout( () => this.setState( { currentSection: section } ), 825 );
    }

    hideProgressBarChart = () => {
        const { programType, programViewStatus } = this.props.program;
        return programType === constants.PT__OBJ && programViewStatus === constants.PVS__NO_PROGRESS; //obj-temp
    }
    requireCustomStyles = () => require( '../../shared/on-the-spot/on-the-spot.scss' );

    render() {
        const {
            program,
            hasOts
        } = this.props;

        const {
            programViewStatus,
            isOwnerEligibleForAward,
        } = program;

        const { currentSection } = this.state;

        const shouldShowFinalResults = programViewStatus === constants.PVS__FINAL_ISSUANCE && isOwnerEligibleForAward;
        return (

            <div className="sticky-container">
                <StickyHeader boundaryElement=".sticky-container" className="nav-bar">
                    <ProgressNavBar
                        links={ [ 'progress', 'details', 'shop', { id: 'ots', show: hasOts } ] }
                        program={program}
                        activeLink={ currentSection }
                        handleNavBarClick={ this.handleNavBarClick }
                    />
                </StickyHeader>
                <div className="manager-progress">
                    { shouldShowFinalResults &&
                        <FinalResults CatalogButton={ CatalogButton } />
                    }
                    <SectionWaypoint onEnter={this.handleEnterWaypoint} section="progress" className="progress-data">
                        <div className="content-container">
                            <StatusTitle program={ program } />
                            { !this.hideProgressBarChart() &&
                                <ProgressBarChart />
                            }
                        </div>
                    </SectionWaypoint>

                    <SectionWaypoint onEnter={this.handleEnterWaypoint} section="details" className="progress-data">
                        <ProgressDetails />
                        <ProgressTables />
                    </SectionWaypoint>

                    <SectionWaypoint onEnter={this.handleEnterWaypoint} section="shop" className="shop-wrapper branding-tinted">
                        <div className="content">
                            <LocaleString TextComponent="h2" code="managerProgress.progressDetails.shopHeader"/>
                            <Shop showHeader showButton showTextLink={ !this.props.isLaunchAs } />
                        </div>
                    </SectionWaypoint>

                    { this.props.hasOts &&
                        <SectionWaypoint onEnter={this.handleEnterWaypoint} section="ots" className="ots-section">
                            <OnTheSpot requireCustomStyles={ this.requireCustomStyles } />
                        </SectionWaypoint>
                    }
                </div>
            </div>
        );
    }
}
