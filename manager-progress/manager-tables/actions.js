import { actions as progressActions } from 'contests-progress-dux';

export const fetchOrgTableData = ( programViewId, data ) => ( dispatch, getState ) => {
    dispatch( progressActions.fetchProgramViewTables( programViewId, data.orgUnitId ) )
        .then( () => dispatch( progressActions.pushOrgItemHierarchy( programViewId, data.orgUnitId, data.orgUnitName ) ) );
};

