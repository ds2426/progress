import React from 'react';
import PropTypes from 'prop-types';

class Row extends React.Component {
    static propTypes = {
        children: PropTypes.node.isRequired,
        extraContent: PropTypes.node,
        expandable: PropTypes.bool
    }

    static defaultProps = {
        expandable: false
    }

    state = {
        collapsed: true
    }

    handleClick = () => {
        if ( this.props.expandable ) {
            this.setState( { collapsed: !this.state.collapsed } );
        }
    }

    render() {
        const { collapsed } = this.state;
        const { children, expandable, extraContent } = this.props;

        return expandable ?
            [
                <tr key={0} className={collapsed ? 'collapsed' : 'expanded'} onClick={this.handleClick}>
                    { children }
                </tr>
            ,
                extraContent && !collapsed &&
                    <tr key={1} className="details">
                        { extraContent }
                    </tr>
            ]
        :
            <tr>
                { children }
            </tr>
        ;
    }
}

export default Row;
