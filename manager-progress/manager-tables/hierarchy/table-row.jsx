import React from 'react';
import PropTypes from 'prop-types';
import Cell from './cell';
import DetailLine from './detail-line';
import Row from '../row';
import { PlusCircleIcon } from 'component-library';

class TableRow extends React.Component {
    static propTypes = {
        data: PropTypes.shape( {} ),
        isMobile: PropTypes.bool,
        onDrillDown: PropTypes.func,
        schema: PropTypes.arrayOf( PropTypes.shape( {} ) ),
        detailSchema: PropTypes.arrayOf( PropTypes.shape( {} ) )
    }

    static defaultProps = {
        isMobile: false,
        onDrillDown: () => {}
    }

    handleDrillDown = ( e ) => {
        e.preventDefault();
        this.props.onDrillDown( this.props.data );
    }

    render() {
        const { data, schema, detailSchema, isMobile } = this.props;
        const canDrillDown = data.isNodeAndBelow === 1;
        return (
            <Row
                expandable={ isMobile }
                extraContent={ isMobile &&
                    <td colSpan={2}>
                        <ul>
                            { detailSchema.map( col => (
                                <DetailLine
                                    key={col.valueProp}
                                    desc={col.headerLabel}
                                    value={data[ col.valueProp ]}
                                    pct={data[ col.pctProp ]}
                                />
                            ) ) }
                        </ul>
                    </td>
                }
            >
                { schema.map( ( col, i ) => i === 0 ?
                    <Cell key={col.valueProp} className="org-unit-cell">
                        { canDrillDown ?
                            <span>
                                { isMobile && <div className="icon"><PlusCircleIcon /></div> }
                                <a href="#" onClick={ this.handleDrillDown }>{ data[ col.valueProp ] }</a>
                            </span>
                        :
                            data[ col.valueProp ]
                        }
                    </Cell>
                :
                    <Cell key={col.valueProp} percentValue={ data[ col.pctProp ] }>
                        { data[ col.valueProp ] }
                    </Cell>
                ) }
            </Row>
        );
    }
}

export default TableRow;
