import { connect } from 'react-redux';
import memoize from 'lodash/memoize';
import some from 'lodash/some';
import get from 'lodash/get';
import { createSelector } from 'reselect';
import { DEFAULT_PAGE_SIZE, SORT_ASCENDING } from '../constants';
import { getInGoalSelection } from '../selectors';
import { fetchOrgTableData } from '../actions';
import { getSchemaForTable, DEFAULT_KEY } from './schema';

import { getActiveProgram } from 'app/router/selectors';
import { selectors as progressSelectors } from 'contests-progress-dux';

const EMPTY_ARRAY = [];
const getSortKey = ( state, props = {} ) => props.sortKey || DEFAULT_KEY;
const getSortDirection = ( state, props = {} ) => props.sortDirection || SORT_ASCENDING;
const getSortFunc = ( k, sortDirection ) => sortDirection === SORT_ASCENDING ?
    ( a, b ) => a[ k ] > b[ k ] ? 1 : a[ k ] < b[ k ] ? -1 : 0 :
    ( a, b ) => a[ k ] < b[ k ] ? 1 : a[ k ] > b[ k ] ? -1 : 0;
const getPageSize = ( state, props = {} ) => props.pageSize || DEFAULT_PAGE_SIZE;

const getTableDataSelector = memoize( programViewId => createSelector(
    progressSelectors.getCurrentTableDataSelector( programViewId ),
    getInGoalSelection,
    ( progressData, inGoalSelection ) => {
        const tableProp = inGoalSelection ? 'goalDetailByHierarchy' : 'goalPaxDetailProgressByHierarchy';
        return get( progressData, `hierarchyTable.${tableProp}` ) || EMPTY_ARRAY;
    }
) );

const getPageViewForTableDataSelector = memoize( programViewId => createSelector(
    getTableDataSelector( programViewId ),
    getPageSize,
    getSortKey,
    getSortDirection,
    ( tableData, pageSize, sortKey, sortDirection  ) => tableData.slice( 0, pageSize + 1 ).sort( getSortFunc( sortKey, sortDirection ) )
) );

const getPayoutLevelsSelector = memoize( programViewId => createSelector(
    progressSelectors.getCurrentTableDataSelector( programViewId ),
    progressData => get( progressData, 'hierarchyTable.totalPayoutLevels' ) || 0
) );

const getHasMqSelector = memoize( programViewId => createSelector(
    getInGoalSelection,
    getTableDataSelector( programViewId ),
    ( inGoalSelection, tableData ) => !inGoalSelection && some( tableData, row => row.hasMq )
) );

export const mapStateToProps = ( state, props ) => {
    const { programViewId } = getActiveProgram( state );
    const totalPayoutLevels = getPayoutLevelsSelector( programViewId )( state );
    const hasMq = getHasMqSelector( programViewId )( state );
    return {
        programViewId,
        tableData: getPageViewForTableDataSelector( programViewId )( state, props ),
        totalCount: getTableDataSelector( programViewId )( state, props ).length,
        schema: getSchemaForTable( state, { totalPayoutLevels, hasMq } ),
    };
};

export const mapDispatchToProps = { fetchOrgTableData };

export default connect( mapStateToProps, mapDispatchToProps );
