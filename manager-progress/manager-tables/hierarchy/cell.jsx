import React from 'react';
import PropTypes from 'prop-types';
import { FormattedNumber } from '@biw/intl';
const PERCENT = { style: 'percent' };

const Cell = ( { children, percentValue, className } ) => ( children !== null ?
    <td className={className}>
        <span className="bold">{ children }</span>
        { typeof percentValue === 'number' &&
            <FormattedNumber className="unit-label" value={ String( percentValue / 100 ) }  formatOptions={ PERCENT } />
        }
    </td>
:
    <td>--</td>
);

Cell.propTypes = {
    children: PropTypes.node,
    percentValue: PropTypes.number,
    className: PropTypes.string
};

Cell.defaultProps = {
    className: '',
    percentValue: null,
    children: null
};

export default Cell;
