import times from 'lodash/times';
import { createSelector } from 'reselect';

import {
    getInGoalSelection,
    getIsFinal,
    getIsObjective
} from '../selectors';
export const DEFAULT_KEY = 'orgUnitName';
export const GOAL_KEYS = {
    ORG_NAME: DEFAULT_KEY,
    TOTAL: 'orgPaxCount',
    NOT_SELECTED: 'nsPaxCount',
    LEVEL_1: 'l1PaxCount',
    LEVEL_2: 'l2PaxCount',
    LEVEL_3: 'l3PaxCount',
    LEVEL_4: 'l4PaxCount',
    LEVEL_5: 'l5PaxCount',
};

export const PROGRESS_KEYS = {
    ORG_NAME: DEFAULT_KEY,
    TOTAL: 'paxCount',
    NOT_SELECTED: 'noSelectionCount',
    SELECTED: 'selectedCount',
    ON_TRACK: 'onTrackCount',
    ACHIEVED: 'achievedCount',
    MQ_MET: 'mqMetCount',
};

const getTotalPayoutLevels = ( state, props ) => props.totalPayoutLevels || 0;
const getHasMq = ( state, props ) => props.hasMq || false;

export const getSchemaForTable = createSelector(
    getIsObjective,
    getInGoalSelection,
    getIsFinal,
    getHasMq,
    getTotalPayoutLevels,
    ( isObjective, inGoalSelection, isFinal, hasMq, totalPayoutLevels ) => {
        const SORT_KEYS = inGoalSelection ? GOAL_KEYS : PROGRESS_KEYS;
        const schema = [
            { headerLabel: 'managerProgress.orgTable.header.hierarchy', valueProp: SORT_KEYS.ORG_NAME },
            { headerLabel: 'managerProgress.orgTable.header.participants', valueProp: SORT_KEYS.TOTAL }
        ];

        if ( !isObjective ) {
            schema.push( {
                headerLabel: 'managerProgress.orgTable.header.noGoal',
                valueProp: SORT_KEYS.NOT_SELECTED,
                pctProp: inGoalSelection ? 'nsPaxPct' : 'noSelectionPct',
            } );

            if ( inGoalSelection ) {
                times( totalPayoutLevels, i => {
                    schema.push( {
                        headerLabel: `managerProgress.orgTable.header.level${i + 1}`,
                        valueProp: SORT_KEYS[ `LEVEL_${i + 1}` ],
                        pctProp: `l${ i + 1 }PaxPct`
                    } );
                } );
            } else {
                schema.push( {
                    headerLabel: 'managerProgress.orgTable.header.goal',
                    valueProp: SORT_KEYS.SELECTED,
                    pctProp: 'onTrackPct'
                } );
            }
        }

        if ( !inGoalSelection ) {
            if ( isFinal ) {
                schema.push( { headerLabel: 'managerProgress.orgTable.header.onTrack', valueProp: SORT_KEYS.ON_TRACK, pctProp: 'onTrackPct' } );
            }

            schema.push( { headerLabel: 'managerProgress.orgTable.header.achieved', valueProp: SORT_KEYS.ACHIEVED, pctProp: 'achievedPct' } );
            if ( hasMq ) {
                schema.push( { headerLabel: 'managerProgress.orgTable.header.mqMet', valueProp: SORT_KEYS.MQ_MET, pctProp: 'mqMetCountPct' } );
            }
        }

        return schema;
    }
);
