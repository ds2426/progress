// libs
import React from 'react';
import PropTypes from 'prop-types';
import { createSelector } from 'reselect';

import { LocaleString } from '@biw/intl';

import TableRow from './table-row';

import TableHead from '../table-head';
import ExportButton from '../export-button';
import Breadcrumbs from '../breadcrumbs';
import RowView from '../row-view';

const getRenderSchemaForProps = createSelector(
    props => props.schema,
    props => props.isMobile,
    ( schema, isMobile ) => isMobile ? schema.slice( 0, 2 ) : schema
);

export default class HierarchyTable extends React.Component {
    static propTypes = {
        programViewId: PropTypes.string.isRequired,
        sortKey: PropTypes.string.isRequired,
        sortDirection: PropTypes.string.isRequired,
        tableData: PropTypes.array.isRequired,
        totalCount: PropTypes.number.isRequired,
        setSort: PropTypes.func.isRequired,
        pageSize: PropTypes.number.isRequired,
        showMore: PropTypes.func.isRequired,
        schema: PropTypes.arrayOf( PropTypes.shape( {} ) ),
        isMobile: PropTypes.bool.isRequired,
        fetchOrgTableData: PropTypes.func.isRequired
    }

    static defaultProps = {
        hasMq: false
    }

    handleDrillDown = ( data ) => {
        this.props.fetchOrgTableData( this.props.programViewId, data );
    }

    render() {
        const {
            sortKey,
            sortDirection,
            tableData,
            setSort,
            schema,
            isMobile,
            showMore,
            pageSize,
            totalCount
        } = this.props;
        const renderSchema = getRenderSchemaForProps( this.props );
        return (
            <div className="data-table org-table" >
                <div className="filter-wrap no-filters">
                    <Breadcrumbs />

                    { tableData.length > 0 ?
                        <ExportButton />
                    :
                        <div className="no-rows-found-wrapper">
                            <LocaleString code="managerProgress.orgTable.noHierarchiesFound" />
                        </div>
                    }
                </div>
                { tableData.length > 0 &&
                    <table className={isMobile ? 'mobile-progress-table' : ''}>
                        <TableHead schema={renderSchema} onClick={setSort} sortDirection={sortDirection} sortKey={sortKey} />
                        <tbody>
                            { tableData.map( ( data, i ) => (
                                <TableRow
                                    key={data.orgUnitId}
                                    schema={renderSchema}
                                    detailSchema={schema}
                                    isMobile={isMobile}
                                    data={data}
                                    onDrillDown={this.handleDrillDown}
                                />
                            ) ) }
                        </tbody>
                    </table>
                }
                <RowView onShowMore={showMore} pageSize={pageSize} totalCount={totalCount} />
            </div>
        );
    }
}
