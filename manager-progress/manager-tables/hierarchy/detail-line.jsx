import React from 'react';
import PropTypes from 'prop-types';
import { FormattedNumber, LocaleString } from '@biw/intl';

const PERCENT = { style: 'percent' };
const DetailLine = ( { desc, value, pct } ) => (
    <li>
            <LocaleString code={desc} />&nbsp;&ndash;&nbsp;
            <strong>{ value }</strong>
            { pct !== null && value !== null && ', ' }
            { pct !== null && <FormattedNumber value={String( pct / 100 )} formatOptions={PERCENT} />
        }
    </li>
);

DetailLine.propTypes = {
    desc: PropTypes.string.isRequired,
    value: PropTypes.node,
    pct: PropTypes.number
};

DetailLine.defaultProps = {
    pct: null,
    value: null
};

export default DetailLine;
