import React from 'react';
import PropTypes from 'prop-types';

import { LocaleString } from '@biw/intl';

import ExportButton from './export-button';
const EMPTY_OBJ = {};

const RowView = ( { onShowMore, totalCount, pageSize, exportFilterOptions } ) => {
    const currentCount = pageSize > totalCount ? totalCount : pageSize;
    return (
        <div className="bottom-row-container">
            <ExportButton filterOptions={exportFilterOptions} />
            <div className="row-count">
                {`1-${currentCount} of ${totalCount}`}
                <br />
                { currentCount < totalCount &&
                    <a href="#" className="load-more" onClick={onShowMore}>
                        <LocaleString code="common.loadMoreLabel" replaceOptions={{ pageSize }} />
                    </a>
                }
            </div>
        </div>
    );
};

RowView.propTypes = {
    onShowMore: PropTypes.func.isRequired,
    totalCount: PropTypes.number.isRequired,
    pageSize: PropTypes.number,
    exportFilterOptions: PropTypes.shape( {} )
};

RowView.defaultProps = {
    exportFilterOptions: EMPTY_OBJ,
    pageSize: 50
};

export default RowView;
