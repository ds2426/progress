// libs
import React from 'react';
import PropTypes from 'prop-types';
import BreadCrumb from './breadcrumb';

const Breadcrumbs = ( {
    orgUnitHierarchy,
    navToBreadcrumb
} ) => ( orgUnitHierarchy.length > 1 &&
    <div className="breadcrumbs">
        { orgUnitHierarchy.map( ( orgUnit, i ) => (
            <BreadCrumb
                key={ orgUnit.id }
                orgUnit={ orgUnit }
                onClick={ orgUnitHierarchy.length - 1 !== i ? navToBreadcrumb : null }
            />
        ) ) }
    </div>
);
Breadcrumbs.propTypes = {
    orgUnitHierarchy: PropTypes.arrayOf( PropTypes.shape( {
        id: PropTypes.any.isRequired,
        name: PropTypes.any.isRequired
    } ) ).isRequired,
    navToBreadcrumb: PropTypes.func.isRequired
};

export default Breadcrumbs;
