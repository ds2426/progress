import { connect } from 'react-redux';

import { getActiveProgram } from 'app/router/selectors';
import {
    selectors as progressSelectors,
    actions as progressActions
} from 'contests-progress-dux';

const EMPTY_ARRAY = [];

export const mapStateToProps = ( state, props = {} ) => {
    const { programViewId } = getActiveProgram( state );
    return {
        orgUnitHierarchy: progressSelectors.getOrgViewHierarchySelector( programViewId )( state ) || EMPTY_ARRAY,
    };
};

export const mapDispatchToProps = dispatch => ( {
    navToBreadcrumb: ( orgView ) => {
        return dispatch( ( dispatch, getState ) => {
            const { programViewId } = getActiveProgram( getState() );
            const rootOrgView = progressSelectors.getRootOrgViewSelector( programViewId )( getState() );
            const orgViewId = rootOrgView.id === orgView.id ? '' : orgView.id;
            dispatch( progressActions.fetchProgramViewTables( programViewId, orgViewId ) );
            dispatch( progressActions.popOrgItemHierarchy( programViewId, orgViewId ) );
        } );
    }
} );

export default connect( mapStateToProps, mapDispatchToProps );
