// libs
import React from 'react';
import PropTypes from 'prop-types';

export default class BreadCrumb extends React.Component {
    static propTypes = {
        orgUnit: PropTypes.shape( {
            id: PropTypes.any.isRequired,
            name: PropTypes.any.isRequired
        } ).isRequired,
        onClick: PropTypes.func
    }

    handleClick = ( e ) => {
        e.preventDefault();
        this.props.onClick( this.props.orgUnit );
    }

    render() {
        const { orgUnit, onClick } = this.props;
        return onClick ?
            <span>
                <a href="#" onClick={this.handleClick}>{ orgUnit.name }</a>
                <span className="separator">&gt;</span>
            </span>
        :
            <span> { orgUnit.name } </span>
        ;
    }
}
