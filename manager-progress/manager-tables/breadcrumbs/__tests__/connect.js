import { createStore } from 'app-test-utils';
import fixture from '@biw/test-utils/fixtures/m5';
import {
    actions as progressActions
} from 'contests-progress-dux';
import { mapDispatchToProps } from '../connect';

describe( 'Breadcrumbs mapDispatchToProps', () => {
    describe( 'navToBreadcrumb', () => {
        let store;
        let props;
        beforeEach( () => {
            store = createStore( { fixture } );
            props = mapDispatchToProps( store.dispatch );
            spyOn( progressActions, 'fetchProgramViewTables' ).and.returnValue( { type: 'no-op' } );
            spyOn( progressActions, 'popOrgItemHierarchy' ).and.returnValue( { type: 'no-op' } );
        } );

        it( 'should call fetchProgramViewTables', () => {
            props.navToBreadcrumb( { id: 'org-id' } );
            expect( progressActions.fetchProgramViewTables ).toHaveBeenCalledWith( fixture.programViewId, 'org-id' );
        } );

        it( 'should call popOrgItemHierarchy', () => {
            props.navToBreadcrumb( { id: 'org-id' } );
            expect( progressActions.popOrgItemHierarchy ).toHaveBeenCalledWith( fixture.programViewId, 'org-id' );
        } );
    } );
} );
