import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';

import BreadCrumb from '../breadcrumb';

describe( 'BreadCrumb :', () => {
    let props;
    beforeEach( () => {
        props = {
            orgUnit: {
                id: 'org-id',
                name: 'org-name'
            },
            onClick: jest.fn()
        };
    } );

    it( 'should render without errors', () => {
        const component = renderer.create(
            <BreadCrumb {...props} />
        );
        expect( component.toJSON() ).toMatchSnapshot();
    } );

    it( 'should call `onClick` with the orgUnit if the anchor is clicked', () => {
        const wrapper = mount( <BreadCrumb {...props} /> );
        wrapper.find( 'a' ).simulate( 'click' );
        expect( props.onClick ).toHaveBeenCalledWith( props.orgUnit );
    } );

    it( 'should not render a clickable tag if there is no `onClick` prop', () => {
        const wrapper = mount( <BreadCrumb orgUnit={props.orgUnit} /> );
        expect( wrapper.find( 'a' ).exists() ).toBe( false );
    } );
} );
