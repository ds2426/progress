import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';

import BreadCrumbs from '../breadcrumbs';

describe( 'BreadCrumbs :', () => {
    let props;
    beforeEach( () => {
        props = {
            orgUnitHierarchy: [ { id: 'org-1', name: 'something' }, { id: 'org-2', name: 'something 2' } ],
            navToBreadcrumb: jest.fn()
        };
    } );

    it( 'should render without errors', () => {
        const component = renderer.create(
            <BreadCrumbs {...props} />
        );
        expect( component.toJSON() ).toMatchSnapshot();
    } );

    it( 'should not render a clickable breadcrumb for the last orgUnit', () => {
        const wrapper = mount( <BreadCrumbs {...props} /> );
        expect( wrapper.find( 'BreadCrumb' ).last().props().onClick ).toBe( null );
    } );

    it( 'should call navToBreadcrumb with the orgUnit if a clickable breadcrumb is activated', () => {
        const wrapper = mount( <BreadCrumbs {...props} /> );
        wrapper.find( 'BreadCrumb' ).first().instance().handleClick( { preventDefault: jest.fn() } );
        expect( props.navToBreadcrumb ).toHaveBeenCalledWith( props.orgUnitHierarchy[ 0 ] );
    } );
} );
