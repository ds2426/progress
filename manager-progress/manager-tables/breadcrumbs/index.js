import enhancer from './connect';
import Breadcrumbs from './breadcrumbs';

export default enhancer( Breadcrumbs );
