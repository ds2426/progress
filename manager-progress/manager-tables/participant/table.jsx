import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { LocaleString } from '@biw/intl';

import TableRow from './table-row';
import Filters, { getExportFilterOptions } from '../filters';
import TableHead from '../table-head';
import ExportButton from '../export-button';
import Breadcrumbs from '../breadcrumbs';
import RowView from '../row-view';

export default class ParticipantTable extends React.Component {
    static propTypes = {
        program: PropTypes.shape( {} ).isRequired,
        sortKey: PropTypes.string.isRequired,
        sortDirection: PropTypes.string.isRequired,
        tableData: PropTypes.array.isRequired,
        totalCount: PropTypes.number.isRequired,
        setSort: PropTypes.func.isRequired,
        pageSize: PropTypes.number.isRequired,
        showMore: PropTypes.func.isRequired,
        schema: PropTypes.arrayOf( PropTypes.shape( { } ) ),
        renderSchema: PropTypes.arrayOf( PropTypes.shape( { } ) ),
        isMobile: PropTypes.bool.isRequired,
        filters: PropTypes.array.isRequired,
        selectedFilters: PropTypes.array.isRequired,
        resetFilters: PropTypes.func.isRequired,
        onFilterSelect: PropTypes.func.isRequired,
    }

    render() {
        const {
            program,
            sortKey,
            sortDirection,
            tableData,
            setSort,
            schema,
            renderSchema,
            isMobile,
            showMore,
            pageSize,
            totalCount,
            filters,
            selectedFilters,
            resetFilters,
            onFilterSelect,
        } = this.props;

        const exportFilterOptions = getExportFilterOptions( selectedFilters );

        return (
            <div className="data-table">
                <div className={ classNames( 'filter-wrap', { 'no-filters': !filters.length } ) }>
                    <Breadcrumbs />

                    <Filters
                        filters={filters}
                        selectedFilters={selectedFilters}
                        resetFilters={resetFilters}
                        onSelect={onFilterSelect}
                    />

                    { tableData.length > 0 ?
                        <ExportButton filterOptions={exportFilterOptions} />
                    :
                        <div className="no-rows-found-wrapper">
                            <LocaleString code="managerProgress.paxTable.noParticipantsFound" />
                        </div>
                    }
                </div>
                { tableData.length > 0 &&
                    <table className={isMobile ? 'mobile-progress-table data-table' : ''}>
                        <TableHead schema={renderSchema} onClick={setSort} sortDirection={sortDirection} sortKey={sortKey} />
                        <tbody>
                            { tableData.map( ( data, i ) => (
                                <TableRow
                                    key={data.participantId}
                                    program={program}
                                    schema={renderSchema}
                                    detailSchema={schema}
                                    isMobile={isMobile}
                                    data={data}
                                />
                            ) ) }
                        </tbody>
                    </table>
                }
                <RowView onShowMore={showMore} pageSize={pageSize} totalCount={totalCount} exportFilterOptions={exportFilterOptions} />
            </div>
        );
    }
}
