import { connect } from 'react-redux';
import memoize from 'lodash/memoize';
import get from 'lodash/get';
import some from 'lodash/some';
import { createSelector } from 'reselect';
import { selectors as progressSelectors } from 'contests-progress-dux';

import { getActiveProgram } from 'app/router/selectors';

import { SORT_ASCENDING, DEFAULT_PAGE_SIZE } from '../constants';
import { getInGoalSelection } from '../selectors';
import { NO_SELECTION } from '../filters';

import { getSchemaForTable, getRenderSchemaForProps, DEFAULT_KEY } from './schema';

const EMPTY_ARRAY = [];
const EMPTY_OBJECT = {};
const getSortKey = ( state, props = {} ) => props.sortKey || DEFAULT_KEY;
const getSortDirection = ( state, props = {} ) => props.sortDirection || SORT_ASCENDING;
const getSortFunc = ( k, sortDirection ) => sortDirection === SORT_ASCENDING ?
    ( a, b ) => a[ k ] > b[ k ] ? 1 : a[ k ] < b[ k ] ? -1 : 0 :
    ( a, b ) => a[ k ] < b[ k ] ? 1 : a[ k ] > b[ k ] ? -1 : 0;
const getPageSize = ( state, props = {} ) => props.pageSize || DEFAULT_PAGE_SIZE;

const getDetailsTableSelector = memoize( programViewId => createSelector(
    progressSelectors.getCurrentTableDataSelector( programViewId ),
    progressData => progressData.detailsTable || EMPTY_OBJECT
) );

const getTableDataSelector = memoize( programViewId => createSelector(
    getDetailsTableSelector( programViewId ),
    getInGoalSelection,
    ( detailsTable, inGoalSelection ) => {
        const tableProp = inGoalSelection ? 'goalSelectedDetailsByPax' : 'goalPaxDataProgressByParticipant';
        return get( detailsTable, tableProp ) || EMPTY_ARRAY;
    }
) );

const getSelectedFilters = ( state, props ) => props.selectedFilters || EMPTY_ARRAY;
const getActiveFilters = createSelector( getSelectedFilters, filters => filters.filter( selection => selection.option !== NO_SELECTION ) );
const getFilteredTableData = memoize( programViewId => createSelector(
    getTableDataSelector( programViewId ),
    getActiveFilters,
    ( tableData, filters ) => !filters.length ?
        tableData :
        tableData.filter( data => !some( filters, filter => data[ filter.key ] !== filter.option.value ) )
) );

const getPageViewForTableDataSelector = memoize( programViewId => createSelector(
    getFilteredTableData( programViewId ),
    getPageSize,
    getSortKey,
    getSortDirection,
    ( tableData, pageSize, sortKey, sortDirection ) => tableData.sort( getSortFunc( sortKey, sortDirection ) ).slice( 0, pageSize + 1 )
) );

export const mapStateToProps = ( state, props ) => {
    const program = getActiveProgram( state );
    const { programViewId } = program;

    const detailsTable = getDetailsTableSelector( programViewId )( state );
    const { hasBaseline, hasBonus, hasMQ } = detailsTable;
    const schema = getSchemaForTable( state, { hasBaseline, hasBonus, hasMQ } );
    return {
        program,
        tableData: getPageViewForTableDataSelector( programViewId )( state, props ),
        totalCount: getTableDataSelector( programViewId )( state, props ).length,
        schema,
        renderSchema: getRenderSchemaForProps( state, { isMobile: props.isMobile, schema } )
    };
};


export default connect( mapStateToProps );
