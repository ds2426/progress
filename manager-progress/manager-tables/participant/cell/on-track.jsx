
import React from 'react';
import PropTypes from 'prop-types';
import { LocaleString } from '@biw/intl';

import { statusClasses, statusCmmoKeys } from '../../constants';

const OnTrack = ( { data } ) => {
    const { onTrackStatus } = data;
    return (
        <span className="status-col">
            <div className={ `status ${statusClasses[ onTrackStatus ]}` }></div>
            { !!onTrackStatus &&
                <LocaleString
                    TextComponent="p"
                    className="bold"
                    code={ `managerProgress.chart.xAxisLabel.${statusCmmoKeys[ onTrackStatus ]}.label` }
                />
            }
        </span>
    );
};

OnTrack.propTypes = {
    data: PropTypes.shape( {} )
};

export default OnTrack;
