
import React from 'react';
import PropTypes from 'prop-types';
import { GoalLabel } from 'shared';
import { FormattedNumber } from '@biw/intl';
import { getGoalLabelProps } from './goal-label-props';


const PERCENT = { style: 'percent' };
const Trend = ( { column, data, program } ) => {
    const labelProps = getGoalLabelProps( column, data, program );

    return ( !data.trendProgressValue ?
        <span>--</span>
    :
        <span>
            <FormattedNumber className="bold" value={ data.trendProgressPct / 100 } formatOptions={PERCENT} />
            <br />
            <GoalLabel className="unit-label" amount={data.trendProgressValue} {...labelProps} />
        </span>
    );
};

Trend.propTypes = {
    column: PropTypes.shape( {} ),
    data: PropTypes.shape( {} ),
    program: PropTypes.shape( {} )
};

export default Trend;
