import * as SCHEMA from '../schema';

export const getGoalLabelProps = ( column, data, program ) => {
    const props = {
        precision: program.goalPrecision,
        rounding: program.goalRoundingMethod,
        labelType: data.goalLabelType,
        labelText: data.goalLabelText,
        labelPosition: data.goalLabelPosition,
        currencyCode: data.goalCurrencyCode,
        currencySymbol: data.goalCurrencySymbol
    };
    if ( column === SCHEMA.BONUS ) {
        props.precision = program.bonusPrecision;
        props.rounding = program.bonusRoundingMethod;
    } else if ( column === SCHEMA.MQ ) {
        props.precision = program.mqPrecision;
        props.rounding = program.mqRoundingMethod;
    }
    return props;
};
