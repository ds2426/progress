
import React from 'react';
import PropTypes from 'prop-types';
import { clientFetch } from 'biw-client-fetch';

import PlanModal from './plan-modal';

const fetchPaxPlan = ( participantId, programId ) => clientFetch( {
    url: `/api/v1.0/manager/viewPlan/${ participantId }/${ programId }`,
    type: 'fetchPaxPlan'
} ).then( ( response ) => response.goalQuestMyPlanVOs );

export default class HasPlanSet extends React.Component {
    static propTypes = {
        data: PropTypes.shape( {} ),
        program: PropTypes.shape( {} ),
        column: PropTypes.shape( {} )
    };

    state = {
        modalIsOpen: false,
        planData: null
    }

    handleClick = ( e ) => {
        e.preventDefault();
        this.setState( { modalIsOpen: true } );
        const { data, program } = this.props;
        fetchPaxPlan( data.participantId, program.programId )
            .then( planData => this.setState( { planData } ) );
    }

    closeModal = () => this.setState( { modalIsOpen: false } )

    render() {
        const { data, column } = this.props;
        const { modalIsOpen, planData } = this.state;

        return (
            <span>
                { modalIsOpen &&
                    <PlanModal onClose={this.closeModal} isOpen={modalIsOpen} planData={planData} data={data} />
                }
                { data[ column.valueProp ] ?
                    <a href="#" className="href" onClick={this.handleClick}>View</a>
                :
                    <span>N</span>
                }
            </span>
        );
    }
}
