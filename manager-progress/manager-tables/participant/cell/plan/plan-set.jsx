import React from 'react';
import PropTypes from 'prop-types';
import { LocaleString } from '@biw/intl';

const PlanSet = ( { children, positive, obstacle } ) => (
    <div className="pax-plan-set">
        <p className="title"><LocaleString code="managerProgress.progressDetails.planSetActionItem1"/></p>
        <p className="description">{ positive }</p>

        <p className="title"><LocaleString code="managerProgress.progressDetails.planSetActionItem2"/></p>
        <p className="description">{ obstacle }</p>

        { children }
    </div>
);

PlanSet.propTypes = {
    children: PropTypes.node,
    positive: PropTypes.string,
    obstacle: PropTypes.string,
};

export default PlanSet;
