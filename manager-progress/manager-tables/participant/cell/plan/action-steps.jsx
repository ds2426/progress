import React from 'react';
import PropTypes from 'prop-types';
// shared components
import { CheckmarkCircleIcon } from 'component-library';
import { LocaleString } from '@biw/intl';

const ActionSteps = ( { actionStepStatus, actionStep } ) => (
    <div>
        <p className="title">
            <LocaleString code="managerProgress.progressDetails.planSetAction"/>
            { actionStepStatus &&
                <span className="icon"><CheckmarkCircleIcon /></span>
            }
        </p>
        <p className="description">{ actionStep }</p>
    </div>
);

ActionSteps.propTypes = {
    actionStepStatus: PropTypes.bool,
    actionStep: PropTypes.string.isRequired
};

ActionSteps.defaultProps = {
    actionStepStatus: false
};

export default ActionSteps;
