import React from 'react';
import PropTypes from 'prop-types';
// shared components
import { Modal } from 'component-library';
import { LocaleString } from '@biw/intl';

import PlanSet from './plan-set';
import ActionSteps from './action-steps';

export const PaxPlanModal = ( {
    planData,
    data,
    isOpen,
    onClose
} ) => (
    <Modal
        className="pax-plan-modal"
        isOpen={ isOpen }
        onClose={ onClose }
    >
        <h4>
            <div className="avatar">
            { data.avatarUrl ?
                <img src={ data.avatarUrl } alt=""/>
            :
                <p className="branding-primary-text">{ `${data.firstName[ 0 ]}${data.lastName[ 0 ]}` }</p>
            }
            </div>
            <div className="pax-name branding-primary-text">
                { `${ data.firstName } ${ data.lastName }` }
            </div>
        </h4>
        <div className="pax-plan-sets">
            { !planData ?
                <p>Loading...</p>
            :
                ( planData.length === 0 ?
                    <p className="title"> <LocaleString code="managerProgress.progressDetails.planSetTitle"/> </p>
                :
                    planData.map( ( plan, i ) => (
                        <PlanSet key={ plan.positive } positive={ plan.positive } obstacle={ plan.obstacle }>
                            { plan.actionSteps.sort( ( a, b ) => a.id - b.id ).map( ( step, i ) => (
                                <ActionSteps key={ step.id } actionStepStatus={ step.actionStepStatus } actionStep={ step.actionStep } />
                            ) ) }
                        </PlanSet>
                    ) )
                )
            }
        </div>
    </Modal>
);

PaxPlanModal.propTypes = {
    planData: PropTypes.any,
    data: PropTypes.any,
    isOpen: PropTypes.bool,
    onClose: PropTypes.func.isRequired
};

PaxPlanModal.defaultProps = {
    isOpen: false
};

export default PaxPlanModal;
