
import React from 'react';
import PropTypes from 'prop-types';
import { GoalLabel } from 'shared';
import { getGoalLabelProps } from './goal-label-props';

const Baseline = ( { column, data, program } ) => data.hasBaseline ?
    <GoalLabel className="manger-table--goal-label" amount={data.baseValue} {...getGoalLabelProps( column, data, program )} />
:
    <span>--</span>
;

Baseline.propTypes = {
    column: PropTypes.shape( {} ),
    data: PropTypes.shape( {} ),
    program: PropTypes.shape( {} )
};

export default Baseline;
