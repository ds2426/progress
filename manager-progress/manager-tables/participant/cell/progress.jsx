
import React from 'react';
import PropTypes from 'prop-types';
import { GoalLabel } from 'shared';
import { FormattedNumber } from '@biw/intl';
import { getGoalLabelProps } from './goal-label-props';

const PERCENT = { style: 'percent' };
const Progress = ( { column, data, program } ) => {
    const labelProps = getGoalLabelProps( column, data, program );

    return ( !data.progressValue ?
        <span>--</span>
    :
        <span>
            <FormattedNumber className="bold" value={ data.progressPct / 100 } formatOptions={PERCENT} />
            <br />
            <GoalLabel className="unit-label" amount={data.progressValue} {...labelProps} />
        </span>
    );
};

Progress.propTypes = {
    column: PropTypes.shape( {} ),
    data: PropTypes.shape( {} ),
    program: PropTypes.shape( {} )
};

export default Progress;
