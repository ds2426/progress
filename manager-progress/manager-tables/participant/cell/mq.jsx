
import React from 'react';
import PropTypes from 'prop-types';
import { FormattedNumber } from '@biw/intl';
import { programIsObjectives } from '../../selectors';

const PERCENT = { style: 'percent' };
const MQ = ( { column, data, program } ) => {
    const isObjectives = programIsObjectives( program );
    const hasNoData = ( isObjectives && typeof data.mqPct !== 'number' ) || ( !isObjectives && !data.tierName );
    if ( hasNoData ) {
        return <span>--</span>;
    } else {
        return (
            <span>
                <FormattedNumber className="bold" value={ data.mqPct / 100 } formatOptions={PERCENT} />
                <span className="unit-label">{data.mqStatus}</span>
            </span>
        );
    }
};

MQ.propTypes = {
    column: PropTypes.shape( {} ),
    data: PropTypes.shape( {} ),
    program: PropTypes.shape( {} )
};

export default MQ;
