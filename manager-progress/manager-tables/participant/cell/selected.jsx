
import React from 'react';
import PropTypes from 'prop-types';
import { GoalLabel } from 'shared';
import { getGoalLabelProps } from './goal-label-props';
import { programIsObjectives } from '../../selectors';

const Selected = ( { column, data, program } ) => {
    const isObjectives = programIsObjectives( program );
    const labelProps = getGoalLabelProps( column, data, program );

    if ( isObjectives ) {
        return ( !data.achieveAmount ?
            <span>--</span>
        :
            <span>
                <span className="bold">{ data.tierName }</span>
                <GoalLabel amount={data.achieveAmount} {...labelProps} />
            </span>
        );
    } else {
        return ( !data.tierName ?
            <span>--</span>
        :
            <span>
                <span className="bold">{ data.tierName }</span>
                <span className="unit-label">
                    <GoalLabel amount={data.achieveAmount} {...labelProps} />
                </span>
            </span>
        );
    }
};

Selected.propTypes = {
    column: PropTypes.shape( {} ),
    data: PropTypes.shape( {} ),
    program: PropTypes.shape( {} )
};

export default Selected;
