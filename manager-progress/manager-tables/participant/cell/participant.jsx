
import React from 'react';
import PropTypes from 'prop-types';
import { PlusCircleIcon } from 'component-library';

const Avatar = ( { avatarUrl, firstName, lastName } ) => (
    <div className="avatar-wrap">
        <div className="avatar">
            { avatarUrl ?
                <img src={ avatarUrl } alt=""/>
            :
                <span className="branding-primary-text">{ `${firstName[ 0 ]}${lastName[ 0 ]}` }</span>
            }
        </div>
    </div>
);

Avatar.propTypes = {
    avatarUrl: PropTypes.string,
    firstName: PropTypes.string,
    lastName: PropTypes.string
};

const Participant = ( { isMobile, data } ) => {
    const { orgUnitName, departmentType, positionType } = data;
    return (
        <span className="user-profile-cell">
            { isMobile ?
                <div className="icon"><PlusCircleIcon /></div>
            :
                <Avatar avatarUrl={data.avatarUrl} firstName={data.firstName} lastName={data.lastName} />
            }
            <div className="participant-info">
                <p className="participant-name">
                    { `${ data.firstName } ${ data.lastName }` }
                </p>
                <p className="participant-org-info">
                    { [ orgUnitName, departmentType, positionType ].filter( Boolean ).join( ', ' ) }
                </p>
            </div>
        </span>
    );
};

Participant.propTypes = {
    isMobile: PropTypes.bool,
    data: PropTypes.shape( {} )
};

export default Participant;
