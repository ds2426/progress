
import React from 'react';
import PropTypes from 'prop-types';
import { GoalLabel } from 'shared';
import { getGoalLabelProps } from './goal-label-props';

const Bonus = ( { column, data, program } ) => {
    const labelProps = getGoalLabelProps( column, data, program );

    return ( !data.hasBonus || !data.bonusValue ?
        <span>--</span>
    :
        <GoalLabel className="manger-table--goal-label" amount={data.bonusValue} {...labelProps} />
    );
};

Bonus.propTypes = {
    column: PropTypes.shape( {} ),
    data: PropTypes.shape( {} ),
    program: PropTypes.shape( {} )
};

export default Bonus;
