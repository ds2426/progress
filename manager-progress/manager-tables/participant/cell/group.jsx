
import React from 'react';
import PropTypes from 'prop-types';

const Group = ( { data } ) => ( data.groupName === null ?
    <span>--</span>
:
    <span>{data.groupName}</span>
);


Group.propTypes = {
    data: PropTypes.shape( {} )
};

export default Group;
