import React from 'react';
import PropTypes from 'prop-types';
import {
    PARTICIPANT,
    GROUP,
    BASELINE,
    SELECTED,
    HAS_PLAN_SET,
    PROGRESS,
    TREND,
    MQ,
    BONUS,
    ON_TRACK
} from '../schema';

import Plan from './plan';
import Baseline from './baseline';
import Bonus from './bonus';
import Selected from './selected';
import Trend from './trend';
import Progress from './progress';
import Mq from './mq';
import Group from './group';
import OnTrack from './on-track';
import Participant from './participant';

function getComponentForColumn( column ) {
    switch ( column ) {
        case HAS_PLAN_SET:
            return Plan;
        case BASELINE:
            return Baseline;
        case BONUS:
            return Bonus;
        case SELECTED:
            return Selected;
        case TREND:
            return Trend;
        case PROGRESS:
            return Progress;
        case MQ:
            return Mq;
        case GROUP:
            return Group;
        case ON_TRACK:
            return OnTrack;
        case PARTICIPANT:
            return Participant;

        default:
            return 'span';
    }
}

const Cell = ( props ) => {
    const Component = getComponentForColumn( props.column );
    return <Component {...props} />;
};
Cell.propTypes = { column: PropTypes.shape( {} ) };
export default Cell;
