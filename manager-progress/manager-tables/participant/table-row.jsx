import React from 'react';
import PropTypes from 'prop-types';
import { LocaleString } from '@biw/intl';
import Cell from './cell';
import Row from '../row';

class TableRow extends React.Component {
    static propTypes = {
        program: PropTypes.shape( {} ),
        data: PropTypes.shape( {} ),
        isMobile: PropTypes.bool,
        schema: PropTypes.arrayOf( PropTypes.shape( {} ) ),
        detailSchema: PropTypes.arrayOf( PropTypes.shape( {} ) )
    }

    static defaultProps = {
        isMobile: false
    }

    render() {
        const { data, schema, detailSchema, isMobile, program } = this.props;
        return (
            <Row
                expandable={ isMobile }
                extraContent={ isMobile &&
                    <td colSpan={2}>
                        <ul>
                            { detailSchema.filter( column => schema.indexOf( column ) === -1 ).map( column => (
                                <li key={column.valueProp}>
                                    <strong><LocaleString code={column.headerLabel} />&nbsp;&ndash;&nbsp;</strong>
                                    <Cell program={program} column={column} data={data} isMobile />
                                </li>
                            ) ) }
                        </ul>
                    </td>
                }
            >
                { schema.map( column => (
                    <td key={column.valueProp}>
                        <Cell column={column} data={data} program={program} isMobile={isMobile} />
                    </td>
                ) ) }
            </Row>
        );
    }
}

export default TableRow;
