import enhancer from './connect';
import Table from './table';

export default enhancer( Table );
