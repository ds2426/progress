import { createSelector } from 'reselect';

import {
    getInGoalSelection,
    getIsMobile,
    getIsFinal
} from '../selectors';
export const DEFAULT_KEY = 'lastName';
export const KEYS = {
    PARTICIPANT: DEFAULT_KEY,
    GROUP: 'groupName',
    BASELINE: 'baseValue',
    SELECTED: 'achieveAmount',
    HAS_PLAN_SET: 'hasPlanSet',
    PROGRESS: 'progressPct',
    TREND: 'trendProgressValue',
    MQ: 'mqPct',
    BONUS: 'bonusValue',
    ON_TRACK: 'onTrackPct'
};

const getHasBaseline = ( state, props ) => props.hasBaseline;
const getHasBonus = ( state, props ) => props.hasBonus;
const getHasMQ = ( state, props ) => props.hasMQ;

export const PARTICIPANT = { headerLabel: 'managerProgress.paxTable.header.participant', valueProp: KEYS.PARTICIPANT };
export const GROUP = { headerLabel: 'managerProgress.paxTable.header.groupName', valueProp: KEYS.GROUP };
export const BASELINE = { headerLabel: 'managerProgress.paxTable.header.baseValue', valueProp: KEYS.BASELINE };
export const SELECTED = { headerLabel: 'managerProgress.paxTable.header.achieveAmount', valueProp: KEYS.SELECTED };
export const HAS_PLAN_SET = { headerLabel: 'managerProgress.paxTable.header.hasPlanSet', valueProp: KEYS.HAS_PLAN_SET };
export const PROGRESS = { headerLabel: 'managerProgress.paxTable.header.progressPct', valueProp: KEYS.PROGRESS };
export const TREND = { headerLabel: 'managerProgress.paxTable.header.trendProgressPct', valueProp: KEYS.TREND };
export const MQ = { headerLabel: 'managerProgress.paxTable.header.mqPct', valueProp: KEYS.MQ };
export const BONUS = { headerLabel: 'managerProgress.paxTable.header.bonus', valueProp: KEYS.BONUS };
export const ON_TRACK = { headerLabel: 'managerProgress.paxTable.header.onTrackPct', valueProp: KEYS.ON_TRACK };

export const getRenderSchemaForProps = createSelector(
    ( state, props ) => props.schema,
    ( state, props ) => props.isMobile,
    getInGoalSelection,
    ( schema, isMobile, inGoalSelection ) => {
        if ( !isMobile ) {
            return schema;
        }
        return [
            PARTICIPANT,
            inGoalSelection ? SELECTED : ON_TRACK
        ];
    }
);

export const getSchemaForTable = createSelector(
    getInGoalSelection,
    getIsFinal,
    getIsMobile,
    getHasBaseline,
    getHasMQ,
    getHasBonus,
    ( inGoalSelection, isFinal, isMobile, hasBaseline, hasMQ, hasBonus ) => {
        if ( inGoalSelection ) {
            const schema = [ PARTICIPANT, GROUP ];
            if ( hasBaseline ) {
                schema.push( BASELINE );
            }
            return [ ...schema, SELECTED, HAS_PLAN_SET ];
        } else {
            let schema = [ PARTICIPANT ];
            if ( hasBaseline ) {
                schema.push( BASELINE );
            }
            schema = [ ...schema, GROUP, SELECTED, PROGRESS ];
            if ( !isFinal ) {
                schema.push( TREND );
            }
            if ( hasMQ ) {
                schema.push( MQ );
            }
            if ( hasBonus ) {
                schema.push( BONUS );
            }
            return [ ...schema, HAS_PLAN_SET, ON_TRACK ];
        }
    }
);
