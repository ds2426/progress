export const SORT_ASCENDING = 'ascending';
export const SORT_DESCENDING = 'descending';
export const DEFAULT_PAGE_SIZE = 50;

export const statusClasses = {
    'under60': 'get-going',
    '60plus': 'stay-focused',
    '80plus': 'sorta',
    '100plus': 'on-track',
    'achieved': 'made-it'
};

export const statusCmmoKeys = {
    'under60': 'reqSeq1',
    '60plus': 'reqSeq2',
    '80plus': 'reqSeq3',
    '100plus': 'reqSeq4',
    'achieved': 'reqSeq5'
};
