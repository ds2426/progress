import { createSelector } from 'reselect';

import { getActiveProgram } from 'app/router/selectors';
import { constants } from 'contests-utils';
const {
    PVS__GOAL_SELECTION,
    PVS__GOAL_SELECTED,
    PVS__NO_PROGRESS,
    PVS__FINAL_ISSUANCE,
    PT__OBJ
} = constants;

export const programIsInGoalSelection = program => [ PVS__GOAL_SELECTION, PVS__GOAL_SELECTED, PVS__NO_PROGRESS ].indexOf( program.programViewStatus ) > -1;
export const getInGoalSelection = createSelector( getActiveProgram, programIsInGoalSelection );
export const getIsFinal = createSelector( getActiveProgram, ( { programViewStatus } ) => programViewStatus === PVS__FINAL_ISSUANCE );
export const programIsObjectives = program => program.programType === PT__OBJ;
export const getIsObjective = createSelector( getActiveProgram, programIsObjectives );

export const getIsMobile = ( state, props ) => props.isMobile || false;
