import React from 'react';
import PropTypes from 'prop-types';

import { SORT_ASCENDING, DEFAULT_PAGE_SIZE } from './constants';

const selectionForFilter = ( filter, option ) => ( { ...filter, option } );
const getSelectedFiltersFor = filters => filters.map( filter => selectionForFilter( filter, filter.options[ 0 ] ) );

export default class TableWrapper extends React.Component {
    static propTypes = {
        Component: PropTypes.func.isRequired,
        filters: PropTypes.array,
    }
    static defaultProps = {
        filters: []
    }

    static getDerivedStateFromProps( nextProps, prevState ) {
        const { filters } = nextProps;
        return filters === prevState.rawFilters ? null : {
            selectedFilters: getSelectedFiltersFor( filters ),
            rawFilters: filters
        };
    }

    constructor( props ) {
        super();
        this.state = {
            sortKey: '',
            sortDirection: SORT_ASCENDING,
            pageSize: DEFAULT_PAGE_SIZE,
            selectedFilters: getSelectedFiltersFor( props.filters ),
            rawFilters: props.filters
        };
    }

    handleShowMore = ( e ) => {
        e.preventDefault();
        this.setState( { pageSize: this.state.pageSize + DEFAULT_PAGE_SIZE } );
    }

    setSort = ( sortOpts = {} ) => {
        const newState = {};
        if ( sortOpts.sortKey ) {
            newState.sortKey = sortOpts.sortKey;
        }
        if ( sortOpts.sortDirection ) {
            newState.sortDirection = sortOpts.sortDirection;
        }
        this.setState( newState );
    }

    resetFilters = () => {
        this.setState( { selectedFilters: getSelectedFiltersFor( this.props.filters ) } );
    }

    selectFilterOption = ( filter, option ) => {
        const selectedFilters = this.state.selectedFilters
            .map( selection => selection.label === filter.label ? selectionForFilter( selection, option ) : selection );
        this.setState( { selectedFilters } );
    }

    render() {
        const { Component, ...props } = this.props;
        const { sortKey, sortDirection, pageSize, selectedFilters } = this.state;

        return (
            <Component
                {...props}
                setSort={this.setSort}
                sortKey={sortKey}
                sortDirection={sortDirection}
                showMore={this.handleShowMore}
                pageSize={pageSize}
                selectedFilters={selectedFilters}
                resetFilters={this.resetFilters}
                onFilterSelect={this.selectFilterOption}
            />
        );
    }
}
