
import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';

import Row from '../row';

const ExtraContent = () => <td />;

describe( 'Manager Table Row :', () => {
    let props;
    beforeEach( () => {
        props = {
           extraContent: <ExtraContent />
        };
    } );

    it( 'should render without errors', () => {
        const component = renderer.create(
            <Row {...props}><td /></Row>
        );
        expect( component.toJSON() ).toMatchSnapshot();
    } );

    it( 'should show extra content after handleClick is executed', () => {
        const wrapper = mount(
            <table><tbody><Row expandable {...props} ><td /></Row></tbody></table>
        );
        expect( wrapper.find( ExtraContent ).exists() ).toBe( false );
        const component = wrapper.find( Row ).instance();
        component.handleClick();
        wrapper.update();
        expect( wrapper.find( ExtraContent ).exists() ).toBe( true );
    } );
} );
