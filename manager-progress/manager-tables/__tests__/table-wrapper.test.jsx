import React from 'react';
import forEach from 'lodash/forEach';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';

import TableWrapper from '../table-wrapper';
import { SORT_ASCENDING, DEFAULT_PAGE_SIZE } from '../constants';

const Component = () => <div />;

describe( 'TableWrapper :', () => {
    let props;
    beforeEach( () => {
        props = {
            Component,
            filters: []
        };
    } );

    it( 'should render without errors', () => {
        const component = renderer.create(
            <TableWrapper {...props} />
        );
        expect( component.toJSON() ).toMatchSnapshot();
    } );
} );

describe( 'TableWrapper props :', () => {
    const wrapper = mount( <TableWrapper Component={Component} filters={[]} /> );
    const state = wrapper.state();
    const component = wrapper.instance();
    const rendered = wrapper.find( Component );

    forEach( {
        setSort: component.setSort,
        sortKey: state.sortKey,
        sortDirection: state.sortDirection,
        showMore: component.handleShowMore,
        pageSize: state.pageSize,
        selectedFilters: state.selectedFilters,
        resetFilters: component.resetFilters,
        onFilterSelect: component.selectFilterOption
    }, ( value, name ) => {
        it( `should render the given component with computed '${name}' and value: '${value}'`, () => {
            expect( rendered.props()[ name ] ).toEqual( value );
        } );
    } );
} );

describe( 'TableWrapper::setSort', () => {
    it( 'should apply the sortKey to the component state', () => {
        const wrapper = mount( <TableWrapper Component={Component} filters={[]} /> );
        const rendered = wrapper.find( Component );
        rendered.props().setSort( { sortKey: 'something' } );
        wrapper.update();
        expect( wrapper.state().sortKey ).toEqual( 'something' );
    } );

    it( 'should apply the sortDirection to the component state', () => {
        const wrapper = mount( <TableWrapper Component={Component} filters={[]} /> );
        const rendered = wrapper.find( Component );
        rendered.props().setSort( { sortDirection: 'something' } );
        wrapper.update();
        expect( wrapper.state().sortDirection ).toEqual( 'something' );
    } );

    it( 'should not barf if setSort is not given any options', () => {
        const wrapper = mount( <TableWrapper Component={Component} filters={[]} /> );
        const rendered = wrapper.find( Component );
        expect( () => rendered.props().setSort() ).not.toThrow();
    } );
} );

describe( 'TableWrapper filters', () => {
    let props;
    beforeEach( () => {
        props = {
            Component,
            filters: [
                { label: 'foo', options: [ 'NO_SELECTION', 'SOME_OPTION' ] },
                { label: 'bar', options: [ 'NO_SELECTION', 'SOME_OPTION' ] },
                { label: 'baz', options: [ 'NO_SELECTION', 'SOME_OPTION' ] }
            ]
        };
    } );

    it( 'should update the selectedFilters list when new filteres are given via props', () => {
        const wrapper = mount( <TableWrapper {...props} /> );
        let rendered = wrapper.find( Component );
        expect( rendered.props().selectedFilters ).toEqual( expect.arrayContaining( [
            expect.objectContaining( { label: 'foo', option: 'NO_SELECTION' } ),
            expect.objectContaining( { label: 'bar', option: 'NO_SELECTION' } ),
            expect.objectContaining( { label: 'baz', option: 'NO_SELECTION' } ),
        ] ) );
        props.filters.pop();
        wrapper.setProps( { filters: props.filters } );
        wrapper.update();
        rendered = wrapper.find( Component );
        expect( rendered.props().selectedFilters ).toEqual( expect.arrayContaining( [
            expect.objectContaining( { label: 'foo', option: 'NO_SELECTION' } ),
            expect.objectContaining( { label: 'bar', option: 'NO_SELECTION' } ),
        ] ) );
    } );

    it( 'should create a list of filter option items', () => {
        const wrapper = mount( <TableWrapper {...props} /> );
        const rendered = wrapper.find( Component );
        expect( rendered.props().selectedFilters ).toEqual( expect.arrayContaining( [
            expect.objectContaining( { label: 'foo', option: 'NO_SELECTION' } ),
            expect.objectContaining( { label: 'bar', option: 'NO_SELECTION' } ),
            expect.objectContaining( { label: 'baz', option: 'NO_SELECTION' } ),
        ] ) );
    } );

    it( 'should set the filter option when onFilterSelect is called', () => {
        const wrapper = mount( <TableWrapper {...props} /> );
        let rendered = wrapper.find( Component );
        rendered.props().onFilterSelect( { label: 'foo' }, 'SOME_OPTION' );
        wrapper.update();
        rendered = wrapper.find( Component );
        expect( rendered.props().selectedFilters ).toEqual( expect.arrayContaining( [
            expect.objectContaining( { label: 'foo', option: 'SOME_OPTION' } ),
            expect.objectContaining( { label: 'bar', option: 'NO_SELECTION' } ),
            expect.objectContaining( { label: 'baz', option: 'NO_SELECTION' } ),
        ] ) );
    } );

    it( 'should reset the filter options to their first option when `resetFilters` is called', () => {
        const wrapper = mount( <TableWrapper {...props} /> );
        let rendered = wrapper.find( Component );
        rendered.props().onFilterSelect( { label: 'foo' }, 'SOME_OPTION' );
        rendered.props().onFilterSelect( { label: 'bar' }, 'SOME_OPTION' );
        rendered.props().onFilterSelect( { label: 'baz' }, 'SOME_OPTION' );
        wrapper.update();
        rendered = wrapper.find( Component );
        expect( rendered.props().selectedFilters ).toEqual( expect.arrayContaining( [
            expect.objectContaining( { label: 'foo', option: 'SOME_OPTION' } ),
            expect.objectContaining( { label: 'bar', option: 'SOME_OPTION' } ),
            expect.objectContaining( { label: 'baz', option: 'SOME_OPTION' } ),
        ] ) );
        rendered.props().resetFilters();
        wrapper.update();
        rendered = wrapper.find( Component );
        expect( rendered.props().selectedFilters ).toEqual( expect.arrayContaining( [
            expect.objectContaining( { label: 'foo', option: 'NO_SELECTION' } ),
            expect.objectContaining( { label: 'bar', option: 'NO_SELECTION' } ),
            expect.objectContaining( { label: 'baz', option: 'NO_SELECTION' } ),
        ] ) );
    } );

} );

describe( 'TableWrapper::handleShowMore', () => {
    it( 'should add the DEFAULT_PAGE_SIZE to it`s pageSize', () => {
        const wrapper = mount( <TableWrapper Component={Component} filters={[]} /> );
        let rendered = wrapper.find( Component );
        expect( rendered.props().pageSize ).toEqual( DEFAULT_PAGE_SIZE );
        rendered.props().showMore( { preventDefault: jest.fn() } );
        wrapper.update();
        rendered = wrapper.find( Component );
        expect( rendered.props().pageSize ).toEqual( DEFAULT_PAGE_SIZE * 2 );
    } );
} );
