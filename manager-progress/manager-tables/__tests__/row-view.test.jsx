import React from 'react';
import { renderWithStore, mountWithStore } from 'app-test-utils';
import fixture from '@biw/test-utils/fixtures/m5';

import RowView from '../row-view';

describe( 'Manager Table RowView :', () => {
    it( 'renders without errors', () => {
        const component = renderWithStore(
            <RowView onShowMore={jest.fn()} totalCount={5} />,
            { fixture }
        );
        expect( component.toJSON() ).toMatchSnapshot();
    } );

    it( 'should have a `showMore` button if the pageSize is less than the total count', () => {
        const component = mountWithStore(
            <RowView onShowMore={jest.fn()} totalCount={5} pageSize={1} />,
            { fixture }
        );
        expect( component.find( 'a.load-more' ).exists() ).toBe( true );
    } );
} );
