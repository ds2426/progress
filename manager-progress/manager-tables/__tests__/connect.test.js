import { createStore } from 'app-test-utils';
import fixture from '@biw/test-utils/fixtures/m5';
import { mapStateToProps } from '../connect';

describe( 'manager-tables mapStateToProps', () => {
    it( 'should return the appropriate props for a given state', () => {
        const store = createStore( { fixture } );
        const props = mapStateToProps( store.getState() );
        expect( props ).toEqual( expect.objectContaining( {
            hasHierarchy: false,
            inGoalSelection: false,
            filters: expect.any( Array )
        } ) );
    } );
} );
