import React from 'react';
import renderer from 'react-test-renderer';

import Tab from '../tab';

describe( 'Manager Table Tab :', () => {
    let props;
    beforeEach( () => {
        props = {
            onClick: jest.fn(),
            name: 'foo',
            activeTab: 'bar'
        };
    } );

    it( 'renders without errors', () => {
        const component = renderer.create(
            <Tab {...props}><span /></Tab>
        );
        const tree = component.toJSON();
        expect( tree ).toMatchSnapshot();
    } );

    it( 'should apply isActive when activeTab is the same as name', () => {
        props.activeTab = props.name;
        const component = renderer.create(
            <Tab {...props}><span /></Tab>
        );
        const tree = component.toJSON();
        expect( tree ).toMatchSnapshot();
    } );

    it( 'calls `onClick` prop when `handleClick` is executed', () => {
        const component = renderer.create(
            <Tab {...props}><span /></Tab>
        );
        component.getInstance().handleClick();
        expect( props.onClick ).toHaveBeenCalledWith( props.name );
    } );
} );
