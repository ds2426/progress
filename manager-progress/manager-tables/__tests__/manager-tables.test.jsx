import React from 'react';
import { renderWithStore, mountWithStore } from 'app-test-utils';
import fixture from '@biw/test-utils/fixtures/m5';

import ManagerTables, { HIERARCHY, PARTICIPANT } from '../manager-tables';
import ParticipantTable from '../participant';
import HierarchyTable from '../hierarchy';

describe( 'Manager Tables :', () => {
    it( 'renders without errors', () => {
        const component = renderWithStore(
            <ManagerTables hasHierarchy inGoalSelection/>,
            { fixture }
        );
        const tree = component.toJSON();
        expect( tree ).toMatchSnapshot();
    } );

    it( 'should render the `ParticipantTable` if hasHierarchy is false', () => {
        const wrapper = mountWithStore(
            <ManagerTables />,
            { fixture }
        );

        expect( wrapper.find( ParticipantTable ).exists() ).toBe( true );
        expect( wrapper.find( HierarchyTable ).exists() ).toBe( false );
    } );

    it( 'should render the `HierarchyTable` if hasHierarchy is true', () => {
        const wrapper = mountWithStore(
            <ManagerTables hasHierarchy />,
            { fixture }
        );

        expect( wrapper.find( ParticipantTable ).exists() ).toBe( false );
        expect( wrapper.find( HierarchyTable ).exists() ).toBe( true );
    } );

    it( 'should render the table for the tab when the tab is clicked on', () => {
        const wrapper = mountWithStore(
            <ManagerTables hasHierarchy />,
            { fixture }
        );
        const hierarchyTab = wrapper.find( `Tab[name="${HIERARCHY}"]` );
        expect( hierarchyTab.exists() ).toBe( true );
        const participantTab = wrapper.find( `Tab[name="${PARTICIPANT}"]` );
        expect( participantTab.exists() ).toBe( true );

        expect( wrapper.find( ParticipantTable ).exists() ).toBe( false );
        expect( wrapper.find( HierarchyTable ).exists() ).toBe( true );

        participantTab.instance().handleClick();
        wrapper.update();

        expect( wrapper.find( ParticipantTable ).exists() ).toBe( true );
        expect( wrapper.find( HierarchyTable ).exists() ).toBe( false );
    } );
} );
