// libs
import React from 'react';
import PropTypes from 'prop-types';
import { LocaleString } from '@biw/intl';

import { statusCmmoKeys } from '../constants';

const Label = ( { hasStatus, option } ) => hasStatus ?
    <span>
        <LocaleString code={ `managerProgress.chart.xAxisLabel.${statusCmmoKeys[ option.value ]}.label` } />
        (<LocaleString code={ `managerProgress.chart.xAxisLabel.${statusCmmoKeys[ option.value ]}.sublabel` } />)
    </span>
:
    option.label;

Label.propTypes = {
    hasStatus: PropTypes.bool,
    option: PropTypes.shape( {} )
};

export default Label;
