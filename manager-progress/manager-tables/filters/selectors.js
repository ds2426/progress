import memoize from 'lodash/memoize';
import get from 'lodash/get';
import find from 'lodash/find';
import { createSelector } from 'reselect';
import { programIsObjectives } from '../selectors';

import { selectors as progressSelectors } from 'contests-progress-dux';
import { selectors as programSelectors } from '@biw/programs-dux';

export const NO_SELECTION = { cmmoLabel: 'common.all', value: '' };
const PLAN_FILTER = {
    label: 'managerProgress.plan',
    key: 'hasPlanSet',
    exportKey: 'hasPlanSet',
    options: [
        NO_SELECTION,
        { cmmoLabel: 'common.yes', value: 1, exportValue: 'Y' },
        { cmmoLabel: 'common.no', value: 0, exportValue: 'N' },
    ]
};
const MQ_FILTER = {
    label: 'managerProgress.minQualifier',
    key: 'hasMetMq',
    exportKey: 'mqPct',
    options: [
        NO_SELECTION,
        { cmmoLabel: 'managerProgress.met', value: 1, exportValue: true },
        { cmmoLabel: 'managerProgress.notMet', value: 0, exportValue: false },
    ]
};

const getActiveFilters = selectedFilters => selectedFilters.filter( f => f.option !== NO_SELECTION );
export const getExportFilterOptions = createSelector(
    getActiveFilters,
    filters => filters.reduce( ( memo, f ) =>{
        memo[ f.exportKey ] = f.option.exportValue;
        if ( f.extraExportKey ) {
            memo[ f.extraExportKey ] = f.option.extraExportValue;
        }
        return memo;
    }, {} )
);

const getProgramSelector = programViewId => state => programSelectors.getProgram( state, programViewId );
export const getFiltersSelector = memoize( programViewId => createSelector(
    progressSelectors.getProgressSelector( programViewId ),
    progressSelectors.getGoalSelector( programViewId ),
    getProgramSelector( programViewId ),
    ( progress, goal, program ) => {
        const isObjectives = programIsObjectives( program );
        if ( get( goal, 'barChart.levelsBarChart' ) ) {
            if ( isObjectives ) {
                return [ PLAN_FILTER ];
            }
            return [
                PLAN_FILTER,
                {
                    label: 'managerProgress.level',
                    key: 'tierName',
                    options: [
                        NO_SELECTION,
                        ...goal.barChart.levelsBarChart.map( level => ( {
                            label: level.tierName,
                            value: level.tierName,
                            exportValue: level.payoutTierId
                        } ) )
                    ]
                }
            ];
        } else if ( goal.progressOnTrackToGoalChart ) {
            const { filterOptions, isMqEnabled } = progress.filterOptions;
            const filters = [
                {
                    label: 'managerProgress.group',
                    key: 'groupName',
                    exportKey: 'groupName',
                    extraExportKey: 'groupId',
                    options: [
                        NO_SELECTION,
                        ...filterOptions.map( filterOption => ( {
                            label: filterOption.groupName,
                            value: filterOption.groupName,
                            exportValue: filterOption.groupName,
                            extraExportValue: filterOption.groupId
                        } ) ).sort( ( a, b ) => a.value > b.value ? 1 : -1 )
                    ]
                }
            ];
            if ( !isObjectives ) {
                filters.push( {
                    label: 'managerProgress.level',
                    key: 'tierName',
                    exportKey: 'payoutTierId',
                    extraExportKey: 'achieveAmount',
                    options: [
                        NO_SELECTION,
                        ...filterOptions
                            .reduce( ( memo, filterOption ) => ( [ ...memo, ...filterOption.levels ] ), [] )
                            .reduce( ( memo, option ) => {
                                const existing = find( memo, item => item.label === option.label );
                                if ( !existing ) {
                                    memo.push( {
                                        label: option.label,
                                        value: option.label,
                                        exportValue: option.key,
                                        extraExportValue: option.label
                                    } );
                                } else {
                                    existing.exportValue = `${existing.exportValue},${option.key}`;
                                }
                                return memo;
                            }, [] )
                    ]
                } );
            }
            if ( isMqEnabled ) {
                filters.push( MQ_FILTER );
            }

            return [
                ...filters,
                PLAN_FILTER,
                {
                    label: 'managerProgress.status',
                    key: 'onTrackStatus',
                    exportKey: 'onTrackPct',
                    options: [
                        NO_SELECTION,
                        ...goal.progressOnTrackToGoalChart.map( level => ( {
                            label: `${ level.onTrackName }${ level.onTrackDesc ? `(${level.onTrackDesc})` : '' }`,
                            value: level.onTrackStatus,
                            exportValue: level.onTrackStatus
                        } ) )
                    ]
                }
            ];
        }
    }
) );
