import React from 'react';
import PropTypes from 'prop-types';

import { LocaleString } from '@biw/intl';
import DropDown from './dropdown';

export default class Filters extends React.Component {
    static propTypes = {
        filters: PropTypes.array.isRequired,
        selectedFilters: PropTypes.array.isRequired,
        resetFilters: PropTypes.func.isRequired,
        onSelect: PropTypes.func.isRequired
    }

    handleResetFilter = ( e ) => {
        e.preventDefault();
        this.props.resetFilters();
    }

    render() {
        const {
            filters,
            selectedFilters,
            onSelect
        } = this.props;

        return filters.length > 0 &&
            <div className="filters">
                { filters.map( ( filter, i ) => (
                    <DropDown
                        key={ filter.label }
                        filter={ filter }
                        selection={ selectedFilters[ i ] }
                        onSelect={ onSelect }
                    />
                ) ) }
                <div className="filter-dropdown-wrap">
                    <a href="#" onClick={ this.handleResetFilter }>
                        <LocaleString code="managerProgress.paxTable.clearFilter" />
                    </a>
                </div>
            </div>;
    }
}
