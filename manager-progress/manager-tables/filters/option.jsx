// libs
import React from 'react';
import PropTypes from 'prop-types';
import { LocaleString } from '@biw/intl';

import { statusClasses } from '../constants';
import Label from './label';

export default class FilterDropdown extends React.Component {
    static propTypes = {
        hasStatus: PropTypes.bool,
        option: PropTypes.shape( {} ),
        onClick: PropTypes.func.isRequired
    }

    handleClick = e => {
        e.preventDefault();
        this.props.onClick( this.props.option );
    }

    render() {
        const { hasStatus, option } = this.props;

        return (
            <li onClick={ this.handleClick }>
                { hasStatus && <div className={ `status ${statusClasses[ option.value ]}` }></div> }
                { option.cmmoLabel ?
                    <LocaleString code={ option.cmmoLabel } />
                :
                    <Label hasStatus={hasStatus} option={option} />
                }
            </li>
        );
    }
}
