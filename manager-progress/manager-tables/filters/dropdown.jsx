// libs
import React from 'react';
import PropTypes from 'prop-types';
import { CSSTransition } from 'react-transition-group';
// shared components
import { CaretDownIcon } from 'component-library';
import { LocaleString } from '@biw/intl';

import Option from './option';

export default class FilterDropdown extends React.Component {
    static propTypes = {
        filter: PropTypes.shape( {
            label: PropTypes.string,
            options: PropTypes.arrayOf( PropTypes.shape( {
                cmmoLabel: PropTypes.string,
            } ) ).isRequired
        } ),
        selection: PropTypes.shape( {
            label: PropTypes.string,
            option: PropTypes.shape( {
                cmmoLabel: PropTypes.string,
            } ).isRequired
        } ),
        onSelect: PropTypes.func.isRequired
    }

    state = {
        isOpen: false
    };

    toggleMenu = () => this.setState( { isOpen: !this.state.isOpen } )
    openMenu   = () => this.setState( { isOpen: true } )
    closeMenu  = () => this.setState( { isOpen: false } )
    handleBlur = () => this.closeMenu()

    selectOption = option => {
        this.props.onSelect( this.props.filter, option );
        this.closeMenu();
    }

    render() {
        const { filter, selection } = this.props;
        const { isOpen }  = this.state;
        const hasStatus = filter.label === 'managerProgress.status';

        return (
            <div className="filter-dropdown-wrap">
                <div className="filter-dropdown-label"><LocaleString code={ filter.label } /></div>
                <div className="filter-dropdown" tabIndex="0" onBlur={ this.handleBlur }>
                    <div className="filter-dd-selected" onClick={ this.toggleMenu }>
                        { selection.option.label || <LocaleString code={ selection.option.cmmoLabel } /> }
                        <div className="icon"><CaretDownIcon /></div>
                    </div>
                    <CSSTransition
                        in={isOpen}
                        timeout={200}
                        classNames="ani-dd-list"
                        unmountOnExit
                    >
                        <ul className="filter-dropdown-list">
                            { filter.options.map( option => (
                                <Option
                                    key={option.label || option.cmmoLabel}
                                    hasStatus={hasStatus}
                                    option={option}
                                    onClick={this.selectOption}
                                />
                            ) ) }
                        </ul>
                    </CSSTransition>
                </div>
            </div>
        );
    }
}
