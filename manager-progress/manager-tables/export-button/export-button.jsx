// libs
import React from 'react';
import PropTypes from 'prop-types';

import { LocaleString } from '@biw/intl';
import DownloadLink from 'shared/download-link';

const EMPTY_OBJ = {};
export default class ExportButton extends React.Component {
    static propTypes = {
        fetchManagerProgressExport: PropTypes.func,
        busyFetchingManagerProgressExport: PropTypes.bool,
        orgUnitId: PropTypes.any,
        filterOptions: PropTypes.shape( {} )
    }

    static defaultProps = {
        busyFetchingManagerProgressExport: false,
        filterOptions: EMPTY_OBJ
    }

    handleClick = ( e ) => {
        if ( !this.props.busyFetchingManagerProgressExport ) {
            this.props.fetchManagerProgressExport( this.props.orgUnitId, this.props.filterOptions );
        }
    }

    render() {
        const { busyFetchingManagerProgressExport } = this.props;
        return (
            <DownloadLink
                onClick={this.handleClick}
                busy={busyFetchingManagerProgressExport}
            >
                <LocaleString code="managerProgress.progressDetails.exportTbl" />
            </DownloadLink>
        );
    }
}
