import enhancer from './connect';
import ExportButton from './export-button';

export default enhancer( ExportButton );
