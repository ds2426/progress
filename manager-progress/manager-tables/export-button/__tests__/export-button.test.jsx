import React from 'react';
import renderer from 'react-test-renderer';

import ExportButton from '../export-button';

describe( 'ExportButton :', () => {
    let props;
    beforeEach( () => {
        props = {
            fetchManagerProgressExport: jest.fn(),
            busyFetchingManagerProgressExport: false,
            orgUnitId: 'org-id',
            filterOptions: {}
        };
    } );

    it( 'should render without errors', () => {
        const component = renderer.create(
            <ExportButton {...props} />
        );
        expect( component.toJSON() ).toMatchSnapshot();
    } );
} );
