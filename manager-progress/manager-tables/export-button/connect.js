import { connect } from 'react-redux';
import get from 'lodash/get';
import { clientFetch } from 'biw-client-fetch';
import { getActiveProgram } from 'app/router/selectors';

const MANAGER_PROGRESS_EXPORT = 'managerProgressExport';
export function fetchManagerProgressExport( orgUnitId, filterOpts = {} ) {
    return ( dispatch, getState ) => {
        const state = getState();
        const { programType, programId, role, programViewStatus } = getActiveProgram( state );
        let url = `/api/v1.0/progress/manager/export/${ programType }/${ programId }/${ role }/${ programViewStatus }`;

        if ( orgUnitId ) {
            url = `${ url }/${ orgUnitId }`;
        }

        const body = { ...filterOpts };

        // TODO this value should really just be boolean, instead of 1, 0
        if ( body.hasPlanSet ) {
            body.hasPlanSet = body.hasPlanSet === 'Y' ? 1 : 0;
        }

        const headers = {
            'Accept': 'text/csv',
        };

        return clientFetch( {
            url: url,
            method: 'POST',
            body: body,
            headers: headers,
            type: MANAGER_PROGRESS_EXPORT
        } ).then( csvText => {

            if ( !csvText ) {
                return;
            }

            const blob = new Blob( [ csvText ], { type: 'text/csv;charset=utf-8;' } ) ;
            let file;
            let a;

            if ( navigator.msSaveOrOpenBlob ) {
                navigator.msSaveOrOpenBlob( blob, 'ManagerProgressExport.csv' );
            } else {
                file = window.URL.createObjectURL( blob );
                a = document.createElement( 'a' );
                a.setAttribute( 'style', 'display: none' );
                document.body.appendChild( a ); // Firefox needs the element in the DOM to click
                a.href = file;
                a.download = 'ManagerProgressExport.csv';
                a.click();
                document.body.removeChild( a );
            }

            return 'exported';
        } );
    };
}

export const mapStateToProps = ( state ) => ( {
    busyFetchingManagerProgressExport: get( state, `data.fetching.${MANAGER_PROGRESS_EXPORT}`, false ),
} );

export const mapDispatchToProps = ( dispatch ) => ( {
    fetchManagerProgressExport: ( id, opts ) => dispatch( fetchManagerProgressExport( id, opts ) ),
} );

export default connect( mapStateToProps, mapDispatchToProps );
