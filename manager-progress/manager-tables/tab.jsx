import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

export default class Tab extends React.Component {
    static propTypes = {
        children: PropTypes.node,
        name: PropTypes.string.isRequired,
        activeTab: PropTypes.string.isRequired,
        onClick: PropTypes.func.isRequired
    }

    handleClick = () => this.props.onClick( this.props.name )

    render() {
        const { name, activeTab, children } = this.props;

        return (
            <li className={ classNames( { 'active': name === activeTab } ) } onClick={ this.handleClick }>
                { children }
            </li>
        );
    }
}
