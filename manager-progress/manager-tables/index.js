import connect from './connect';
import ManagerTables from './manager-tables';

export default connect( ManagerTables );
