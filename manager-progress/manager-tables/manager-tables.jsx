import React from 'react';
import PropTypes from 'prop-types';

import { LocaleString } from '@biw/intl';

import Tab from './tab';
import TableWrapper from './table-wrapper';
import HierarchyTable from './hierarchy';
import ParticipantTable from './participant';

export const HIERARCHY = 'hierarchy';
export const PARTICIPANT = 'participant';

const getIsMobile = () => window.innerWidth < 961;

export default class ManagerTables extends React.Component {
    static propTypes = {
        hasHierarchy: PropTypes.bool,
        inGoalSelection: PropTypes.bool,
        filters: PropTypes.array
    }

    static defaultProps = {
        hasHierarchy: false,
        inGoalSelection: false,
        filters: []
    }

    constructor( props ) {
        super( props );
        this.state = {
            activeTab: HIERARCHY,
            isMobile: getIsMobile()
        };
        window.addEventListener( 'resize', this.updateIsMobile );
    }

    componentWillUnmount() {
        window.removeEventListener( 'resize', this.updateIsMobile );
    }

    updateIsMobile = event => this.setState( { isMobile: getIsMobile() } )

    handleTabClick = tabName => this.setState( { activeTab: tabName } )

    render() {
        const { hasHierarchy, inGoalSelection, filters } = this.props;
        const { activeTab, isMobile } = this.state;

        const tableHeaderCode = inGoalSelection ?
            'managerProgress.progressDetails.selectedGoalTblHeader' :
            'managerProgress.progressDetails.progressTblHeader';

        return (
            <div className="progress-by-participant">
                <LocaleString TextComponent="h3" code={tableHeaderCode} />

                { hasHierarchy ?
                    <div className="tab-panel">
                        <ul className="tabs">
                            <Tab name={HIERARCHY} activeTab={activeTab} onClick={this.handleTabClick}>
                                <LocaleString code="managerProgress.progressDetails.progressTblByHierarchy" />
                            </Tab>
                            <Tab name={PARTICIPANT} activeTab={activeTab} onClick={this.handleTabClick}>
                                <LocaleString code="managerProgress.progressDetails.progressTblByPax" />
                            </Tab>
                        </ul>
                        { activeTab === HIERARCHY ?
                            <TableWrapper Component={HierarchyTable} isMobile={isMobile} />
                        :
                            <TableWrapper Component={ParticipantTable} isMobile={isMobile} filters={filters} />
                        }
                    </div>
                :
                    <TableWrapper Component={ParticipantTable} isMobile={isMobile} filters={filters} />
                }
            </div>
        );
    }
}
