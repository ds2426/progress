import React from 'react';
import PropTypes from 'prop-types';
import { CaretDownIcon } from 'component-library';

import { SORT_ASCENDING, SORT_DESCENDING } from '../constants';

export default class ColumnHead extends React.Component {
    static propTypes = {
        onClick: PropTypes.func.isRequired,
        sortKey: PropTypes.string.isRequired,
        sortDirection: PropTypes.string.isRequired,
        activeSortKey: PropTypes.string.isRequired,
        children: PropTypes.node
    }

    handleClick = () => {
        const { activeSortKey, sortKey, sortDirection } = this.props;
        const sortOptions = { sortKey, sortDirection: SORT_ASCENDING };
        if ( activeSortKey === sortKey ) {
            sortOptions.sortDirection = sortDirection === SORT_ASCENDING ? SORT_DESCENDING : SORT_ASCENDING;
        }
        this.props.onClick( sortOptions );
    }

    render() {
        const { sortKey, sortDirection, activeSortKey, children } = this.props;
        const sortClass = sortDirection === SORT_ASCENDING ? 'asc' : 'desc';
        return (
            <th onClick={ this.handleClick } className={ sortKey === activeSortKey ? `sorted-on ${sortClass}` : '' } >
                { children }
                <div className="icon"><CaretDownIcon /></div>
            </th>
        );
    }
}
