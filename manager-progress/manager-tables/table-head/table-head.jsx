// libs
import React from 'react';
import PropTypes from 'prop-types';

import Column from './column';
import { LocaleString } from '@biw/intl';

const TableHead = ( {
    sortKey,
    sortDirection,
    onClick,
    schema,
} ) => (
    <thead>
        <tr>
            { schema.map( col => (
                <Column
                    key={col.valueProp}
                    onClick={onClick}
                    sortDirection={sortDirection}
                    sortKey={col.valueProp}
                    activeSortKey={sortKey}
                >
                    <LocaleString code={col.headerLabel} />
                </Column>
            ) ) }
        </tr>
    </thead>
);

TableHead.propTypes = {
    sortKey: PropTypes.string.isRequired,
    sortDirection: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    schema: PropTypes.arrayOf( PropTypes.shape( {
        valueProp: PropTypes.string.isRequired,
        headerLabel: PropTypes.string.isRequired
    } ) ).isRequired
};

export default TableHead;
