import { connect } from 'react-redux';
import memoize from 'lodash/memoize';
import { createSelector } from 'reselect';

import { getActiveProgram } from 'app/router/selectors';
import { selectors as progressSelectors } from 'contests-progress-dux';


import { constants } from 'contests-utils';
import { getFiltersSelector } from './filters';
const {
    PVS__GOAL_SELECTION,
    PVS__GOAL_SELECTED,
    PVS__NO_PROGRESS,
} = constants;

const getInGoalSelection = createSelector(
    getActiveProgram,
    program => [ PVS__GOAL_SELECTION, PVS__GOAL_SELECTED, PVS__NO_PROGRESS ].indexOf( program.programViewStatus ) > -1
);

const getHasHierarchySelector = memoize( programViewId => createSelector(
    progressSelectors.getProgressSelector( programViewId ),
    ( { hierarchyTable } ) => !!hierarchyTable
) );

export const mapStateToProps = ( state ) => {
    const program = getActiveProgram( state );

    return {
        hasHierarchy: getHasHierarchySelector( program.programViewId )( state ),
        inGoalSelection: getInGoalSelection( state ),
        filters: getFiltersSelector( program.programViewId )( state )
    };

};

export default connect( mapStateToProps );
