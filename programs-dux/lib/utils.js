'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.STATUS_ORDER = undefined;
exports.programViewSort = programViewSort;

var _parse = require('date-fns/parse');

var _parse2 = _interopRequireDefault(_parse);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var STATUS_ORDER = exports.STATUS_ORDER = ['goal_selection', 'goal_selected', 'no_goal_selected', 'no_progress', 'progress_loaded_no_issuance', 'final_issuance'];

function programViewSort(a, b) {
    if (a.programViewStatus !== b.programViewStatus) {
        return STATUS_ORDER.indexOf(a.programViewStatus) - STATUS_ORDER.indexOf(b.programViewStatus);
    }
    return (0, _parse2.default)(a.goalSelectionToDate) > (0, _parse2.default)(b.goalSelectionToDate) ? -1 : 1;
}
//# sourceMappingURL=utils.js.map
