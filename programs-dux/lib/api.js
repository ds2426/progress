'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getProgramViews = getProgramViews;
exports.getProgramView = getProgramView;

var _biwClientFetch = require('biw-client-fetch');

var _types = require('./types');

var types = _interopRequireWildcard(_types);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function getProgramViews() {
    return (0, _biwClientFetch.clientFetch)({
        url: '/api/v1.0/program-views',
        type: types.FETCHING_PROGRAM_VIEWS
    });
}

function getProgramView(programViewId) {
    return (0, _biwClientFetch.clientFetch)({
        url: '/api/v1.0/program-views/' + programViewId,
        type: types.fetchingProgramView(programViewId)
    });
}
//# sourceMappingURL=api.js.map
