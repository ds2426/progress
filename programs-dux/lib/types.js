'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var USER_SET_ELIGIBLE_PROGRAMS = exports.USER_SET_ELIGIBLE_PROGRAMS = 'USER_SET_ELIGIBLE_PROGRAMS';
var PROGRAMS_SET_PROGRAM_VIEWS = exports.PROGRAMS_SET_PROGRAM_VIEWS = 'PROGRAMS_SET_PROGRAM_VIEWS';
var FETCHING_PROGRAM_VIEWS = exports.FETCHING_PROGRAM_VIEWS = 'FETCHING_PROGRAM_VIEWS';
var PROGRAM_VIEWS_SET_LAST_FETCHED = exports.PROGRAM_VIEWS_SET_LAST_FETCHED = 'PROGRAM_VIEWS_SET_LAST_FETCHED';

var fetchingProgramView = exports.fetchingProgramView = function fetchingProgramView(programViewId) {
  return 'FETCHING_PROGRAM_VIEW_' + programViewId;
};
//# sourceMappingURL=types.js.map
