'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
// actionCreators


var _is_after = require('date-fns/is_after');

var _is_after2 = _interopRequireDefault(_is_after);

var _parse = require('date-fns/parse');

var _parse2 = _interopRequireDefault(_parse);

var _difference_in_days = require('date-fns/difference_in_days');

var _difference_in_days2 = _interopRequireDefault(_difference_in_days);

var _contestsUtils = require('contests-utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var PVS__PROGRESS_LOADED_NO_ISSUANCE = _contestsUtils.constants.PVS__PROGRESS_LOADED_NO_ISSUANCE,
    PVS__GOAL_SELECTION = _contestsUtils.constants.PVS__GOAL_SELECTION,
    ROLE__SELECTOR = _contestsUtils.constants.ROLE__SELECTOR,
    PVS__NO_PROGRESS = _contestsUtils.constants.PVS__NO_PROGRESS,
    PT__OBJ = _contestsUtils.constants.PT__OBJ;


var selectorRoleRegex = new RegExp('^' + ROLE__SELECTOR + '$', 'i');

var daysToDate = function daysToDate(date) {
    return date ? (0, _difference_in_days2.default)((0, _parse2.default)(date), new Date()) + 1 : 0;
};

var ProgramModel = function () {
    function ProgramModel(program) {
        var _this = this;

        _classCallCheck(this, ProgramModel);

        _extends(this, program || {});
        this.__state = program || {};
        var CACHE = {};
        var proto = this.constructor.prototype;
        Object.getOwnPropertyNames(proto).forEach(function (methodName) {
            if (methodName !== 'constructor') {
                Object.defineProperty(_this, methodName, {
                    get: function get() {
                        return methodName in CACHE ? CACHE[methodName] : CACHE[methodName] = proto[methodName].call(_this);
                    },
                    set: function set(val) {
                        return CACHE[methodName] = val;
                    }
                });
            }
        });
    }

    _createClass(ProgramModel, [{
        key: 'isObjectives',
        value: function isObjectives() {
            return this.__state.programType === PT__OBJ;
        }
    }, {
        key: 'isManager',
        value: function isManager() {
            return !selectorRoleRegex.test(this.__state.role);
        }
    }, {
        key: 'isGoalSelection',
        value: function isGoalSelection() {
            return this.__state.programViewStatus === PVS__GOAL_SELECTION;
        }
    }, {
        key: 'programHasEnded',
        value: function programHasEnded() {
            return (0, _is_after2.default)(new Date(), (0, _parse2.default)(this.__state.endDate || Date.now()));
        }
    }, {
        key: 'programHasStarted',
        value: function programHasStarted() {
            return (0, _is_after2.default)(new Date(), (0, _parse2.default)(this.__state.startDate || Date.now()));
        }
    }, {
        key: 'isCheckingResults',
        value: function isCheckingResults() {
            return this.__state.programViewStatus === PVS__PROGRESS_LOADED_NO_ISSUANCE && this.programHasEnded;
        }
    }, {
        key: 'isWaitingForProgress',
        value: function isWaitingForProgress() {
            return this.__state.programViewStatus === PVS__NO_PROGRESS && this.programHasStarted;
        }
    }, {
        key: 'daysToGoalSelection',
        value: function daysToGoalSelection() {
            return daysToDate(this.__state.goalSelectionToDate);
        }
    }, {
        key: 'daysToProgramStartDate',
        value: function daysToProgramStartDate() {
            return daysToDate(this.__state.startDate);
        }
    }, {
        key: 'daysToProgramEndDate',
        value: function daysToProgramEndDate() {
            return daysToDate(this.__state.endDate);
        }
    }]);

    return ProgramModel;
}();

exports.default = ProgramModel;
module.exports = exports['default'];
//# sourceMappingURL=program-model.js.map
