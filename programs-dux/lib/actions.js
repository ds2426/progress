'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getProgramView = exports.getProgramViews = exports.setProgramViewsLastFetched = exports.setProgramViews = undefined;

var _types = require('./types');

var types = _interopRequireWildcard(_types);

var _api = require('./api');

var api = _interopRequireWildcard(_api);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var setProgramViews = exports.setProgramViews = function setProgramViews(programs) {
    return {
        type: types.PROGRAMS_SET_PROGRAM_VIEWS,
        programs: programs
    };
};

var setProgramViewsLastFetched = exports.setProgramViewsLastFetched = function setProgramViewsLastFetched() {
    return {
        type: types.PROGRAM_VIEWS_SET_LAST_FETCHED
    };
};

var getProgramViews = exports.getProgramViews = function getProgramViews() {
    return function (dispatch) {
        return api.getProgramViews().then(function (response) {
            dispatch(setProgramViewsLastFetched());
            dispatch(setProgramViews(response.programViews));
        });
    };
};

var getProgramView = exports.getProgramView = function getProgramView(programViewId) {
    return function (dispatch) {
        return api.getProgramView(programViewId).then(function (response) {
            return dispatch(setProgramViews([response]));
        });
    };
};
//# sourceMappingURL=actions.js.map
