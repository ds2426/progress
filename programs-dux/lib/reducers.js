'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.setProgramViewsLastFetched = exports.setProgramViews = exports.ROOT_KEY = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.default = programs;

var _reduce = require('lodash/reduce');

var _reduce2 = _interopRequireDefault(_reduce);

var _types = require('./types');

var types = _interopRequireWildcard(_types);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var ROOT_KEY = exports.ROOT_KEY = 'programs';

var DEFAULT_STATE = {
    views: {},
    viewsLastFetched: null
};

var setProgramViews = exports.setProgramViews = function setProgramViews(state) {
    var programViews = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
    return _extends({}, state, {
        views: (0, _reduce2.default)(programViews, function (result) {
            var program = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
            var programViewId = program.programViewId;

            if (!programViewId) {
                return result;
            }
            return _extends({}, result, _defineProperty({}, programViewId, _extends({}, result[programViewId], {
                program: _extends({}, program)
            })));
        }, state.views)
    });
};

var setProgramViewsLastFetched = exports.setProgramViewsLastFetched = function setProgramViewsLastFetched(state) {
    return _extends({}, state, {
        viewsLastFetched: Date.now()
    });
};

function programs() {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : DEFAULT_STATE;
    var action = arguments[1];

    switch (action.type) {
        case types.USER_SET_ELIGIBLE_PROGRAMS:
        case types.PROGRAMS_SET_PROGRAM_VIEWS:
            return setProgramViews(state, action.programs);
        case types.PROGRAM_VIEWS_SET_LAST_FETCHED:
            return setProgramViewsLastFetched(state);
    }
    return state;
}
//# sourceMappingURL=reducers.js.map
