'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getProgramViews = exports.getProgram = exports.getProgramSelector = exports.EMPTY_OBJ = undefined;
exports.getProgramViewsLastFetched = getProgramViewsLastFetched;

var _get = require('lodash/get');

var _get2 = _interopRequireDefault(_get);

var _memoize = require('lodash/memoize');

var _memoize2 = _interopRequireDefault(_memoize);

var _reselect = require('reselect');

var _reducers = require('./reducers');

var _utils = require('./utils');

var _programModel = require('./program-model');

var _programModel2 = _interopRequireDefault(_programModel);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var EMPTY_OBJ = exports.EMPTY_OBJ = {};
var _getViews = function _getViews(state) {
    return (0, _get2.default)(state, _reducers.ROOT_KEY + '.views') || EMPTY_OBJ;
};

var memoizedModelGetter = (0, _memoize2.default)(function (programViewId) {
    return (0, _reselect.createSelector)(function (program) {
        return program;
    }, function (program) {
        return new _programModel2.default(program);
    });
});
var getModelForProgram = function getModelForProgram(program) {
    return memoizedModelGetter(program.programViewId)(program);
};

var getProgramSelector = exports.getProgramSelector = (0, _memoize2.default)(function (programViewId) {
    return (0, _reselect.createSelector)(_getViews, function (views) {
        var program = (0, _get2.default)(views, programViewId + '.program');
        return program ? getModelForProgram(program) : null;
    });
});

var getProgram = exports.getProgram = function getProgram(state, programViewId) {
    return getProgramSelector(programViewId)(state);
};

function getProgramViewsLastFetched(state) {
    return (0, _get2.default)(state, _reducers.ROOT_KEY + '.viewsLastFetched', null);
}

var getProgramViews = exports.getProgramViews = (0, _reselect.createSelector)(_getViews, function (views) {
    return Object.keys(views).map(function (programViewId) {
        return getModelForProgram(views[programViewId].program);
    }).sort(_utils.programViewSort);
});
//# sourceMappingURL=selectors.js.map
