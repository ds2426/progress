import { clientFetch } from 'biw-client-fetch';
import * as types from './types';

export function getProgramViews() {
    return clientFetch( {
        url: '/api/v1.0/program-views',
        type: types.FETCHING_PROGRAM_VIEWS
    } );
}

export function getProgramView( programViewId ) {
    return clientFetch( {
        url: `/api/v1.0/program-views/${programViewId}`,
        type: types.fetchingProgramView( programViewId )
    } );
}
