import ProgramModel from '../program-model';
import addDays from 'date-fns/add_days';
import { constants } from 'contests-utils';

describe( 'ProgramModel', () => {
    describe( 'isObjectives ::', () => {
        let model;
        beforeEach( () => {
            model = new ProgramModel();
        } );

        it( 'should be true if the programType is the objective type', () => {
            model.__state.programType = constants.PT__OBJ;
            expect( model.isObjectives ).toBe( true );
        } );

        it( 'should be true if the programType is not objective type', () => {
            expect( model.isObjectives ).toBe( false );
        } );
    } );

    describe( 'isManager ::', () => {
        let model;
        beforeEach( () => {
            model = new ProgramModel();
        } );

        it( 'should be true if the isManager has a manager role', () => {
            model.__state.role = constants.ROLE__MANAGER;
            expect( model.isManager ).toBe( true );
        } );

        it( 'should be true if the isManager has a manager role', () => {
            model.__state.role = constants.ROLE__MULTIMANAGER;
            expect( model.isManager ).toBe( true );
        } );

        it( 'should be true if the isManager has a manager role', () => {
            model.__state.role = constants.ROLE__SUPERVIEWER;
            expect( model.isManager ).toBe( true );
        } );

        it( 'should be false if the isManager does not have a manager role', () => {
            model.__state.role = constants.ROLE__SELECTOR;
            expect( model.isManager ).toBe( false );
        } );
    } );

    describe( 'isGoalSelection ::', () => {
        let model;
        beforeEach( () => {
            model = new ProgramModel();
        } );

        it( 'should be true if the programViewStatus is in goal selection', () => {
            model.__state.programViewStatus = constants.PVS__GOAL_SELECTION;
            expect( model.isGoalSelection ).toBe( true );
        } );

        it( 'should be true if the programViewStatus is not in goal selection', () => {
            expect( model.isGoalSelection ).toBe( false );
        } );
    } );

    describe( 'programHasEnded', () => {
        let model;
        beforeEach( () => {
            model = new ProgramModel();
        } );

        it( 'should not barf if there is no endDate', () => {
            expect( () => model.programHasEnded ).not.toThrow();
        } );

        it( 'should return true if the `endDate` is less than now', () => {
            model.__state.endDate = addDays( Date.now(), -1 ).toISOString();
            expect( model.programHasEnded ).toBe( true );
        } );

        it( 'should return false if the `endDate` is greater than now', () => {
            model.__state.endDate = addDays( Date.now(), 1 ).toISOString();
            expect( model.programHasEnded ).toBe( false );
        } );
    } );

    describe( 'programHasStarted', () => {
        let model;
        beforeEach( () => {
            model = new ProgramModel();
        } );

        it( 'should not barf if there is no startDate', () => {
            expect( () => model.programHasStarted ).not.toThrow();
        } );

        it( 'should return true if the `startDate` is less than now', () => {
            model.__state.startDate = addDays( Date.now(), -1 ).toISOString();
            expect( model.programHasStarted ).toBe( true );
        } );

        it( 'should return false if the `startDate` is greater than now', () => {
            model.__state.startDate = addDays( Date.now(), 1 ).toISOString();
            expect( model.programHasStarted ).toBe( false );
        } );
    } );

    describe( 'isCheckingResults', () => {
        let model;
        beforeEach( () => {
            model = new ProgramModel();
        } );

        it( 'should not barf if there is no data', () => {
            expect( () => model.isCheckingResults ).not.toThrow();
        } );

        it( 'should return true if `programHasEnded` is true and the program has progress loaded but no issuance', () => {
            model.programHasEnded = true;
            model.__state.programViewStatus = constants.PVS__PROGRESS_LOADED_NO_ISSUANCE;
            expect( model.isCheckingResults ).toBe( true );
        } );

        it( 'should return false if `programHasEnded` is false and the program has progress loaded but no issuance', () => {
            model.programHasEnded = false;
            model.__state.programViewStatus = constants.PVS__PROGRESS_LOADED_NO_ISSUANCE;
            expect( model.isCheckingResults ).toBe( false );
        } );

        it( 'should return false if `programHasEnded` is true and the program is not progress loaded but no issuance', () => {
            model.programHasEnded = false;
            expect( model.isCheckingResults ).toBe( false );
        } );
    } );

    describe( 'isWaitingForProgress', () => {
        let model;
        beforeEach( () => {
            model = new ProgramModel();
        } );

        it( 'should return true if the program is in no progress but the program has started', () => {
            model.programHasStarted = true;
            model.__state.programViewStatus = constants.PVS__NO_PROGRESS;
            expect( model.isWaitingForProgress ).toBe( true );
        } );

        it( 'should return false if the program is in no progress but the program has not started', () => {
            model.programHasStarted = false;
            model.__state.programViewStatus = constants.PVS__NO_PROGRESS;
            expect( model.isWaitingForProgress ).toBe( false );
        } );

        it( 'should return false if the program is not in no progress but the program has started', () => {
            model.programHasStarted = true;
            expect( model.isWaitingForProgress ).toBe( false );
        } );
    } );

    describe( 'daysToGoalSelection', () => {
        let model;
        beforeEach( () => {
            model = new ProgramModel();
        } );

        it( 'should not barf if there is no data', () => {
            expect( () => model.daysToGoalSelection ).not.toThrow();
        } );

        it( 'should return 0 if there is no goalSelectionToDate', () => {
            expect( model.daysToGoalSelection ).toBe( 0 );
        } );

        it( 'should return 0 if the goalSelectionToDate was yesterday', () => {
            model.__state.goalSelectionToDate = addDays( Date.now(), -1 ).toISOString();
            expect( model.daysToGoalSelection ).toBe( 0 );
        } );

        it( 'should return 1 if the goalSelectionToDate is today', () => {
            model.__state.goalSelectionToDate = Date.now();
            expect( model.daysToGoalSelection ).toBe( 1 );
        } );

        it( 'should return 2 if the goalSelectionToDate is tomorrow', () => {
            model.__state.goalSelectionToDate = addDays( Date.now(), 1 ).toISOString();
            expect( model.daysToGoalSelection ).toBe( 2 );
        } );
    } );

    describe( 'daysToProgramStartDate', () => {
        let model;
        beforeEach( () => {
            model = new ProgramModel();
        } );

        it( 'should not barf if there is no data', () => {
            expect( () => model.daysToProgramStartDate ).not.toThrow();
        } );

        it( 'should return 0 if there is no startDate', () => {
            expect( model.daysToProgramStartDate ).toBe( 0 );
        } );

        it( 'should return 0 if the startDate was yesterday', () => {
            model.__state.startDate = addDays( Date.now(), -1 ).toISOString();
            expect( model.daysToProgramStartDate ).toBe( 0 );
        } );

        it( 'should return 1 if the startDate is today', () => {
            model.__state.startDate = Date.now();
            expect( model.daysToProgramStartDate ).toBe( 1 );
        } );

        it( 'should return 2 if the startDate is tomorrow', () => {
            model.__state.startDate = addDays( Date.now(), 1 ).toISOString();
            expect( model.daysToProgramStartDate ).toBe( 2 );
        } );
    } );

    describe( 'daysToProgramEndDate', () => {
        let model;
        beforeEach( () => {
            model = new ProgramModel();
        } );

        it( 'should not barf if there is no data', () => {
            expect( () => model.daysToProgramEndDate ).not.toThrow();
        } );

        it( 'should return 0 if there is no endDate', () => {
            expect( model.daysToProgramEndDate ).toBe( 0 );
        } );

        it( 'should return 0 if the endDate was yesterday', () => {
            model.__state.endDate = addDays( Date.now(), -1 ).toISOString();
            expect( model.daysToProgramEndDate ).toBe( 0 );
        } );

        it( 'should return 1 if the endDate is today', () => {
            model.__state.endDate = Date.now();
            expect( model.daysToProgramEndDate ).toBe( 1 );
        } );

        it( 'should return 2 if the endDate is tomorrow', () => {
            model.__state.endDate = addDays( Date.now(), 1 ).toISOString();
            expect( model.daysToProgramEndDate ).toBe( 2 );
        } );
    } );

} );
