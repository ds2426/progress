jest.mock( 'biw-client-fetch', () => ( { clientFetch: jest.fn() } ) );
import { clientFetch } from 'biw-client-fetch';
import { createTestStore } from '@biw/test-utils';
import programsReducer, { ROOT_KEY } from '../reducers';
import * as actions from '../actions';
import * as selectors from '../selectors';
import * as types from '../types';
import ProgramModel from '../program-model';

function buildStore( defaultState = {} ) {
    return createTestStore( {
        data: ( state = {} ) => state,
        [ ROOT_KEY ]: programsReducer
    }, {
        data: {
            fetched: {},
            fetching: {},
            fetchingErrors: []
        },
        ...defaultState,
    } );
}

describe( 'contests-programs-dux', () => {
    describe( 'fetching plan views', () => {
        let store;
        let response;
        let program;
        beforeEach( () => {
            store = buildStore();
            program = {
                programViewId: '123---owner',
                programId: 123,
                role: 'owner'
            };
            response = {
                programViews: [ program ]
            };
            clientFetch.mockReturnValueOnce( Promise.resolve( response ) );
        } );

        afterEach( () => {
            clientFetch.mockReset();
        } );

        it( 'should put a list of programs in the store', async () => {
            await store.dispatch( actions.getProgramViews() );
            expect( selectors.getProgram( store.getState(), program.programViewId ) ).not.toBeNull();
        } );

        it( 'should add a new program to an existing list', async () => {
            const newProgram = { programId: 333, role: 'owner', programViewId: '333---owner' };
            store.dispatch( actions.setProgramViews( [ newProgram ] ) );

            await store.dispatch( actions.getProgramViews() );
            expect( selectors.getProgram( store.getState(), newProgram.programViewId ) ).not.toBeNull();

            const programId = program.programViewId;
            expect( selectors.getProgram( store.getState(), programId ) ).not.toBeNull();
        } );

        it( 'should not barf if the backend does not return appropriate data', () => {
            response.programViews = [ { wut: 'wut' } ];
            expect( selectors.getProgram( store.getState(), program.programViewId ) ).toBeNull();
        } );

        it( 'should not barf if the backend has been extra bad', () => {
            response.programViews = undefined;
            expect( selectors.getProgram( store.getState(), program.programViewId ) ).toBeNull();
        } );

        it( 'should set the fetched time after fetching', async () => {
            expect( selectors.getProgramViewsLastFetched( store.getState() ) ).toBeNull();
            await store.dispatch( actions.getProgramViews() );
            expect( selectors.getProgramViewsLastFetched( store.getState() ) ).not.toBeNull();
        } );
    } );

    describe( 'fetching plan view', () => {
        let store;
        let program;
        beforeEach( () => {
            store = buildStore();
            program = {
                programViewId: '123---owner',
                programId: 123,
                role: 'owner'
            };
            clientFetch.mockReturnValueOnce( Promise.resolve( program ) );
        } );

        afterEach( () => {
            clientFetch.mockReset();
        } );

        it( 'should put a program in the store', async () => {
            await store.dispatch( actions.getProgramView( '123---owner' ) );
            expect( selectors.getProgram( store.getState(), program.programViewId ) ).not.toBeNull();
        } );

        it( 'should add a new program to an existing list', async () => {
            const newProgram = { programId: 333, role: 'owner', programViewId: '333---owner' };
            store.dispatch( actions.setProgramViews( [ newProgram ] ) );

            await store.dispatch( actions.getProgramView( '123---owner' ) );
            expect( selectors.getProgram( store.getState(), newProgram.programViewId ) ).not.toBeNull();

            const programId = program.programViewId;
            expect( selectors.getProgram( store.getState(), programId ) ).not.toBeNull();
        } );
    } );

    describe( 'setting active programs', () => {
        let store;
        let program;
        beforeEach( () => {
            store = buildStore();
            program = {
                programViewId: '123---owner',
                programId: 123,
                role: 'owner'
            };
        } );

        it( `should respond to the ${types.USER_SET_ELIGIBLE_PROGRAMS} action`, () => {
            store.dispatch( { type: types.USER_SET_ELIGIBLE_PROGRAMS, programs: [ program ] } );
            expect( selectors.getProgram( store.getState(), program.programViewId ) ).not.toBeNull();
        } );

        it( 'should not barf if given bad data', () => {
            store.dispatch( actions.setProgramViews() );
            expect( selectors.getProgram( store.getState(), program.programViewId ) ).toBeNull();
        } );

        it( 'should not modify state if there is no programViewId', () => {
            const state = store.getState();
            store.dispatch( { type: types.USER_SET_ELIGIBLE_PROGRAMS, programs: [ {} ] } );
            expect( store.getState().programs.views ).toBe( state.programs.views );
        } );
    } );

    describe( 'getProgramViews', () => {
        let store;
        let program1;
        let program2;
        beforeEach( () => {
            store = buildStore();
            program1 = {
                programViewId: '123---owner',
                programId: 123,
                role: 'owner',
                goalSelectionToDate: '2017-02-01T23:59:59-0600'
            };
            program2 = {
                programViewId: '124---owner',
                programId: 124,
                role: 'owner',
                goalSelectionToDate: '2017-01-15T23:59:59-0600'
            };
            store.dispatch( { type: types.USER_SET_ELIGIBLE_PROGRAMS, programs: [ program2, program1 ] } );
        } );

        it( 'should return instances of a program model', () => {
            const state = store.getState();
            const views = selectors.getProgramViews( state );
            views.forEach( view => expect( view ).toBeInstanceOf( ProgramModel ) );
        } );

        it( 'should return an array containing the programs', () => {
            const state = store.getState();
            expect( selectors.getProgramViews( state ) ).toEqual( expect.arrayContaining( [
                expect.objectContaining( { ...program1 } ),
                expect.objectContaining( { ...program2 } )
            ] ) );
        } );

        it( 'should cache the response', () => {
            const state = store.getState();
            const result = selectors.getProgramViews( state );
            expect( selectors.getProgramViews( state ) ).toBe( result );
        } );

        it( 'should break the cache if more programViews are added', () => {
            const result = selectors.getProgramViews( store.getState() );
            store.dispatch( { type: types.USER_SET_ELIGIBLE_PROGRAMS, programs: [ { programViewId: 'something' } ] } );
            expect( selectors.getProgramViews( store.getState() ) ).not.toBe( result );
        } );

        it( 'should return a brand new instances of ProgramModels when the cache is broken', () => {
            const result = selectors.getProgramViews( store.getState() );
            const model = result.find( v => v.programViewId === '123---owner' );
            store.dispatch( { type: types.USER_SET_ELIGIBLE_PROGRAMS, programs: [ { programViewId: '123---owner' } ] } );
            const newResult = selectors.getProgramViews( store.getState() );
            const newModel = newResult.find( v => v.programViewId === '123---owner' );
            expect( model ).not.toBe( newModel );
        } );
    } );

    describe( 'getProgramViews', () => {
        let store;
        let program;
        const programViewId = '123---owner';
        beforeEach( () => {
            store = buildStore();
            program = {
                programViewId,
                programId: 123,
                role: 'owner',
                goalSelectionToDate: '2017-02-01T23:59:59-0600'
            };
            store.dispatch( { type: types.USER_SET_ELIGIBLE_PROGRAMS, programs: [ program ] } );
        } );

        it( 'should return an instance of a ProgramModel', () => {
            const result = selectors.getProgram( store.getState(), programViewId );
            expect( result ).toBeInstanceOf( ProgramModel );
        } );

        it( 'should return the same instance of a ProgramModel on additional calls', () => {
            const result = selectors.getProgram( store.getState(), programViewId );
            const result2 = selectors.getProgram( store.getState(), programViewId );

            expect( result ).toBe( result2 );
        } );

        it( 'should return a new instance of a ProgramModel on broken cache', () => {
            const result = selectors.getProgram( store.getState(), programViewId );

            store.dispatch( { type: types.USER_SET_ELIGIBLE_PROGRAMS, programs: [ { programViewId } ] } );
            const result2 = selectors.getProgram( store.getState(), programViewId );

            expect( result ).not.toBe( result2 );
        } );
    } );
} );
