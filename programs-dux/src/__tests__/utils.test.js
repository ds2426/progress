import { programViewSort, STATUS_ORDER } from '../utils';

describe( 'programView sort: ', () => {
    it( 'should sort by view status', () => {
        const program1 = { programViewStats: STATUS_ORDER[ 0 ] };
        const program2 = { programViewStats: STATUS_ORDER[ 1 ] };

        const list = [ program2, program1 ];
        const newList = list.sort( programViewSort );

        expect( newList[ 0 ] ).toBe( program1 );
        expect( newList[ 1 ] ).toBe( program2 );
    } );

    it( 'should perform secondary sort by the goalSelectionToDate', () => {
        const program1 = { goalSelectionToDate: '2017-02-01T23:59:59-0600' };
        const program2 = { goalSelectionToDate: '2017-01-15T23:59:59-0600' };

        const list = [ program2, program1 ];
        const newList = list.sort( programViewSort );

        expect( newList[ 0 ] ).toBe( program1 );
        expect( newList[ 1 ] ).toBe( program2 );
    } );
} );
