/* istanbul ignore file */
import * as actions from './actions';
import * as selectors from './selectors';
import * as types from './types';
import ProgramModel from './program-model';

import rootReducer, { ROOT_KEY } from './reducers';

export default rootReducer;
export { actions, selectors, types, ROOT_KEY, ProgramModel };

