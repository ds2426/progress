import isAfter from 'date-fns/is_after';
import parseDate from 'date-fns/parse';
import differenceInDays from 'date-fns/difference_in_days';
// actionCreators
import { constants } from 'contests-utils';
const {
    PVS__PROGRESS_LOADED_NO_ISSUANCE,
    PVS__GOAL_SELECTION,
    ROLE__SELECTOR,
    PVS__NO_PROGRESS,
    PT__OBJ
} = constants;

const selectorRoleRegex = new RegExp( `^${ROLE__SELECTOR}$`, 'i' );

const daysToDate = date => date ? differenceInDays( parseDate ( date ),  new Date() ) + 1 : 0;

export default class ProgramModel {
    constructor( program ) {
        Object.assign( this, program || {} );
        this.__state = program || {};
        const CACHE = {};
        const proto = this.constructor.prototype;
        Object.getOwnPropertyNames( proto ).forEach( ( methodName ) => {
            if ( methodName !== 'constructor' ) {
                Object.defineProperty( this, methodName, {
                    get: () => methodName in CACHE ?
                        CACHE[ methodName ] :
                        CACHE[ methodName ] = proto[ methodName ].call( this ),
                    set: val => CACHE[ methodName ] = val
                } );
            }
        } );
    }

    isObjectives() {
        return this.__state.programType === PT__OBJ;
    }

    isManager() {
        return !selectorRoleRegex.test( this.__state.role );
    }

    isGoalSelection() {
        return this.__state.programViewStatus === PVS__GOAL_SELECTION;
    }

    programHasEnded() {
        return isAfter( new Date(), parseDate( this.__state.endDate || Date.now() ) );
    }

    programHasStarted() {
        return isAfter( new Date(), parseDate( this.__state.startDate || Date.now() ) );
    }

    isCheckingResults() {
        return this.__state.programViewStatus === PVS__PROGRESS_LOADED_NO_ISSUANCE && this.programHasEnded;
    }

    isWaitingForProgress() {
        return this.__state.programViewStatus === PVS__NO_PROGRESS && this.programHasStarted;
    }

    daysToGoalSelection() {
        return daysToDate( this.__state.goalSelectionToDate );
    }

    daysToProgramStartDate() {
        return daysToDate( this.__state.startDate );
    }

    daysToProgramEndDate() {
        return daysToDate( this.__state.endDate );
    }
}
