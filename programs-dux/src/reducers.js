import reduce from 'lodash/reduce';
import * as types from './types';

export const ROOT_KEY = 'programs';

const DEFAULT_STATE = {
    views: {},
    viewsLastFetched: null
};

export const setProgramViews = ( state, programViews = [] ) => ( {
    ...state,
    views: reduce( programViews, ( result, program = {} ) => {
        const { programViewId } = program;
        if ( !programViewId ) {
            return result;
        }
        return {
            ...result,
            [ programViewId ]: {
                ...result[ programViewId ],
                program: { ...program }
            }
        };
    }, state.views )
} );

export const setProgramViewsLastFetched = ( state ) => ( {
    ...state,
    viewsLastFetched: Date.now()
} );

export default function programs( state = DEFAULT_STATE, action ) {
    switch ( action.type ) {
        case types.USER_SET_ELIGIBLE_PROGRAMS:
        case types.PROGRAMS_SET_PROGRAM_VIEWS:
            return setProgramViews( state, action.programs );
        case types.PROGRAM_VIEWS_SET_LAST_FETCHED:
            return setProgramViewsLastFetched( state );
    }
    return state;
}
