import { register, _Date } from 'timezone-mock';

// doing some crazy monkeypatching because the timezone-mock doesn't handle `new Date()` correctly
register( 'UTC' );
const MockDate = Date;
const TIME_TEST = /T.*$/i;

const convertToISO = t => new _Date( t ).toISOString();

Date = function TestEnvDate( timestamp, ...args ) { //eslint-disable-line no-global-assign
    if ( timestamp === undefined ) {
        timestamp = _Date.now();
    }

    if ( !args.length ) {
        if ( typeof timestamp === 'number' || !isNaN( Number( timestamp ) ) ) {
            timestamp = convertToISO( timestamp );
        }
    }

    if ( typeof timestamp === 'string' && TIME_TEST.test( timestamp ) ) {
        timestamp = timestamp.replace( TIME_TEST, 'T00:00:00' ); // replacing the hour to fix some time sensitive weirdness
    }

    return MockDate.prototype.constructor.call( this, timestamp, ...args );
};
Date.prototype = MockDate.prototype;
Date.now = _Date.now;
Date.UTC = _Date.UTC;
