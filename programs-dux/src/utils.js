import parseDate from 'date-fns/parse';

export const STATUS_ORDER = [
    'goal_selection',
    'goal_selected',
    'no_goal_selected',
    'no_progress',
    'progress_loaded_no_issuance',
    'final_issuance'
];

export function programViewSort( a, b ) {
    if ( a.programViewStatus !== b.programViewStatus ) {
        return STATUS_ORDER.indexOf( a.programViewStatus ) - STATUS_ORDER.indexOf( b.programViewStatus );
    }
    return parseDate( a.goalSelectionToDate ) > parseDate( b.goalSelectionToDate ) ? -1 : 1;
}
