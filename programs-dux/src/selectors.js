import get from 'lodash/get';
import memoize from 'lodash/memoize';
import { createSelector } from 'reselect';
import { ROOT_KEY } from './reducers';
import { programViewSort } from './utils';
import ProgramModel from './program-model';

export const EMPTY_OBJ = {};
const _getViews = state => get( state, `${ROOT_KEY}.views` ) || EMPTY_OBJ;

const memoizedModelGetter = memoize( programViewId => createSelector(
    program => program,
    program => new ProgramModel( program )
) );
const getModelForProgram = program => memoizedModelGetter( program.programViewId )( program );

export const getProgramSelector = memoize( programViewId => createSelector(
    _getViews,
    ( views ) => {
        const program = get( views, `${programViewId}.program` );
        return program ? getModelForProgram( program ) : null;
    }
) );

export const getProgram = ( state, programViewId ) => {
    return getProgramSelector( programViewId )( state );
};

export function getProgramViewsLastFetched( state ) {
    return get( state, `${ROOT_KEY}.viewsLastFetched`, null );
}

export const getProgramViews = createSelector( _getViews, ( views ) => {
    return Object.keys( views )
        .map( programViewId => getModelForProgram( views[ programViewId ].program ) )
        .sort( programViewSort );
} );
