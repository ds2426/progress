import * as types from './types';
import * as api from './api';

export const setProgramViews = programs => ( {
    type: types.PROGRAMS_SET_PROGRAM_VIEWS,
    programs
} );

export const setProgramViewsLastFetched = () => ( {
    type: types.PROGRAM_VIEWS_SET_LAST_FETCHED
} );

export const getProgramViews = () => dispatch => {
    return api.getProgramViews()
        .then( ( response ) => {
            dispatch( setProgramViewsLastFetched() );
            dispatch( setProgramViews( response.programViews ) );
        } );
};

export const getProgramView = programViewId => dispatch => {
    return api.getProgramView( programViewId )
        .then( response => dispatch( setProgramViews( [ response ] ) ) );
};
