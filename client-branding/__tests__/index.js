import AppStore from 'app/store';
import { mapStateToProps } from '../index';
import { selectors as clientSelectors } from '@biw/client-dux';


describe( 'Client Branding mapStateToProps', () => {
    let store;
    let ownProps;
    let clientBranding = {
        primaryColor: 'red',
        secondaryColor: 'blue'
    };
    beforeEach( () => {
        store = AppStore();
        clientBranding = {
            primaryColor: 'red',
            secondaryColor: 'blue'
        };
    } );

    it( 'should work without errors', () => {
        expect( mapStateToProps( store ) ).toMatchSnapshot();
    } );
    it( 'should return client branding colors from the clientBranding object ', () => {
            spyOn( clientSelectors, 'getClientBranding' ).and.returnValue( clientBranding );
            const result = mapStateToProps( AppStore().getState() );
            expect ( result.primaryColor ).toEqual( 'red' );
            expect ( result.secondaryColor ).toEqual( 'blue' );
    } );

    it( 'should return client branding default colors if they are null ', () => {
        const clientBranding = {
            primaryColor: null,
            secondaryColor: null
        };
        spyOn( clientSelectors, 'getClientBranding' ).and.returnValue( clientBranding );
        const result = mapStateToProps( AppStore().getState() );
        expect ( result.primaryColor ).toEqual( '#44c9fb' );
        expect ( result.secondaryColor ).toEqual( '#0c7eac' );
    } );

    it( 'should return client branding default colors if program object is null', () => {
        const clientBranding = { };
        spyOn( clientSelectors, 'getClientBranding' ).and.returnValue( clientBranding );
        const result = mapStateToProps( AppStore().getState() );
        expect ( result.primaryColor ).toEqual( '#44c9fb' );
        expect ( result.secondaryColor ).toEqual( '#0c7eac' );
    } );

    it( 'should use ownProps if they are passed in', () => {
        ownProps = {
            scopingSelector: '.login-active-session'
        };
        const result = mapStateToProps( AppStore().getState(), ownProps );
        expect ( result.scopingSelector ).toEqual( '.login-active-session' );
    } );


} );
