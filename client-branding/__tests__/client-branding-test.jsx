import React from 'react';
import renderer from 'react-test-renderer';
import ClientBranding from '../client-branding';

describe( 'ClientBranding component', () => {
    let defaultProps;
    beforeEach( () => {
        defaultProps = {
            primaryColor: '#000000',
            secondaryColor: '#888888',
            scopingSelector: '#client-branding-wrapper'
        };
    } );

    it( 'renders the Client Branding component with no errors', () => {
        const component = renderer.create( <ClientBranding { ...defaultProps }  /> );
        const tree = component.toJSON();
        expect( tree ).toMatchSnapshot();
    } );


} );
