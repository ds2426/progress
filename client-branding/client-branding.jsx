
// libs
import React from 'react';
import PropTypes from 'prop-types';
import tinycolor from 'tinycolor2';

import { Style } from 'react-style-tag';


export default class ClientBranding extends React.Component {
    static propTypes = {
        primaryColor: PropTypes.string,
        secondaryColor: PropTypes.string,
        scopingSelector: PropTypes.string
    };
    getColor = ( color ) => tinycolor( color )
    getLuminance = ( color ) => color.getLuminance();
    getHoverColor = ( color ) => color.darken( 10 ).toString();
    getSecondaryHoverColor = ( color ) => color.lighten( 20 ).toString();

    render() {
        const {
           primaryColor,
           secondaryColor,
           scopingSelector
        } = this.props;

        const primaryTextColor = this.getLuminance( this.getColor( primaryColor ) ) > .7 ? '#353a40' : '#FFFFFF';
        const primaryButtonHover = this.getHoverColor( this.getColor( primaryColor ) );
        const secondaryColorHover = this.getSecondaryHoverColor( this.getColor( secondaryColor ) );

        return (
           <Style>{`
                #app-root ${scopingSelector} {
                    .privacy {
                        a {
                            color: ${secondaryColor};
                        }
                    }
                    .client-svg {
                        fill: ${primaryColor};
                    }
                    .client-primary-text {
                        color: ${primaryColor};
                    }
                    .client-primary-bg-color {
                        background-color: ${primaryColor};
                        color: ${primaryTextColor};
                    }
                    .client-secondary-text {
                        color: ${secondaryColor};
                    }
                    .client-link {
                        color: ${secondaryColor};
                        &:hover {
                            color: ${secondaryColorHover};
                            text-decoration: none;
                        }
                    }
                    .client-primary-hover {
                        svg:hover {
                            fill: ${primaryColor};
                        }
                    }
                    .client-primary-button {
                        background: ${primaryColor};
                        color: ${primaryTextColor};
                        span {
                            color: ${primaryTextColor};
                        }

                        &.gq-btn {
                            background-color: ${primaryColor};
                            color: ${primaryTextColor};
                        }

                        &:hover {
                            background: ${primaryButtonHover};
                        }

                        &.active {
                            background: ${primaryColor};
                            span {
                                color: ${primaryTextColor};
                            }
                        }

                        &.gq-btn.disabled {
                            background-color: #E8E8E8;
                            color: #999999;
                        }

                        a:hover {
                            background: ${primaryButtonHover};
                        }

                    }

                }
            `}
            </Style>
        );
    }
}


