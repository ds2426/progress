// libs
import { connect } from 'react-redux';
// selectors
import { selectors as clientSelectors } from '@biw/client-dux';
// components
import ClientBranding from './client-branding';

export const mapStateToProps = ( state, ownProps ) => {
    return {
        primaryColor: clientSelectors.getClientBranding( state ).primaryColor || '#44c9fb',
        secondaryColor: clientSelectors.getClientBranding( state ).secondaryColor || '#0c7eac',
        scopingSelector: ownProps && ownProps.scopingSelector || ''
    };
};

export default connect( mapStateToProps )( ClientBranding );
