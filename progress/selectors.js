import { createSelector } from 'reselect';
import { getStringFromBundle } from '@biw/intl';
import { selectors as progressSelectors } from 'contests-progress-dux';
import { selectors as clientSelectors } from '@biw/client-dux';
import { getActiveProgram } from 'app/router/selectors';

const EMPTY_OBJ = {};

export const getGetString = createSelector(
    clientSelectors.getUserBundle,
    bundle => ( code, opts ) => getStringFromBundle( bundle, code, opts )
);

export const getContestUtilsMessageProps = state => {
    const program  = getActiveProgram( state ) || {};
    const programViewId = program.programViewId;
    const getString = getGetString( state );
    const locale = clientSelectors.getUserLocale( state );
    const goal = progressSelectors.getProgramViewGoal( state, programViewId ) || EMPTY_OBJ;
    const paxGoalSelection = goal.paxGoalSelection || EMPTY_OBJ;
    const mediaLabel = clientSelectors.getMediaLabel( state );
    const finalResult = progressSelectors.getProgramViewFinalResults( state, programViewId ) || EMPTY_OBJ;


    return {
        getString,
        locale,
        mediaLabel,

        goalPayoutTierType: goal.goalPayoutTierType,
        goalLevels: goal.goalLevels,
        goalLabel: goal.goalLabel,
        baseLevel: goal.baseLevel,
        bonusPayout: goal.bonusPayout,

        incrementQuantity: paxGoalSelection.incrementQuantity,
        goalAchieveAmount: paxGoalSelection.goalAchieveAmount,
        incrementAward: paxGoalSelection.incrementAward,
        maxPoints: paxGoalSelection.maxPoints,

        goalRoundingMethod: program.goalRoundingMethod,
        programType: program.programType,
        bonusPrecision: program.bonusPrecision,
        bonusRoundingMethod: program.bonusRoundingMethod,

        bonusPoints: finalResult.totalBonusPoints,

    };
};
