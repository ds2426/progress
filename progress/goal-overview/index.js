import { connect }  from 'react-redux';
import { getActiveProgram } from 'app/router/selectors';
import { selectors as clientSelectors } from '@biw/client-dux';
import { selectors as progressSelectors } from 'contests-progress-dux';
import { routeTo } from 'app/router/actions';
import { GOAL_SELECTION } from 'app/router/types';
import GoalOverview from './goal-overview';
import { getBonusMessage, getBonusReplaceOptions, getChallengePointBaseEarningDescription } from 'contests-utils';
import { getContestUtilsMessageProps, getGetString } from 'routes/progress/selectors';
import { getString } from '@biw/intl';
const EMPTY_OBJ = {};

export const mapStateToProps = ( state ) => {
    const program = getActiveProgram( state ) || EMPTY_OBJ;
    const goal = progressSelectors.getProgramViewGoal( state, program.programViewId ) || EMPTY_OBJ;

    const messageProps = getContestUtilsMessageProps( state );
    const [ , cpBaseEarningDescription ] = getChallengePointBaseEarningDescription( messageProps ) || [];

    return {
        program,
        mediaLabel: clientSelectors.getMediaLabel( state ),
        goalLabel: goal.goalLabel || EMPTY_OBJ,
        paxGoalSelection: goal.paxGoalSelection || EMPTY_OBJ,
        isBaseLineRequired: goal.isBaseLineRequired,
        isBonusEnabled: goal.isBonusEnabled,
        isMinimumQualifierEnabled: goal.isMinimumQualifierEnabled,
        goalPayoutTierType: goal.goalPayoutTierType,
        bonusPayout: goal.bonusPayout || EMPTY_OBJ,
        baseLevel: goal.baseLevel || EMPTY_OBJ,
        baseValue: goal.baseValue || EMPTY_OBJ,
        mqLabel: goal.mqLabel || EMPTY_OBJ,
        calculationLabel: getString( state, `goal.perperiod.${goal.calculationLabel}label` ),
        getString: getGetString( state ),

        bonusMessage: getBonusMessage( messageProps ),
        bonusReplaceOptions: getBonusReplaceOptions( messageProps ),
        cpBaseEarningDescription
    };
};

export const mapDispatchToProps = {
    changeLevel: () => ( dispatch, getState ) => {
        const { programViewId } = getActiveProgram( getState() ) || {};
        return dispatch( routeTo( {
            type: GOAL_SELECTION,
            payload: { programViewId }
        } ) );
    }
};

export default connect( mapStateToProps, mapDispatchToProps )( GoalOverview );
