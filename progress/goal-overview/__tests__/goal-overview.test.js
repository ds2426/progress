import React from 'react';
import { renderWithStore, mountWithStore } from 'app-test-utils';
import * as NoProgressFixture from '../../__fixtures__/13288---Selector';
import * as GoalSelectedFixture from '../../__fixtures__/13289---Selector';
import * as FinalResultsFixture from '../../__fixtures__/13316---Selector';

import GoalOverview from '../index';


describe( 'Goal Overview', () => {
    let props;
    beforeEach( () => {
        props = {
            fixedPlusRatePayoutMessage: 'fixedPlusRatePayoutMessage'
        };
    } );

	it( 'can render an error free Goal Overview component', () => {
        const component = renderWithStore(
            <GoalOverview {...props} />,
            { fixture: NoProgressFixture }
        );
        expect( component.toJSON() ).toMatchSnapshot();
	} );

    it( 'can render progress info goal overview title', () => {
        const goalOverview = mountWithStore(
            <GoalOverview { ...props } />,
            { fixture: NoProgressFixture }
        );
        expect( goalOverview.find( '.progress-info-goal-overview-title' ) ).toHaveLength( 1 );
    } );

    it( 'can render fixed rate payout message', () => {
        const goalOverview = mountWithStore(
            <GoalOverview { ...props } />,
            { fixture: NoProgressFixture }
        );
        expect( goalOverview.find( '.progress-info-fixed-rate-payout-message-item' ) ).toHaveLength( 1 );
    } );

    it( 'can render minimum qualifier', () => {
        const goalOverview = mountWithStore(
            <GoalOverview { ...props } />,
            { fixture: GoalSelectedFixture }
        );
        expect( goalOverview.find( '.progress-info-minimum-qualifier-enabled-item' ) ).toHaveLength( 1 );
    } );

    it( 'can render bonus', () => {
        const goalOverview = mountWithStore(
            <GoalOverview { ...props } />,
            { fixture: NoProgressFixture }
        );
        expect( goalOverview.find( '.progress-info-bonus-item' ) ).toHaveLength( 1 );
    } );

    it( 'can render base earning section', () => {
        const goalOverview = mountWithStore(
            <GoalOverview { ...props } />,
            { fixture: FinalResultsFixture }
        );
        expect( goalOverview.find( '.progress-info-base-earning-item' ) ).toHaveLength( 1 );
    } );

    it( ' base earning section should have an h4 heading', () => {
        const goalOverview = mountWithStore(
            <GoalOverview { ...props } />,
            { fixture: FinalResultsFixture }
        );
        expect( goalOverview.find( '.progress-info-base-earning-item h4' ) ).toHaveLength( 1 );
    } );

    it( 'can render base line section', () => {
        const goalOverview = mountWithStore(
            <GoalOverview { ...props } />,
            { fixture: NoProgressFixture }
        );
        expect( goalOverview.find( '.progress-info-base-line-item' ) ).toHaveLength( 1 );
    } );

    it( 'can render reselect goal button', () => {
        const goalOverview = mountWithStore(
            <GoalOverview { ...props } />,
            { fixture: GoalSelectedFixture }
        );
        expect( goalOverview.find( '.change-level-button' ) ).toHaveLength( 1 );
    } );
} );
