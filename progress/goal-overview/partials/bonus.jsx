import React from 'react';
import PropTypes from 'prop-types';
// utils
import { constants } from 'contests-utils';
// shared components
import { LocaleString } from '@biw/intl';

const GoalText = ( { bonusPayout, isMinimumQualifierEnabled } ) => {
    if ( bonusPayout.isGoalNeedToAchieve === true && isMinimumQualifierEnabled ) {
        return <LocaleString code="common.bonus.goalMQAchievedText"/>;
    }
    if ( bonusPayout.isGoalNeedToAchieve === true ) {
        return <LocaleString code="common.bonus.goalAchievementText"/>;
    }
    return null;
};

GoalText.propTypes = {
    isMinimumQualifierEnabled: PropTypes.bool,
    bonusPayout: PropTypes.shape( {
        isGoalNeedToAchieve: PropTypes.bool
    } )
};

const PayoutStructure = ( {
    bonusPayout,
    bonusMessage,
    bonusReplaceOptions
} ) => {
    const isFixed = bonusPayout.payoutStructureType === constants.PT__FIXED;

    if ( isFixed ) {
        return <LocaleString code="common.bonus.fixedLabelText" replaceOptions={ bonusReplaceOptions }/>;
    }
    return <span dangerouslySetInnerHTML={ { __html: bonusMessage } } />;
};

PayoutStructure.propTypes = {
    bonusMessage: PropTypes.string,
    bonusReplaceOptions: PropTypes.shape( {} ),
    bonusPayout: PropTypes.shape( {
        payoutStructureType: PropTypes.string
    } )
};

const Bonus = ( {
    bonusMessage,
    bonusReplaceOptions,
    bonusPayout,
    isMinimumQualifierEnabled
} ) => {
    if ( !bonusPayout ) {
        return <div />;
    }

    return (
        <article className="progress-info-item progress-info-bonus-item">
            <h3><LocaleString className="branding-primary-text info-header bonus-header" code="progress.bonusHeader"/></h3>
            <h4 className="bonus-desc info-desc">
                <PayoutStructure
                    bonusPayout={bonusPayout}
                    bonusMessage={bonusMessage}
                    bonusReplaceOptions={bonusReplaceOptions}
                />&nbsp;<GoalText bonusPayout={bonusPayout} isMinimumQualifierEnabled={isMinimumQualifierEnabled} />
            </h4>
        </article>
    );
};

Bonus.propTypes = {
    bonusMessage: PropTypes.string,
    bonusReplaceOptions: PropTypes.shape( {} ),
    isMinimumQualifierEnabled: PropTypes.bool,
    bonusPayout: PropTypes.shape( {
        isGoalNeedToAchieve: PropTypes.bool,
        payoutStructureType: PropTypes.string
    } )
};

export default Bonus;
