// libs
import React from 'react';
import PropTypes from 'prop-types';
// shared components
import { LocaleString } from '@biw/intl';
import { GoalLabel } from 'shared';

const Baseline = ( {
    goalLabel,
    goalBaseCalculationValue,
    programCalculationType,
    calculationLabel,
    goalRoundingMethod,
    goalPrecision,
    goalBaseValue
} ) => (
    <article className="progress-info-item progress-info-base-line-item">
        <h3><LocaleString className="info-header baseline-header branding-primary-text" code="progress.baselineHeader"/></h3>
        <h4 className="info-desc">
            <GoalLabel
                { ...goalLabel }
                amount={ goalBaseValue }
                precision={ goalPrecision }
                rounding={ goalRoundingMethod }
                className="baseline"
                strong
            />
        </h4>
        { calculationLabel && programCalculationType !== 'average' &&
            <span>
                <GoalLabel
                    { ...goalLabel }
                    amount={ goalBaseCalculationValue }
                    precision={ goalPrecision }
                    rounding={ goalRoundingMethod }
                />/{ calculationLabel }
            </span>
        }
    </article>
);

Baseline.propTypes = {
    goalLabel: PropTypes.shape( {} ),
    goalBaseCalculationValue: PropTypes.oneOfType( [
        PropTypes.string,
        PropTypes.number
    ] ),
    programCalculationType: PropTypes.string,
    calculationLabel: PropTypes.string,
    goalRoundingMethod: PropTypes.string,
    goalPrecision: PropTypes.number,
    goalBaseValue: PropTypes.number
};

export default Baseline;
