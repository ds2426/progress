import React from 'react';
import PropTypes from 'prop-types';
// shared components
import { LocaleString } from '@biw/intl';
import { GoalLabel } from 'shared';

const MinimumQualifier = ( {
    paxGoalSelection,
    mqLabel,
    mqPrecision,
    mqRoundingMethod,
} ) => {
    if ( !paxGoalSelection ) {
        return <div />;
    }
    return (
        <div className="progress-info-item progress-info-minimum-qualifier-enabled-item">
            <h3><LocaleString className="branding-primary-text info-header mq-header" code="progress.minimumQualifierHeader"/></h3>
            <h4 className="mq-desc info-desc">
                <GoalLabel
                    { ...mqLabel }
                    amount={ paxGoalSelection.minQualifier }
                    precision={ mqPrecision }
                    rounding={ mqRoundingMethod }
                />
             </h4>
        </div>
    );
};

MinimumQualifier.propTypes = {
    paxGoalSelection: PropTypes.shape( {
        minQualifier: PropTypes.number,
    } ),
    mqLabel: PropTypes.shape( {} ),
    mqPrecision: PropTypes.number,
    mqRoundingMethod: PropTypes.string,
};

export default MinimumQualifier;
