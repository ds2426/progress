import React from 'react';
import PropTypes from 'prop-types';
// shared components
import { LocaleString } from '@biw/intl';
import { GoalLabel } from 'shared';

const Goal = ( {
    goalAchieveAmount,
    goalCalculationValue,
    calculationLabel,
    goalLabel,
    goalPrecision,
    goalRoundingMethod,
} ) => {
    const showGoalCalculationLabel =  !!( goalCalculationValue && calculationLabel );
    return (
        <article className="progress-info-item progress-info-goal-item">
            <h3><LocaleString className="branding-primary-text info-header goal-header" code="progress.goalHeader"/></h3>
            <h4 className="goal-desc info-desc">
                <GoalLabel
                    { ...goalLabel }
                    amount={ goalAchieveAmount }
                    precision={ goalPrecision }
                    rounding={ goalRoundingMethod }
                    strong
                />

            </h4>
            { showGoalCalculationLabel &&
                <h5 className="goal-per-period">
                    &#40;
                    <GoalLabel
                        { ...goalLabel }
                        amount={ goalCalculationValue }
                        precision={ goalPrecision }
                        rounding={ goalRoundingMethod }
                    />
                    &#47;
                    { calculationLabel }
                    &#41;
                </h5>
            }
        </article>
    );
};

Goal.propTypes = {
    goalAchieveAmount: PropTypes.number,
    goalCalculationValue: PropTypes.number,
    calculationLabel: PropTypes.string,
    goalLabel: PropTypes.shape( {} ),
    goalPrecision: PropTypes.number,
    goalRoundingMethod: PropTypes.string,
};

export default Goal;
