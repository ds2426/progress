// libs
import React from 'react';
import PropTypes from 'prop-types';
// shared components
import { LocaleString } from '@biw/intl';

const FixedPlusRatePayout = ( { fixedPlusRatePayoutMessage } ) => (
    <article className="progress-info-item progress-info-fixed-rate-payout-message-item">
        <h3><LocaleString className="info-header fixed-payout-header branding-primary-text" code="common.fixedRate.acceleratorHeader"/></h3>
        <h4 className="fixed-payout-desc info-desc" dangerouslySetInnerHTML={ { __html: fixedPlusRatePayoutMessage } } />
    </article>
);

FixedPlusRatePayout.propTypes = {
    fixedPlusRatePayoutMessage: PropTypes.string
};

export default FixedPlusRatePayout;
