// libs
import React from 'react';
import PropTypes from 'prop-types';
// shared components
import { LocaleString } from '@biw/intl';

const BaseEarning = ( { goalLabel, cpBaseEarningDescription } ) => (
    <article className="progress-info-item progress-info-base-earning-item">
        <LocaleString TextComponent="h1" className="info-header base-earn-header branding-primary-text" code="progress.baseEarningHeader" />
        <h4 className="base-earning info-desc">
            { goalLabel &&
                <p dangerouslySetInnerHTML={ { __html: cpBaseEarningDescription } } />
            }
        </h4>
    </article>
);

BaseEarning.propTypes = {
    cpBaseEarningDescription: PropTypes.string,
    goalLabel: PropTypes.shape( {} ),
};

export default BaseEarning;
