import React from 'react';
import PropTypes from 'prop-types';
import { FormattedNumber, LocaleString } from '@biw/intl';

const Award = ( { goalAwardAmount, mediaLabel } ) => (
    <article className="progress-info-item potential-award-amount">
        <h3><LocaleString className="potential-header branding-primary-text info-header" code="progress.potentialAwardHeader"/></h3>
        <h4 className="potential-award info-desc" data-sel="progress-goal-award-amount">
            <FormattedNumber className="potential-award" value={ goalAwardAmount || 0 } />
            <span className="potential-award">&nbsp;{ mediaLabel }</span>
        </h4>
    </article>
);

Award.propTypes = {
    goalAwardAmount: PropTypes.number,
    mediaLabel: PropTypes.string
};

export default Award;
