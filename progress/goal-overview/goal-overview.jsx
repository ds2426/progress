// libs
import React from 'react';
import PropTypes from 'prop-types';
// shared components
import { LoaderButton } from 'shared';
import { LocaleString } from '@biw/intl';
import { constants } from 'contests-utils';
// goal-overview partials
import Bonus from './partials/bonus';
import MinimumQualifier from './partials/minimum-qualifier' ;
import FixedPlusRatePayout from './partials/fixed-plus-rate-payout';
import Award from './partials/award';
import Goal from './partials/goal' ;
import BaseEarning from './partials/base-earning';
import Baseline from './partials/base-line';
import './goal-overview.scss';

const RESELECT_PVS = [ constants.PVS__GOAL_SELECTION, constants.PVS__GOAL_SELECTED ];
const GoalOverview = ( {
    program,
    paxGoalSelection,

    mediaLabel,
    changeLevel,
    goalPayoutTierType,
    isBaseLineRequired,
    fixedPlusRatePayoutMessage,
    isMinimumQualifierEnabled,
    isBonusEnabled,
    bonusPayout,
    goalLabel,
    mqLabel,
    baseLevel,
    getString,
    baseValue,
    calculationLabel,
    bonusMessage,
    bonusReplaceOptions,
    cpBaseEarningDescription
} ) => (
    <section className="progress-info text-center overview-content">
        { paxGoalSelection &&
            <article className="progress-info-item progress-info-goal-overview-title">
                <h2 data-sel="progress-level-name" className="level-txt goal-overview-title branding-primary-text">
                    { paxGoalSelection.name }
                </h2>
                <h4 data-sel="progress-level-description" className="level-description">
                    { paxGoalSelection.description }
                </h4>
            </article>
        }
        <div className="divider"></div>
        <div className="item-wrapper">
            <Goal
                goalLabel={goalLabel}
                calculationLabel={calculationLabel}

                goalAchieveAmount={paxGoalSelection.goalAchieveAmount}
                goalCalculationValue={paxGoalSelection.goalCalculationValue}

                goalPrecision={program.goalPrecision}
                goalRoundingMethod={program.goalRoundingMethod}
            />
            { isBaseLineRequired &&
                <Baseline
                    calculationLabel={calculationLabel}
                    goalLabel={goalLabel}

                    goalBaseCalculationValue={baseValue.goalBaseCalculationValue}
                    goalBaseValue={baseValue.goalBaseValue}

                    programCalculationType={program.programCalculationType}
                    goalRoundingMethod={program.goalRoundingMethod}
                    goalPrecision={program.goalPrecision}
                />
            }

            { program.programType === constants.PT__CP &&
                <BaseEarning cpBaseEarningDescription={cpBaseEarningDescription} goalLabel={goalLabel} />
            }

            { fixedPlusRatePayoutMessage &&
                <FixedPlusRatePayout fixedPlusRatePayoutMessage={fixedPlusRatePayoutMessage} />
            }

            { isMinimumQualifierEnabled &&
                <MinimumQualifier
                    paxGoalSelection={paxGoalSelection}
                    mqLabel={mqLabel}

                    mqPrecision={program.mqPrecision}
                    mqRoundingMethod={program.mqRoundingMethod}
                />
            }
        </div>
        <div className="divider"></div>
        { isBonusEnabled &&
            <div>
                <Bonus
                    isMinimumQualifierEnabled={isMinimumQualifierEnabled}
                    bonusPayout={bonusPayout}
                    bonusMessage={bonusMessage}
                    bonusReplaceOptions={bonusReplaceOptions}
                />
                <div className="divider"></div>
            </div>
        }
        <Award goalAwardAmount={ paxGoalSelection.goalAwardAmount } mediaLabel={ mediaLabel } />

        { RESELECT_PVS.indexOf( program.programViewStatus ) !== -1 &&
            <LoaderButton data-sel="progress-goal-change-level" customClass="change-level-button branding-primary-button" handleClick={ changeLevel }>
                <LocaleString code="progress.reselectLabel" />
            </LoaderButton>
        }
    </section>
);

GoalOverview.propTypes = {
    bonusMessage: PropTypes.string,
    cpBaseEarningDescription: PropTypes.string,
    bonusReplaceOptions: PropTypes.shape( {} ),
    program: PropTypes.shape( {
        programType: PropTypes.string,
        programViewStatus: PropTypes.string,
        mqPrecision: PropTypes.number,
        mqRoundingMethod: PropTypes.string,
    } ),
    changeLevel: PropTypes.func,
    paxGoalSelection: PropTypes.shape( {
        name: PropTypes.string,
        description: PropTypes.string,
        goalAwardAmount: PropTypes.number,
        goalAchieveAmount: PropTypes.number,
        goalCalculationValue: PropTypes.number,
    } ),
    fixedPlusRatePayoutMessage: PropTypes.string,
    calculationLabel: PropTypes.string,
    isBaseLineRequired: PropTypes.bool,
    isBonusEnabled: PropTypes.bool,
    isMinimumQualifierEnabled: PropTypes.bool,
    goalPayoutTierType: PropTypes.string,
    mediaLabel: PropTypes.string,
    bonusPayout: PropTypes.shape( {} ),
    goalLabel: PropTypes.shape( {} ),
    mqLabel: PropTypes.shape( {} ),
    baseLevel: PropTypes.shape( {} ),
    baseValue: PropTypes.shape( {
        goalBaseCalculationValue: PropTypes.number,
        goalBaseValue: PropTypes.number
    } ),
    getString: PropTypes.func
};

GoalOverview.defaultProps = {
    fixedPlusRatePayoutMessage: '',

    isBaseLineRequired: false,
    isMinimumQualifierEnabled: false,
    isBonusEnabled: false
};

export default GoalOverview;
