// libs
import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
// shared components
import { CheckmarkIcon } from 'component-library';
import PlanSetInput from './input';

const ACTION_STEP_PLACEHOLDER = 'paxProgress.plan.actionStepPlaceholder';

/**
 * ACTION STEP COMPONENT
 */
export class ActionStep extends React.Component {

    static propTypes = {
        id: PropTypes.number,
        value: PropTypes.string,
        status: PropTypes.bool,
        getString: PropTypes.func.isRequired,
        onChange: PropTypes.func.isRequired,
        save: PropTypes.func.isRequired
    };

    static defaultProps = {
        value: ''
    };

    state = {
        isCompleted: false,
        actionText: ''
    };

    handleActionField = ( event ) => {
        this.setState( { actionText: event.target.value } );
        this.props.onChange( this.state.isCompleted, event.target.value );
    }

    handleCheckmark = () => {
        this.props.onChange( !this.state.isCompleted, this.props.value );
        this.props.save();
        this.setState( { isCompleted: !this.state.isCompleted } );
    }

    componentDidMount() {
        this.setState( {
            isCompleted: this.props.status,
            actionText: this.props.value
        } );
    }

    render() {
        const {
            getString,
            id,
            value,
            save,
            ...props
        } = this.props;

        const { isCompleted } = this.state;

        return (
            <div className="action-step">
                <PlanSetInput
                    data-sel={props[ 'data-sel' ]}
                    id={`action-step-${id}`}
                    name="actionStep"
                    onChange={this.handleActionField}
                    value={value}
                    onBlur={save}
                    disabled={isCompleted}
                    aria-label="Action Step"
                />
                {!this.state.actionText && ( <div className="plan-example"> {getString( ACTION_STEP_PLACEHOLDER )}</div> )}
                <div
                    className={classNames( 'checkmark', { 'completed': isCompleted } )}
                    onClick={this.handleCheckmark}
                    data-sel="progress-planset-done-checkbox"
                >
                    <span className="checkmark-icon branding-primary-text">
                        <CheckmarkIcon />
                    </span>
                </div>
            </div>
        );
    }
}

export default ActionStep;
