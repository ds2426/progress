import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { PlusCircleIcon } from 'component-library';

const ADD_ACTION_STEP_LABEL = 'paxProgress.plan.addActionStepLabel';

export default class AddActionStep extends React.Component {
    static propTypes = {
        disabled: PropTypes.bool,
        active: PropTypes.bool,
        onClick: PropTypes.func,
        getString: PropTypes.func.isRequired,
    };

    static defaultProps = {
        disabled: false,
        active: false,
        onClick: () => {},
    };

    handleClick = ( ev ) => {
        const { active, onClick } = this.props;
        if ( active ) {
            onClick( ev );
        }
    }

    render() {
        const { disabled, getString } = this.props;

        return (
            <span className={ classnames(
                    'add-action-step',
                    {
                        'disabled-during-save': disabled
                    }
                ) }
                onClick={ this.handleClick }
                data-sel="progress-planset-add-step"
            >
                <span className="icon-plus-circle branding-primary-text"><PlusCircleIcon/></span>
                <span className="txt branding-secondary-text">{ getString( ADD_ACTION_STEP_LABEL ) }</span>
            </span>
        );
    }
}
