import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const DELETE_LABEL = 'paxProgress.plan.delete';

export default class DeleteButton extends React.Component {
    static propTypes = {
        disabled: PropTypes.bool,
        active: PropTypes.bool,
        onClick: PropTypes.func,
        getString: PropTypes.func.isRequired,
        children: PropTypes.node
    };

    static defaultProps = {
        disabled: false,
        active: false,
        onClick: () => {}
    };

    handleClick = ( e ) => {
        const { active, onClick } = this.props;
        if ( active ) {
            onClick( e );
        }
    }

    render() {
        const {
            active,
            disabled,
            getString,
            children
        } = this.props;

        const className = classnames(
            'btn-pax-progress-plan-action btn-danger branding-primary-button',
            { 'disabled': disabled, active: active }
        );
        return (
            <div className="pax-progress-save-pill-bar">
                <span className={ className } onClick={ this.handleClick }>
                    { getString( DELETE_LABEL ) }
                </span>
                { children }
            </div>
        );
    }
}
