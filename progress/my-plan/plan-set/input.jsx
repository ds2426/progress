import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';


const isTwoRowValue = value => value && value.length > 70;

const propTypes = {
    value: PropTypes.string,
    name: PropTypes.string.isRequired
};

const defaultProps = {
    value: ''
};

export default function PlanSetInput( { name, value, ...props } ) {
    return (
        <div className="input-group">
            <textarea
                rows="1"
                className={ classnames( {
                    'gq-input my-plan-input area': true,
                    'two-row': isTwoRowValue( value ),
                    'one-row': !isTwoRowValue( value )
                } ) }
                name={ name }
                value={ value || '' }
                { ...props }
            />
        </div>
    );
}

PlanSetInput.propTypes = propTypes;
PlanSetInput.defaultProps = defaultProps;
