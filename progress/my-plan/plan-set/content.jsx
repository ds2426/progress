import React from 'react';
import PropTypes from 'prop-types';
import PlanSetInput from './input';
import { LocaleString } from '@biw/intl';

const OBSTACLE_LABEL = 'paxProgress.plan.obstacleLabel';
const OBSTACLE_PLACEHOLDER = 'paxProgress.plan.obstaclePlaceholder';
const POSITIVE_LABEL = 'paxProgress.plan.positiveLabel';
const POSITIVE_PLACEHOLDER = 'paxProgress.plan.positivePlaceholder';

const propTypes = {
    getString: PropTypes.func.isRequired,
    children: PropTypes.node,
    obstacleValue: PropTypes.string,
    positiveValue: PropTypes.string,
    onChange: PropTypes.func,
    save: PropTypes.func
};

const defaultProps = {
    obstacleValue: '',
    positiveValue: '',
    onChange: () => {},
    save: () => {}
};

export default function PlanSetContent( {
    getString,
    children,
    obstacleValue,
    positiveValue,
    onChange,
    save
} ) {
    return (
        <div className="plan-item-content-wrapper">
            <LocaleString TextComponent="label" htmlFor="positive" className="strong" code={POSITIVE_LABEL} />
            <PlanSetInput
                name="positive"
                onChange={ onChange }
                value={ positiveValue || '' }
                onBlur={save}
                data-sel="progress-planset-positive"
                aria-label="positive"
            />
            { !positiveValue && <LocaleString className="plan-example" TextComponent="div" code={POSITIVE_PLACEHOLDER} /> }
            <LocaleString TextComponent="label" htmlFor="obstacle" className="strong" code={OBSTACLE_LABEL} />
            <PlanSetInput
                name="obstacle"
                onChange={ onChange }
                value={ obstacleValue || '' }
                onBlur={save}
                data-sel="progress-planset-obstacle"
                aria-label="obstacle"
            />
            { !obstacleValue && <LocaleString className="plan-example" TextComponent="div" code={OBSTACLE_PLACEHOLDER} /> }
            <div className="action-step-list">
                { children }
            </div>
        </div>
    );
}

PlanSetContent.propTypes = propTypes;
PlanSetContent.defaultProps = defaultProps;
