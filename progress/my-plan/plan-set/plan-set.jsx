// libs
import React from 'react';
import PropTypes from 'prop-types';
import some from 'lodash/some';
// progress child components
import ActionStep from './action-step';
import PlanSetContent from './content';
import AddActionStep from './add-action-step';
import SaveButton from './save-button';
import DeleteButton from './delete-button';


const ACTION_STEP_LABEL = 'paxProgress.plan.actionStepLabel';
const ACTION_STEP_DONE_CHECKBOX_LABEL = 'paxProgress.plan.actionStepDoneCheckboxLabel';

const ErrorMessage = ( { ASErrorMessage, errorMessage } ) => (
    <span style={ { color: 'red', fontSize: '16px' } }>{`${errorMessage} ${ASErrorMessage} `}</span>
);

ErrorMessage.propTypes = {
    ASErrorMessage: PropTypes.string,
    errorMessage: PropTypes.string
};

export default class PlanSet extends React.Component {
    static propTypes = {
        addActionStep: PropTypes.func,
        savePaxPlanSets: PropTypes.func,
        updateActionStep: PropTypes.func,
        updatePlan: PropTypes.func,
        deletePlanSet: PropTypes.func,

        getString: PropTypes.func.isRequired,

        planId: PropTypes.number,
        planPositive: PropTypes.string,
        planObstacle: PropTypes.string,

        actionSteps: PropTypes.arrayOf( PropTypes.shape( {
            actionStep: PropTypes.string,
            id: PropTypes.number,
            actionStepStatus: PropTypes.bool
        } ) ),

        index: PropTypes.number.isRequired,
        isLastPlanSet: PropTypes.bool.isRequired,
        status: PropTypes.node,
        isBusySavingPlan: PropTypes.bool
    };

    static defaultProps = {
        isBusySavingPlan: false,
        actionSteps: []
    };

    state = {
        hasError: false,
        hasASError: false,
        errorMessage: '',
        ASErrorMessage: ''
    };

    // class change at 61 chars
    handleInputChange = ( event ) => {
        const value = event.target.value;
        const name = event.target.name;
        const plan = {
            obstacle: this.props.planObstacle,
            positive: this.props.planPositive
        };
        let errorMessage = '';
        let hasError = false;
        plan[ name ] = value;

        if ( plan.positive && plan.positive.length > 500 ) {
            hasError = true;
            errorMessage = errorMessage + 'Positive is too long. ';
        }

        if ( plan.obstacle && plan.obstacle.length > 500 ) {
            hasError = true;
            errorMessage = errorMessage + 'Obstacle is too long. ';

        }

        this.setState( { ...this.state, hasError: hasError, errorMessage: errorMessage } );
        this.props.updatePlan( this.props.index, plan.positive, plan.obstacle, true );
    };

    handleAddActionStep = ( ) => {
        this.props.addActionStep( this.props.planId );
    };

    handleDeletePlanSet = () => {
        this.props.deletePlanSet( this.props.planId );
    }

    handleActionStepChange = ( i ) => {
        return ( checked, value ) => {
            let errorMessage = '';
            let hasASError = false;
            if ( ( value && value.length > 500 ) ) {
                errorMessage = errorMessage + 'Action Step is too long. ';
                hasASError = true;
            }

            this.setState( { ...this.state, hasASError: hasASError, ASErrorMessage: errorMessage } );
            this.props.updateActionStep( this.props.index, i, value, checked );
        };
    };

    emptyActionSteps() {
        return this.props.actionSteps.map( step => step.actionStepStatus || step.actionStep );
    }

    isDeleteable() {
        const {
            planPositive,
            planObstacle,
            isLastPlanSet,
        } = this.props;
        if ( isLastPlanSet ) {return false;}
        return !some( [ planPositive, planObstacle, ...this.emptyActionSteps() ], Boolean );
    }

    render() {
        const {
            getString,
            planId,
            planPositive,
            planObstacle,

            actionSteps,
            status,
            isBusySavingPlan,
            savePaxPlanSets,
            ...props
        } = this.props;

        const { errorMessage, ASErrorMessage } = this.state;

        const disabled = isBusySavingPlan || this.state.hasError || this.state.hasASError;
        const active = !isBusySavingPlan && !this.state.hasError && !this.state.hasASError;

        return (
            <div className="plan-item" data-sel={props[ 'data-sel' ]}>
                <PlanSetContent
                    getString={getString}
                    obstacleValue={planObstacle || ''}
                    positiveValue={planPositive || ''}
                    onChange={ this.handleInputChange }
                    save={savePaxPlanSets}
                >
                    <label className="action-steps-list">
                        <span className="action-step-label">{getString( ACTION_STEP_LABEL )}</span>
                        <span className="checkmark-label">{getString( ACTION_STEP_DONE_CHECKBOX_LABEL )}</span>
                    </label>
                    {actionSteps.sort( ( a, b ) => a.id - b.id ).map( ( step, i ) => (
                        <ActionStep
                            getString={getString}
                            value={step.actionStep}
                            id={step.id}
                            status={step.actionStepStatus}
                            planId={ planId }
                            key={ `${planId}-${step.id}` }
                            onChange={ this.handleActionStepChange( i ) }
                            isBusySavingPlan={ isBusySavingPlan }
                            save={savePaxPlanSets}
                            data-sel={`progress-planset-action-step-${i}`}
                        />
                    ) )}
                </PlanSetContent>

                <div className="pax-progress-my-plan-controls">

                    <div className="full-width">
                        <div className="half-width">
                            <AddActionStep
                                getString={getString}
                                disabled={disabled}
                                active={active}
                                onClick={ this.handleAddActionStep }
                                data-sel="progress-planset-add-planset"
                            />
                        </div>

                        <div className="half-width save-area">
                            <ErrorMessage errorMessage={errorMessage} ASErrorMessage={ASErrorMessage} />
                            {status}
                            { this.isDeleteable() ? (
                                <DeleteButton
                                    active={active}
                                    disabled={disabled}
                                    getString={getString}
                                    onClick={this.handleDeletePlanSet}
                                    data-sel="progress-planset-delete"
                                />
                            ) : (
                                <SaveButton
                                    active={active}
                                    disabled={disabled}
                                    getString={getString}
                                    onClick={savePaxPlanSets}
                                    data-sel="progress-planset-save"
                                />
                            )}

                        </div>

                        <div style={ { clear: 'both' } }/>
                    </div>
                </div>
            </div>
        );
    }
}
