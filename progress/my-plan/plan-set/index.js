import { connect } from 'react-redux';
import { actions, selectors } from 'contests-progress-dux';

import PlanSet from './plan-set';

const {
    getBusySavingPlanSets
} = selectors;

const {
    addActionStep,
    savePaxPlanSets,
    updateActionStep,
    updatePlan,
    deletePlanSet
} = actions;

export const mapStateToProps = state => ( {
    isBusySavingPlan: getBusySavingPlanSets( state ),
} );

export const mapDispatchToProps = {
    addActionStep,
    savePaxPlanSets,
    updateActionStep,
    updatePlan,
    deletePlanSet
};

export default connect( mapStateToProps, mapDispatchToProps )( PlanSet );
