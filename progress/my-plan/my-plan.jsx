// libs
import React from 'react';
import PropTypes from 'prop-types';
// shared components
import { PlusCircleIcon } from 'component-library';
// progress child components
import PlanSet from './plan-set';

const BEGINNING_EMPTY_SPACE = /^ /i;
function formatPlanSetValue( val ) {
    // obstacle and positive will sometimes have an empty space to
    // prevent the api from deleting, the UI should treat the empty
    // space as not being there
    return ( val || '' ).replace( BEGINNING_EMPTY_SPACE, '' );
}

export default class MyPlan extends React.Component {
    static propTypes = {
        addPlan: PropTypes.func,

        goalQuestMyPlanVOs: PropTypes.array,
        savePlanSetsError: PropTypes.bool,
        busySavingPlanSets: PropTypes.bool,
        noSavedMsg: PropTypes.bool,

        getString: PropTypes.func.isRequired,
    };

    static defaultProps = {
        goalQuestMyPlanVOs: []
    };

    handleAddPlan = () => {
        this.props.addPlan( this.props.goalQuestMyPlanVOs );
    };

    getSavingText = () => {
        const {
            busySavingPlanSets, noSavedMsg, getString
        } = this.props;

        if ( busySavingPlanSets ) {
            return getString( 'paxProgress.plan.saving' );
        } else if ( noSavedMsg ) {
            return '';
        } else {
            return getString( 'paxProgress.plan.saved' );
        }
    }

    renderStatus = () => {
        const {
            savePlanSetsError, getString, busySavingPlanSets
        } = this.props;

        if ( savePlanSetsError && !busySavingPlanSets ) {
            return <span className="saving-indicator save-error">{ getString( 'paxProgress.plan.saveError' ) }</span>;
        } else {
            return <span className="saving-indicator">{ this.getSavingText() }</span>;
        }
    }

    render() {
        const { getString, goalQuestMyPlanVOs } = this.props;

        return (
            <div className="plan-set">
                <div className="plan">
                    <div className="plan-item-list">
                        { goalQuestMyPlanVOs.map( ( { id, obstacle, positive, actionSteps }, i ) => (
                            <PlanSet
                                getString= { getString }
                                status={ this.renderStatus() }
                                key={ i }
                                planId={id}
                                planObstacle={formatPlanSetValue( obstacle )}
                                planPositive={formatPlanSetValue( positive )}
                                actionSteps={actionSteps}
                                index={ i }
                                isLastPlanSet={ goalQuestMyPlanVOs.length === 1 }
                                data-sel={`progress-planset-${i}`}
                            />
                        ) ) }
                    </div>
                    <div className="add-plan-set" onClick={ this.handleAddPlan }>
                        <span className="icon-plus-circle branding-primary-text"><PlusCircleIcon /></span>
                        <span className="txt branding-secondary-text">{ getString( 'paxProgress.plan.addPlanSetLabel' ) }</span>
                    </div>
                </div>
            </div>
        );
    }
}
