import { connect } from 'react-redux';
import { actions, selectors } from 'contests-progress-dux';

import MyPlan from './my-plan';
import { getGetString } from '../selectors';
const {
    addPlan
} = actions;
const {
    getPlanSets,
    getSavePlanSetsError,
    getBusySavingPlanSets,
    getNoSavedMsg
} = selectors;

export const mapStateToProps = state => ( {
    goalQuestMyPlanVOs: getPlanSets( state ),
    savePlanSetsError: getSavePlanSetsError( state ),
    busySavingPlanSets: getBusySavingPlanSets( state ),
    noSavedMsg: getNoSavedMsg( state ),
    getString: getGetString( state )
} );

export const mapDispatchToProps = { addPlan };
export default connect( mapStateToProps, mapDispatchToProps )( MyPlan );
