
import React from 'react';
import PlanSet from '../plan-set/plan-set';
import renderer from 'react-test-renderer';

const plan = [ {
            'id': 8366, 'positive': null, 'obstacle': null, 'actionSteps': [
            { 'id': 9986, 'actionStep': '1', 'actionStepStatus': false },
            { 'id': 9987, 'actionStep': '2', 'actionStepStatus': true },
            { 'id': 9988, 'actionStep': '3', 'actionStepStatus': true },
            {
                'id': 10004,
                'actionStep': '4!!!!!!!!!!!!!!',
                'actionStepStatus': false
            } ]
        },
];
describe( 'Plan Set', () => {
    it( 'renders Planset with no errors', () => {
        const wrapper = renderer.create(
            <PlanSet index={0} isLastPlanSet plan={ plan } getString={code => code} />
        ).toJSON();

        expect( wrapper ).toMatchSnapshot();
    } );
} );
