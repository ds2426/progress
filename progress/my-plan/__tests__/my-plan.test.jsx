import React from 'react';
import renderer from 'react-test-renderer';
import createStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import MyPlan from '../my-plan.jsx';


const props = {
        addPlan: () => {},
        'programId': 9708,
        'goalQuestMyPlanVOs': [
            {
                'id': 8366, 'positive': null, 'obstacle': null, 'actionSteps': [
                { 'id': 9986, 'actionStep': '1', 'actionStepStatus': false },
                { 'id': 9987, 'actionStep': '2', 'actionStepStatus': true },
                { 'id': 9988, 'actionStep': '3', 'actionStepStatus': true },
                {
                    'id': 10004,
                    'actionStep': '4!!!!!!!!!!!!!!',
                    'actionStepStatus': false
                } ]
            },
            {
                'id': 8367, 'positive': 'plan 2 positive', 'obstacle': 'plan 2 obj', 'actionSteps': [
                { 'id': 9994, 'actionStep': '5', 'actionStepStatus': false },
                { 'id': 9995, 'actionStep': '6', 'actionStepStatus': false },
                { 'id': 9996, 'actionStep': '7', 'actionStepStatus': false },
                { 'id': 9997, 'actionStep': null, 'actionStepStatus': false }
            ]
            }
        ],
        getString: ( str ) => str
};

describe( 'My Plan Component', () => {
    let store;
    beforeEach( () => {
        store = createStore()();
    } );

    it( 'renders Planset with no errors', () => {
        const wrapper = renderer.create(
            <Provider store={store}>
                <MyPlan { ...props } />
            </Provider>
        ).toJSON();
        expect( wrapper ).toMatchSnapshot();
    } );
} );
