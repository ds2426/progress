import React from 'react';
import renderer from 'react-test-renderer';
import ActionStep from '../plan-set/action-step.jsx';


const props =  {
    actionStepLabel: 'test',
    actionStepPlaceholder: 'test',
    actionStepDoneCheckboxLabel: true,
    busy: false,
    actionStep: 'actionStep',
    actionStepStatus: false,
    save: () => {},
    onChange: jest.fn()
};
describe( 'ActionStep', () => {
    beforeEach( () => {
    } );

    it( 'renders Action Step with no errors', () => {
        const wrapper = renderer.create(
            <ActionStep { ...props } getString={code => code}  />
        ).toJSON();
        expect( wrapper ).toMatchSnapshot();
    } );


} );
