// libs
import { connect } from 'react-redux';
import get from 'lodash/get';
import {
    getFixedPlusRatePayoutMessage,
    getFixedPlusRatePayoutArray,
    getChallengePointBaseEarningDescription,
    getBonusMessage,
} from 'contests-utils';
import { getActiveProgram } from 'app/router/selectors';
import { getContestUtilsMessageProps } from './selectors';
import Progress from './progress';

export const mapStateToProps = state => {
    const program  = getActiveProgram( state ) || {};
    const contestUtilMessageProps = getContestUtilsMessageProps( state );
    return {
        program,

        fixedPlusRatePayoutMessage: getFixedPlusRatePayoutMessage( contestUtilMessageProps ) || '',
        fixedPlusRatePayoutMessageArray: getFixedPlusRatePayoutArray( contestUtilMessageProps ),
        challengePointBaseEarningDescription: getChallengePointBaseEarningDescription( contestUtilMessageProps ) || '',
        bonusMessage: getBonusMessage( contestUtilMessageProps ) || '',

        isLaunchAs: get( state, 'user.isLaunchAs' ) || false,
        hasOts: get( state, 'user.participantFeatures.ots' ),
    };
};

export default connect( mapStateToProps )( Progress );
