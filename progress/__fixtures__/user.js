export default {
    'avatarUrl': 'https://s3.us-west-2.amazonaws.com/contests-webapp.biw.cloud--user-content--qa/linda/avatars/F2Tve8DO6np6IYxTmY29EU5CzIyFHngo.jpg',
    'emailAddress': 'parag.pattankar@biworldwide.com',
    'firstName': 'Jeremy',
    'lastName': 'Veach',
    'userId': 246863,
    'participantFeatures': {
      'ots': false
    },
    'accountBalance': {
      'result': 1800
    },
    'accountExpired': false,
    'accountLocked': false,
    'adminUser': null,
    'credentialsExpired': false,
    'enabled': true,
    'firstTimeSetupDone': true,
    'locale': 'en_US',
    'middleName': null,
    'suffix': null,
    'userName': 'CP-011',
    'countryCode': 'USA'
  };
