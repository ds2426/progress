import user from './user';
const program = {
    'programId': 14692,
    'programName': 'OBJ-Standard-no progress',
    'programType': 'objective',
    'programLogo': null,
    'programCalculationType': 'standard',
    'role': 'Selector',
    'partnerUser': null,
    'partnerId': null,
    'isEligbleForPartnerSelection': false,
    'goalSelectionFromDate': null,
    'goalSelectionToDate': null,
    'startDate': '2018-05-01T00:00:00-0500',
    'endDate': '2018-12-31T23:59:59-0600',
    'isGoalSelected': true,
    'groupId': 24842,
    'timePeriodId': 14451,
    'paxGoalId': 129619,
    'programViewStatus': 'no_progress',
    'objectiveSummary': 'This contest is all about increasing revenue across the sales channel.',
    'isMinimumQualifierEnabled': true,
    'goalPrecision': 2,
    'bonusPrecision': 0,
    'mqPrecision': 0,
    'goalRoundingMethod': 'standard',
    'bonusRoundingMethod': null,
    'mqRoundingMethod': 'standard',
    'isOwnerEligibleForAward': false,
    'isBonusEnabled': false,
    'theme': 'none',
    'primaryColor': '#ff0000',
    'secondaryColor': '#f0a51a',
    'heroImageUrl': 'https://apiqa.farm1.honeycombsvc.com/hcapi/participantMedia/linda/logo/1525189457218_logo.png',
    'programViewId': '14692---Selector',
    'clientLogo': 'https://apipprd.farm1.honeycombsvc.com/hcapi/participantMedia/linda/logo/1516995361085_logo.png'
};

const progress = {
    'goal': {
      'paxGoalId': 129619,
      'userInfo': {
        'userId': null,
        'firstName': 'Jeremy',
        'lastName': 'Veach',
        'middleName': null,
        'suffix': null,
        'userName': 'CP-011',
        'loginToken': null,
        'clientCode': null,
        'firstTimeSetupDone': null,
        'enabled': null,
        'accountNonExpired': null,
        'credentialsNonExpired': null,
        'accountNonLocked': null,
        'clientId': null,
        'locale': null,
        'emailAddress': null,
        'adminUser': null,
        'countryName': null,
        'isInActiveHasPoints': null
      },
      'programType': 'objective',
      'programName': 'OBJ-Standard-no progress',
      'programId': 14692,
      'programDates': {
        'timePeriodId': 14451,
        'programStartDate': '2018-05-01T00:00:00-0500',
        'programEndDate': '2018-12-31T23:59:59-0600'
      },
      'groupName': 'Group 1',
      'goalPayoutTierType': 'fixed',
      'goalAchievementRule': 'fixed_amount',
      'programCalculationType': 'standard',
      'calculationLabel': 'day',
      'programViewStatus': null,
      'mediaLabel': 'AwardPoints',
      'goalSelectionQuestion': null,
      'goalSelectionHelpText1': null,
      'goalSelectionHelpText2': null,
      'goalLabel': {
        'labelType': 'currency',
        'labelText': 'in product revenue',
        'labelPosition': 'after',
        'currencyCode': 'usd',
        'currencySymbol': '$',
        'currencyPosition': 'before',
        'precision': 2,
        'roundingMethod': 'standard'
      },
      'mqLabel': {
        'labelType': 'none',
        'labelText': 'Unit',
        'labelPosition': 'after',
        'currencyCode': null,
        'currencySymbol': null,
        'currencyPosition': null,
        'precision': 0,
        'roundingMethod': 'standard'
      },
      'baseValue': {
        'goalBaseValue': 22147.44,
        'goalBaseCalculationValue': 126.56,
        'mqBaseValue': null,
        'bonusBaseValue': null
      },
      'isMinimumQualifierEnabled': true,
      'isBonusEnabled': false,
      'isBaseLineRequired': false,
      'baseLevel': null,
      'bonusPayout': null,
      'goalLevels': [
        {
          'payoutTierId': 31855,
          'name': null,
          'description': 'Achieve your revenue goal by selling all products',
          'orderNumber': 1,
          'incrementQuantity': null,
          'incrementAward': null,
          'incrementMinQualified': null,
          'goalAchieveAmount': 26576.93,
          'goalAwardAmount': 1000,
          'partnerAward': null,
          'minQualifier': 10,
          'maxPoints': null,
          'threshold': null,
          'goalCalculationValue': 151.87,
          'programNumber': null,
          'catalogName': null
        }
      ],
      'paxGoalSelection': {
        'payoutTierId': 31855,
        'name': null,
        'description': 'Achieve your revenue goal by selling all products',
        'orderNumber': 1,
        'incrementQuantity': null,
        'incrementAward': null,
        'incrementMinQualified': null,
        'goalAchieveAmount': 26576.93,
        'goalAwardAmount': 1000,
        'partnerAward': null,
        'minQualifier': 10,
        'maxPoints': null,
        'threshold': null,
        'goalCalculationValue': 151.87,
        'programNumber': null,
        'catalogName': null
      },
      'productId': null,
      'paxProgressResponse': {
        'dateThroughPercentage': 9,
        'progressPercentage': null,
        'onTrackPercentage': null,
        'resultsToDate': null,
        'toAchieve': 26576.93,
        'dateThrough': null,
        'dataThrough': null,
        'resultsToDateCalculationValue': null,
        'toAchieveCalculationValue': 167.6,
        'isMinQualifierAchieved': false,
        'currentMQValue': null,
        'currentBonusValue': null,
        'currentGoalValue': null,
        'earnedToDate': null
      },
      'paxProgressGraphVO': {
        'trending': [],
        'progress': [],
        'goalPace': [],
        'starting': null,
        'onTrack': null
      },
      'progressHistory': [],
      'errorMessage': null
    },
    'finalResult': null
};

const rules = {
    'key': 'gq.group.selector.rules.12553',
    'value': '<h3 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center"><span style="font-size:16.0pt"><span style="font-family:"Arial","sans-serif"">Reduction Average Program GoalQuest<sup>® </sup></span></span></h3><h3 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center"><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif"">Official Rules </span></span></h3><p style="margin:0in; margin-bottom:.0001pt"> </p><p style="margin:0in; margin-bottom:.0001pt"><strong><u><span style="font-size:14.0pt"><span style="font-family:"Arial","sans-serif""><span style="text-transform:uppercase">Program Overview</span></span></span></u></strong></p><p style="margin:0in; margin-bottom:.0001pt; text-align:justify"> </p><p style="margin:0in; margin-bottom:.0001pt; text-align:justify"><span style="text-autospace:none"><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif""><span style="color:black">From </span></span></span><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif"">11/1/2017 – 1/31/2017 BI WORLDWIDE</span></span><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif""><span style="color:black"> is offering a special incentive to improve our average handle time.</span></span></span></span></p><p style="margin:0in; margin-bottom:.0001pt; text-align:justify"> </p><p style="margin:0in; margin-bottom:.0001pt; text-align:justify"><span style="text-autospace:none"><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif""><span style="color:black">GoalQuest is a unique incentive program that allows you to select a personal goal. You are competing only against yourself to increase your performance and if you do, you can reap the rewards that go with it. </span></span></span></span></p><p style="margin:0in; margin-bottom:.0001pt; text-align:justify"> </p><p style="margin:0in; margin-bottom:.0001pt; text-align:justify"><span style="text-autospace:none"><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif""><span style="color:black">Read the rules carefully as GoalQuest is All-or-Nothing. However, the higher the goal you select, the more you can earn! </span></span></span></span></p><p style="margin:0in; margin-bottom:.0001pt; text-align:justify"> </p><p style="margin:0in; margin-bottom:.0001pt; text-align:justify"><span style="text-autospace:none"><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif""><span style="color:black">You – and only you -  have the power to make this happen.</span></span></span></span></p><p style="margin:0in; margin-bottom:.0001pt"> </p><p style="margin:0in; margin-bottom:.0001pt"><strong><u><span style="font-size:14.0pt"><span style="font-family:"Arial","sans-serif""><span style="text-transform:uppercase">How GoalQuest Works</span></span></span></u></strong></p><p style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt; margin-left:.5in; text-align:justify"> </p><ol><li style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt; text-align:justify"><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif"">PICK YOUR GOAL </span></span></li></ol><p style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt; margin-left:.5in; text-align:justify"><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif"">You must select a goal between 10/15/2017 – 11/15/2017 on the GoalQuest website. If you do not select a goal during the goal selection period, you will not be eligible to earn any awards in the program.<strong><span style="font-family:"Arial","sans-serif""> No goal changes or selections can be made after </span></strong><b>11/15/2017 at 11:59 pm<strong><span style="font-family:"Arial","sans-serif"">. </span></strong></b></span></span></p><p style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt; margin-left:.5in; text-align:justify"> </p><p style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt; margin-left:.5in; text-align:justify"><strong><span style="font-family:"Arial","sans-serif"">Your goal levels:</span></strong></p><table class="Table" width="675" style="width:405.0pt; margin-left:41.4pt; border-collapse:collapse; border:solid windowtext 1.0pt"><tbody><tr><td width="116" style="width:69.3pt; border:solid windowtext 1.0pt; background:#44c9fb"><h1 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"> </h1></td><td width="185" style="width:110.7pt; border:solid windowtext 1.0pt; border-left:none; background:#44c9fb"><h1 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="font-size:14.0pt"><span style="color:white">Level 1</span></span></h1></td><td width="185" style="width:110.7pt; border:solid windowtext 1.0pt; border-left:none; background:#44c9fb"><h1 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="font-size:14.0pt"><span style="color:white">Level 2</span></span></h1></td><td width="191" style="width:114.3pt; border:solid windowtext 1.0pt; border-left:none; background:#44c9fb"><h1 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="font-size:14.0pt"><span style="color:white">Level 3</span></span></h1></td></tr><tr style="height:33.25pt"><td width="116" style="width:69.3pt; border:solid windowtext 1.0pt; border-top:none"><h1 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="height:33.25pt"><span style="font-size:14.0pt"><span style="font-weight:normal">GOALS</span></span></span></h1></td><td width="185" style="width:110.7pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt"><h1 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="height:33.25pt"><span style="font-size:10.0pt"><span style="font-weight:normal">Achieve an average handle time of 4.00</span></span></span></h1></td><td width="185" style="width:110.7pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt"><p align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="height:33.25pt"><span style="font-size:10.0pt"><span style="font-family:"Arial","sans-serif"">Achieve an average handle time of 3.50</span></span></span></p></td><td width="191" style="width:114.3pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt"><p align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="height:33.25pt"><span style="font-size:10.0pt"><span style="font-family:"Arial","sans-serif"">Achieve an average handle time of 3.00</span></span></span></p></td></tr><tr style="height:40.0pt"><td width="116" style="width:69.3pt; border:solid windowtext 1.0pt; border-top:none"><h1 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="height:40.0pt"><span style="font-size:14.0pt"><span style="font-weight:normal">AWARDS</span></span></span></h1></td><td width="185" style="width:110.7pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt"><h1 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="height:40.0pt"><span style="font-size:10.0pt"><span style="font-weight:normal">150 POINTS</span></span></span></h1></td><td width="185" style="width:110.7pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt"><h1 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="height:40.0pt"><span style="font-size:10.0pt"><span style="font-weight:normal">450 POINTS</span></span></span></h1></td><td width="191" style="width:114.3pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt"><h1 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="height:40.0pt"><span style="font-size:10.0pt"><span style="font-weight:normal">900 POINTS</span></span></span></h1></td></tr></tbody></table><p style="margin:0in; margin-bottom:.0001pt; text-align:justify"> </p><p style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt; margin-left:.5in; text-align:justify"><b><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif"">IMPORTANT:</span></span></b><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif""> You can only earn at the goal level you select! Selecting<strong><span style="font-family:"Arial","sans-serif""> too low of a goal limits the awards you can earn. But, if you pick too high a goal and fall short you earn nothing. Choose a goal that is a stretch for you, but one that you believe you can achieve.</span></strong></span></span></p><p style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt; margin-left:.5in; text-align:justify"> </p><ol start="2"><li style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt; text-align:justify"><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif"">WORK HARD </span></span></li></ol><p style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt; margin-left:.5in; text-align:justify"><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif"">From 11/1/2017 – 1/31/2017 strive to reach or exceed your selected goal. If you do, you will earn the awards associated with your selected goal level.'
};

const { programViewId } = program;
export { user, program, progress, rules, programViewId };
