import user from './user';
const program = {
    'programId': 13288,
    'programName': 'DB UAT-AVE-no progress',
    'programType': 'goalquest',
    'programLogo': 'https://apiqa.farm1.honeycombsvc.com/hcapi/participantMedia/linda/logo/1518723263286_logo.png',
    'programCalculationType': 'average',
    'role': 'Selector',
    'partnerUser': null,
    'partnerId': null,
    'isEligbleForPartnerSelection': false,
    'goalSelectionFromDate': '2017-12-24T00:00:00-0600',
    'goalSelectionToDate': '2017-12-31T23:59:59-0600',
    'startDate': '2018-01-01T00:00:00-0600',
    'endDate': '2018-12-31T23:59:59-0600',
    'isGoalSelected': true,
    'groupId': 19832,
    'timePeriodId': 12950,
    'paxGoalId': 81055,
    'programViewStatus': 'no_progress',
    'objectiveSummary': 'Increase average sales per work order',
    'isMinimumQualifierEnabled': false,
    'goalPrecision': 2,
    'bonusPrecision': 2,
    'mqPrecision': 0,
    'goalRoundingMethod': 'standard',
    'bonusRoundingMethod': 'down',
    'mqRoundingMethod': null,
    'isOwnerEligibleForAward': false,
    'isBonusEnabled': true,
    'theme': null,
    'primaryColor': null,
    'secondaryColor': null,
    'heroImageUrl': null,
    'programViewId': '13288---Selector',
    'clientLogo': 'https://apipprd.farm1.honeycombsvc.com/hcapi/participantMedia/linda/logo/1516995361085_logo.png'
  };

const progress = {
    'goal': {
      'paxGoalId': 81055,
      'userInfo': {
        'userId': null,
        'firstName': 'Jeremy',
        'lastName': 'Veach',
        'middleName': null,
        'suffix': null,
        'userName': 'CP-011',
        'loginToken': null,
        'clientCode': null,
        'firstTimeSetupDone': null,
        'enabled': null,
        'accountNonExpired': null,
        'credentialsNonExpired': null,
        'accountNonLocked': null,
        'clientId': null,
        'locale': null,
        'emailAddress': null,
        'adminUser': null,
        'countryName': null,
        'isInActiveHasPoints': null
      },
      'programType': 'goalquest',
      'programName': 'DB UAT-AVE-no progress',
      'programId': 13288,
      'programDates': {
        'timePeriodId': 12950,
        'programStartDate': '2018-01-01T00:00:00-0600',
        'programEndDate': '2018-12-31T23:59:59-0600'
      },
      'groupName': 'Group 1',
      'goalPayoutTierType': 'fixed',
      'goalAchievementRule': 'baseline_plus_fixed_amount',
      'programCalculationType': 'average',
      'calculationLabel': null,
      'programViewStatus': null,
      'mediaLabel': 'AwardPoints',
      'goalSelectionQuestion': 'How much can you improve your average sales per work order?',
      'goalSelectionHelpText1': 'Most people who select the lowest level goal could have achieved at a higher goal. Could you do more?',
      'goalSelectionHelpText2': 'People who choose the highest goal achieve at the highest rate. Go for it!',
      'goalLabel': {
        'labelType': 'currency',
        'labelText': 'average per work order',
        'labelPosition': 'after',
        'currencyCode': 'usd',
        'currencySymbol': '$',
        'currencyPosition': 'before',
        'precision': 2,
        'roundingMethod': 'standard'
      },
      'mqLabel': null,
      'baseValue': {
        'goalBaseValue': 11.41,
        'goalBaseCalculationValue': null,
        'mqBaseValue': null,
        'bonusBaseValue': 21301.56
      },
      'isMinimumQualifierEnabled': false,
      'isBonusEnabled': true,
      'isBaseLineRequired': true,
      'baseLevel': null,
      'bonusPayout': {
        'label': {
          'labelType': 'currency',
          'labelText': 'Revenue',
          'labelPosition': 'after',
          'currencyCode': 'usd',
          'currencySymbol': '$',
          'currencyPosition': 'before',
          'precision': 2,
          'roundingMethod': 'down'
        },
        'payoutStructureType': 'fixed',
        'bonusAward': 50,
        'bonusAchievementAmount': 31301.56,
        'bonusIncrementalQuantity': null,
        'bonusMaximumPoints': null,
        'bonusMQ': null,
        'isGoalNeedToAchieve': true,
        'bonusAchievementRule': 'baseline_plus_fixed_amount'
      },
      'goalLevels': [
        {
          'payoutTierId': 25254,
          'name': 'Level 1',
          'description': 'Baseline + $2.50 per WO',
          'orderNumber': 1,
          'incrementQuantity': null,
          'incrementAward': null,
          'incrementMinQualified': null,
          'goalAchieveAmount': 13.91,
          'goalAwardAmount': 200,
          'partnerAward': null,
          'minQualifier': null,
          'maxPoints': null,
          'threshold': null,
          'goalCalculationValue': null,
          'programNumber': null,
          'catalogName': null
        },
        {
          'payoutTierId': 25253,
          'name': 'Level 2',
          'description': 'Baseline + $4.50 per WO',
          'orderNumber': 2,
          'incrementQuantity': null,
          'incrementAward': null,
          'incrementMinQualified': null,
          'goalAchieveAmount': 15.91,
          'goalAwardAmount': 600,
          'partnerAward': null,
          'minQualifier': null,
          'maxPoints': null,
          'threshold': null,
          'goalCalculationValue': null,
          'programNumber': null,
          'catalogName': null
        },
        {
          'payoutTierId': 25255,
          'name': 'Level 3',
          'description': 'Baseline + $6.50',
          'orderNumber': 3,
          'incrementQuantity': null,
          'incrementAward': null,
          'incrementMinQualified': null,
          'goalAchieveAmount': 17.91,
          'goalAwardAmount': 1200,
          'partnerAward': null,
          'minQualifier': null,
          'maxPoints': null,
          'threshold': null,
          'goalCalculationValue': null,
          'programNumber': null,
          'catalogName': null
        }
      ],
      'paxGoalSelection': {
        'payoutTierId': 25254,
        'name': 'Level 1',
        'description': 'Baseline + $2.50 per WO',
        'orderNumber': 1,
        'incrementQuantity': null,
        'incrementAward': null,
        'incrementMinQualified': null,
        'goalAchieveAmount': 13.91,
        'goalAwardAmount': 200,
        'partnerAward': null,
        'minQualifier': null,
        'maxPoints': null,
        'threshold': null,
        'goalCalculationValue': null,
        'programNumber': null,
        'catalogName': null
      },
      'productId': null,
      'paxProgressResponse': {
        'dateThroughPercentage': 39,
        'progressPercentage': null,
        'onTrackPercentage': null,
        'resultsToDate': null,
        'toAchieve': 13.91,
        'dateThrough': null,
        'dataThrough': null,
        'resultsToDateCalculationValue': null,
        'toAchieveCalculationValue': null,
        'isMinQualifierAchieved': null,
        'currentMQValue': null,
        'currentBonusValue': null,
        'currentGoalValue': null,
        'earnedToDate': null
      },
      'paxProgressGraphVO': {
        'trending': [],
        'progress': [],
        'goalPace': [],
        'starting': null,
        'onTrack': null
      },
      'progressHistory': [],
      'errorMessage': null
    },
    'finalResult': null
  };

const rules = {
    'key': 'gq.group.selector.rules.12550',
    'value': 'rules'
};

const { programViewId } = program;
export { user, program, progress, rules, programViewId };
