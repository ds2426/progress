import user from './user';
const program = {
    'programId': 13316,
    'programName': 'CP-final results',
    'programType': 'challengepoint',
    'programLogo': null,
    'programCalculationType': 'standard',
    'role': 'Selector',
    'partnerUser': null,
    'partnerId': null,
    'isEligbleForPartnerSelection': false,
    'goalSelectionFromDate': '2017-08-30T00:00:00-0500',
    'goalSelectionToDate': '2017-08-31T23:59:59-0500',
    'startDate': '2017-09-01T00:00:00-0500',
    'endDate': '2017-12-31T23:59:59-0600',
    'isGoalSelected': true,
    'groupId': 19914,
    'timePeriodId': 12972,
    'paxGoalId': 82052,
    'programViewStatus': 'final_issuance',
    'objectiveSummary': 'Increase revenue on all products ',
    'isMinimumQualifierEnabled': false,
    'goalPrecision': 2,
    'bonusPrecision': 0,
    'mqPrecision': 0,
    'goalRoundingMethod': 'standard',
    'bonusRoundingMethod': 'standard',
    'mqRoundingMethod': null,
    'isOwnerEligibleForAward': false,
    'isBonusEnabled': true,
    'theme': 'none',
    'primaryColor': '#0080ff',
    'secondaryColor': '#800080',
    'heroImageUrl': 'https://apiqa.farm1.honeycombsvc.com/hcapi/participantMedia/linda/logo/1525899632277_logo.jpeg',
    'programViewId': '13316---Selector',
    'clientLogo': 'https://apipprd.farm1.honeycombsvc.com/hcapi/participantMedia/linda/logo/1516995361085_logo.png'
  };

const progress = {
    'goal': {
      'paxGoalId': 82052,
      'userInfo': {
        'userId': null,
        'firstName': 'Jeremy',
        'lastName': 'Veach',
        'middleName': null,
        'suffix': null,
        'userName': 'CP-011',
        'loginToken': null,
        'clientCode': null,
        'firstTimeSetupDone': null,
        'enabled': null,
        'accountNonExpired': null,
        'credentialsNonExpired': null,
        'accountNonLocked': null,
        'clientId': null,
        'locale': null,
        'emailAddress': null,
        'adminUser': null,
        'countryName': null,
        'isInActiveHasPoints': null
      },
      'programType': 'challengepoint',
      'programName': 'CP-final results',
      'programId': 13316,
      'programDates': {
        'timePeriodId': 12972,
        'programStartDate': '2017-09-01T00:00:00-0500',
        'programEndDate': '2017-12-31T23:59:59-0600'
      },
      'groupName': 'Group 1',
      'goalPayoutTierType': 'fixed',
      'goalAchievementRule': 'fixed_amount',
      'programCalculationType': 'standard',
      'calculationLabel': 'day',
      'programViewStatus': null,
      'mediaLabel': 'AwardPoints',
      'goalSelectionQuestion': 'How much can you increase your product revenue?',
      'goalSelectionHelpText1': 'Most people who select the lowest level goal could have achieved at a higher goal. Could you do more?',
      'goalSelectionHelpText2': 'People who choose the highest goal achieve at the highest rate. Go for it!',
      'goalLabel': {
        'labelType': 'currency',
        'labelText': 'in product revenue',
        'labelPosition': 'after',
        'currencyCode': 'usd',
        'currencySymbol': '$',
        'currencyPosition': 'before',
        'precision': 2,
        'roundingMethod': 'standard'
      },
      'mqLabel': null,
      'baseValue': {
        'goalBaseValue': 22147.44,
        'goalBaseCalculationValue': 254.15,
        'mqBaseValue': null,
        'bonusBaseValue': null
      },
      'isMinimumQualifierEnabled': false,
      'isBonusEnabled': true,
      'isBaseLineRequired': false,
      'baseLevel': {
        'incrementQuantity': 1000,
        'incrementAward': 100,
        'maxPoints': 7000,
        'awardThreshold': 5000,
        'thresholdAchievementRule': 'fixed_amount',
        'baseAwardIncrementRule': 'fixed_amount'
      },
      'bonusPayout': {
        'label': {
          'labelType': 'none',
          'labelText': 'Closed opportunities',
          'labelPosition': 'after',
          'currencyCode': null,
          'currencySymbol': null,
          'currencyPosition': null,
          'precision': 0,
          'roundingMethod': 'standard'
        },
        'payoutStructureType': 'fixed',
        'bonusAward': 50,
        'bonusAchievementAmount': 500,
        'bonusIncrementalQuantity': null,
        'bonusMaximumPoints': null,
        'bonusMQ': null,
        'isGoalNeedToAchieve': false,
        'bonusAchievementRule': 'fixed_amount'
      },
      'goalLevels': [
        {
          'payoutTierId': 25375,
          'name': 'Level 1',
          'description': 'Sell $20,000 in product revenue',
          'orderNumber': 1,
          'incrementQuantity': null,
          'incrementAward': null,
          'incrementMinQualified': null,
          'goalAchieveAmount': 20000,
          'goalAwardAmount': 500,
          'partnerAward': null,
          'minQualifier': null,
          'maxPoints': null,
          'threshold': null,
          'goalCalculationValue': 229.51,
          'programNumber': null,
          'catalogName': null
        },
        {
          'payoutTierId': 25374,
          'name': 'Level 2',
          'description': 'Sell $30,000 in product revenue',
          'orderNumber': 2,
          'incrementQuantity': null,
          'incrementAward': null,
          'incrementMinQualified': null,
          'goalAchieveAmount': 30000,
          'goalAwardAmount': 1500,
          'partnerAward': null,
          'minQualifier': null,
          'maxPoints': null,
          'threshold': null,
          'goalCalculationValue': 344.26,
          'programNumber': null,
          'catalogName': null
        },
        {
          'payoutTierId': 25373,
          'name': 'Level 3',
          'description': 'Sell $40,000 in product revenue',
          'orderNumber': 3,
          'incrementQuantity': null,
          'incrementAward': null,
          'incrementMinQualified': null,
          'goalAchieveAmount': 40000,
          'goalAwardAmount': 3000,
          'partnerAward': null,
          'minQualifier': null,
          'maxPoints': null,
          'threshold': null,
          'goalCalculationValue': 459.02,
          'programNumber': null,
          'catalogName': null
        }
      ],
      'paxGoalSelection': {
        'payoutTierId': 25375,
        'name': 'Level 1',
        'description': 'Sell $20,000 in product revenue',
        'orderNumber': 1,
        'incrementQuantity': null,
        'incrementAward': null,
        'incrementMinQualified': null,
        'goalAchieveAmount': 20000,
        'goalAwardAmount': 500,
        'partnerAward': null,
        'minQualifier': null,
        'maxPoints': null,
        'threshold': null,
        'goalCalculationValue': 229.51,
        'programNumber': null,
        'catalogName': null
      },
      'productId': null,
      'paxProgressResponse': {
        'dateThroughPercentage': 100,
        'progressPercentage': 71,
        'onTrackPercentage': 71,
        'resultsToDate': 14250.49,
        'toAchieve': 5749.51,
        'dateThrough': '2017-12-31T23:59:59-0600',
        'dataThrough': '2017-12-31T23:59:59-0600',
        'resultsToDateCalculationValue': 163.53,
        'toAchieveCalculationValue': null,
        'isMinQualifierAchieved': null,
        'currentMQValue': 10000,
        'currentBonusValue': 24,
        'currentGoalValue': 14250.49,
        'earnedToDate': 900
      },
      'paxProgressGraphVO': {
        'trending': [],
        'progress': [
          {
            'xaxis': '2017-09-29T00:00:00-0500',
            'yaxis': 3562.62,
            'xAxis': '2017-09-29T00:00:00-0500',
            'yAxis': 3562.62
          },
          {
            'xaxis': '2017-10-27T00:00:00-0500',
            'yaxis': 7125.24,
            'xAxis': '2017-10-27T00:00:00-0500',
            'yAxis': 7125.24
          },
          {
            'xaxis': '2017-11-24T00:00:00-0600',
            'yaxis': 10687.87,
            'xAxis': '2017-11-24T00:00:00-0600',
            'yAxis': 10687.87
          },
          {
            'xaxis': '2017-12-31T00:00:00-0600',
            'yaxis': 14250.49,
            'xAxis': '2017-12-31T00:00:00-0600',
            'yAxis': 14250.49
          }
        ],
        'goalPace': [
          {
            'xaxis': '2017-09-29T00:00:00-0500',
            'yaxis': 4754.0983598,
            'xAxis': '2017-09-29T00:00:00-0500',
            'yAxis': 4754.0983598
          },
          {
            'xaxis': '2017-10-27T00:00:00-0500',
            'yaxis': 9344.2622934,
            'xAxis': '2017-10-27T00:00:00-0500',
            'yAxis': 9344.2622934
          },
          {
            'xaxis': '2017-11-24T00:00:00-0600',
            'yaxis': 13934.426227,
            'xAxis': '2017-11-24T00:00:00-0600',
            'yAxis': 13934.426227
          },
          {
            'xaxis': '2017-12-31T00:00:00-0600',
            'yaxis': 19999.9999964,
            'xAxis': '2017-12-31T00:00:00-0600',
            'yAxis': 19999.9999964
          }
        ],
        'starting': {
          'xaxis': '2017-09-01T00:00:00-0500',
          'yaxis': 0,
          'xAxis': '2017-09-01T00:00:00-0500',
          'yAxis': 0
        },
        'onTrack': {
          'xaxis': '2017-12-31T23:59:59-0600',
          'yaxis': 14250.49,
          'xAxis': '2017-12-31T23:59:59-0600',
          'yAxis': 14250.49
        }
      },
      'progressHistory': [
        {
          'progressDate': '2017-12-31T00:00:00-0600',
          'goalProgress': 14250.49,
          'mqProgress': 10000,
          'bonusProgress': 24,
          'loadType': 'replace'
        },
        {
          'progressDate': '2017-11-24T00:00:00-0600',
          'goalProgress': 10687.87,
          'mqProgress': 8000,
          'bonusProgress': 6,
          'loadType': 'replace'
        },
        {
          'progressDate': '2017-10-27T00:00:00-0500',
          'goalProgress': 7125.24,
          'mqProgress': 4000,
          'bonusProgress': 4,
          'loadType': 'replace'
        },
        {
          'progressDate': '2017-09-29T00:00:00-0500',
          'goalProgress': 3562.62,
          'mqProgress': 2000,
          'bonusProgress': 2,
          'loadType': 'replace'
        }
      ],
      'errorMessage': null
    },
    'finalResult': {
      'isBonusAchieved': false,
      'isMQAchieved': null,
      'isGoalAchieved': false,
      'totalNumberOfPoints': 900,
      'levelName': 'Level 1',
      'totalBonusPoints': 0,
      'totalGoalPoints': 0,
      'totalNumberofPaxWhoAchievedGoal': null,
      'isPartiallyCompleted': null,
      'isManagerEligibleForAward': null,
      'managerRange': null
    }
  };

const rules = {
    'key': 'gq.group.selector.rules.12553',
    'value': '<h3 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center"><span style="font-size:16.0pt"><span style="font-family:"Arial","sans-serif"">Reduction Average Program GoalQuest<sup>® </sup></span></span></h3><h3 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center"><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif"">Official Rules </span></span></h3><p style="margin:0in; margin-bottom:.0001pt"> </p><p style="margin:0in; margin-bottom:.0001pt"><strong><u><span style="font-size:14.0pt"><span style="font-family:"Arial","sans-serif""><span style="text-transform:uppercase">Program Overview</span></span></span></u></strong></p><p style="margin:0in; margin-bottom:.0001pt; text-align:justify"> </p><p style="margin:0in; margin-bottom:.0001pt; text-align:justify"><span style="text-autospace:none"><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif""><span style="color:black">From </span></span></span><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif"">11/1/2017 – 1/31/2017 BI WORLDWIDE</span></span><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif""><span style="color:black"> is offering a special incentive to improve our average handle time.</span></span></span></span></p><p style="margin:0in; margin-bottom:.0001pt; text-align:justify"> </p><p style="margin:0in; margin-bottom:.0001pt; text-align:justify"><span style="text-autospace:none"><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif""><span style="color:black">GoalQuest is a unique incentive program that allows you to select a personal goal. You are competing only against yourself to increase your performance and if you do, you can reap the rewards that go with it. </span></span></span></span></p><p style="margin:0in; margin-bottom:.0001pt; text-align:justify"> </p><p style="margin:0in; margin-bottom:.0001pt; text-align:justify"><span style="text-autospace:none"><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif""><span style="color:black">Read the rules carefully as GoalQuest is All-or-Nothing. However, the higher the goal you select, the more you can earn! </span></span></span></span></p><p style="margin:0in; margin-bottom:.0001pt; text-align:justify"> </p><p style="margin:0in; margin-bottom:.0001pt; text-align:justify"><span style="text-autospace:none"><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif""><span style="color:black">You – and only you -  have the power to make this happen.</span></span></span></span></p><p style="margin:0in; margin-bottom:.0001pt"> </p><p style="margin:0in; margin-bottom:.0001pt"><strong><u><span style="font-size:14.0pt"><span style="font-family:"Arial","sans-serif""><span style="text-transform:uppercase">How GoalQuest Works</span></span></span></u></strong></p><p style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt; margin-left:.5in; text-align:justify"> </p><ol><li style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt; text-align:justify"><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif"">PICK YOUR GOAL </span></span></li></ol><p style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt; margin-left:.5in; text-align:justify"><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif"">You must select a goal between 10/15/2017 – 11/15/2017 on the GoalQuest website. If you do not select a goal during the goal selection period, you will not be eligible to earn any awards in the program.<strong><span style="font-family:"Arial","sans-serif""> No goal changes or selections can be made after </span></strong><b>11/15/2017 at 11:59 pm<strong><span style="font-family:"Arial","sans-serif"">. </span></strong></b></span></span></p><p style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt; margin-left:.5in; text-align:justify"> </p><p style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt; margin-left:.5in; text-align:justify"><strong><span style="font-family:"Arial","sans-serif"">Your goal levels:</span></strong></p><table class="Table" width="675" style="width:405.0pt; margin-left:41.4pt; border-collapse:collapse; border:solid windowtext 1.0pt"><tbody><tr><td width="116" style="width:69.3pt; border:solid windowtext 1.0pt; background:#44c9fb"><h1 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"> </h1></td><td width="185" style="width:110.7pt; border:solid windowtext 1.0pt; border-left:none; background:#44c9fb"><h1 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="font-size:14.0pt"><span style="color:white">Level 1</span></span></h1></td><td width="185" style="width:110.7pt; border:solid windowtext 1.0pt; border-left:none; background:#44c9fb"><h1 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="font-size:14.0pt"><span style="color:white">Level 2</span></span></h1></td><td width="191" style="width:114.3pt; border:solid windowtext 1.0pt; border-left:none; background:#44c9fb"><h1 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="font-size:14.0pt"><span style="color:white">Level 3</span></span></h1></td></tr><tr style="height:33.25pt"><td width="116" style="width:69.3pt; border:solid windowtext 1.0pt; border-top:none"><h1 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="height:33.25pt"><span style="font-size:14.0pt"><span style="font-weight:normal">GOALS</span></span></span></h1></td><td width="185" style="width:110.7pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt"><h1 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="height:33.25pt"><span style="font-size:10.0pt"><span style="font-weight:normal">Achieve an average handle time of 4.00</span></span></span></h1></td><td width="185" style="width:110.7pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt"><p align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="height:33.25pt"><span style="font-size:10.0pt"><span style="font-family:"Arial","sans-serif"">Achieve an average handle time of 3.50</span></span></span></p></td><td width="191" style="width:114.3pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt"><p align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="height:33.25pt"><span style="font-size:10.0pt"><span style="font-family:"Arial","sans-serif"">Achieve an average handle time of 3.00</span></span></span></p></td></tr><tr style="height:40.0pt"><td width="116" style="width:69.3pt; border:solid windowtext 1.0pt; border-top:none"><h1 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="height:40.0pt"><span style="font-size:14.0pt"><span style="font-weight:normal">AWARDS</span></span></span></h1></td><td width="185" style="width:110.7pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt"><h1 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="height:40.0pt"><span style="font-size:10.0pt"><span style="font-weight:normal">150 POINTS</span></span></span></h1></td><td width="185" style="width:110.7pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt"><h1 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="height:40.0pt"><span style="font-size:10.0pt"><span style="font-weight:normal">450 POINTS</span></span></span></h1></td><td width="191" style="width:114.3pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt"><h1 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="height:40.0pt"><span style="font-size:10.0pt"><span style="font-weight:normal">900 POINTS</span></span></span></h1></td></tr></tbody></table><p style="margin:0in; margin-bottom:.0001pt; text-align:justify"> </p><p style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt; margin-left:.5in; text-align:justify"><b><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif"">IMPORTANT:</span></span></b><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif""> You can only earn at the goal level you select! Selecting<strong><span style="font-family:"Arial","sans-serif""> too low of a goal limits the awards you can earn. But, if you pick too high a goal and fall short you earn nothing. Choose a goal that is a stretch for you, but one that you believe you can achieve.</span></strong></span></span></p><p style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt; margin-left:.5in; text-align:justify"> </p><ol start="2"><li style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt; text-align:justify"><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif"">WORK HARD </span></span></li></ol><p style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt; margin-left:.5in; text-align:justify"><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif"">From 11/1/2017 – 1/31/2017 strive to reach or exceed your selected goal. If you do, you will earn the awards associated with your selected goal level.'
};

const { programViewId } = program;
export { user, program, progress, rules, programViewId };
