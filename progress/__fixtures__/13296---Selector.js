import user from './user';
const program = {
    'programId': 13296,
    'programName': 'CP - Goal Selection',
    'programType': 'challengepoint',
    'programLogo': null,
    'programCalculationType': 'standard',
    'role': 'Selector',
    'partnerUser': null,
    'partnerId': null,
    'isEligbleForPartnerSelection': false,
    'goalSelectionFromDate': '2018-01-01T00:00:00-0600',
    'goalSelectionToDate': '2018-05-23T23:59:59-0500',
    'startDate': '2019-01-01T00:00:00-0600',
    'endDate': '2019-03-31T23:59:59-0500',
    'isGoalSelected': false,
    'groupId': 19860,
    'timePeriodId': 12957,
    'paxGoalId': 0,
    'programViewStatus': 'no_goal_selected',
    'objectiveSummary': 'Increase revenue on all products ',
    'isMinimumQualifierEnabled': false,
    'goalPrecision': 2,
    'bonusPrecision': 0,
    'mqPrecision': 0,
    'goalRoundingMethod': 'standard',
    'bonusRoundingMethod': 'standard',
    'mqRoundingMethod': null,
    'isOwnerEligibleForAward': false,
    'isBonusEnabled': true,
    'theme': 'none',
    'primaryColor': '#00ff00',
    'secondaryColor': '#8000ff',
    'heroImageUrl': 'https://apiqa.farm1.honeycombsvc.com/hcapi/participantMedia/linda/logo/1524773035924_logo.png',
    'programViewId': '13296---Selector',
    'clientLogo': 'https://apipprd.farm1.honeycombsvc.com/hcapi/participantMedia/linda/logo/1516995361085_logo.png'
  };

const progress = {
    'goal': {
      'paxGoalId': null,
      'userInfo': null,
      'programType': 'challengepoint',
      'programName': 'CP - Goal Selection',
      'programId': 13296,
      'programDates': {
        'timePeriodId': 12957,
        'programStartDate': '2019-01-01T00:00:00-0600',
        'programEndDate': '2019-03-31T23:59:59-0500'
      },
      'groupName': 'Group 1',
      'goalPayoutTierType': 'fixed',
      'goalAchievementRule': 'fixed_amount',
      'programCalculationType': 'standard',
      'calculationLabel': 'day',
      'programViewStatus': null,
      'mediaLabel': 'AwardPoints',
      'goalSelectionQuestion': 'How much can you increase your product revenue?',
      'goalSelectionHelpText1': 'test 1 me more',
      'goalSelectionHelpText2': 'test 2 help me',
      'goalLabel': {
        'labelType': 'currency',
        'labelText': 'in product revenue',
        'labelPosition': 'after',
        'currencyCode': 'usd',
        'currencySymbol': '$',
        'currencyPosition': 'before',
        'precision': 2,
        'roundingMethod': 'standard'
      },
      'mqLabel': null,
      'baseValue': {
        'goalBaseValue': 22147.44,
        'goalBaseCalculationValue': 344.52,
        'mqBaseValue': null,
        'bonusBaseValue': null
      },
      'isMinimumQualifierEnabled': false,
      'isBonusEnabled': true,
      'isBaseLineRequired': false,
      'baseLevel': {
        'incrementQuantity': 1000,
        'incrementAward': 100,
        'maxPoints': 7000,
        'awardThreshold': 5000,
        'thresholdAchievementRule': 'fixed_amount',
        'baseAwardIncrementRule': 'fixed_amount'
      },
      'bonusPayout': {
        'label': {
          'labelType': 'none',
          'labelText': 'Closed opportunities',
          'labelPosition': 'after',
          'currencyCode': null,
          'currencySymbol': null,
          'currencyPosition': null,
          'precision': 0,
          'roundingMethod': 'standard'
        },
        'payoutStructureType': 'fixed',
        'bonusAward': 50,
        'bonusAchievementAmount': 500,
        'bonusIncrementalQuantity': null,
        'bonusMaximumPoints': null,
        'bonusMQ': null,
        'isGoalNeedToAchieve': false,
        'bonusAchievementRule': 'fixed_amount'
      },
      'goalLevels': [
        {
          'payoutTierId': 25292,
          'name': 'Level 1',
          'description': 'Sell $20,000 in product revenue',
          'orderNumber': 1,
          'incrementQuantity': null,
          'incrementAward': null,
          'incrementMinQualified': null,
          'goalAchieveAmount': 20000,
          'goalAwardAmount': 500,
          'partnerAward': null,
          'minQualifier': null,
          'maxPoints': null,
          'threshold': null,
          'goalCalculationValue': 311.11,
          'programNumber': null,
          'catalogName': null
        },
        {
          'payoutTierId': 25293,
          'name': 'Level 2',
          'description': 'Sell $30,000 in product revenue',
          'orderNumber': 2,
          'incrementQuantity': null,
          'incrementAward': null,
          'incrementMinQualified': null,
          'goalAchieveAmount': 30000,
          'goalAwardAmount': 1500,
          'partnerAward': null,
          'minQualifier': null,
          'maxPoints': null,
          'threshold': null,
          'goalCalculationValue': 466.67,
          'programNumber': null,
          'catalogName': null
        },
        {
          'payoutTierId': 25291,
          'name': 'Level 3',
          'description': 'Sell $40,000 in product revenue',
          'orderNumber': 3,
          'incrementQuantity': null,
          'incrementAward': null,
          'incrementMinQualified': null,
          'goalAchieveAmount': 40000,
          'goalAwardAmount': 3000,
          'partnerAward': null,
          'minQualifier': null,
          'maxPoints': null,
          'threshold': null,
          'goalCalculationValue': 622.22,
          'programNumber': null,
          'catalogName': null
        }
      ],
      'paxGoalSelection': null,
      'productId': null,
      'paxProgressResponse': null,
      'paxProgressGraphVO': null,
      'progressHistory': null,
      'errorMessage': null
    },
    'finalResult': null
  };

const rules = {
    'key': 'gq.group.selector.rules.12553',
    'value': '<h3 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center"><span style="font-size:16.0pt"><span style="font-family:"Arial","sans-serif"">Reduction Average Program GoalQuest<sup>® </sup></span></span></h3><h3 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center"><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif"">Official Rules </span></span></h3><p style="margin:0in; margin-bottom:.0001pt"> </p><p style="margin:0in; margin-bottom:.0001pt"><strong><u><span style="font-size:14.0pt"><span style="font-family:"Arial","sans-serif""><span style="text-transform:uppercase">Program Overview</span></span></span></u></strong></p><p style="margin:0in; margin-bottom:.0001pt; text-align:justify"> </p><p style="margin:0in; margin-bottom:.0001pt; text-align:justify"><span style="text-autospace:none"><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif""><span style="color:black">From </span></span></span><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif"">11/1/2017 – 1/31/2017 BI WORLDWIDE</span></span><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif""><span style="color:black"> is offering a special incentive to improve our average handle time.</span></span></span></span></p><p style="margin:0in; margin-bottom:.0001pt; text-align:justify"> </p><p style="margin:0in; margin-bottom:.0001pt; text-align:justify"><span style="text-autospace:none"><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif""><span style="color:black">GoalQuest is a unique incentive program that allows you to select a personal goal. You are competing only against yourself to increase your performance and if you do, you can reap the rewards that go with it. </span></span></span></span></p><p style="margin:0in; margin-bottom:.0001pt; text-align:justify"> </p><p style="margin:0in; margin-bottom:.0001pt; text-align:justify"><span style="text-autospace:none"><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif""><span style="color:black">Read the rules carefully as GoalQuest is All-or-Nothing. However, the higher the goal you select, the more you can earn! </span></span></span></span></p><p style="margin:0in; margin-bottom:.0001pt; text-align:justify"> </p><p style="margin:0in; margin-bottom:.0001pt; text-align:justify"><span style="text-autospace:none"><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif""><span style="color:black">You – and only you -  have the power to make this happen.</span></span></span></span></p><p style="margin:0in; margin-bottom:.0001pt"> </p><p style="margin:0in; margin-bottom:.0001pt"><strong><u><span style="font-size:14.0pt"><span style="font-family:"Arial","sans-serif""><span style="text-transform:uppercase">How GoalQuest Works</span></span></span></u></strong></p><p style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt; margin-left:.5in; text-align:justify"> </p><ol><li style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt; text-align:justify"><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif"">PICK YOUR GOAL </span></span></li></ol><p style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt; margin-left:.5in; text-align:justify"><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif"">You must select a goal between 10/15/2017 – 11/15/2017 on the GoalQuest website. If you do not select a goal during the goal selection period, you will not be eligible to earn any awards in the program.<strong><span style="font-family:"Arial","sans-serif""> No goal changes or selections can be made after </span></strong><b>11/15/2017 at 11:59 pm<strong><span style="font-family:"Arial","sans-serif"">. </span></strong></b></span></span></p><p style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt; margin-left:.5in; text-align:justify"> </p><p style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt; margin-left:.5in; text-align:justify"><strong><span style="font-family:"Arial","sans-serif"">Your goal levels:</span></strong></p><table class="Table" width="675" style="width:405.0pt; margin-left:41.4pt; border-collapse:collapse; border:solid windowtext 1.0pt"><tbody><tr><td width="116" style="width:69.3pt; border:solid windowtext 1.0pt; background:#44c9fb"><h1 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"> </h1></td><td width="185" style="width:110.7pt; border:solid windowtext 1.0pt; border-left:none; background:#44c9fb"><h1 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="font-size:14.0pt"><span style="color:white">Level 1</span></span></h1></td><td width="185" style="width:110.7pt; border:solid windowtext 1.0pt; border-left:none; background:#44c9fb"><h1 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="font-size:14.0pt"><span style="color:white">Level 2</span></span></h1></td><td width="191" style="width:114.3pt; border:solid windowtext 1.0pt; border-left:none; background:#44c9fb"><h1 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="font-size:14.0pt"><span style="color:white">Level 3</span></span></h1></td></tr><tr style="height:33.25pt"><td width="116" style="width:69.3pt; border:solid windowtext 1.0pt; border-top:none"><h1 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="height:33.25pt"><span style="font-size:14.0pt"><span style="font-weight:normal">GOALS</span></span></span></h1></td><td width="185" style="width:110.7pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt"><h1 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="height:33.25pt"><span style="font-size:10.0pt"><span style="font-weight:normal">Achieve an average handle time of 4.00</span></span></span></h1></td><td width="185" style="width:110.7pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt"><p align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="height:33.25pt"><span style="font-size:10.0pt"><span style="font-family:"Arial","sans-serif"">Achieve an average handle time of 3.50</span></span></span></p></td><td width="191" style="width:114.3pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt"><p align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="height:33.25pt"><span style="font-size:10.0pt"><span style="font-family:"Arial","sans-serif"">Achieve an average handle time of 3.00</span></span></span></p></td></tr><tr style="height:40.0pt"><td width="116" style="width:69.3pt; border:solid windowtext 1.0pt; border-top:none"><h1 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="height:40.0pt"><span style="font-size:14.0pt"><span style="font-weight:normal">AWARDS</span></span></span></h1></td><td width="185" style="width:110.7pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt"><h1 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="height:40.0pt"><span style="font-size:10.0pt"><span style="font-weight:normal">150 POINTS</span></span></span></h1></td><td width="185" style="width:110.7pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt"><h1 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="height:40.0pt"><span style="font-size:10.0pt"><span style="font-weight:normal">450 POINTS</span></span></span></h1></td><td width="191" style="width:114.3pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt"><h1 align="center" style="margin:0in; margin-bottom:.0001pt; text-align:center; padding:0in 5.4pt 0in 5.4pt"><span style="height:40.0pt"><span style="font-size:10.0pt"><span style="font-weight:normal">900 POINTS</span></span></span></h1></td></tr></tbody></table><p style="margin:0in; margin-bottom:.0001pt; text-align:justify"> </p><p style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt; margin-left:.5in; text-align:justify"><b><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif"">IMPORTANT:</span></span></b><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif""> You can only earn at the goal level you select! Selecting<strong><span style="font-family:"Arial","sans-serif""> too low of a goal limits the awards you can earn. But, if you pick too high a goal and fall short you earn nothing. Choose a goal that is a stretch for you, but one that you believe you can achieve.</span></strong></span></span></p><p style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt; margin-left:.5in; text-align:justify"> </p><ol start="2"><li style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt; text-align:justify"><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif"">WORK HARD </span></span></li></ol><p style="margin-top:0in; margin-right:0in; margin-bottom:.0001pt; margin-left:.5in; text-align:justify"><span style="font-size:11.0pt"><span style="font-family:"Arial","sans-serif"">From 11/1/2017 – 1/31/2017 strive to reach or exceed your selected goal. If you do, you will earn the awards associated with your selected goal level.'
};

const { programViewId } = program;
export { user, program, progress, rules, programViewId };
