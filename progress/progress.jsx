// libs
import React from 'react';
import PropTypes from 'prop-types';
import scrollToElement from 'scroll-to-element';
import { createSelector } from 'reselect';
import classNames from 'classnames';
import get from 'lodash/get';
// utils
import { LocaleString } from '@biw/intl';
import { constants } from 'contests-utils';
//local import
import {
    FinalResults,
    StickyHeader,
    Shop,
    Rules,
    ProgressHeaderMessage,
    ProgressNavBar,
    StatusTitle,
    OnTheSpot,
    SectionWaypoint,
    LevelComparisonTable
} from 'shared';

import GoalOverview from './goal-overview';
import ProgressDetails from './progress-details';
import MyPlan from './my-plan';
import ProgressTracker from './progress-tracker';

// scss
import './progress.scss';

const getHasOts = ( { hasOts } ) => hasOts || false;
const getProgramViewStatus = ( { program } ) => program.programViewStatus;
const getHasViewPart = createSelector( getProgramViewStatus, programViewStatus => ( {
    goalOverview: [ constants.PVS__GOAL_SELECTION, constants.PVS__GOAL_SELECTED, constants.PVS__NO_PROGRESS ].indexOf( programViewStatus ) !== -1,
    levelTable: programViewStatus === constants.PVS__NO_GOAL_SELECTED,
    progressHeaderMessage: programViewStatus === constants.PVS__NO_GOAL_SELECTED,
    progressTracker: [ constants.PVS__PROGRESS_LOADED_NO_ISSUANCE, constants.PVS__FINAL_ISSUANCE ].indexOf( programViewStatus ) !== -1,
    progressDetails: [ constants.PVS__PROGRESS_LOADED_NO_ISSUANCE, constants.PVS__FINAL_ISSUANCE ].indexOf( programViewStatus ) !== -1,
    finalResults: programViewStatus === constants.PVS__FINAL_ISSUANCE,
    myPlan: programViewStatus !== constants.PVS__NO_GOAL_SELECTED
} ) );

const getLinks = createSelector( getHasViewPart, getHasOts, ( hasViewPart, hasOts ) => [
    'progress',
    { id: 'plan', show: hasViewPart.myPlan },
    'shop',
    { id: 'ots', show: hasOts },
    'rules'
] );

export default class Progress extends React.Component {
    static propTypes = {
        fixedPlusRatePayoutMessage: PropTypes.string,
        challengePointBaseEarningDescription: PropTypes.string,
        fixedPlusRatePayoutMessageArray: PropTypes.arrayOf( PropTypes.string ),
        bonusMessage: PropTypes.string,

        isLaunchAs: PropTypes.bool,
        program: PropTypes.shape( {
            groupId: PropTypes.number,
            timePeriodId: PropTypes.number,
            programViewStatus: PropTypes.string,
            programType: PropTypes.string,
        } ),

        hasOts: PropTypes.bool.isRequired,
    }

    state = {
        currentSection: 'progress'
    }

    componentDidMount() {
        if ( window.location.hash ) {
            this.scrollToSection( window.location.hash );
            this.handleEnterWaypoint(  window.location.hash.substr( 1 ) );
        }
    }

    handleEnterWaypoint = ( section ) => {
        this.setState( { currentSection: section } );
    }

    handleNavBarClick = ( event ) => {
        const hash = event.currentTarget.hash;
        this.scrollToSection( hash );
    }

    scrollToSection = ( hash ) => {
        const section = hash.substring( 1 );

        if ( section == 'progress' ) {
            scrollToElement( document.body, { duration: 800 } );
        } else {
            scrollToElement( hash, { duration: 800, offset: -210 } );
        }

        setTimeout( () => {
            this.setState( {
                currentSection: section
            } );
        }, 825 );
    }

    requireCustomStyles = () => require( '../../shared/on-the-spot/on-the-spot.scss' )

    render() {
        const {
            fixedPlusRatePayoutMessage,
            challengePointBaseEarningDescription,
            fixedPlusRatePayoutMessageArray,
            bonusMessage,
            program,
            hasOts,
            isLaunchAs,
        } = this.props;
        const hasViewPart = getHasViewPart( { program } );

        const { programViewStatus } = program;
        const theme = get( program, 'theme', 'mountain' );
        const hasInProgressClass = [ constants.PVS__PROGRESS_LOADED_NO_ISSUANCE, constants.PVS__FINAL_ISSUANCE ].indexOf( programViewStatus ) !== -1;

        return (
            <div className={ classNames( 'sticky-container', { 'overview-wrapper': hasViewPart.goalOverview } ) }>
                <StickyHeader>
                    <ProgressNavBar
                        links={ getLinks( { hasOts, program } ) }
                        program={ program }
                        activeLink={ this.state.currentSection }
                        handleNavBarClick={ this.handleNavBarClick }
                    />
                </StickyHeader>

                <div className="progress" id="progress-container">
                    { hasViewPart.finalResults &&
                        <FinalResults />
                    }

                    { hasViewPart.progressHeaderMessage &&
                        <ProgressHeaderMessage
                            header="progress.oops"
                            subheader="progress.noSelection"
                            text="progress.noSelectionSubText"
                        />
                    }
                    <SectionWaypoint section="progress" onEnter={ this.handleEnterWaypoint } className={ classNames( 'progress-wrapper', { 'in-progress': hasInProgressClass, 'overview-wrapper': hasViewPart.goalOverview } ) }>
                        <div className="content">
                            <StatusTitle program={ program } />
                            { hasViewPart.levelTable &&
                                <div className="level-comparison-container">
                                    <LevelComparisonTable
                                        fixedPlusRatePayoutMessageArray={ fixedPlusRatePayoutMessageArray }
                                        challengePointBaseEarningDescription={ challengePointBaseEarningDescription[ 1 ] }
                                        bonusMessage={ bonusMessage }
                                    />
                                </div>
                            }

                            { hasViewPart.progressTracker &&
                                <ProgressTracker />
                            }

                            { hasViewPart.goalOverview &&
                                <GoalOverview fixedPlusRatePayoutMessage={ fixedPlusRatePayoutMessage } />
                            }
                        </div>
                    </SectionWaypoint>

                    { hasViewPart.progressDetails &&
                        <section className="progress-details">
                            <div className="content">
                                <ProgressDetails fixedPlusRatePayoutMessage={ fixedPlusRatePayoutMessage } />
                            </div>
                        </section>
                    }

                    { hasViewPart.myPlan &&
                        <SectionWaypoint section="plan" onEnter={ this.handleEnterWaypoint } className="my-plan-wrapper">
                            <div className="content">
                                <LocaleString TextComponent="h2" id="myPlan" className="sub-title text-center" code="paxProgress.plan.myPlanHeader" />
                                <MyPlan />
                            </div>
                        </SectionWaypoint>
                    }

                    <SectionWaypoint section="shop" onEnter={ this.handleEnterWaypoint } className="shop-wrapper branding-tinted">
                        <div className="content">
                            <LocaleString TextComponent="h2" id="shopLabel" code="progress.shopHeader" />
                            <Shop
                                showHeader
                                showButton
                                showTextLink={ !isLaunchAs }
                                carouselSize="large"
                            />
                        </div>
                    </SectionWaypoint>

                    { hasOts &&
                        <SectionWaypoint section="ots" onEnter={ this.handleEnterWaypoint } className="ots-section" >
                            <OnTheSpot branding requireCustomStyles={ this.requireCustomStyles } />
                        </SectionWaypoint>
                    }

                    <SectionWaypoint section="rules" onEnter={ this.handleEnterWaypoint } className={ classNames( 'rules-wrapper', { 'mountain': theme === 'mountain' } ) }>
                        <div className="content">
                            <LocaleString TextComponent="h2" id="rulesLabel" className="section-header"  code="rules.header" />
                            <Rules />
                        </div>
                    </SectionWaypoint>
                </div>
            </div>
        );
    }
}
