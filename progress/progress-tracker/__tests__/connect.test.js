import { createStore } from 'app-test-utils';

import { mapStateToProps } from '../connect';
import * as fixture from '../../__fixtures__/13315---Selector';

describe( 'chart props', () => {
    let store;
    let props;
    beforeEach( () => {
        store = createStore( { fixture } );
        props = mapStateToProps( store.getState() );
    } );

    it( 'should return days in the correct format', () => {
        expect( props.days ).toEqual( [
            {
                'date': '2017-09-27T00:00:00-0500',
                'day': 27,
                'value': 7125.24
            },
            {
                'date': '2017-12-31T00:00:00-0600',
                'day': 122,
                'value': 14250.49
            }
        ] );
    } );

    it( 'should return baseDay in the correct format', () => {
        expect( props.baseDay ).toEqual( {
            'date': '2017-09-01T00:00:00-0500',
            'day': 0,
            'value': 0
        } );
    } );

    it( 'should return onTrackDay in the correct format', () => {
        expect( props.onTrackDay ).toEqual( {
            'date': '2017-12-31T23:59:59-0600',
            'day': 123,
            'value': 14250.49
        } );
    } );

    it( 'should calculate the lastDay correctly', () => {
        expect( props.lastDay ).toEqual( 122 );
    } );
} );
