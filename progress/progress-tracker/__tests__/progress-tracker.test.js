import React from 'react';
import { renderWithStore } from 'app-test-utils';
import * as fixture from '../../__fixtures__/13315---Selector';

import ProgressTracker from '../progress-tracker';

describe( 'ProgressTracker :', () => {
    it( 'should render without errors', () => {
        const component = renderWithStore( <ProgressTracker />, { fixture } );
        expect( component.toJSON() ).toMatchSnapshot();
    } );
} );
