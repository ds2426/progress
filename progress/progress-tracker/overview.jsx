// libs
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedNumber, LocaleString } from '@biw/intl';
import { selectors as progressSelectors } from 'contests-progress-dux';
import { getActiveProgram } from 'app/router/selectors';

class Overview extends React.Component {
    static propTypes = {
        name: PropTypes.string,
        description: PropTypes.string,
        goalAmount: PropTypes.number,
        mediaLabel: PropTypes.any
    }
    render() {
        const { name, description, goalAmount, mediaLabel } =  this.props;
        return (
            <div className="overview">
                { name && <h2 className="branding-primary-text">{ name }</h2> }
                { description && <div className="level-text">{ description }</div> }
                <div className="award-header"><LocaleString className="branding-primary-text" code='progress.potentialAwardHeader' /></div>
                <div className="award-amount"><FormattedNumber value={ goalAmount || 0 }/>{ ' ' + mediaLabel }</div>
            </div>
        );
    }
}

const EMPTY_OBJ = {};
export default connect( state => {
    const program = getActiveProgram( state ) || {};
    const goal = progressSelectors.getProgramViewGoal( state, program.programViewId ) || EMPTY_OBJ;
    const paxGoalSelection = goal.paxGoalSelection || EMPTY_OBJ;
    return {
        name: paxGoalSelection.name || '',
        description: paxGoalSelection.description || '',
        goalAmount: paxGoalSelection.goalAwardAmount || 0,
        mediaLabel: goal.mediaLabel || ''
    };
} )( Overview );
