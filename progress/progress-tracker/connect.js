// libs
import { connect } from 'react-redux';
import get from 'lodash/get';
import { createSelector } from 'reselect';
import { getString } from '@biw/intl';
import { selectors as clientSelectors } from '@biw/client-dux';
import { selectors as progressSelectors } from 'contests-progress-dux';
import { getActiveProgram } from 'app/router/selectors';
import differenceInCalendarDays from 'date-fns/difference_in_calendar_days';

const EMPTY_ARRAY = [];
const EMPTY_OBJ = {};

const getProgramDates = ( state, props ) => get( props, 'goal.programDates' ) || EMPTY_OBJ;
const getProgramStartDate = createSelector(
    getProgramDates,
    dates => dates.programStartDate
);
const getProgramEndDate = createSelector(
    getProgramDates,
    dates => dates.programEndDate
);

const getLastDay = createSelector(
    getProgramEndDate,
    getProgramStartDate,
    ( endDate, startDate ) => differenceInCalendarDays( endDate, startDate )
);

const getDays = createSelector(
    ( state, props ) => get( props, 'goal.paxProgressGraphVO.progress' ) || EMPTY_ARRAY,
    getProgramStartDate,
    ( progressList, programStartDate ) => progressList.map( dataPoint => ( {
        day: differenceInCalendarDays( dataPoint.xAxis, programStartDate ) + 1,
        value: dataPoint.yAxis,
        date: dataPoint.xAxis
    } ) ).sort( ( a, b ) => {
        const aSortValue = a.date + a.value,
            bSortValue = b.date + b.value;
        if ( aSortValue < bSortValue ) {
            return -1;
        }
        if ( aSortValue > bSortValue ) {
            return 1;
        }
        return 0;
    } )
);

const getDaysFromBaseStart = createSelector(
    ( state, props ) => get( props, 'goal.paxProgressGraphVO.starting' ) || EMPTY_OBJ,
    getProgramStartDate,
    ( starting, programStartDate ) => starting.xAxis || programStartDate
);

const getOnTrackDay = createSelector(
    ( state, props ) => get( props, 'goal.paxProgressGraphVO.onTrack' ) || EMPTY_OBJ,
    getDaysFromBaseStart,
    ( onTrack, daysFromBaseStart ) => ( {
            day: differenceInCalendarDays( onTrack.xAxis, daysFromBaseStart ) + 1,
            value: onTrack.yAxis,
            date: onTrack.xAxis
    } )
);

const getBaseDay = createSelector(
    ( state, props ) => get( props, 'goal.paxProgressGraphVO.starting' ) || EMPTY_OBJ,
    getProgramStartDate,
    getDaysFromBaseStart,
    getDays,
    ( starting, programStartDate, daysFromBaseStart, days ) => ( {
        day: differenceInCalendarDays( daysFromBaseStart, programStartDate ),
        value: typeof starting.yAxis === 'number' ? starting.yAxis : get( days, '0.value', 0 ),
        date: daysFromBaseStart
    } )
);

export const mapStateToProps = ( state ) => {
    const locale = clientSelectors.getUserLocale( state );
    const program = getActiveProgram( state ) || {};
    const goal = progressSelectors.getProgramViewGoal( state, program.programViewId ) || EMPTY_OBJ;
    const paxGoalSelection = goal.paxGoalSelection || EMPTY_OBJ;

    return {
        locale,

        goalPaceLabel: getString( state, 'paxProgress.graph.goalKey' ),
        goalLineLabel: getString( state, 'paxProgress.graph.goalLineLabel' ),
        trendingKeyLabel: getString( state, 'paxProgress.graph.trendingKey' ),
        progressKeyLabel: getString( state, 'paxProgress.graph.progressKey' ),
        xAxisLabel: getString( state, 'paxProgress.graph.xAxisLabel' ),

        programViewStatus: program.programViewStatus,
        primaryColor: program.primaryColor || '#0c7eac',
        goal: paxGoalSelection.goalAchieveAmount,
        calculationLabel: getString( state, `goal.perperiod.${goal.calculationLabel}label` ),
        calculationType: program.programCalculationType,

        lastDay: getLastDay( state, { goal } ),
        days: getDays( state, { goal } ),
        onTrackDay: getOnTrackDay( state, { goal } ),
        baseDay: getBaseDay( state, { goal } ),

        goalLabel: goal.goalLabel || EMPTY_OBJ
    };
};

export default connect( mapStateToProps );
