import React from 'react';
import { ProgressGraph } from 'shared';
import Overview from './overview';
import graphEnhancer from './connect';

import './progress-tracker.scss';

const ConnectedGraph = graphEnhancer( ProgressGraph );

const ProgressTracker = () => (
    <div className="progress-tracker">
        <Overview />
        <ConnectedGraph />
    </div>
);

export default ProgressTracker;
