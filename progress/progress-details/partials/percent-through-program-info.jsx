// libs
import React from 'react';
import PropTypes from 'prop-types';
// shared components
import { LocaleString, FormattedDate, FormattedNumber } from '@biw/intl';
const PERCENT = { style: 'percent' };

const PercentThroughProgramInfo = ( props ) => {

    const {
        is,
        dataThrough
    } = props;

    const dateThroughPercentage = props.dateThroughPercentage || 0;

    return (
        <section className="progress-info pct-through-program-info">
            <LocaleString TextComponent="h1" code="progress.throughProgramHeader"/>
            { is.finalIssuance ?
                <FormattedNumber
                    TextComponent="p"
                    value={ 1 }
                    formatOptions={PERCENT}
                    className="status pct-through-program-final-issuance-status"
                />
            :
                <FormattedNumber
                    TextComponent="p"
                    value={ dateThroughPercentage / 100 }
                    formatOptions={PERCENT}
                    className="status pct-through-program-date-through-percentage-status"
                />
            }

            { dataThrough &&
                <div className="extra-details pct-through-program-info-data-through">
                    <LocaleString TextComponent="h3" code="progress.dataThroughProgramHeader"/>
                    <FormattedDate TextComponent="h4" className="amount-display" value={ dataThrough } />
                </div>
            }

        </section>
    );
};

PercentThroughProgramInfo.propTypes = {
    is: PropTypes.object.isRequired,
    dataThrough: PropTypes.oneOfType( [ PropTypes.number, PropTypes.string ] ),
    dateThroughPercentage: PropTypes.oneOfType( [ PropTypes.number, PropTypes.string ] )
};

PercentThroughProgramInfo.defaultProps = {
    dataThrough: null,
    dateThroughPercentage: null
};

export default PercentThroughProgramInfo;
