// libs
import React from 'react';
import PropTypes from 'prop-types';
// shared components
import { LocaleString } from '@biw/intl';

const AcceleratorInfo = ( props ) => {
    return (
        <section>
            <div className="structure-info">
                <div className="final-result-structure-info accelerator-info">
                    <LocaleString TextComponent="h1" code="common.fixedRate.acceleratorHeader"/>
                    <h6 dangerouslySetInnerHTML={ { __html: props.message } }/>
                </div>
            </div>
        </section>
    );
};
AcceleratorInfo.propTypes = {
    message: PropTypes.string
};
export default AcceleratorInfo;
