import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
// shared components
import { LocaleString } from '@biw/intl';
import { GoalLabel } from 'shared';

const MinimumQualifierInfo = ( props ) => {

    const {
        is,
        mqLabel,
        currentMQValue,
        mqPrecision,
        mqRoundingMethod,
        minQualifier,
        isMinQualifierAchieved
    } = props;

    return (
        <section className="minimum-qualifier-info">
            <div className="final-result-structure-info">
                { is.finalIssuance ?
                    <LocaleString TextComponent="h1" code="progress.finalMqProgressHeader"/>
                :
                    <LocaleString TextComponent="h1" code="progress.mqProgressHeader"/>
                }
                <h2><GoalLabel
                    { ...mqLabel }
                    amount={ currentMQValue || 0 }
                    precision={  mqPrecision }
                    rounding={  mqRoundingMethod }
                    className="mq-label"
                    strong
                /></h2>
            </div>
            <span className="structure-progress"><LocaleString code="progress.minimumQualifierHeader"/><span>: </span>
                <GoalLabel
                    { ...mqLabel }
                    amount={  minQualifier }
                    precision={  mqPrecision }
                    rounding={  mqRoundingMethod }
                    className="mq-label"
                    strong
                />
                &nbsp;<span>|</span>&nbsp;
            </span>
            <span className="structure-progress"><LocaleString code="progress.mqAchievedHeader"/><span>: </span>
                <strong>
                    { !isMinQualifierAchieved ?
                        <span className={ classNames( { 'not-achieved': is.finalIssuance } ) }>
                            <LocaleString code="progress.mqNotAchievedText"/>
                        </span>
                    :
                        <LocaleString className="achieved" code="progress.mqAchievedText"/>
                    }
                </strong>
            </span>
        </section>
    );
};

MinimumQualifierInfo.propTypes = {
    is: PropTypes.object,
    mqLabel: PropTypes.object,
    currentMQValue: PropTypes.number,
    mqPrecision: PropTypes.number,
    mqRoundingMethod: PropTypes.string,
    minQualifier: PropTypes.number,
    isMinQualifierAchieved: PropTypes.bool
};

export default MinimumQualifierInfo;
