import '@biw/intl/mock-package';
import renderer from 'react-test-renderer';
import React from 'react';
import { shallow } from 'enzyme';
import OnTrackInfo from '../on-track-info.jsx';
import { constants } from 'contests-utils';

describe( 'On Track Info Partial', () => {
    let defaultProps;
    beforeEach( () => {
        defaultProps = {
            onTrackPercentage: 85,
            programViewStatus: constants.PVS__PROGRESS_LOADED_NO_ISSUANCE,
            progressPercentage: 85,
            programCalculationType: 'reduction',
            isMinQualifierAchieved: null,
            is: { finalIssuance: false, challengePoint: false },
            isBaseLineRequired: false,
            goalLabel: {},
            goalBaseValue: null,
            goalBaseCalculationValue: null,
            calculationLabel: 'day',
            goalAchieveAmount: 1000,
            goalPrecision: 1,
            goalRoundingMethod: 'standard',
            goalCalculationValue: 45.2,
        };
    } );

    it( 'renders on track info partial with no errors', () => {

        const wrapper = renderer.create(
            <OnTrackInfo { ...defaultProps } />
        ).toJSON();

        expect( wrapper ).toMatchSnapshot();

    } );

    it( 'should not render the goalBaseCalculationValue if it is set to 0', () => {
        defaultProps.goalBaseCalculationValue = 0;
        defaultProps.isBaseLineRequired = true;
        const wrapper = renderer.create(
            <OnTrackInfo { ...defaultProps } />
        ).toJSON();

        expect( wrapper ).toMatchSnapshot();

    } );

    it( 'should render per period goal calculation value if calculation type is not average', () => {

        const bonus = shallow( <OnTrackInfo { ...defaultProps } /> );

        expect( bonus.find( '.progress-goal-per-period-label' ) ).toHaveLength( 1 );
    } );

    it( 'should not render per period goal calculation value if calculation type is average', () => {
		const props = {
			...defaultProps,
			programCalculationType: 'average'
		};

        const bonus = shallow( <OnTrackInfo { ...props } /> );

        expect( bonus.find( '.progress-goal-per-period-label' ) ).toHaveLength( 0 );
    } );

} );

