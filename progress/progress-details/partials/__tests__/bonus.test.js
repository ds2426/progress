import '@biw/intl/mock-package';
import renderer from 'react-test-renderer';
import React from 'react';
import { mount } from 'enzyme';
import { Bonus } from '../bonus';
import { constants } from 'contests-utils';

const defaultProps = {
    locale: { code: 'en_US', intlCode: 'en-US' },
    currentBonusValue: 0,
    bonusPayout: {
        bonusAchievementAmount: 0,
        bonusAchievementRule: 'percentage_of_baseline',
        bonusAward: 2,
        bonusIncrementalQuantity: null,
        bonusMQ: null,
        bonusMaximumPoints: null,
        isGoalNeedToAchieve: true,
        label: {
            currencyCode: 'usd',
            currencyPosition: 'before',
            currencySymbol: '$',
            labelPosition: 'after',
            labelText: 'bonus dollars',
            labelType: 'currency',
            precision: 2
        },
        payoutStructureType: 'fixed'
    },
    isMinimumQualifierEnabled: false,
    finalResult: {
        bonusPoints: undefined,
        bonusAchieved: undefined,
        goalAchieved: undefined
    },
    isGoalNeedToAchieve: undefined,
    programViewStatus: constants.PVS__PROGRESS_LOADED_NO_ISSUANCE,
    bonusPrecision: 2,
    bonusRoundingMethod: 'standard',
    isBonusEnabled: true
};

describe( 'Bonus Partial', () => {

    it( 'renders bonus partial with no errors', () => {

        const wrapper = renderer.create(
            <Bonus { ...defaultProps } />
        ).toJSON();

        expect( wrapper ).toMatchSnapshot();

    } );

    it( 'can render structure progress', () => {

        const bonus = mount( <Bonus { ...defaultProps } /> );

        expect( bonus.find( '.structure-progress' ) ).toHaveLength( 1 );
    } );

} );
