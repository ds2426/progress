import '@biw/intl/mock-package';
import renderer from 'react-test-renderer';
import React from 'react';
import { mount } from 'enzyme';
import PercentThroughProgramInfo from '../percent-through-program-info';

const defaultProps = {
    is: { finalIssuance: false },
    dataThrough: null,
    dateThroughPercentage: null
};

describe( 'On Track Info Partial', () => {

    it( 'renders percent through program info partial with no errors', () => {
        const props = {
            is: { finalIssuance: true },
            dataThrough: 1513031793000,
            dateThroughPercentage: 50
        };

        const wrapper = renderer.create(
            <PercentThroughProgramInfo { ...props } />
        ).toJSON();

        expect( wrapper ).toMatchSnapshot();

    } );

    it( 'should not render dateThroughPercentage value if final issuance', () => {
        const props = {
            ...defaultProps,
            is: { finalIssuance: true }
        };

        const percentThroughProgramInfo = mount( <PercentThroughProgramInfo { ...props } /> );

        expect( percentThroughProgramInfo.find( '.pct-through-program-date-through-percentage-status' ) ).toHaveLength( 0 );
        expect( percentThroughProgramInfo.find( '.pct-through-program-final-issuance-status' ) ).toHaveLength( 1 );
    } );

    it( 'should render dateThroughPercentage value if no final issuance', () => {
        const props = {
            ...defaultProps,
            is: { finalIssuance: false },
            dateThroughPercentage: 50
        };

        const percentThroughProgramInfo = mount( <PercentThroughProgramInfo { ...props } /> );

        expect( percentThroughProgramInfo.find( '.pct-through-program-date-through-percentage-status' ) ).toHaveLength( 1 );
        expect( percentThroughProgramInfo.find( '.pct-through-program-final-issuance-status' ) ).toHaveLength( 0 );
    } );

    it( 'should render dataThrough value if value exists', () => {
        const props = {
            ...defaultProps,
            dataThrough: 200
        };

        const percentThroughProgramInfo = mount( <PercentThroughProgramInfo { ...props } /> );

        expect( percentThroughProgramInfo.find( '.pct-through-program-info-data-through' ) ).toHaveLength( 1 );
    } );

    it( 'should not render per period goal calculation value if calculation type is average', () => {

        const percentThroughProgramInfo = mount( <PercentThroughProgramInfo { ...defaultProps } /> );

        expect( percentThroughProgramInfo.find( '.pct-through-program-info-data-through' ) ).toHaveLength( 0 );
    } );

} );

