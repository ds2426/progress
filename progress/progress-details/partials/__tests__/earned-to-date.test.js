import React from 'react';
import renderer from 'react-test-renderer';
import EarnedToDate from '../earned-to-date';

const defaultProps = {
    mediaLabel: 'Points',
    earnedToDate: 100
};

describe( 'Earned to date partial', () => {

    it( 'renders on track info partial with no errors', () => {
        const wrapper = renderer.create(
            <EarnedToDate { ...defaultProps } />
        ).toJSON();

        expect( wrapper ).toMatchSnapshot();
    } );

} );
