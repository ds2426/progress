import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
// shared components
import { LocaleString, FormattedNumber } from '@biw/intl';
import { GoalLabel, PerPeriod } from 'shared';
// utils
import { getProgressStatusTreatment } from 'contests-utils';
const PERCENT = { style: 'percent' };

const ProgressPercentInfo = ( props ) => {
    const {
        is,
        programViewStatus,
        isMinQualifierAchieved,
        programCalculationType,
        goalLabel,
        resultsToDate,
        goalPrecision,
        goalRoundingMethod,
        resultsToDateCalculationValue,
        calculationLabel,
        toAchieve,
        toAchieveCalculationValue,
        onTrackPercentage
    } = props;

    const progressPercentage = props.progressPercentage || 0;

    const getProgressHeaderText = () => {
        const programCalculationType = programCalculationType;
        const isReductionOrAverage = [ 'average', 'reduction'  ].find( ( calculationType ) => calculationType === programCalculationType );

        if ( isReductionOrAverage ) {
            return 'progress.percentToGoalHeaders';
        } else {
            return 'progress.Headers';
        }
    };

    const statusTreatment = getProgressStatusTreatment( onTrackPercentage, programViewStatus, progressPercentage, programCalculationType, isMinQualifierAchieved );
    const hasNoResultsToAchieveDetails = ( programCalculationType !== 'standard' || progressPercentage > 99 || is.finalIssuance );
    const showToAchieveData = !!toAchieveCalculationValue && !!calculationLabel && !is.finalIssuance;

    return (
        <section className="progress-info progress-pct-info">

            { is.finalIssuance ?
                <LocaleString TextComponent="h1" code="progress.goalPercentageHeader"/>
            :
                <LocaleString TextComponent="h1" code={ getProgressHeaderText() } />
            }

            <FormattedNumber
                TextComponent="h2"
                value={ progressPercentage / 100 }
                formatOptions={PERCENT}
                className={classNames( 'status', statusTreatment.className )}
            />

            <article className="extra-details">
                { is.finalIssuance ?
                    <LocaleString TextComponent="h3" code="progress.finalResultsHeaders"/>
                :
                    <LocaleString TextComponent="h3" code="progress.resultToDateHeader"/>
                }
                <GoalLabel
                    { ...goalLabel }
                    amount={ resultsToDate || 0 }
                    precision={ goalPrecision }
                    rounding={ goalRoundingMethod }
                    className="goal-label"
                />
                { !!resultsToDateCalculationValue && calculationLabel ?
                    <PerPeriod calculationLabel={ calculationLabel }>
                        <GoalLabel
                            { ...goalLabel }
                            amount={ resultsToDateCalculationValue }
                            precision={ goalPrecision }
                            rounding={ goalRoundingMethod }
                        />
                    </PerPeriod>
                :
                    <p/> /* PerPeriod renders a p tag, removing it the tag breaks the inline layout */
                }
            </article>

            { !hasNoResultsToAchieveDetails &&
                <article className="extra-details">
                    <LocaleString TextComponent="h3" code="progress.resultsToAchieveHeader"/>
                    <GoalLabel
                        { ...goalLabel }
                        amount={ toAchieve }
                        precision={ goalPrecision }
                        rounding={ goalRoundingMethod }
                        className="goal-label"
                    />
                    { showToAchieveData &&
                        <PerPeriod calculationLabel={ calculationLabel }>
                            <GoalLabel
                                { ...goalLabel }
                                precision={ goalPrecision }
                                rounding={ goalRoundingMethod }
                                amount={ toAchieveCalculationValue }
                            />
                        </PerPeriod>
                    }
                </article>
            }
        </section>
    );
};

ProgressPercentInfo.propTypes = {
    is: PropTypes.object,
    programViewStatus: PropTypes.string,
    onTrackPercentage: PropTypes.number,
    progressPercentage: PropTypes.number,
    programCalculationType: PropTypes.string,
    isMinQualifierAchieved: PropTypes.bool,
    goalLabel: PropTypes.object,
    calculationLabel: PropTypes.string,
    goalPrecision: PropTypes.number,
    goalRoundingMethod: PropTypes.string,
    resultsToDate: PropTypes.number,
    resultsToDateCalculationValue: PropTypes.number,
    toAchieve: PropTypes.number,
    toAchieveCalculationValue: PropTypes.number
};

export default ProgressPercentInfo;
