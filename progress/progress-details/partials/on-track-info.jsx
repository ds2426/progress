// libs
import React from 'react';
import PropTypes from 'prop-types';
// shared components
import { LocaleString } from '@biw/intl';
import { GoalLabel, PerPeriod } from 'shared';
// utils
import { getProgressStatusTreatment } from 'contests-utils';

const OnTrackInfo = ( props ) => {

    const {
        onTrackPercentage,
        programViewStatus,
        progressPercentage,
        programCalculationType,
        isMinQualifierAchieved,
        is,
        isBaseLineRequired,
        goalLabel,
        goalBaseValue,
        goalBaseCalculationValue,
        calculationLabel,
        goalAchieveAmount,
        goalPrecision,
        goalRoundingMethod,
        goalCalculationValue
    } = props;

    const statusTreatment = getProgressStatusTreatment(
        onTrackPercentage,
        programViewStatus,
        progressPercentage,
        programCalculationType,
        isMinQualifierAchieved
    );

    return (
        <section className="progress-info on-track-info">
                <article>
                    {
                        is.finalIssuance
                        ?
                        <LocaleString TextComponent="h1" code="progress.finalResultsHeaders"/>
                        :
                        <LocaleString TextComponent="h1" code="progress.onTrackHeader"/>
                    }

                    <LocaleString TextComponent="h2" code={ statusTreatment.label } className={ 'status ' + statusTreatment.className }/>
                </article>
                <article>
                {
                    isBaseLineRequired
                    &&
                    <div className="extra-details">
                        <LocaleString TextComponent="h1" code="progress.baselineHeader"/>
                        <h2>
                            <GoalLabel
                                { ...goalLabel }
                                amount={ goalBaseValue }
                                className="goal-label"
                            />
                        </h2>
                        {
                            !!goalBaseCalculationValue &&
                            !!calculationLabel
                            &&
                            <PerPeriod calculationLabel={ calculationLabel }>
                                <GoalLabel
                                    { ...goalLabel }
                                    amount={ goalBaseCalculationValue }
                                />
                            </PerPeriod>
                        }
                    </div>
                }
                </article>
                <article>
                    <div className="extra-details">
                        <h3><LocaleString code="progress.goalHeader"/></h3>
                        <GoalLabel
                            { ...goalLabel }
                            amount={ goalAchieveAmount }
                            precision={ goalPrecision }
                            rounding={ goalRoundingMethod }
                            className="goal-label"
                        />
                        {
                            !!goalCalculationValue &&
                            !!calculationLabel &&
                            programCalculationType !== 'average'
                            &&
                            <PerPeriod calculationLabel={ calculationLabel } className="progress-goal-per-period-label">
                                <GoalLabel
                                    { ...goalLabel }
                                    precision={ goalPrecision }
                                    rounding={ goalRoundingMethod }
                                    amount={ goalCalculationValue }
                                />
                            </PerPeriod>
                        }
                    </div>
                </article>
            </section>
    );
};

OnTrackInfo.propTypes = {
    onTrackPercentage: PropTypes.number,
    programViewStatus: PropTypes.string,
    progressPercentage: PropTypes.number,
    programCalculationType: PropTypes.string,
    isMinQualifierAchieved: PropTypes.bool,
    is: PropTypes.object,
    isBaseLineRequired: PropTypes.bool,
    goalLabel: PropTypes.object,
    goalBaseValue: PropTypes.number,
    goalBaseCalculationValue: PropTypes.number,
    calculationLabel: PropTypes.string,
    goalAchieveAmount: PropTypes.number,
    goalPrecision: PropTypes.number,
    goalRoundingMethod: PropTypes.string,
    goalCalculationValue: PropTypes.number
};

export default OnTrackInfo;
