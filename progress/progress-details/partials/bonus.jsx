// libs
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
// utils
import { LocaleString } from '@biw/intl';
import { getLabelString, constants } from 'contests-utils';

const propTypes = {
    bonusMessage: PropTypes.string,
    bonusReplaceOptions: PropTypes.shape( {} ),
    bonusPayout: PropTypes.shape( {
        isGoalNeedToAchieve: PropTypes.bool,
        payoutStructureType: PropTypes.string,
        label: PropTypes.shape( {} )
    } ),
    finalResult: PropTypes.shape( {
        bonusAchieved: PropTypes.bool,
        goalAchieved: PropTypes.bool,
        bonusPoints: PropTypes.number
    } ),
    isMinimumQualifierEnabled: PropTypes.bool.isRequired,
    currentBonusValue: PropTypes.number,
    bonusPrecision: PropTypes.number.isRequired,
    programViewStatus: PropTypes.string.isRequired,
    bonusRoundingMethod: PropTypes.string,
    locale: PropTypes.shape( {} ).isRequired,
    mediaLabel: PropTypes.string,
};

const defaultProps = {
    bonusPayout: null,
    finalResult: null,
    currentBonusValue: 0
};

export const Bonus = ( {
    locale,
    mediaLabel,

    currentBonusValue,
    isMinimumQualifierEnabled,
    programViewStatus,
    bonusPrecision,
    finalResult,
    bonusRoundingMethod,
    bonusPayout,
    bonusMessage,
    bonusReplaceOptions
} ) => {
    if ( !bonusPayout ) {
        return false;
    }

    const {
        bonusPoints,
        bonusAchieved,
        goalAchieved,
    } = finalResult;

    const madeItClass = bonusAchieved && ( goalAchieved || !bonusPayout.isGoalNeedToAchieve );
    const notAchievedClass = ( !bonusAchieved || !goalAchieved && bonusPayout.isGoalNeedToAchieve ) && programViewStatus === constants.PVS__FINAL_ISSUANCE;

    return (
        <section>
            <div className="structure-info">
                <div className="final-result-structure-info">
                    <LocaleString TextComponent="h1" code="progress.bonusHeader" />
                    <span
                        className={classNames(
                            'goal-label-amount',
                            {
                                'made-it': madeItClass,
                                'not-achieved': notAchievedClass,
                            }
                        )}>
                        &nbsp;{getLabelString( bonusPayout.label, ( currentBonusValue || 0 ), bonusPrecision, bonusRoundingMethod, locale )}
                    </span>
                </div>

                {programViewStatus !== constants.PVS__FINAL_ISSUANCE ?
                    <h6 className="structure-progress">

                        { bonusPayout.payoutStructureType === constants.PT__FIXED &&
                            <LocaleString className="branding-primarg-text" code="common.bonus.fixedLabelText" replaceOptions={bonusReplaceOptions} />
                        }
                        { bonusPayout.payoutStructureType === constants.PT__RATE &&
                            <span dangerouslySetInnerHTML={{ __html: bonusMessage }} />
                        }

                        { bonusPayout.isGoalNeedToAchieve === true &&
                            <LocaleString
                                className="branding-primarg-text"
                                code={ isMinimumQualifierEnabled ? 'common.bonus.goalMQAchievedText' : 'common.bonus.goalAchievementText' }
                            />
                        }
                    </h6>
                :
                    bonusPoints > 0 &&
                        <LocaleString className="branding-primary-text" code="common.bonus.bonusAchieved" replaceOptions={bonusReplaceOptions} />

                }
            </div>
        </section>
    );
};

Bonus.propTypes = propTypes;
Bonus.defaultProps = defaultProps;

export default Bonus;
