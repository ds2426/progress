import React from 'react';
import PropTypes from 'prop-types';
import { FormattedNumber, LocaleString } from '@biw/intl';

const EarnedToDate = ( { mediaLabel, earnedToDate } ) => (
    <section className="structure-info">
        <article className="final-result-structure-info">
            <LocaleString TextComponent="h1" code="finalResults.selector.earnedToDateSubhead"/>
            <span className="goal-label-amount">
                <FormattedNumber value={earnedToDate} />&nbsp;{ mediaLabel }
            </span>
        </article>
    </section>
);

EarnedToDate.propTypes = {
	mediaLabel: PropTypes.string.isRequired,
	earnedToDate: PropTypes.number.isRequired
};

export default EarnedToDate;
