import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// shared components
import { selectors as clientSelectors } from '@biw/client-dux';
import { LocaleString, getString } from '@biw/intl';
// utils
import { getChallengePointBaseEarningDescription } from 'contests-utils';

const BaseEarning = ( {
    locale,
    getString,
    mediaLabel,

    baseLevel,
    programType,
    goalRoundingMethod,
    goalPayoutTierType,
    goalLabel,
} ) => {
    const challengePointBaseEarningDescription = getChallengePointBaseEarningDescription( {
        baseLevel,
        programType,
        locale,
        getString,
        goalRoundingMethod,
        goalPayoutTierType,
        goalLabel,
        mediaLabel
    } );

	return (
		<div className="base-earning">
			<div className="final-result-structure-info">
                <LocaleString TextComponent="h1" code="finalResults.selector.baseEarningSubhead"/>
			</div>
			<h6 className="structure-progress">
				<p dangerouslySetInnerHTML={ { __html: challengePointBaseEarningDescription[ 1 ] } } />
			</h6>
		</div>
	);
};

BaseEarning.propTypes = {
    locale: PropTypes.object,
    getString: PropTypes.func,
    mediaLabel: PropTypes.string,

    baseLevel: PropTypes.object,
    programType: PropTypes.string,
    goalRoundingMethod: PropTypes.string,
    goalPayoutTierType: PropTypes.string,
    goalLabel: PropTypes.object,
};

export const mapStateToProps = state => ( {
    locale: clientSelectors.getUserLocale( state ),
    getString: ( key, replaceOptions ) => getString( state, key, replaceOptions ),
    mediaLabel: clientSelectors.getMediaLabel( state ),
} );

export default connect( mapStateToProps )( BaseEarning );

