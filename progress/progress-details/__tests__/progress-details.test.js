import React from 'react';
import { renderWithStore } from 'app-test-utils';
import * as ProgressFixture from '../../__fixtures__/13315---Selector';

import ProgressDetails from '../index';

describe( 'ProgressDetails', () => {
    let props;
    beforeEach( () => {
        props = {
            fixedPlusRatePayoutMessage: 'fixedPlusRatePayoutMessage'
        };
    } );

	it( 'should render without errors', () => {
        const component = renderWithStore(
            <ProgressDetails {...props} />,
            { fixture: ProgressFixture }
        );
        expect( component.toJSON() ).toMatchSnapshot();
	} );

} );
