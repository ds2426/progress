// libs
import React from 'react';
import PropTypes from 'prop-types';
import { createSelector } from 'reselect';
// partials
import AcceleratorInfo from './partials/accelerator-info';
import BaseEarning from './partials/base-earning';
import EarnedToDate from './partials/earned-to-date';
import Bonus from './partials/bonus';
import OnTrackInfo from './partials/on-track-info';
import ProgressPercentInfo from './partials/progress-percent-info';
import PercentThroughProgramInfo from './partials/percent-through-program-info';
import MinimumQualifierInfo from './partials/minimum-qualifier-info';
import { constants } from 'contests-utils';
import './progress-details.scss';

const getIs = createSelector( props => props.program, program => ( {
    finalIssuance: program.programViewStatus === constants.PVS__FINAL_ISSUANCE,
    challengePoint: program.programType === constants.PT__CP
} ) );

class ProgressDetails extends React.Component {
    static propTypes = {
        locale: PropTypes.shape( {} ),
        mediaLabel: PropTypes.string,
        program: PropTypes.shape( {
            programViewStatus: PropTypes.string,
            programType: PropTypes.string,
            programCalculationType: PropTypes.string,
            goalPrecision: PropTypes.number,
            goalRoundingMethod: PropTypes.string,
            mqPrecision: PropTypes.number,
            mqRoundingMethod: PropTypes.string,
            bonusPrecision: PropTypes.number.isRequired,
            bonusRoundingMethod: PropTypes.string,
        } ),

        paxProgressResponse: PropTypes.shape( {
            onTrackPercentage: PropTypes.number,
            progressPercentage: PropTypes.number,
            isMinQualifierAchieved: PropTypes.bool,
            resultsToDate: PropTypes.number,
            resultsToDateCalculationValue: PropTypes.number,
            toAchieve: PropTypes.number,
            toAchieveCalculationValue: PropTypes.number,
            dataThrough: PropTypes.oneOfType( [ PropTypes.number, PropTypes.string ] ),
            dateThroughPercentage: PropTypes.oneOfType( [ PropTypes.number, PropTypes.string ] ),
            currentMQValue: PropTypes.number,
            currentBonusValue: PropTypes.number,
            earnedToDate: PropTypes.number,
        } ),

        paxGoalSelection: PropTypes.shape( {
            goalAchieveAmount: PropTypes.number,
            goalCalculationValue: PropTypes.number,
            minQualifier: PropTypes.number,
        } ),

        goal: PropTypes.shape( {
            isBaseLineRequired: PropTypes.bool,
            isMinimumQualifierEnabled: PropTypes.bool,
            isBonusEnabled: PropTypes.bool,
            calculationLabel: PropTypes.string,
            goalPayoutTierType: PropTypes.string,
        } ),

        goalLabel: PropTypes.shape( {} ),
        baseValue: PropTypes.shape( {
            goalBaseValue: PropTypes.number,
            goalBaseCalculationValue: PropTypes.number,
        } ),
        baseLevel: PropTypes.shape( {} ),

        bonusPayout: PropTypes.shape( {
            isGoalNeedToAchieve: PropTypes.bool,
            payoutStructureType: PropTypes.string,
            label: PropTypes.shape( {} )
        } ),

        finalResult: PropTypes.shape( {
            bonusAchieved: PropTypes.bool,
            goalAchieved: PropTypes.bool,
            bonusPoints: PropTypes.number
        } ),

        mqLabel: PropTypes.shape( {} ),
        fixedPlusRatePayoutMessage: PropTypes.string,
        bonusMessage: PropTypes.string,
        bonusReplaceOptions: PropTypes.shape( {} ),
    }

    render() {
        const {
            program,
            paxProgressResponse,
            paxGoalSelection,
            goalLabel,
            goal,
            fixedPlusRatePayoutMessage,
            baseValue,
            baseLevel,
            bonusPayout,
            finalResult,
            mqLabel,
            bonusMessage,
            bonusReplaceOptions,
            locale,
            mediaLabel
        } = this.props;

        const is = getIs( this.props );

        return (
            <section role="presentation">
                <div className="details-wrapper">
                    <OnTrackInfo
                        is={is}
                        programViewStatus={program.programViewStatus}
                        onTrackPercentage={paxProgressResponse.onTrackPercentage}
                        progressPercentage={paxProgressResponse.progressPercentage}
                        programCalculationType={program.programCalculationType}
                        isMinQualifierAchieved={paxProgressResponse.isMinQualifierAchieved}
                        isBaseLineRequired={goal.isBaseLineRequired}
                        goalLabel={goalLabel}
                        goalBaseValue={baseValue.goalBaseValue}
                        goalBaseCalculationValue={baseValue.goalBaseCalculationValue}
                        calculationLabel={goal.calculationLabel}
                        goalAchieveAmount={paxGoalSelection.goalAchieveAmount}
                        goalPrecision={program.goalPrecision}
                        goalRoundingMethod={program.goalRoundingMethod}
                        goalCalculationValue={paxGoalSelection.goalCalculationValue}
                    />

                    <ProgressPercentInfo
                        is={is}
                        programViewStatus={program.programViewStatus}
                        isMinQualifierAchieved={paxProgressResponse.isMinQualifierAchieved}
                        programCalculationType={program.programCalculationType}
                        goalLabel={goalLabel}
                        resultsToDate={paxProgressResponse.resultsToDate}
                        goalPrecision={program.goalPrecision}
                        goalRoundingMethod={program.goalRoundingMethod}
                        resultsToDateCalculationValue={paxProgressResponse.resultsToDateCalculationValue}
                        calculationLabel={goal.calculationLabel}
                        toAchieve={paxProgressResponse.toAchieve}
                        toAchieveCalculationValue={paxProgressResponse.toAchieveCalculationValue}
                        onTrackPercentage={paxProgressResponse.onTrackPercentage}
                        progressPercentage={paxProgressResponse.progressPercentage}
                    />

                    <PercentThroughProgramInfo
                        is={is}
                        dataThrough={paxProgressResponse.dataThrough}
                        dateThroughPercentage={paxProgressResponse.dateThroughPercentage}
                    />
                </div>
                { is.challengePoint &&
                <div className="base-earnings">
                    <BaseEarning
                        baseLevel={baseLevel}
                        programType={program.programType}
                        goalRoundingMethod={program.goalRoundingMethod}
                        goalPayoutTierType={goal.goalPayoutTierType}
                        goalLabel={goalLabel}
                    />
                    <EarnedToDate earnedToDate={paxProgressResponse.earnedToDate} mediaLabel={mediaLabel} />
                </div>
                }
                { fixedPlusRatePayoutMessage &&
                    <AcceleratorInfo message={ fixedPlusRatePayoutMessage } />
                }

                { goal.isMinimumQualifierEnabled &&
                    <MinimumQualifierInfo
                        is={is}
                        mqLabel={mqLabel}
                        currentMQValue={paxProgressResponse.currentMQValue}
                        mqPrecision={program.mqPrecision}
                        mqRoundingMethod={program.mqRoundingMethod}
                        minQualifier={paxGoalSelection.minQualifier}
                        isMinQualifierAchieved={paxProgressResponse.isMinQualifierAchieved}
                    />
                }
                <div className="award-wrapper">
                    { goal.isBonusEnabled &&
                        <Bonus
                            currentBonusValue={paxProgressResponse.currentBonusValue}
                            isMinimumQualifierEnabled={goal.isMinimumQualifierEnabled}
                            programViewStatus={program.programViewStatus}
                            bonusPrecision={program.bonusPrecision}
                            finalResult={finalResult}
                            bonusRoundingMethod={program.bonusRoundingMethod}
                            bonusPayout={bonusPayout}
                            bonusMessage={bonusMessage}
                            bonusReplaceOptions={bonusReplaceOptions}
                            locale={locale}
                            mediaLabel={mediaLabel}
                        />
                    }
                </div>
            </section>
        );
    }
}

export default ProgressDetails;
