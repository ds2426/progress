
// components
import { connect }  from 'react-redux';
import { getActiveProgram } from 'app/router/selectors';
import { selectors as progressSelectors } from 'contests-progress-dux';
import { selectors as clientSelectors } from '@biw/client-dux';
import ProgressDetails from './progress-details';
import { getBonusMessage, getBonusReplaceOptions } from 'contests-utils';
import { getContestUtilsMessageProps } from 'routes/progress/selectors';
import { getString } from '@biw/intl';


const EMPTY_OBJ = {};
export const mapStateToProps = ( state ) => {
    const program = getActiveProgram( state );
    const goal = progressSelectors.getProgramViewGoal( state, program.programViewId ) || EMPTY_OBJ;
    const messageProps = getContestUtilsMessageProps( state );
    return {
        program,
        goal,
        paxProgressResponse: goal.paxProgressResponse || EMPTY_OBJ,
        paxGoalSelection: goal.paxGoalSelection || EMPTY_OBJ,
        goalLabel: goal.goalLabel || EMPTY_OBJ,
        baseValue: goal.baseValue || EMPTY_OBJ,
        baseLevel: goal.baseLevel || EMPTY_OBJ,
        bonusPayout: goal.bonusPayout || EMPTY_OBJ,
        mqLabel: goal.mqLabel || EMPTY_OBJ,
        finalResult: progressSelectors.getProgramViewFinalResults( state, program.programViewId ),
        bonusMessage: getBonusMessage( messageProps ),
        bonusReplaceOptions: getBonusReplaceOptions( messageProps ),
        locale: clientSelectors.getUserLocale( state ),
        mediaLabel: clientSelectors.getMediaLabel( state ),
        calculationLabel: getString( state, `goal.perperiod.${goal.calculationLabel}label` )
    };
};

export default connect( mapStateToProps )( ProgressDetails );
