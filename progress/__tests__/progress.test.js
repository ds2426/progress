import React from 'react';
import { renderWithStore } from 'app-test-utils';
import * as NoProgressFixture from '../__fixtures__/13288---Selector';

import Progress from '../index';

describe( 'Progress', () => {
	it( 'should render without errors', () => {
        const component = renderWithStore( <Progress />, { fixture: NoProgressFixture } );
        expect( component.toJSON() ).toMatchSnapshot();
	} );
} );
