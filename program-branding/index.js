// libs
import { connect } from 'react-redux';
import get from 'lodash/get';
// selectors
import { getActiveProgramViewId } from 'app/router/selectors';
import { selectors as programSelectors } from '@biw/programs-dux';
// components
import ProgramBranding from './program-branding';

export const mapStateToProps = ( state, ownProps ) => {
    const programViewId = ownProps && ownProps.programViewId || getActiveProgramViewId( state );
    const program = programSelectors.getProgram( state, programViewId );
    const defaultPrimaryColor = ownProps && ownProps.isDetailsPage ? '#44c9fb' : '#0c7eac';
    return {
        primaryColor: get( program, 'primaryColor' ) || defaultPrimaryColor,
        secondaryColor: get( program, 'secondaryColor' ) || '#0c7eac',
        heroImageUrl: get( program, 'heroImageUrl' ) || 'null',
        theme: get( program, 'theme' ) || 'mountain',
        scopingSelector: ownProps && ownProps.scopingSelector || ''
    };
};

export default connect( mapStateToProps )( ProgramBranding );
