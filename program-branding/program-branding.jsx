
// libs
import React from 'react';
import PropTypes from 'prop-types';
import tinycolor from 'tinycolor2';

import { Style } from 'react-style-tag';
import ImageSizer from '../image-sizer';

import defaultHeaderImg from '!file-loader!app/assets/img/themes/default/dashboard_header_bg.jpg';
import defaultWelcomeImg from '!file-loader!app/assets/img/themes/default/bg-welcome1-large.jpg';
import defaultRulesImg from '!file-loader!app/assets/img/themes/default/bg6-mountain-theme.jpg';

export default class ProgramBranding extends React.Component {
    static propTypes = {
        primaryColor: PropTypes.string,
        secondaryColor: PropTypes.string,
        heroImageUrl: PropTypes.string,
        theme: PropTypes.string,
        scopingSelector: PropTypes.string
    };
    getColor = ( color ) => tinycolor( color )
    getLuminance = ( color ) => color.getLuminance();
    getHoverColor = ( color ) => color.darken( 10 ).toString();
    render() {
        const {
           primaryColor,
           secondaryColor,
           heroImageUrl,
           theme,
           scopingSelector
        } = this.props;

        const primaryTextColor = this.getLuminance( this.getColor( primaryColor ) ) > .7 ? '#353a40' : '#FFFFFF';
        const primaryButtonHover = this.getHoverColor( this.getColor( primaryColor ) );
        const brandingHeroImageUrl = theme === 'none' ? ImageSizer( heroImageUrl, window.innerWidth ) : defaultHeaderImg;
        const brandingWelcomeHeroImageUrl = theme === 'none' ? ImageSizer( heroImageUrl ) : defaultWelcomeImg;
        const rulesBG = theme === 'none' ? '#f3f3f3' : `url( ${defaultRulesImg} ) no-repeat 50% 50% / cover fixed;`;
        const textColor = theme === 'none' ? primaryColor : '#0c7eac';
        return (
           <Style hasSourceMap={false}>{`
                #app-root ${scopingSelector} {
                    .branding-overylay {
                        &:before {
                            content: '';
                            position: absolute;
                            top: 0;
                            right: 0;
                            bottom: 0;
                            left: 0;
                            z-index: 0;
                            background-image: linear-gradient(#000000, #000000);
                        }
                    }
                    .branding-tinted {
                        svg {
                            fill: ${textColor};
                        }

                        .checkmark  {
                            svg {
                                fill: #FFFFFF;
                            }
                        }
                        &:before {
                            content: '';
                            position: absolute;
                            top: 0;
                            right: 0;
                            bottom: 0;
                            left: 0;
                            z-index: 0;
                            background-image: linear-gradient(${primaryColor}, ${primaryColor});
                            opacity: .6;
                        }
                    }
                    .branding-tinted-custom {
                        &:before {
                            content: '';
                            position: absolute;
                            top: 0;
                            right: 0;
                            bottom: 0;
                            left: 0;
                            z-index: 0;
                            background-image: linear-gradient(#9a15f9, #9a15f9);
                            opacity: .3;
                        }
                    }

                    .branding-tinted-custom-green {
                        &:before {
                            content: '';
                            position: absolute;
                            top: 0;
                            right: 0;
                            bottom: 0;
                            left: 0;
                            z-index: 0;
                            background-image: linear-gradient(#458e54, #458e54);
                            opacity: .3;
                        }
                    }
                    .branding-primary-button-inverted {
                        background: #FFFFFF;
                        color: ${primaryColor}
                        a, span {
                            color: ${primaryColor}
                        }
                    }
                    .branding-primary-button {
                        background: ${primaryColor};

                        span {
                            color: ${primaryTextColor};
                        }

                        a:hover {
                            background: ${primaryButtonHover};
                        }

                        &:hover {
                            background: ${primaryButtonHover};
                        }

                        &.active {
                            background: ${primaryColor};
                            span {
                                color: ${primaryTextColor}
                            }
                        }

                        &.disabled {
                            background: #cccccc;
                            color: #c0c0c0;
                        }
                    }

                    .branding-primary-text {
                        color: ${primaryColor};

                        > span {
                            color: ${primaryColor};
                        }

                        svg {
                            fill: ${primaryColor};
                        }

                        .cpd-icon {
                            svg {
                                fill: ${primaryColor};
                            }
                        }
                    }

                    .branding-secondary-text {
                        color: ${secondaryColor};

                        a & {
                            color: ${secondaryColor};
                        }

                        > span {
                            color: ${secondaryColor};
                        }

                        svg {
                            fill: ${secondaryColor};
                        }
                    }

                    .branding-primary-bg-color {
                        background-color: ${primaryColor};
                        color: ${primaryTextColor};
                        svg {
                            fill: ${primaryTextColor};
                        }

                        .cart-shop-item {
                                svg {
                                fill: #FFFFFF;
                                stroke: none;
                            }
                        }
                    }

                    .branding-hero-image {
                        position: relative;
                        background: url(${brandingHeroImageUrl});
                        background-size: cover;
                        background-repeat: no-repeat;
                        background-position-y: 50%;
                        color: transparent;
                        &:before {
                            content: '';
                            position: absolute;
                            top: 0;
                            right: 0;
                            bottom: 0;
                            left: 0;
                            z-index: 0;
                            background-image: linear-gradient( #000000, #000000 );
                            opacity: .54;
                        }
                    }
                    .branding-welcome-hero-image {
                        position: relative;
                        background: url(${brandingWelcomeHeroImageUrl});
                        background-size: cover;
                        background-repeat: no-repeat;
                        color: transparent;

                        &:before {
                            content: '';
                            position: absolute;
                            top: 0;
                            right: 0;
                            bottom: 0;
                            left: 0;
                            z-index: 0;
                            background-image: linear-gradient( #000000, #000000 );
                            opacity: .54;
                        }
                    }
                    .manager-progress {
                        .data-table {
                            a, .contests-link {
                                color: ${primaryColor}
                            }
                        }
                    }

                    .rules-wrapper {
                        background: ${rulesBG};
                    }

                    .progress-by-participant .tab-panel .tabs li.active {
                        border-color: ${primaryColor};
                    }
                }
            `}
            </Style>
        );
    }
}


