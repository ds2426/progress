import AppStore from 'app/store';
import { mapStateToProps } from '../index';
import { selectors as programSelectors } from '@biw/programs-dux';


describe( 'ProgramBranding mapStateToProps', () => {
    let store;
    let ownProps;
    const program = {
        primaryColor: 'red',
        secondaryColor: 'blue'
    };
    beforeEach( () => {
        store = AppStore();
        ownProps = {
            programViewId: '',
            scopingSelector: ''
        };
    } );

    it( 'should work without errors', () => {
        expect( mapStateToProps( store ) ).toMatchSnapshot();
    } );
    it( 'should return program branding colors from the program object ', () => {
            spyOn( programSelectors, 'getProgram' ).and.returnValue( program );
            const result = mapStateToProps( AppStore().getState() );
            expect ( result.primaryColor ).toEqual( 'red' );
            expect ( result.secondaryColor ).toEqual( 'blue' );
    } );

    it( 'should return program branding default colors if they are null ', () => {
        const program = {
            primaryColor: null,
            secondaryColor: null
        };
        spyOn( programSelectors, 'getProgram' ).and.returnValue( program );
        const result = mapStateToProps( AppStore().getState() );
        expect ( result.primaryColor ).toEqual( '#0c7eac' );
        expect ( result.secondaryColor ).toEqual( '#0c7eac' );
    } );

    it( 'should return program branding default colors if program object is null', () => {
        const program = { };
        spyOn( programSelectors, 'getProgram' ).and.returnValue( program );
        const result = mapStateToProps( AppStore().getState() );
        expect ( result.primaryColor ).toEqual( '#0c7eac' );
        expect ( result.secondaryColor ).toEqual( '#0c7eac' );
    } );
    it( 'should use ownProps if they are passed in', () => {
        ownProps = {
            scopingSelector: '#branding-wrapper-11222'
        };
        const result = mapStateToProps( AppStore().getState(), ownProps );
        expect ( result.scopingSelector ).toEqual( '#branding-wrapper-11222' );
    } );


} );
