jest.mock( '../../shared/shop/catalog-button', () => { const CatalogButton = () => <div />;  return CatalogButton; } );
import React from 'react';
import renderer from 'react-test-renderer';
import ProgramBranding from '../program-branding';

describe( 'ProgramBranding component', () => {
    let defaultProps;
    beforeEach( () => {
        defaultProps = {
            primaryColor: '#000000',
            secondaryColor: '#888888',
            scopingSelector: '#branding-wrapper-13297'
        };
    } );

    it( 'renders the Program Branding component with no errors', () => {
        const component = renderer.create( <ProgramBranding { ...defaultProps }  /> );
        const tree = component.toJSON();
        expect( tree ).toMatchSnapshot();
    } );


} );
